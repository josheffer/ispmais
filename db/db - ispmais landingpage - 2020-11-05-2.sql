-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 192.168.10.10    Database: ispmais_landingpages
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `configs_landing_pages`
--

DROP TABLE IF EXISTS `configs_landing_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `configs_landing_pages` (
  `id_configs` int(11) NOT NULL,
  `fone1_landingPage` varchar(45) DEFAULT NULL,
  `fone2_landingPage` varchar(45) DEFAULT NULL,
  `fone1TextoWhats_landingPage` varchar(256) DEFAULT NULL,
  `fone2TextoWhats_landingPage` varchar(256) DEFAULT NULL,
  `verificaWhatsapp1_landingPage` enum('0','1') DEFAULT '1',
  `verificaWhatsapp2_landingPage` enum('0','1') DEFAULT '1',
  `fone1Ativo_landingPage` enum('0','1') DEFAULT '1',
  `fone2Ativo_landingPage` enum('0','1') DEFAULT '1',
  `linkFacebook_landingPage` varchar(60) DEFAULT NULL,
  `linkInstagram_landingPage` varchar(60) DEFAULT NULL,
  `facebookAtivo_landingPage` enum('0','1') DEFAULT '1',
  `instagramAtivo_landingPage` enum('0','1') DEFAULT '1',
  `linkSite_landingPage` varchar(60) DEFAULT NULL,
  `linkAlternativo1_landingPage` varchar(60) DEFAULT NULL,
  `linkAlternativo2_landingPage` varchar(60) DEFAULT NULL,
  `email_landingPage` varchar(120) DEFAULT NULL,
  `nomeEmail_landingPage` varchar(90) DEFAULT NULL,
  `endereco_landingPage` varchar(145) DEFAULT NULL,
  `logo_landingPage` varchar(45) DEFAULT 'imagem_default1.png',
  `favicon_landingPage` varchar(45) DEFAULT 'imagem_default1.png',
  `googleMaps_landingPage` text,
  `host_config_landingpage` varchar(145) DEFAULT NULL,
  `usuario_config_landingpage` varchar(145) DEFAULT NULL,
  `senha_config_landingpage` varchar(145) DEFAULT NULL,
  `debug_config_landingpage` enum('0','1','2') DEFAULT NULL,
  `seguranca_config_landingpage` varchar(15) DEFAULT NULL,
  `auth_config_landingpage` varchar(145) DEFAULT NULL,
  `porta_config_landingpage` int(5) DEFAULT NULL,
  `encoding_config_landingpage` varchar(145) DEFAULT NULL,
  `charset_config_landingpage` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`id_configs`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configs_landing_pages`
--

LOCK TABLES `configs_landing_pages` WRITE;
/*!40000 ALTER TABLE `configs_landing_pages` DISABLE KEYS */;
INSERT INTO `configs_landing_pages` VALUES (1,'','','','','1','1','1','1','joshefferrobert','joshefferr','1','1','teste.com','','','josheffer@gmail.com','Jozin','endereco','imagem_default1.png','imagem_default1.png','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d61152.03025844469!2d-49.34097843224888!3d-16.676788807968148!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x16d0acbabdf231e5!2sSupermercado%20Hora%20Extra!5e0!3m2!1spt-BR!2sbr!4v1586979740914!5m2!1spt-BR!2sbr\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>','mail.agenciasolid.com.br','naoresponda@agenciasolid.com.br','123@mudar!@!@','0','ssl','true',465,'base64','UTF-8');
/*!40000 ALTER TABLE `configs_landing_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `landing_pages`
--

DROP TABLE IF EXISTS `landing_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `landing_pages` (
  `id_landingPage` int(11) NOT NULL AUTO_INCREMENT,
  `nome_landingPage` varchar(145) NOT NULL COMMENT 'Nome para identificação da landing page',
  `titulo_landingPage` varchar(512) NOT NULL COMMENT 'Texto que aparecerá na aba do browser ao abrir a página',
  `status_landingPage` enum('0','1') NOT NULL DEFAULT '1' COMMENT 'Status da Landing Page, 0 = desativada, 1 ativada',
  `template_landingPage` int(11) NOT NULL DEFAULT '1' COMMENT 'Template da landing page, por padrão iniciará com o template 1 (template padrão)',
  `link_landingPage` varchar(512) NOT NULL,
  `grupoLista_landingPage` varchar(45) NOT NULL,
  PRIMARY KEY (`id_landingPage`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `landing_pages`
--

LOCK TABLES `landing_pages` WRITE;
/*!40000 ALTER TABLE `landing_pages` DISABLE KEYS */;
INSERT INTO `landing_pages` VALUES (1,'sdfsdf','sdfsdf','1',1,'http://ispmais.net.br/landingpages/1/sdfsdf','1');
/*!40000 ALTER TABLE `landing_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads_cadastros`
--

DROP TABLE IF EXISTS `leads_cadastros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `leads_cadastros` (
  `id_cadastros` int(11) NOT NULL,
  `nome_cadastros` varchar(145) DEFAULT NULL,
  `telefone_cadastros` varchar(45) DEFAULT NULL,
  `email_cadastros` varchar(90) DEFAULT NULL,
  `ondeNosConheceu_cadastros` varchar(45) DEFAULT NULL,
  `landingPage_cadastros` int(11) DEFAULT NULL,
  `grupoLista_cadastros` int(11) DEFAULT NULL,
  `cidade_cadastros` varchar(45) DEFAULT NULL,
  `estado_cadastros` varchar(45) DEFAULT NULL,
  `data_cadastros` varchar(9) DEFAULT NULL,
  `hora_cadastros` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_cadastros`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads_cadastros`
--

LOCK TABLES `leads_cadastros` WRITE;
/*!40000 ALTER TABLE `leads_cadastros` DISABLE KEYS */;
INSERT INTO `leads_cadastros` VALUES (1,'AAAAAAAAAAAAA','+5511111111111','AAAAAA@GMAIL.COM','Facebook',1,1,'Goiânia','Goias','20201104','1557'),(2,'Agencia Solid','+5562981955636','revenda@agenciasolid.com.br','Facebook',1,1,'Aparecida de Goiania','Goias','20201105','0001');
/*!40000 ALTER TABLE `leads_cadastros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_landing_pages_clicks`
--

DROP TABLE IF EXISTS `lp_landing_pages_clicks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_landing_pages_clicks` (
  `id_clicks` int(11) NOT NULL,
  `idLandingPage_clicks` int(11) DEFAULT NULL,
  `token_clicks` varchar(90) DEFAULT NULL,
  `nomeBotao_clicks` varchar(45) DEFAULT NULL,
  `linkBotao_clicks` varchar(145) DEFAULT NULL,
  `dataClick__clicks` varchar(45) DEFAULT NULL,
  `horaClick__clicks` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_clicks`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_landing_pages_clicks`
--

LOCK TABLES `lp_landing_pages_clicks` WRITE;
/*!40000 ALTER TABLE `lp_landing_pages_clicks` DISABLE KEYS */;
INSERT INTO `lp_landing_pages_clicks` VALUES (1,1,'2thshshdvmi5pnl06fqm9n8anekasm6h','botaoRodapeFacebook','https://www.facebook.com/joshefferrobert','20201104','1654'),(2,1,'2thshshdvmi5pnl06fqm9n8anekasm6h','botaoRodapeInstagram','https://www.instagram.com/joshefferr','20201104','1654'),(3,1,'2thshshdvmi5pnl06fqm9n8anekasm6h','botaoRodapeWhatsApp1','https://api.whatsapp.com/send?phone=55&text=','20201104','1654'),(4,1,'2thshshdvmi5pnl06fqm9n8anekasm6h','botaoRodapeWhatsApp2','https://api.whatsapp.com/send?phone=55&text=','20201104','1654'),(5,1,'2thshshdvmi5pnl06fqm9n8anekasm6h','botaoLateralWhatsApp1','https://api.whatsapp.com/send?phone=55&text=','20201104','1654');
/*!40000 ALTER TABLE `lp_landing_pages_clicks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_landing_pages_contador`
--

DROP TABLE IF EXISTS `lp_landing_pages_contador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_landing_pages_contador` (
  `id_contador` int(11) NOT NULL AUTO_INCREMENT,
  `ip_contador` varchar(45) DEFAULT NULL,
  `cidade_contador` varchar(65) DEFAULT NULL,
  `estado_contador` varchar(45) DEFAULT NULL,
  `pais_contador` varchar(45) DEFAULT NULL,
  `dispositivo_contador` varchar(45) DEFAULT NULL,
  `navegador_contador` varchar(45) DEFAULT NULL,
  `SO_contador` varchar(45) DEFAULT NULL,
  `landingPage_contador` int(11) DEFAULT NULL,
  `data_contador` varchar(45) DEFAULT NULL,
  `hora_contador` varchar(45) DEFAULT NULL,
  `tempo_contador` varchar(45) DEFAULT NULL,
  `token_contador` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`id_contador`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_landing_pages_contador`
--

LOCK TABLES `lp_landing_pages_contador` WRITE;
/*!40000 ALTER TABLE `lp_landing_pages_contador` DISABLE KEYS */;
INSERT INTO `lp_landing_pages_contador` VALUES (1,'177.30.111.114','Goiania','Goias','Brazil','0','Chrome','Windows',1,'20201102','1606','189','8t5rk2qn1i868oh3lk88migke302vuet'),(2,'177.30.111.114','Goiania','Goias','Brazil','0','Chrome','Windows',1,'20201102','1609','2408','feapqo63lk8ov9snri7f4fo9d95c5ivf'),(3,'177.30.111.114','Goiania','Goias','Brazil','0','Opera','Windows',1,'20201103','1322','52','06kfb7apjfakas4qti9l2snlns1b58kp'),(4,'177.30.111.114','Goiania','Goias','Brazil','0','Opera','Windows',1,'20201104','1421','798','c86prm8ksg6dol97psbnrf26omsh4rgb'),(5,'177.30.111.114','Goiania','Goias','Brazil','0','Opera','Windows',1,'20201104','1556','17','a990vdpht0jt1srs3fui30nofeknhpuv'),(6,'177.30.111.114','Goiania','Goias','Brazil','0','Opera','Windows',1,'20201104','1654',NULL,'2thshshdvmi5pnl06fqm9n8anekasm6h'),(7,'177.30.111.114','Goiania','Goias','Brazil','0','Chrome','Windows',1,'20201105','0001','16','c680s1mk3g1j2i7l6cvm50rh2cbmjqkc');
/*!40000 ALTER TABLE `lp_landing_pages_contador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modeloconfig_head`
--

DROP TABLE IF EXISTS `lp_modeloconfig_head`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modeloconfig_head` (
  `id_modelo` int(11) DEFAULT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `topo_head_css` text,
  `topo_head_js` text,
  `topo_head_SEO_keywords` text,
  `topo_head_SEO_description` text,
  `topo_head_SEO_description2` text,
  `topo_head_SEO_description3` text,
  `topo_head_SEO_imagem` varchar(145) DEFAULT 'imagem_default1.png',
  `topo_head_coordenadasMaps` varchar(80) DEFAULT NULL,
  `topo_head_cidade` varchar(80) DEFAULT NULL,
  `topo_head_estado` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modeloconfig_head`
--

LOCK TABLES `lp_modeloconfig_head` WRITE;
/*!40000 ALTER TABLE `lp_modeloconfig_head` DISABLE KEYS */;
INSERT INTO `lp_modeloconfig_head` VALUES (1,1,'','','eqwe','qweqw','qweqwe','qweqwe','imagem_default1.png','qweqwe','qweqwe','adasd');
/*!40000 ALTER TABLE `lp_modeloconfig_head` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modeloconfig_logomenuslide`
--

DROP TABLE IF EXISTS `lp_modeloconfig_logomenuslide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modeloconfig_logomenuslide` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_logo1` enum('0','1') DEFAULT '1',
  `body_logo2` enum('0','1') DEFAULT '1',
  `body_imagemPrincipalSlide` enum('0','1') DEFAULT '1',
  `body_imagemFundoSlide` enum('0','1') DEFAULT '1',
  `body_menu_topo` enum('0','1') DEFAULT '1',
  `body_slide1` enum('0','1') DEFAULT '1',
  `body_slide` enum('0','1') DEFAULT '1',
  `body_slide_titulo1` varchar(145) DEFAULT 'Titulo',
  `body_slide_titulo2` varchar(145) DEFAULT 'Titulo',
  `body_slide_titulo3` varchar(145) DEFAULT 'Titulo',
  `body_slide_descricao1` varchar(256) DEFAULT 'Descrição',
  `body_slide_descricao2` varchar(256) DEFAULT 'Descrição',
  `body_slide_descricao3` varchar(256) DEFAULT 'Descrição',
  `body_slide_imagemFundoSlide` varchar(256) DEFAULT 'default2.png',
  `body_slide_imagem1` varchar(256) DEFAULT 'default.png',
  `body_slide_imagem2` varchar(256) DEFAULT 'default.png',
  `body_slide_imagem3` varchar(256) DEFAULT 'default.png',
  `body_slide_imagem1_SEO` varchar(256) DEFAULT NULL,
  `body_slide_imagem2_SEO` varchar(256) DEFAULT NULL,
  `body_slide_imagem3_SEO` varchar(256) DEFAULT NULL,
  `body_slide_botao1` enum('0','1') DEFAULT '1',
  `body_slide_botao2` enum('0','1') DEFAULT '1',
  `body_slide_botao3` enum('0','1') DEFAULT '1',
  `body_slide_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_slide_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_slide_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_slide_botaoLink1` varchar(145) DEFAULT 'www.google.com.br',
  `body_slide_botaoLink2` varchar(145) DEFAULT 'www.google.com.br',
  `body_slide_botaoLink3` varchar(145) DEFAULT 'www.google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modeloconfig_logomenuslide`
--

LOCK TABLES `lp_modeloconfig_logomenuslide` WRITE;
/*!40000 ALTER TABLE `lp_modeloconfig_logomenuslide` DISABLE KEYS */;
INSERT INTO `lp_modeloconfig_logomenuslide` VALUES (1,1,'1','1','1','1','1','1','1','Titulo','Titulo','Titulo','Descrição','Descrição','Descrição','default2.png','default.png','default.png','default.png',NULL,'',NULL,'1','1','1','Ver +','Ver +','Ver +','www.google.com.br','www.google.com.br','www.google.com.br');
/*!40000 ALTER TABLE `lp_modeloconfig_logomenuslide` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_forms`
--

DROP TABLE IF EXISTS `lp_modelosconfig_forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_forms` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico1_form1` enum('0','1') DEFAULT '1',
  `body_servico1_form1_corFundoBotao` varchar(10) DEFAULT '#000000',
  `body_servico1_form1_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico1_form1_corFundoForm` varchar(10) DEFAULT '#ffffff',
  `body_servico1_form1_corTextBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico1_form2` enum('0','1') DEFAULT '1',
  `body_servico1_form2_corFundoBotao` varchar(10) DEFAULT '#000000',
  `body_servico1_form2_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico1_form2_corFundoForm` varchar(10) DEFAULT '#ffffff',
  `body_servico1_form2_corTextBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico2_form3` enum('0','1') DEFAULT '1',
  `body_servico2_form3_corFundoBotao` varchar(10) DEFAULT '#000000',
  `body_servico2_form3_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico2_form3_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico2_form3_corTextBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico3_form4` enum('0','1') DEFAULT '1',
  `body_servico3_form4_corFundoBotao` varchar(10) DEFAULT '#000000',
  `body_servico3_form4_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico3_form4_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico3_form4_corTextBotao` varchar(45) DEFAULT '#ffffff',
  `body_servico4_form5` enum('0','1') DEFAULT '1',
  `body_servico4_form5_corFundoBotao` varchar(10) DEFAULT '#000000',
  `body_servico4_form5_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico4_form5_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico4_form5_corTextBotao` varchar(45) DEFAULT '#ffffff',
  `body_servico5_form6` enum('0','1') DEFAULT '1',
  `body_servico5_form6_corFundoBotao` varchar(10) DEFAULT '#000000',
  `body_servico5_form6_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico5_form6_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico5_form6_corTextBotao` varchar(45) DEFAULT '#ffffff',
  `body_servico6_form7` enum('0','1') DEFAULT '1',
  `body_servico6_form7_corFundoBotao` varchar(10) DEFAULT '#000000',
  `body_servico6_form7_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico6_form7_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico6_form7_corTextBotao` varchar(45) DEFAULT '#ffffff',
  `body_servico7_form8` enum('0','1') DEFAULT '1',
  `body_servico7_form8_corFundoBotao` varchar(10) DEFAULT '#000000',
  `body_servico7_form8_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico7_form8_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico7_form8_corTextBotao` varchar(45) DEFAULT '#ffffff',
  `body_servico_RedesSociais_form` enum('0','1') DEFAULT '1',
  `body_servico_RedesSociais_form_corFundoBotao` varchar(10) DEFAULT '#000000',
  `body_servico_RedesSociais_form_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico_RedesSociais_form_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico_RedesSociais_form_corTextBotao` varchar(45) DEFAULT '#ffffff',
  `nosConheceuOpcao1` varchar(45) DEFAULT 'Facebook',
  `nosConheceuOpcao2` varchar(45) DEFAULT 'Instagram',
  `nosConheceuOpcao3` varchar(45) DEFAULT 'WhatsApp',
  `nosConheceuOpcao4` varchar(45) DEFAULT 'Google',
  `nosConheceuOpcao5` varchar(45) DEFAULT 'YouTube',
  `nosConheceuOpcao6` varchar(45) DEFAULT 'Messenger',
  `nosConheceuOpcao7` varchar(45) DEFAULT 'Amigos',
  `nosConheceuOpcao8` varchar(45) DEFAULT 'Outros',
  `nosConheceuOpcao1_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao2_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao3_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao4_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao5_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao6_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao7_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao8_visivel` enum('0','1') DEFAULT '1',
  `body_servicoForm_tituloForm1` varchar(128) DEFAULT 'Titulo 1',
  `body_servicoForm_descricaoForm1` varchar(512) DEFAULT 'Descrição 1',
  `body_servicoForm_tituloForm2` varchar(128) DEFAULT 'Titulo 2',
  `body_servicoForm_descricaoForm2` varchar(512) DEFAULT 'Descrição 2',
  `body_servicoForm_tituloForm3` varchar(128) DEFAULT 'Titulo 3',
  `body_servicoForm_descricaoForm3` varchar(512) DEFAULT 'Descrição 3',
  `body_servicoForm_tituloForm4` varchar(128) DEFAULT 'Titulo 4',
  `body_servicoForm_descricaoForm4` varchar(512) DEFAULT 'Descrição 4',
  `body_servicoForm_tituloForm5` varchar(128) DEFAULT 'Titulo 5',
  `body_servicoForm_descricaoForm5` varchar(512) DEFAULT 'Descrição 5',
  `body_servicoForm_tituloForm6` varchar(128) DEFAULT 'Titulo 6',
  `body_servicoForm_descricaoForm6` varchar(512) DEFAULT 'Descrição 6',
  `body_servicoForm_tituloForm7` varchar(128) DEFAULT 'Titulo 7',
  `body_servicoForm_descricaoForm7` varchar(512) DEFAULT 'Descrição 7',
  `body_servicoForm_tituloForm8` varchar(128) DEFAULT 'Titulo 8',
  `body_servicoForm_descricaoForm8` varchar(512) DEFAULT 'Descrição 8',
  `body_servicoForm_tituloFormRS` varchar(128) DEFAULT 'Titulo RS',
  `body_servicoForm_descricaoFormRS` varchar(512) DEFAULT 'Descrição RS',
  `body_servico_form1_corTitulo` varchar(10) DEFAULT '#000000',
  `body_servico_form1_corDescricao` varchar(10) DEFAULT '#000000',
  `body_servico_form2_corTitulo` varchar(10) DEFAULT '#000000',
  `body_servico_form2_corDescricao` varchar(10) DEFAULT '#000000',
  `body_servico_form3_corTitulo` varchar(10) DEFAULT '#000000',
  `body_servico_form3_corDescricao` varchar(10) DEFAULT '#000000',
  `body_servico_form4_corTitulo` varchar(10) DEFAULT '#000000',
  `body_servico_form4_corDescricao` varchar(10) DEFAULT '#000000',
  `body_servico_form5_corTitulo` varchar(10) DEFAULT '#000000',
  `body_servico_form5_corDescricao` varchar(10) DEFAULT '#000000',
  `body_servico_form6_corTitulo` varchar(10) DEFAULT '#000000',
  `body_servico_form6_corDescricao` varchar(10) DEFAULT '#000000',
  `body_servico_form7_corTitulo` varchar(10) DEFAULT '#000000',
  `body_servico_form7_corDescricao` varchar(10) DEFAULT '#000000',
  `body_servico_form8_corTitulo` varchar(10) DEFAULT '#000000',
  `body_servico_form8_corDescricao` varchar(10) DEFAULT '#000000',
  `body_servico_formRS_corTitulo` varchar(10) DEFAULT '#000000',
  `body_servico_formRS_corDescricao` varchar(10) DEFAULT '#000000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_forms`
--

LOCK TABLES `lp_modelosconfig_forms` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_forms` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_forms` VALUES (1,1,'1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','Facebook','Instagram','WhatsApp','Google','YouTube','Messenger','Amigos','Outros','1','1','1','1','1','1','1','1','Titulo 1','Descrição 1','Titulo 2','Descrição 2','Titulo 3','Descrição 3','Titulo 4','Descrição 4','Titulo 5','Descrição 5','Titulo 6','Descrição 6','Titulo 7','Descrição 7','Titulo 8','Descrição 8','Titulo RS','Descrição RS','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000');
/*!40000 ALTER TABLE `lp_modelosconfig_forms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_personalizacao`
--

DROP TABLE IF EXISTS `lp_modelosconfig_personalizacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_personalizacao` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `corFundoMenu` varchar(10) DEFAULT '#8cff00',
  `corFundoSlide1` varchar(10) DEFAULT '#000000',
  `corFundoSlide` varchar(10) DEFAULT '#000000',
  `corTextoTituloSlide` varchar(10) DEFAULT '#ffffff',
  `corTextoDescricaoSlide` varchar(10) DEFAULT '#ffffff',
  `corTextoBotaoSlide` varchar(10) DEFAULT '#000000',
  `corFundoBotaoSlide` varchar(10) DEFAULT '#ffffff',
  `corFundoHoverBotaoSlide` varchar(10) DEFAULT '#cac8c8',
  `corTextoHoverBotaoSlide` varchar(10) DEFAULT '#ffffff',
  `servico1_corTituloGenerico1` varchar(10) DEFAULT '#ffffff',
  `servico1_corDescricaoGenerico1` varchar(10) DEFAULT '#ffffff',
  `servico1_corBg` varchar(10) DEFAULT '#000000',
  `servico1_corBoxImage` varchar(10) DEFAULT '#ffffff',
  `servico1_corTexto` varchar(10) DEFAULT '#000000',
  `servico2_corTituloGenerico1` varchar(10) DEFAULT '#ffffff',
  `servico2_corDescricaoGenerico1` varchar(10) DEFAULT '#ffffff',
  `servico2_corBg` varchar(10) DEFAULT '#000000',
  `servico2_corTextoBotao` varchar(10) DEFAULT '#000000',
  `servico2_corTextoHoverBotao` varchar(10) DEFAULT '#ff0000',
  `servico2_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `servico2_corFundoHoverBotao` varchar(10) DEFAULT '#cac8c8',
  `servico3_corTituloGenerico1` varchar(10) DEFAULT '#ffffff',
  `servico3_corDescricaoGenerico1` varchar(10) DEFAULT '#ffffff',
  `servico3_corBg` varchar(10) DEFAULT '#000000',
  `servico3_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico3_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico4_corTituloGenerico1` varchar(45) DEFAULT '#ffffff',
  `servico4_corDescricaoGenerico1` varchar(10) DEFAULT '#ffffff',
  `servico4_corBg` varchar(10) DEFAULT '#000000',
  `servico4_corBoxImage` varchar(10) DEFAULT '#ffffff',
  `servico4_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico4_corTextoBotao` varchar(10) DEFAULT '#000000',
  `servico4_corTextoHoverBotao` varchar(45) DEFAULT '#ff0000',
  `servico4_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `servico4_corFundoHoverBotao` varchar(10) DEFAULT '#cac8c8',
  `servico5_corTituloGenerico1` varchar(45) DEFAULT '#ffffff',
  `servico5_corDescricaoGenerico1` varchar(10) DEFAULT '#ffffff',
  `servico5_corBg` varchar(10) DEFAULT '#000000',
  `servico5_corBoxImage` varchar(10) DEFAULT '#ffffff',
  `servico5_corTexto` varchar(10) DEFAULT '#000000',
  `servico5_corTextoBotao` varchar(45) DEFAULT '#ffffff',
  `servico5_corTextoHoverBotao` varchar(45) DEFAULT '#ff0000',
  `servico5_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico5_corFundoHoverBotao` varchar(10) DEFAULT '#cac8c8',
  `servico6_corTituloGenerico1` varchar(45) DEFAULT '#ffffff',
  `servico6_corDescricaoGenerico1` varchar(10) DEFAULT '#ffffff',
  `servico6_corBg` varchar(10) DEFAULT '#000000',
  `servico6_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico6_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico6_corTextoBotao` varchar(45) DEFAULT '#ffffff',
  `servico6_corTextoHoverBotao` varchar(45) DEFAULT '#000000',
  `servico6_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico6_corFundoHoverBotao` varchar(10) DEFAULT '#ffffff',
  `servico7_corTituloGenerico1` varchar(45) DEFAULT '#ffffff',
  `servico7_corDescricaoGenerico1` varchar(10) DEFAULT '#ffffff',
  `servico7_corBg` varchar(10) DEFAULT '#000000',
  `servico7_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico7_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico7_corTextoBotao` varchar(45) DEFAULT '#ffffff',
  `servico7_corTextoHoverBotao` varchar(45) DEFAULT '#000000',
  `servico7_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico7_corFundoHoverBotao` varchar(10) DEFAULT '#ffffff',
  `servico_redesSociais_corTituloGenerico1` varchar(45) DEFAULT '#ffffff',
  `servico_redesSociais_corDescricaoGenerico1` varchar(10) DEFAULT '#ffffff',
  `servico_redesSociais_corBg` varchar(10) DEFAULT '#000000',
  `servico_redesSociais_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico_redesSociais_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico_redesSociais_corTextoBotao` varchar(45) DEFAULT '#000000',
  `servico_redesSociais_corTextoHoverBotao` varchar(45) DEFAULT '#ff0000',
  `servico_redesSociais_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `servico_redesSociais_corFundoHoverBotao` varchar(10) DEFAULT '#cac8c8',
  `servico_maps_corTituloGenerico1` varchar(45) DEFAULT '#ffffff',
  `servico_maps_corDescricaoGenerico1` varchar(10) DEFAULT '#ffffff',
  `servico_maps_corBg` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corTextoBotao` varchar(10) DEFAULT '#ffffff',
  `servico_paginaSucesso_corTextoHoverBotao` varchar(10) DEFAULT '#ffffff',
  `servico_paginaSucesso_corTextoTitulo` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corTextoDescricao` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corFundoHoverBotao` varchar(10) DEFAULT '#353131',
  `servico_paginaSucesso_corBG` varchar(10) DEFAULT '#ffffff',
  `corFundoRodape` varchar(10) DEFAULT '#ffffff',
  `corTextoRodape` varchar(10) DEFAULT '#000000',
  `corIconesRodape` varchar(10) DEFAULT '#ffffff',
  `corFundoIconesMenu` varchar(10) DEFAULT '#ffffff',
  `corIconesMenu` varchar(10) DEFAULT '#000000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_personalizacao`
--

LOCK TABLES `lp_modelosconfig_personalizacao` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_personalizacao` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_personalizacao` VALUES (1,1,'#8cff00','#000000','#000000','#ffffff','#ffffff','#000000','#ffffff','#cac8c8','#ffffff','#ffffff','#ffffff','#000000','#ffffff','#000000','#ffffff','#ffffff','#000000','#000000','#ff0000','#ffffff','#cac8c8','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#ffffff','#000000','#ffffff','#ffffff','#000000','#ff0000','#ffffff','#cac8c8','#ffffff','#ffffff','#000000','#ffffff','#000000','#ffffff','#ff0000','#000000','#cac8c8','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#ff0000','#ffffff','#cac8c8','#ffffff','#ffffff','#000000','#ffffff','#ffffff','#000000','#000000','#000000','#353131','#ffffff','#ffffff','#000000','#ffffff','#ffffff','#000000');
/*!40000 ALTER TABLE `lp_modelosconfig_personalizacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_redessociais`
--

DROP TABLE IF EXISTS `lp_modelosconfig_redessociais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_redessociais` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_redesSociais` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico_redesSociais` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo_redesSociais` varchar(45) DEFAULT 'Titulo serviço Redes Sociais',
  `body_textoGenericoDescricao_redesSociais` varchar(512) DEFAULT 'Descrição serviço Redes Sociais',
  `body_redesSociais_opcao1` enum('0','1') DEFAULT '1',
  `body_redesSociais_opcao2` enum('0','1') DEFAULT '1',
  `body_redesSociais_opcao3` enum('0','1') DEFAULT '1',
  `body_servico_redesSociais_Imagem1` varchar(145) DEFAULT 'default.png',
  `body_servico_redesSociais_Imagem2` varchar(145) DEFAULT 'default.png',
  `body_servico_redesSociais_Imagem3` varchar(145) DEFAULT 'default.png',
  `body_servico_redesSociais_Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico_redesSociais_Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico_redesSociais_Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico_redesSociais_Descricao1` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico_redesSociais_Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico_redesSociais_Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico_redesSociais_ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico_redesSociais_ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico_redesSociais_ImagemSEO3` varchar(90) DEFAULT 'info SEO3',
  `body_servico_redesSociais_botao1` enum('0','1') DEFAULT '1',
  `body_servico_redesSociais_botao2` enum('0','1') DEFAULT '1',
  `body_servico_redesSociais_botao3` enum('0','1') DEFAULT '1',
  `body_servico_redesSociais_botaoTexto1` varchar(45) DEFAULT 'ver +',
  `body_servico_redesSociais_botaoTexto2` varchar(45) DEFAULT 'ver +',
  `body_servico_redesSociais_botaoTexto3` varchar(45) DEFAULT 'ver +',
  `body_servico_redesSociais_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico_redesSociais_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico_redesSociais_botaoLink3` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_redessociais`
--

LOCK TABLES `lp_modelosconfig_redessociais` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_redessociais` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_redessociais` VALUES (1,1,'1','1','Titulo serviço Redes Sociais','Descrição serviço Redes Sociais','1','1','1','default.png','default.png','default.png','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','descrição serviço 2','descrição serviço 2','descrição serviço 3','info SEO 1','info SEO 2','info SEO3','1','1','1','ver +','ver +','ver +','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_redessociais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico1`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico1` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico1` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico1` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo1` varchar(45) DEFAULT 'Titulo serviço 1',
  `body_textoGenericoDescricao1` varchar(512) DEFAULT 'Descrição serviço 1',
  `body_servico1_opcao1` enum('0','1') DEFAULT '1',
  `body_servico1_opcao2` enum('0','1') DEFAULT '1',
  `body_servico1_opcao3` enum('0','1') DEFAULT '1',
  `body_servico1_opcao4` enum('0','1') DEFAULT '1',
  `body_servico1_opcao5` enum('0','1') DEFAULT '1',
  `body_servico1_opcao6` enum('0','1') DEFAULT '1',
  `body_servico1_opcao7` enum('0','1') DEFAULT '1',
  `body_servico1_opcao8` enum('0','1') DEFAULT '1',
  `body_servico1Imagem1` varchar(145) DEFAULT 'default_servico_1.png',
  `body_servico1Imagem2` varchar(145) DEFAULT 'default_servico_1.png',
  `body_servico1Imagem3` varchar(145) DEFAULT 'default_servico_1.png',
  `body_servico1Imagem4` varchar(145) DEFAULT 'default_servico_1.png',
  `body_servico1Imagem5` varchar(145) DEFAULT 'default_servico_1.png',
  `body_servico1Imagem6` varchar(145) DEFAULT 'default_servico_1.png',
  `body_servico1Imagem7` varchar(145) DEFAULT 'default_servico_1.png',
  `body_servico1Imagem8` varchar(145) DEFAULT 'default_servico_1.png',
  `body_servico1Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico1Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico1Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico1Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico1Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico1Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico1Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico1Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico1Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico1Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico1Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico1Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico1Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico1Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico1Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico1Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico1ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico1ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico1ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico1ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico1ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico1ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico1ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico1ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico1_botao1` enum('0','1') DEFAULT '1',
  `body_servico1_botao2` enum('0','1') DEFAULT '1',
  `body_servico1_botao3` enum('0','1') DEFAULT '1',
  `body_servico1_botao4` enum('0','1') DEFAULT '1',
  `body_servico1_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico1_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico1_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico1_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico1_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico1_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico1_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico1_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela onde será armazenado os dados da seção de serviços 1';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico1`
--

LOCK TABLES `lp_modelosconfig_servico1` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico1` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico1` VALUES (1,1,'1','1','Titulo serviço 1','Descrição serviço 1','1','1','1','1','1','1','1','1','default_servico_1.png','default_servico_1.png','default_servico_1.png','default_servico_1.png','default_servico_1.png','default_servico_1.png','default_servico_1.png','default_servico_1.png','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico2`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico2` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico2` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico2` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo2` varchar(45) DEFAULT 'Titulo serviço 2',
  `body_textoGenericoDescricao2` varchar(512) DEFAULT 'Descrição serviço 2',
  `body_servico2Imagem1` varchar(145) DEFAULT 'default_servico_2.png',
  `body_servico2Imagem2` varchar(145) DEFAULT 'default_servico_2.png',
  `body_servico2Imagem3` varchar(145) DEFAULT 'default_servico_2.png',
  `body_servico2Imagem4` varchar(145) DEFAULT 'default_servico_2.png',
  `body_servico2Imagem5` varchar(145) DEFAULT 'default_servico_2.png',
  `body_servico2Imagem6` varchar(145) DEFAULT 'default_servico_2.png',
  `body_servico2Imagem7` varchar(145) DEFAULT 'default_servico_2.png',
  `body_servico2Imagem8` varchar(145) DEFAULT 'default_servico_2.png',
  `body_servico2Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico2Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico2Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico2Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico2Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico2Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico2Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico2Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico2Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico2Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico2Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico2Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico2Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico2Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico2Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico2Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico2ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico2ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico2ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico2ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico2ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico2ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico2ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico2ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico2_botao1` enum('0','1') DEFAULT '1',
  `body_servico2_botao2` enum('0','1') DEFAULT '1',
  `body_servico2_botao3` enum('0','1') DEFAULT '1',
  `body_servico2_botao4` enum('0','1') DEFAULT '1',
  `body_servico2_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico2_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico2_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico2_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico2_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico2_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico2_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico2_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico2`
--

LOCK TABLES `lp_modelosconfig_servico2` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico2` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico2` VALUES (1,1,'1','1','Titulo serviço 2','Descrição serviço 2','default_servico_2.png','default_servico_2.png','default_servico_2.png','default_servico_2.png','default_servico_2.png','default_servico_2.png','default_servico_2.png','default_servico_2.png','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico3`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico3` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico3` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico3` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo3` varchar(45) DEFAULT 'Titulo serviço 3',
  `body_textoGenericoDescricao3` varchar(512) DEFAULT 'Descrição serviço 3',
  `body_servico3_TodasOpcoes` enum('0','1') DEFAULT '1',
  `body_servico3_opcao1` enum('0','1') DEFAULT '1',
  `body_servico3_opcao2` enum('0','1') DEFAULT '1',
  `body_servico3_opcao3` enum('0','1') DEFAULT '1',
  `body_servico3_opcao4` enum('0','1') DEFAULT '1',
  `body_servico3_opcao5` enum('0','1') DEFAULT '1',
  `body_servico3_opcao6` enum('0','1') DEFAULT '1',
  `body_servico3_opcao7` enum('0','1') DEFAULT '1',
  `body_servico3_opcao8` enum('0','1') DEFAULT '1',
  `body_servico3Imagem1` varchar(145) DEFAULT 'default_servico_3_1.png',
  `body_servico3Imagem2` varchar(145) DEFAULT 'default_servico_3_1.png',
  `body_servico3Imagem3` varchar(145) DEFAULT 'default_servico_3_1.png',
  `body_servico3Imagem4` varchar(145) DEFAULT 'default_servico_3_1.png',
  `body_servico3Imagem5` varchar(145) DEFAULT 'default_servico_3_2.png',
  `body_servico3Imagem6` varchar(145) DEFAULT 'default_servico_3_2.png',
  `body_servico3Imagem7` varchar(145) DEFAULT 'default_servico_3_2.png',
  `body_servico3Imagem8` varchar(145) DEFAULT 'default_servico_3_2.png',
  `body_servico3Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico3Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico3Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico3Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico3Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico3Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico3Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico3Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico3Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico3Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico3Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico3Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico3Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico3Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico3Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico3Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico3ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico3ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico3ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico3ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico3ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico3ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico3ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico3ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico3_botao1` enum('0','1') DEFAULT '1',
  `body_servico3_botao2` enum('0','1') DEFAULT '1',
  `body_servico3_botao3` enum('0','1') DEFAULT '1',
  `body_servico3_botao4` enum('0','1') DEFAULT '1',
  `body_servico3_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico3_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico3_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico3_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico3_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico3_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico3_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico3_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico3`
--

LOCK TABLES `lp_modelosconfig_servico3` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico3` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico3` VALUES (1,1,'1','1','Titulo serviço 3','Descrição serviço 3','1','1','1','1','1','1','1','1','1','default_servico_3_1.png','default_servico_3_1.png','default_servico_3_1.png','default_servico_3_1.png','default_servico_3_2.png','default_servico_3_2.png','default_servico_3_2.png','default_servico_3_2.png','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico4`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico4` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico4` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico4` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo4` varchar(45) DEFAULT 'Titulo serviço 4',
  `body_textoGenericoDescricao4` varchar(512) DEFAULT 'Descrição serviço 4',
  `body_servico4Imagem1` varchar(145) DEFAULT 'default_servico_4.png',
  `body_servico4Imagem2` varchar(145) DEFAULT 'default_servico_4.png',
  `body_servico4Imagem3` varchar(145) DEFAULT 'default_servico_4.png',
  `body_servico4Imagem4` varchar(145) DEFAULT 'default_servico_4.png',
  `body_servico4Imagem5` varchar(145) DEFAULT 'default_servico_4.png',
  `body_servico4Imagem6` varchar(145) DEFAULT 'default_servico_4.png',
  `body_servico4Imagem7` varchar(145) DEFAULT 'default_servico_4.png',
  `body_servico4Imagem8` varchar(145) DEFAULT 'default_servico_4.png',
  `body_servico4Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico4Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico4Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico4Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico4Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico4Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico4Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico4Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico4Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico4Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico4Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico4Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico4Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico4Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico4Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico4Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico4ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico4ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico4ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico4ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico4ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico4ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico4ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico4ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico4_botao1` enum('0','1') DEFAULT '1',
  `body_servico4_botao2` enum('0','1') DEFAULT '1',
  `body_servico4_botao3` enum('0','1') DEFAULT '1',
  `body_servico4_botao4` enum('0','1') DEFAULT '1',
  `body_servico4_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico4_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico4_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico4_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico4_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico4_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico4_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico4_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico4`
--

LOCK TABLES `lp_modelosconfig_servico4` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico4` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico4` VALUES (1,1,'1','1','Titulo serviço 4','Descrição serviço 4','default_servico_4.png','default_servico_4.png','default_servico_4.png','default_servico_4.png','default_servico_4.png','default_servico_4.png','default_servico_4.png','default_servico_4.png','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico5`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico5` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico5` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico5` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo5` varchar(45) DEFAULT 'Titulo serviço 5',
  `body_textoGenericoDescricao5` varchar(512) DEFAULT 'Descrição serviço 5',
  `body_servico5_opcao1` enum('0','1') DEFAULT '1',
  `body_servico5_opcao2` enum('0','1') DEFAULT '1',
  `body_servico5_opcao3` enum('0','1') DEFAULT '1',
  `body_servico5_opcao4` enum('0','1') DEFAULT '1',
  `body_servico5_opcao5` enum('0','1') DEFAULT '1',
  `body_servico5_opcao6` enum('0','1') DEFAULT '1',
  `body_servico5_opcao7` enum('0','1') DEFAULT '1',
  `body_servico5_opcao8` enum('0','1') DEFAULT '1',
  `body_servico5Imagem1` varchar(145) DEFAULT 'default_servico_5.png',
  `body_servico5Imagem2` varchar(145) DEFAULT 'default_servico_5.png',
  `body_servico5Imagem3` varchar(145) DEFAULT 'default_servico_5.png',
  `body_servico5Imagem4` varchar(145) DEFAULT 'default_servico_5.png',
  `body_servico5Imagem5` varchar(145) DEFAULT 'default_servico_5.png',
  `body_servico5Imagem6` varchar(145) DEFAULT 'default_servico_5.png',
  `body_servico5Imagem7` varchar(145) DEFAULT 'default_servico_5.png',
  `body_servico5Imagem8` varchar(145) DEFAULT 'default_servico_5.png',
  `body_servico5Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico5Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico5Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico5Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico5Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico5Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico5Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico5Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico5Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico5Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico5Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico5Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico5Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico5Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico5Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico5Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico5ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico5ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico5ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico5ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico5ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico5ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico5ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico5ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico5_botao1` enum('0','1') DEFAULT '1',
  `body_servico5_botao2` enum('0','1') DEFAULT '1',
  `body_servico5_botao3` enum('0','1') DEFAULT '1',
  `body_servico5_botao4` enum('0','1') DEFAULT '1',
  `body_servico5_botao5` enum('0','1') DEFAULT '1',
  `body_servico5_botao6` enum('0','1') DEFAULT '1',
  `body_servico5_botao7` enum('0','1') DEFAULT '1',
  `body_servico5_botao8` enum('0','1') DEFAULT '1',
  `body_servico5_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto5` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto6` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto7` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto8` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink4` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink5` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink6` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink7` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink8` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_preco1_opcao1` enum('0','1') DEFAULT '1',
  `body_servico5_preco2_opcao2` enum('0','1') DEFAULT '1',
  `body_servico5_preco3_opcao3` enum('0','1') DEFAULT '1',
  `body_servico5_preco4_opcao4` enum('0','1') DEFAULT '1',
  `body_servico5_preco5_opcao5` enum('0','1') DEFAULT '1',
  `body_servico5_preco6_opcao6` enum('0','1') DEFAULT '1',
  `body_servico5_preco7_opcao7` enum('0','1') DEFAULT '1',
  `body_servico5_preco8_opcao8` enum('0','1') DEFAULT '1',
  `body_servico5_preco1` varchar(45) DEFAULT '0.00',
  `body_servico5_preco2` varchar(45) DEFAULT '0.00',
  `body_servico5_preco3` varchar(45) DEFAULT '0.00',
  `body_servico5_preco4` varchar(45) DEFAULT '0.00',
  `body_servico5_preco5` varchar(45) DEFAULT '0.00',
  `body_servico5_preco6` varchar(45) DEFAULT '0.00',
  `body_servico5_preco7` varchar(45) DEFAULT '0.00',
  `body_servico5_preco8` varchar(45) DEFAULT '0.00',
  `body_servico5_opcao1Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao1Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao1Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao1Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao1Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao1Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao1Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao1Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao1Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao1Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao2Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao2Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao2Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao2Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao2Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao2Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao2Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao2Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao2Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao2Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao3Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao3Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao3Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao3Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao3Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao3Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao3Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao3Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao3Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao3Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao4Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao4Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao4Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao4Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao4Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao4Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao4Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao4Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao4Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao4Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao5Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao5Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao5Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao5Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao5Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao5Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao5Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao5Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao5Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao5Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao6Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao6Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao6Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao6Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao6Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao6Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao6Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao6Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao6Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao6Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao7Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao7Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao7Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao7Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao7Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao7Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao7Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao7Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao7Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao7Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao8Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao8Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao8Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao8Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao8Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao8Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao8Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao8Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao8Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao8Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao9Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao9Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao9Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao9Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao9Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao9Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao9Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao9Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao9Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao9Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao10Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao10Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao10Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao10Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao10Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao10Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao10Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao10Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao10Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao10Descricao10` varchar(45) DEFAULT 'Descrição 10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico5`
--

LOCK TABLES `lp_modelosconfig_servico5` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico5` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico5` VALUES (1,1,'1','1','Titulo serviço 5','Descrição serviço 5','1','1','1','1','1','1','1','1','default_servico_5.png','default_servico_5.png','default_servico_5.png','default_servico_5.png','default_servico_5.png','default_servico_5.png','default_servico_5.png','default_servico_5.png','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','1','1','1','1','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','1','1','1','1','1','1','1','1','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10');
/*!40000 ALTER TABLE `lp_modelosconfig_servico5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico6`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico6` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico6` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico6` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo6` varchar(45) DEFAULT 'Titulo serviço 6',
  `body_textoGenericoDescricao6` varchar(512) DEFAULT 'Descrição serviço 6',
  `body_servico6_opcao1` enum('0','1') DEFAULT '1',
  `body_servico6_opcao2` enum('0','1') DEFAULT '1',
  `body_servico6_opcao3` enum('0','1') DEFAULT '1',
  `body_servico6_opcao4` enum('0','1') DEFAULT '1',
  `body_servico6_opcao5` enum('0','1') DEFAULT '1',
  `body_servico6_opcao6` enum('0','1') DEFAULT '1',
  `body_servico6_opcao7` enum('0','1') DEFAULT '1',
  `body_servico6_opcao8` enum('0','1') DEFAULT '1',
  `body_servico6Imagem1` varchar(145) DEFAULT 'default_servico_6.png',
  `body_servico6Imagem2` varchar(145) DEFAULT 'default_servico_6.png',
  `body_servico6Imagem3` varchar(145) DEFAULT 'default_servico_6.png',
  `body_servico6Imagem4` varchar(145) DEFAULT 'default_servico_6.png',
  `body_servico6Imagem5` varchar(145) DEFAULT 'default_servico_6.png',
  `body_servico6Imagem6` varchar(145) DEFAULT 'default_servico_6.png',
  `body_servico6Imagem7` varchar(145) DEFAULT 'default_servico_6.png',
  `body_servico6Imagem8` varchar(145) DEFAULT 'default_servico_6.png',
  `body_servico6Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico6Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico6Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico6Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico6Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico6Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico6Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico6Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico6Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico6Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico6Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico6Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico6Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico6Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico6Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico6Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico6ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico6ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico6ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico6ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico6ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico6ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico6ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico6ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico6_botao1` enum('0','1') DEFAULT '1',
  `body_servico6_botao2` enum('0','1') DEFAULT '1',
  `body_servico6_botao3` enum('0','1') DEFAULT '1',
  `body_servico6_botao4` enum('0','1') DEFAULT '1',
  `body_servico6_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico6_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico6_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico6_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico6_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico6_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico6_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico6_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico6`
--

LOCK TABLES `lp_modelosconfig_servico6` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico6` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico6` VALUES (1,1,'1','1','Titulo serviço 6','Descrição serviço 6','1','1','1','1','1','1','1','1','default_servico_6.png','default_servico_6.png','default_servico_6.png','default_servico_6.png','default_servico_6.png','default_servico_6.png','default_servico_6.png','default_servico_6.png','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico7`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico7` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico7` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico7` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo7` varchar(45) DEFAULT 'Titulo serviço 7',
  `body_textoGenericoDescricao7` varchar(512) DEFAULT 'Descrição serviço 7',
  `body_servico7_opcao1` enum('0','1') DEFAULT '1',
  `body_servico7_opcao2` enum('0','1') DEFAULT '1',
  `body_servico7_opcao3` enum('0','1') DEFAULT '1',
  `body_servico7_opcao4` enum('0','1') DEFAULT '1',
  `body_servico7_opcao5` enum('0','1') DEFAULT '1',
  `body_servico7_opcao6` enum('0','1') DEFAULT '1',
  `body_servico7_opcao7` enum('0','1') DEFAULT '1',
  `body_servico7_opcao8` enum('0','1') DEFAULT '1',
  `body_servico7Imagem1` varchar(145) DEFAULT 'default_servico_7.png',
  `body_servico7Imagem2` varchar(145) DEFAULT 'default_servico_7.png',
  `body_servico7Imagem3` varchar(145) DEFAULT 'default_servico_7.png',
  `body_servico7Imagem4` varchar(145) DEFAULT 'default_servico_7.png',
  `body_servico7Imagem5` varchar(145) DEFAULT 'default_servico_7.png',
  `body_servico7Imagem6` varchar(145) DEFAULT 'default_servico_7.png',
  `body_servico7Imagem7` varchar(145) DEFAULT 'default_servico_7.png',
  `body_servico7Imagem8` varchar(145) DEFAULT 'default_servico_7.png',
  `body_servico7Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico7Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico7Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico7Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico7Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico7Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico7Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico7Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico7Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico7Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico7Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico7Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico7Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico7Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico7Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico7Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico7ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico7ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico7ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico7ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico7ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico7ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico7ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico7ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico7_botao1` enum('0','1') DEFAULT '1',
  `body_servico7_botao2` enum('0','1') DEFAULT '1',
  `body_servico7_botao3` enum('0','1') DEFAULT '1',
  `body_servico7_botao4` enum('0','1') DEFAULT '1',
  `body_servico7_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico7_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico7_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico7_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico7_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico7_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico7_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico7_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico7`
--

LOCK TABLES `lp_modelosconfig_servico7` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico7` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico7` VALUES (1,1,'1','1','Titulo serviço 7','Descrição serviço 7','1','1','1','1','1','1','1','1','default_servico_7.png','default_servico_7.png','default_servico_7.png','default_servico_7.png','default_servico_7.png','default_servico_7.png','default_servico_7.png','default_servico_7.png','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico8`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico8` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico8` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico8` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo8` varchar(45) DEFAULT 'Titulo serviço 8',
  `body_textoGenericoDescricao8` varchar(512) DEFAULT 'Descrição serviço 8',
  `body_servico8Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem4` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem5` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem6` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem7` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem8` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico8Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico8Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico8Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico8Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico8Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico8Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico8Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico8Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico8Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico8Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico8Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico8Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico8Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico8Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico8Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico8ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico8ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico8ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico8ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico8ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico8ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico8ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico8ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico8_botao1` enum('0','1') DEFAULT '1',
  `body_servico8_botao2` enum('0','1') DEFAULT '1',
  `body_servico8_botao3` enum('0','1') DEFAULT '1',
  `body_servico8_botao4` enum('0','1') DEFAULT '1',
  `body_servico8_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico8_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico8_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico8_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico8_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico8_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico8_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico8_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico8`
--

LOCK TABLES `lp_modelosconfig_servico8` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico8` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico8` VALUES (1,1,'1','1','Titulo serviço 8','Descrição serviço 8','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico_maps`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico_maps` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_maps` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico_maps` enum('0','1') DEFAULT '1',
  `body_maps_textoGenericoTitulo1` varchar(45) DEFAULT 'Titulo serviço MAPS',
  `body_maps_textoGenericoDescricao1` varchar(512) DEFAULT 'Descrição serviço MAPS'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico_maps`
--

LOCK TABLES `lp_modelosconfig_servico_maps` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico_maps` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico_maps` VALUES (1,1,'1','1','Titulo serviço MAPS','Descrição serviço MAPS');
/*!40000 ALTER TABLE `lp_modelosconfig_servico_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico_paginasucesso`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico_paginasucesso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico_paginasucesso` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_textoGenerico_paginaSucesso` varchar(512) DEFAULT 'Que legal, você se cadastrou com sucesso!',
  `body_textoGenericoDescricao_paginaSucesso` varchar(3500) DEFAULT '<p>Ao se cadastrar voc&ecirc; concordou em receber novidades e informativos sobre os nossos produtos e servi&ccedil;os! N&atilde;o esque&ccedil;a de olhar na <span style="color:#c0392b"><u><strong>caixa de spam</strong></u></span>, alguns e-mails costumam cair l&aacute;, fique atento, t&aacute; bom?</p><p>&nbsp;</p><p><img alt="" src="https://i.imgur.com/dTlV0XF.png" style="height:233px; width:350px" /></p>',
  `body_icone_paginaSucesso` varchar(100) DEFAULT ' mdi mdi-checkbox-marked-circle-outline',
  `body_iconeBotao_paginaSucesso` varchar(100) DEFAULT ' mdi mdi-arrow-left-thick',
  `body_textoBotao_paginaSucesso` varchar(45) DEFAULT 'Voltar',
  `body_linkBotao_paginaSucesso` varchar(256) DEFAULT 'https://google.com'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico_paginasucesso`
--

LOCK TABLES `lp_modelosconfig_servico_paginasucesso` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico_paginasucesso` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico_paginasucesso` VALUES (1,1,'Que legal, você se cadastrou com sucesso!','<p>Ao se cadastrar voc&ecirc; concordou em receber novidades e informativos sobre os nossos produtos e servi&ccedil;os! N&atilde;o esque&ccedil;a de olhar na <span style=\"color:#c0392b\"><u><strong>caixa de spam</strong></u></span>, alguns e-mails costumam cair l&aacute;, fique atento, t&aacute; bom?</p><p>&nbsp;</p><p><img alt=\"\" src=\"https://i.imgur.com/dTlV0XF.png\" style=\"height:233px; width:350px\" /></p>',' mdi mdi-checkbox-marked-circle-outline',' mdi mdi-arrow-left-thick','Voltar','https://google.com');
/*!40000 ALTER TABLE `lp_modelosconfig_servico_paginasucesso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relatorios`
--

DROP TABLE IF EXISTS `relatorios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `relatorios` (
  `id` int(11) NOT NULL,
  `nomePessoa` varchar(75) NOT NULL,
  `emailPessoa` varchar(45) NOT NULL,
  `nomeNewsletter` varchar(145) NOT NULL,
  `ipPessoa` varchar(45) DEFAULT NULL,
  `dispositivoPessoa` varchar(145) DEFAULT NULL,
  `navegadorPessoa` varchar(145) DEFAULT NULL,
  `SOPessoa` varchar(145) DEFAULT NULL,
  `modeloDispositivo` varchar(145) DEFAULT NULL,
  `dataHora` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relatorios`
--

LOCK TABLES `relatorios` WRITE;
/*!40000 ALTER TABLE `relatorios` DISABLE KEYS */;
/*!40000 ALTER TABLE `relatorios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates_landing_pages`
--

DROP TABLE IF EXISTS `templates_landing_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `templates_landing_pages` (
  `id_templates` int(11) NOT NULL AUTO_INCREMENT,
  `nomeTemplate_templates` varchar(145) NOT NULL,
  `imagemTemplate_templates` varchar(145) NOT NULL,
  `statusTemplate_templates` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_templates`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates_landing_pages`
--

LOCK TABLES `templates_landing_pages` WRITE;
/*!40000 ALTER TABLE `templates_landing_pages` DISABLE KEYS */;
INSERT INTO `templates_landing_pages` VALUES (1,'Padrão','img_templates_821578965.jpg','1'),(2,'qweqweqwe','img_templates_502070628.jpg','1');
/*!40000 ALTER TABLE `templates_landing_pages` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-05 17:53:18
