-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 192.168.10.10    Database: ispmais_newsletter
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `newsletter_aberturas_email`
--

DROP TABLE IF EXISTS `newsletter_aberturas_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_aberturas_email` (
  `id_aberturas` bigint(14) unsigned NOT NULL,
  `nome_aberturas` varchar(145) DEFAULT NULL,
  `email_aberturas` varchar(256) DEFAULT NULL,
  `aberto_aberturas` int(11) DEFAULT '0',
  `campanha_aberturas` varchar(512) DEFAULT NULL,
  `data_aberturas` varchar(15) DEFAULT NULL,
  `hora_aberturas` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_aberturas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_aberturas_email`
--

LOCK TABLES `newsletter_aberturas_email` WRITE;
/*!40000 ALTER TABLE `newsletter_aberturas_email` DISABLE KEYS */;
INSERT INTO `newsletter_aberturas_email` VALUES (1,NULL,NULL,1,NULL,'20201102','20:28:45'),(2,NULL,NULL,1,NULL,'20201102','20:30:24');
/*!40000 ALTER TABLE `newsletter_aberturas_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_campanhas`
--

DROP TABLE IF EXISTS `newsletter_campanhas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_campanhas` (
  `id_campanha` bigint(15) NOT NULL,
  `nome_campanha` varchar(90) NOT NULL,
  `titulo_campanha` varchar(90) NOT NULL,
  `assunto_campanha` varchar(150) NOT NULL,
  `destino_campanha` text NOT NULL,
  `mensagem_campanha` longblob NOT NULL,
  `statusBotao_campanha` enum('0','1','2') NOT NULL COMMENT '0 = pendente\n1 = em andamento\n2 = enviada',
  `id_templateEmail` int(11) NOT NULL,
  PRIMARY KEY (`id_campanha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_campanhas`
--

LOCK TABLES `newsletter_campanhas` WRITE;
/*!40000 ALTER TABLE `newsletter_campanhas` DISABLE KEYS */;
INSERT INTO `newsletter_campanhas` VALUES (1,'qweqweqwe','qweqweqwe','qweqweqwe','1','<p>asdasdasdqweqweqwe</p>\r\n','2',1);
/*!40000 ALTER TABLE `newsletter_campanhas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_configs_envio`
--

DROP TABLE IF EXISTS `newsletter_configs_envio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_configs_envio` (
  `id_configsEnvios` int(11) NOT NULL AUTO_INCREMENT,
  `host_configsEnvios` varchar(145) DEFAULT 'servidor',
  `usuario_configsEnvios` varchar(145) DEFAULT 'e-mail',
  `senha_configsEnvios` varchar(145) DEFAULT 'senha',
  `porta_configsEnvios` int(5) DEFAULT '465',
  `debug_configsEnvios` enum('0','1','2') DEFAULT '0',
  `SMTPSecure_configsEnvios` varchar(15) DEFAULT 'ssl',
  `SMTPAuth_configsEnvios` varchar(145) DEFAULT 'true',
  `CharSet_configsEnvios` varchar(145) DEFAULT 'UTF-8',
  `Encoding_configsEnvios` varchar(145) DEFAULT 'base64',
  PRIMARY KEY (`id_configsEnvios`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Tabela que contém as configurações da conta que irá enviar os e-mails.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_configs_envio`
--

LOCK TABLES `newsletter_configs_envio` WRITE;
/*!40000 ALTER TABLE `newsletter_configs_envio` DISABLE KEYS */;
INSERT INTO `newsletter_configs_envio` VALUES (1,'mail.agenciasolid.com.br','newsletter@agenciasolid.com.br','123@mudar!@!@',465,'0','ssl','true','UTF-8','base64');
/*!40000 ALTER TABLE `newsletter_configs_envio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_configs_template_email`
--

DROP TABLE IF EXISTS `newsletter_configs_template_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_configs_template_email` (
  `id_configs_template` int(11) NOT NULL AUTO_INCREMENT,
  `linkImagem_configs_template` varchar(512) DEFAULT NULL,
  `linkLogotipo_configs_template` varchar(512) DEFAULT NULL,
  `nomeEmpresa_configs_template` varchar(145) DEFAULT NULL,
  `endereco_configs_template` varchar(145) DEFAULT NULL,
  `dominio_configs_template` varchar(145) DEFAULT NULL,
  `linkDominio_configs_template` varchar(512) DEFAULT NULL,
  `linkFacebook_configs_template` varchar(512) DEFAULT NULL,
  `linkInstagram_configs_template` varchar(512) DEFAULT NULL,
  `linkWhatsApp_configs_template` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id_configs_template`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_configs_template_email`
--

LOCK TABLES `newsletter_configs_template_email` WRITE;
/*!40000 ALTER TABLE `newsletter_configs_template_email` DISABLE KEYS */;
INSERT INTO `newsletter_configs_template_email` VALUES (1,'http://www.crfgo.org.br','https://i.imgur.com/SNLpEMJ.png','CRF-GO | Conselho Regional de Farmácia do Estado de Goiás','Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110','www.crfgo.org.br','http://www.crfgo.org.br','https://www.facebook.com/CRFGO','https://www.instagram.com/crfgo/','http://www.crfgo.org.br');
/*!40000 ALTER TABLE `newsletter_configs_template_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_emails`
--

DROP TABLE IF EXISTS `newsletter_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_emails` (
  `id_emails` int(11) NOT NULL,
  `news_nome` varchar(65) DEFAULT NULL,
  `news_emails` varchar(120) DEFAULT NULL,
  `news_status` enum('0','1') DEFAULT NULL,
  `news_grupoLista` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_emails`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_emails`
--

LOCK TABLES `newsletter_emails` WRITE;
/*!40000 ALTER TABLE `newsletter_emails` DISABLE KEYS */;
INSERT INTO `newsletter_emails` VALUES (1,'AAAAAAAAAAAAA','AAAAAA@GMAIL.COM','1',1),(2,'Agencia Solid','revenda@agenciasolid.com.br','0',1);
/*!40000 ALTER TABLE `newsletter_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_envios`
--

DROP TABLE IF EXISTS `newsletter_envios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_envios` (
  `id_envios` bigint(14) unsigned NOT NULL,
  `id_campanha` int(11) DEFAULT NULL,
  `nome_campanha_envios` varchar(120) DEFAULT NULL,
  `nomePessoa_envios` varchar(90) DEFAULT NULL,
  `mensagem_envios` longblob,
  `assunto_envios` varchar(120) DEFAULT NULL,
  `destino_envios` text,
  `emails_envios` varchar(120) DEFAULT NULL,
  `statusEmails_envios` enum('0','1','2') DEFAULT NULL,
  PRIMARY KEY (`id_envios`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_envios`
--

LOCK TABLES `newsletter_envios` WRITE;
/*!40000 ALTER TABLE `newsletter_envios` DISABLE KEYS */;
INSERT INTO `newsletter_envios` VALUES (20201105000001,1,'qweqweqwe','AAAAAAAAAAAAA','<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        Entice the open with some amazing preheader text. Use a little mystery and get those subscribers to read through...\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"http://www.crfgo.org.br\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"https://i.imgur.com/SNLpEMJ.png\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">AAAAAAAAAAAAA</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\"><p>asdasdasdqweqweqwe</p>\r\n</p>\r\n\r\n<img src=\'http://ispmais.net.br/newsletter/confirmarEmail?nome=AAAAAAAAAAAAA&email=AAAAAA@GMAIL.COM&campanha=qweqweqwe\' width=\'1px\' height=\'1px\'>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"http://ispmais.net.br/newsletter/cancelar_inscricao?email=AAAAAA@GMAIL.COM&campanha=1\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"http://www.crfgo.org.br\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">www.crfgo.org.br</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','qweqweqwe','1','aaaaaa@gmail.com','1'),(20201105000002,1,'qweqweqwe','Agencia Solid','<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        Entice the open with some amazing preheader text. Use a little mystery and get those subscribers to read through...\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"http://www.crfgo.org.br\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"https://i.imgur.com/SNLpEMJ.png\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">Agencia Solid</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\"><p>asdasdasdqweqweqwe</p>\r\n</p>\r\n\r\n<img src=\'http://ispmais.net.br/newsletter/confirmarEmail?nome=Agencia Solid&email=revenda@agenciasolid.com.br&campanha=qweqweqwe\' width=\'1px\' height=\'1px\'>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"http://ispmais.net.br/newsletter/cancelar_inscricao?email=revenda@agenciasolid.com.br&campanha=1\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"http://www.crfgo.org.br\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">www.crfgo.org.br</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','qweqweqwe','1','revenda@agenciasolid.com.br','1');
/*!40000 ALTER TABLE `newsletter_envios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_grupos`
--

DROP TABLE IF EXISTS `newsletter_grupos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_grupos` (
  `id_grupos` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `nome_grupos` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_grupos`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_grupos`
--

LOCK TABLES `newsletter_grupos` WRITE;
/*!40000 ALTER TABLE `newsletter_grupos` DISABLE KEYS */;
INSERT INTO `newsletter_grupos` VALUES (1,'teste'),(3,'wqeqweqwe'),(4,'qweqwe'),(5,'qweqweqwe');
/*!40000 ALTER TABLE `newsletter_grupos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_grupos_x_emails`
--

DROP TABLE IF EXISTS `newsletter_grupos_x_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_grupos_x_emails` (
  `id_grupos` tinyint(2) unsigned NOT NULL,
  `id_emails` bigint(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_grupos_x_emails`
--

LOCK TABLES `newsletter_grupos_x_emails` WRITE;
/*!40000 ALTER TABLE `newsletter_grupos_x_emails` DISABLE KEYS */;
INSERT INTO `newsletter_grupos_x_emails` VALUES (1,1),(1,2);
/*!40000 ALTER TABLE `newsletter_grupos_x_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_template_email`
--

DROP TABLE IF EXISTS `newsletter_template_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_template_email` (
  `id_templateEmail` int(11) NOT NULL AUTO_INCREMENT,
  `nome_templateEmail` varchar(145) DEFAULT NULL,
  `html_templateEmail` longblob,
  `status_templateEmail` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_templateEmail`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_template_email`
--

LOCK TABLES `newsletter_template_email` WRITE;
/*!40000 ALTER TABLE `newsletter_template_email` DISABLE KEYS */;
INSERT INTO `newsletter_template_email` VALUES (1,'Modelo 1','<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        Entice the open with some amazing preheader text. Use a little mystery and get those subscribers to read through...\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"{{linkImagem}}\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"{{linkImagemLogotipo}}\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">{{nomePessoa}}</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\">{{mensagem}}</p>\r\n\r\n<img src=\'{{contadorAbertura}}\' width=\'1px\' height=\'1px\'>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">{{endereco}}</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"{{cancelarInscricao}}\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"{{linkDominio}}\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">{{dominio}}</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','1');
/*!40000 ALTER TABLE `newsletter_template_email` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-05  1:38:04
