CREATE DATABASE  IF NOT EXISTS `ispmais_site` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ispmais_site`;
-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 192.168.10.10    Database: ispmais_site
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `site_bio_canais_atendimento`
--

DROP TABLE IF EXISTS `site_bio_canais_atendimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_bio_canais_atendimento` (
  `id_canal_atendimento` int(11) NOT NULL,
  `nome_canal_atendimento` varchar(90) DEFAULT NULL,
  `url_canal_atendimento` text,
  `icone_canal_atendimento` varchar(80) DEFAULT NULL,
  `tipo_canal_atendimento` enum('1','2','3','4','5','6') DEFAULT NULL,
  `ativo_canal_atendimento` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_canal_atendimento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_bio_canais_atendimento`
--

LOCK TABLES `site_bio_canais_atendimento` WRITE;
/*!40000 ALTER TABLE `site_bio_canais_atendimento` DISABLE KEYS */;
INSERT INTO `site_bio_canais_atendimento` VALUES (1,'WhatsApp','https://api.whatsapp.com/send?phone=5508006068164&text=Ol%C3%A1%20vi%20o%20a%20promo%C3%A7%C3%A3o%20no%20instagram,%20tenho%20interesse%20em%20adquirir%20o%20plano.','mdi mdi-whatsapp','1','1'),(2,'Direct','https://api.whatsapp.com/send?phone=5508006068164&text=Ol%C3%A1%20vi%20o%20a%20promo%C3%A7%C3%A3o%20no%20instagram,%20tenho%20interesse%20em%20adquirir%20o%20plano.','mdi mdi-instagram','2','1'),(3,'Messenger','https://api.whatsapp.com/send?phone=5508006068164&text=Ol%C3%A1%20vi%20o%20a%20promo%C3%A7%C3%A3o%20no%20instagram,%20tenho%20interesse%20em%20adquirir%20o%20plano.','mdi mdi-facebook-messenger','3','1'),(4,'Telegram','https://api.whatsapp.com/send?phone=5508006068164&text=Ol%C3%A1%20vi%20o%20a%20promo%C3%A7%C3%A3o%20no%20instagram,%20tenho%20interesse%20em%20adquirir%20o%20plano.','mdi mdi-telegram','4','1'),(5,'josheffer@gmail.com','josheffer@gmail.com','mdi mdi-email','5','1'),(6,'(62) 9999-9999','6299999999','mdi mdi-phone-classic','6','0');
/*!40000 ALTER TABLE `site_bio_canais_atendimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_bio_cores_estilos`
--

DROP TABLE IF EXISTS `site_bio_cores_estilos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_bio_cores_estilos` (
  `id_cores_estilos` int(11) NOT NULL,
  `corBg_cores_estilos` varchar(10) DEFAULT '#ffffff',
  `corBoxMeio_cores_estilos` varchar(10) DEFAULT '#000000',
  `CorFaixas_cores_estilos` varchar(10) DEFAULT '#b8b8b8',
  `corTextoFaixa_cores_estilos` varchar(10) DEFAULT '#000000',
  `CorTextos_cores_estilos` varchar(10) DEFAULT '#ffffff',
  `corTextosBotoes_cores_estilos` varchar(10) DEFAULT '#000000',
  `corTextosBotoesHover_cores_estilos` varchar(10) DEFAULT '#000000',
  `corBgBotoes_cores_estilos` varchar(10) DEFAULT '#ffffff',
  `corBgBotoesHover_cores_estilos` varchar(10) DEFAULT '#adb5bd',
  `corTextoRodape_cores_estilos` varchar(10) DEFAULT '#000000',
  PRIMARY KEY (`id_cores_estilos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_bio_cores_estilos`
--

LOCK TABLES `site_bio_cores_estilos` WRITE;
/*!40000 ALTER TABLE `site_bio_cores_estilos` DISABLE KEYS */;
INSERT INTO `site_bio_cores_estilos` VALUES (1,'#040d2a','#183eaf','#8e9ecc','#000000','#ffffff','#000000','#ffffff','#ffffff','#020f36','#ffffff');
/*!40000 ALTER TABLE `site_bio_cores_estilos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_bio_infos`
--

DROP TABLE IF EXISTS `site_bio_infos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_bio_infos` (
  `id_infos_bio` int(11) NOT NULL,
  `nome_infos_bio` varchar(45) DEFAULT NULL,
  `descricao_infos_bio` varchar(545) DEFAULT NULL,
  `imagem_infos_bio` varchar(145) DEFAULT 'http://ispmais.net.br/assets/sites/ispmais/img/bio/default.png',
  `anoEmpresa_infos_bio` varchar(10) DEFAULT NULL,
  `textoEmpresa_infos_bio` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`id_infos_bio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_bio_infos`
--

LOCK TABLES `site_bio_infos` WRITE;
/*!40000 ALTER TABLE `site_bio_infos` DISABLE KEYS */;
INSERT INTO `site_bio_infos` VALUES (1,'ISPMAIS Telecom','Fique a vontade e escolha a melhor opção!','img_bio_578885919.png','2011','Todos os direitos reservados ISPMAIS Telecom | www.ispmais.com');
/*!40000 ALTER TABLE `site_bio_infos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_bio_redes_sociais`
--

DROP TABLE IF EXISTS `site_bio_redes_sociais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_bio_redes_sociais` (
  `id_redes_sociais` int(11) NOT NULL,
  `nome_redes_sociais` varchar(60) DEFAULT NULL,
  `url_redes_sociais` text,
  `icone_redes_sociais` varchar(90) DEFAULT NULL,
  `ativo_redes_sociais` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_redes_sociais`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_bio_redes_sociais`
--

LOCK TABLES `site_bio_redes_sociais` WRITE;
/*!40000 ALTER TABLE `site_bio_redes_sociais` DISABLE KEYS */;
INSERT INTO `site_bio_redes_sociais` VALUES (1,'asdasd','qweqwe','mdi mdi-instagram','1'),(2,'asdasd','qweqwe','mdi mdi-youtube','1'),(3,'adasdqweqw','asdasd','mdi mdi-facebook-box','1');
/*!40000 ALTER TABLE `site_bio_redes_sociais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_bio_sites`
--

DROP TABLE IF EXISTS `site_bio_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_bio_sites` (
  `id_sites_bio` int(11) NOT NULL,
  `nome_sites_bio` varchar(145) DEFAULT NULL,
  `url_sites_bio` text,
  `status_sites_bio` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`id_sites_bio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_bio_sites`
--

LOCK TABLES `site_bio_sites` WRITE;
/*!40000 ALTER TABLE `site_bio_sites` DISABLE KEYS */;
INSERT INTO `site_bio_sites` VALUES (1,'Ourilândia','https://ispmais.com/site/ourilandia_do_norte','1'),(2,'Tucumã','www.facebook.com','1'),(3,'São Felix','www.twitter.com','1'),(4,'Escolher região','https://ispmais.com','1');
/*!40000 ALTER TABLE `site_bio_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_intro_seo`
--

DROP TABLE IF EXISTS `site_intro_seo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_intro_seo` (
  `id_intro_seo` int(11) NOT NULL,
  `intro_seo_tituloSite` text,
  `intro_seo_nomeSite` text,
  `intro_seo_corTema` varchar(10) DEFAULT NULL,
  `intro_seo_css` longtext,
  `intro_seo_js` longtext,
  `intro_seo_js_body` longtext,
  `intro_seo_keywords` text,
  `intro_seo_description1` text,
  `intro_seo_description2` text,
  `intro_seo_description3` text,
  `intro_seo_coordenadasMaps` varchar(80) DEFAULT NULL,
  `intro_seo_cidade` varchar(80) DEFAULT NULL,
  `intro_seo_estado` varchar(80) DEFAULT NULL,
  `intro_seo_googleVerificacao` text,
  `intro_seo_imagem` varchar(145) DEFAULT 'logo_seo_intro_default.png',
  PRIMARY KEY (`id_intro_seo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_intro_seo`
--

LOCK TABLES `site_intro_seo` WRITE;
/*!40000 ALTER TABLE `site_intro_seo` DISABLE KEYS */;
INSERT INTO `site_intro_seo` VALUES (1,'ISPMAIS Telecom | Soluções Completas em Internet para sua Casa e Empresa. Alta Velocidade e Disponibilidade. Qualidade e Economia. Atendimento Diferenciado.','ISPMAIS Telecom | Soluções Completas em Internet para sua Casa e Empresa. Alta Velocidade e Disponibilidade. Qualidade e Economia. Atendimento Diferenciado.','#ffffff','.bg-orange\r\n		{\r\n			background-color: #ff722d !important;\r\n		}\r\n\r\n		.text-orange\r\n		{\r\n			color: #ff722d !important;\r\n		}','<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-35963898-6\"></script>\r\n<script>\r\n        window.dataLayer = window.dataLayer || [];\r\n        function gtag(){dataLayer.push(arguments);}\r\n        gtag(\'js\', new Date());\r\n\r\n        gtag(\'config\', \'UA-35963898-6\');\r\n    </script>','','Internet banda larga em Tucumã,  Internet banda larga em Ourilândia do Norte, Internet banda larga em São Felix do Xingú,  Fibra Optica em Tucumã, Fibra Optica em Ourilândia do Norte, Fibra Optica em São Felix do Xingú, Banda larga em Tucumã, Banda larga em Ourilândia do Norte, Banda larga em São Felix do Xingú, fibra optica, banda larga, internet rápida, internet com ultra velocidade, instalação de internet em Tucumã, instalação de internet em Ourilândia do Norte, instalação de internet em São Felix do Xingú','Soluções Completas em Internet para sua Casa e Empresa. Alta Velocidade e Disponibilidade. Qualidade e Economia. Atendimento Diferenciado.','Soluções Completas em Internet para sua Casa e Empresa. Alta Velocidade e Disponibilidade. Qualidade e Economia. Atendimento Diferenciado.','Soluções Completas em Internet para sua Casa e Empresa. Alta Velocidade e Disponibilidade. Qualidade e Economia. Atendimento Diferenciado.','-6.751993, -51.076262','Ourilândia do Norte - PA','PA-BR','_08_IqvqVfzFuUcA9HOC9wEg31k7y1AN7-CiDqUgxog','img_seo_2115074428.png');
/*!40000 ALTER TABLE `site_intro_seo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_ourilandia_avisoglobal`
--

DROP TABLE IF EXISTS `site_ourilandia_avisoglobal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_ourilandia_avisoglobal` (
  `id_aviso` int(11) NOT NULL,
  `texto_aviso` text,
  `ativo_aviso` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_aviso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_ourilandia_avisoglobal`
--

LOCK TABLES `site_ourilandia_avisoglobal` WRITE;
/*!40000 ALTER TABLE `site_ourilandia_avisoglobal` DISABLE KEYS */;
INSERT INTO `site_ourilandia_avisoglobal` VALUES (0,'qwwwwwwwww','0'),(1,'Com os acontecimentos do Covid-19 estaremos atendendo somente que estiver com o uso da máscara no local da instalação!','1');
/*!40000 ALTER TABLE `site_ourilandia_avisoglobal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_ourilandia_barra_superior_fixa`
--

DROP TABLE IF EXISTS `site_ourilandia_barra_superior_fixa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_ourilandia_barra_superior_fixa` (
  `id_barraSuperiorFixa` int(11) NOT NULL,
  `mensagem_barraSuperiorFixa` text,
  `email_barraSuperiorFixa` varchar(145) DEFAULT NULL,
  `telefone_barraSuperiorFixa` varchar(45) DEFAULT NULL,
  `facebook_barraSuperiorFixa` varchar(145) DEFAULT NULL,
  `instagram_barraSuperiorFixa` varchar(145) DEFAULT NULL,
  `ativo_facebook_barraSuperiorFixa` enum('0','1') DEFAULT NULL,
  `ativo_instagram_barraSuperiorFixa` enum('0','1') DEFAULT NULL,
  `ativo_minhaConta_barraSuperiorFixa` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_barraSuperiorFixa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_ourilandia_barra_superior_fixa`
--

LOCK TABLES `site_ourilandia_barra_superior_fixa` WRITE;
/*!40000 ALTER TABLE `site_ourilandia_barra_superior_fixa` DISABLE KEYS */;
INSERT INTO `site_ourilandia_barra_superior_fixa` VALUES (1,'Seja bem vindo (a) ao nosso site! ','ourilandiaaaa@ispmais.net.br','(62) 9 9999-9999','https://www.facebook.com/ispmais','https://www.instagram.com/ispmais','1','1','1');
/*!40000 ALTER TABLE `site_ourilandia_barra_superior_fixa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_ourilandia_carousel`
--

DROP TABLE IF EXISTS `site_ourilandia_carousel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_ourilandia_carousel` (
  `id_carousel` int(11) NOT NULL,
  `descricao_carousel` text,
  `imagem_carousel` varchar(145) DEFAULT NULL,
  `ativo_carousel` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_carousel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_ourilandia_carousel`
--

LOCK TABLES `site_ourilandia_carousel` WRITE;
/*!40000 ALTER TABLE `site_ourilandia_carousel` DISABLE KEYS */;
INSERT INTO `site_ourilandia_carousel` VALUES (1,'Tecnologia de ponta em Ourilândia do Norte, ISPMAIS Telecom','img_slide_1762228655.jpg','1'),(2,'Os melhores planos de internet banda larga em Ourilândia do Norte','img_slide_731429473.jpg','1'),(3,'Tecnologia do Futuro ao seu alcance, ISPMAIS em Ourilândia do Norte!','img_slide_496921225.jpg','1');
/*!40000 ALTER TABLE `site_ourilandia_carousel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_ourilandia_cobertura_tendimento`
--

DROP TABLE IF EXISTS `site_ourilandia_cobertura_tendimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_ourilandia_cobertura_tendimento` (
  `id_cobertura` int(11) NOT NULL,
  `ativo_cobertura` enum('0','1') DEFAULT NULL,
  `titulo_cobertura` varchar(145) DEFAULT NULL,
  `descricao_cobertura` varchar(245) DEFAULT NULL,
  `textoBotao1_cobertura` varchar(65) DEFAULT NULL,
  `linkBotao1_cobertura` varchar(512) DEFAULT NULL,
  `textoBotao2_cobertura` varchar(65) DEFAULT NULL,
  `ativoBotao1_cobertura` enum('0','1') DEFAULT NULL,
  `ativoBotao2_cobertura` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_cobertura`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_ourilandia_cobertura_tendimento`
--

LOCK TABLES `site_ourilandia_cobertura_tendimento` WRITE;
/*!40000 ALTER TABLE `site_ourilandia_cobertura_tendimento` DISABLE KEYS */;
INSERT INTO `site_ourilandia_cobertura_tendimento` VALUES (1,'1','A sua região em Ourilândia do Norte tem cobertura? ','Entre em contato para mais informações.','(94) 9 9102-8571','https://api.whatsapp.com/send?phone=5594993040097&text=Desejo%20saber%20mais%20sobre%20os%20planos%20residenciais','Ligaremos para você!!!','1','1');
/*!40000 ALTER TABLE `site_ourilandia_cobertura_tendimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_ourilandia_links_barra_superior_fixa`
--

DROP TABLE IF EXISTS `site_ourilandia_links_barra_superior_fixa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_ourilandia_links_barra_superior_fixa` (
  `id_links_barraSuperiorFixa` int(11) NOT NULL,
  `nome_links_barraSuperiorFixa` varchar(145) DEFAULT NULL,
  `link_links_barraSuperiorFixa` text,
  `status_links_barraSuperiorFixa` enum('0','1') DEFAULT NULL,
  `target_links_barraSuperiorFixa` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`id_links_barraSuperiorFixa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_ourilandia_links_barra_superior_fixa`
--

LOCK TABLES `site_ourilandia_links_barra_superior_fixa` WRITE;
/*!40000 ALTER TABLE `site_ourilandia_links_barra_superior_fixa` DISABLE KEYS */;
INSERT INTO `site_ourilandia_links_barra_superior_fixa` VALUES (1,'Home','/site/ourilandia_do_norte','1','0'),(2,'2° via do boleto','javascript:void(0);','1','0'),(3,'Teste de velocidade','http://dualstack.speedtestcustom.com','1','1'),(4,'Trabalhe conosco','javascript:void(0) ;','1','0'),(5,'Link 1sss','link1','1','0'),(6,'Link 2','link2','1','0');
/*!40000 ALTER TABLE `site_ourilandia_links_barra_superior_fixa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_ourilandia_newsletter`
--

DROP TABLE IF EXISTS `site_ourilandia_newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_ourilandia_newsletter` (
  `id_newsletter` int(11) NOT NULL,
  `nomeCliente_newsletter` varchar(90) DEFAULT NULL,
  `telefoneCliente_newsletter` varchar(18) DEFAULT NULL,
  `emailCliente_newsletter` varchar(120) DEFAULT NULL,
  `data_newsletter` varchar(10) DEFAULT NULL,
  `hora_newsletter` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`id_newsletter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_ourilandia_newsletter`
--

LOCK TABLES `site_ourilandia_newsletter` WRITE;
/*!40000 ALTER TABLE `site_ourilandia_newsletter` DISABLE KEYS */;
INSERT INTO `site_ourilandia_newsletter` VALUES (1,'Josheffer Robert','(62) 9 9399-872','josheffer@gmail.com','20200701','15:37'),(2,'asdssss','(13) 1 2312-312','josheffers@gmail.com','20200701','15:47'),(3,'qweqwe1223','(12) 3 1231-231','12asdasd13123123@gmail.com','20200701','15:48'),(4,'12asdasd123','(12) 3 1123-123','sdasd321@gmail.com','20200701','15:48'),(5,'sasdasd','(31) 2 3123-312','jojo@gmail.com','20200701','15:48'),(6,'asd123123sdasd','(12) 3 1231-233','asdiasido@gmail.com','20200701','15:48'),(7,'adsdqweqwe','(13) 2 1231-321','asdasd123@gmail.com','20200701','15:48'),(8,'asdasdqw','(12) 3 1231-231','asdasd@hotmail.com','20200701','15:54'),(9,'adweqwe','(12) 3 1231-231','sdasdsd@hotmail.com','20200701','15:54'),(10,'cadasdqweqw','(12) 3 1231-231','asdfasdf@hotmail.com','20200701','15:54'),(11,'1323123','(13) 1 2312-312','asdasdsa@hotmail.com','20200701','15:54'),(12,'qweqweasdasd','(12) 3 1231-233','asdasdas@hotmail.com','20200701','15:54'),(13,'asdu89us','(89) 9 3008-098','aksdhkashdkajsdh@hotmail.com','20200701','15:54'),(14,'asdasdasdasd','(12) 2 1111-112','asdasd@gmail.com','20200701','15:59'),(15,'asdqweqwe','(66) 6 6666-666','asdjaaskdj@bol.com','20200701','15:59'),(16,'eqweqwe1212','(12) 2 2222-222','qweqweqw@bol.com','20200701','15:59'),(17,'eqweqweq','(12) 3 2312-312','sdasdasd@bol.com','20200701','15:59'),(18,'ASDASDAS','(12) 3 1231-231','joshefferssss@gmail.com','20200701','16:02'),(19,'sdwerwer','(12) 3 1231-231','3asdasdasdasd','20200701','20:36'),(20,'asdasd','(12) 3 1231-231','asdasdss@gmail.com','20200701','20:43'),(21,'Incidunt est sed do','(12) 3 1231-231','gibuqyxa@mailinator.com','20200804','17:15');
/*!40000 ALTER TABLE `site_ourilandia_newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_ourilandia_pagina_sobre`
--

DROP TABLE IF EXISTS `site_ourilandia_pagina_sobre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_ourilandia_pagina_sobre` (
  `id_sobre` int(11) NOT NULL,
  `titulo_sobre` varchar(500) DEFAULT 'Titulo da página',
  `descricao_sobre` varchar(1500) DEFAULT 'Descrição da página',
  `imagem_sobre` varchar(80) DEFAULT 'logo_default.png',
  `texto_sobre` varchar(2500) DEFAULT 'Texto sobre a empresa',
  `missao_sobre` varchar(500) DEFAULT 'Missão da empresa',
  `visao_sobre` varchar(500) DEFAULT 'Visão da empresa',
  `valores_sobre` varchar(500) DEFAULT 'Valores da empresa',
  `nomeEmpresa_sobre` varchar(200) DEFAULT 'Nome da empresa',
  PRIMARY KEY (`id_sobre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_ourilandia_pagina_sobre`
--

LOCK TABLES `site_ourilandia_pagina_sobre` WRITE;
/*!40000 ALTER TABLE `site_ourilandia_pagina_sobre` DISABLE KEYS */;
INSERT INTO `site_ourilandia_pagina_sobre` VALUES (1,'Sobre ISPMAIS Telecom','Um pouco da nossa empresa!','img_sobre_1635840503.jpg','Fundada em 2011 como provedora de internet para o mercado Paraense, a ISPMAIS Telecom nasceu com o objetivo de oferecer qualidade de suporte e cobertura para seus clientes, além de um atendimento diferenciado. Oferecemos agilidade, prontidão, conexões seguras e um conceito mais amplo de conectividade. A ISPMAIS Telecom traz consigo um novo conceito em conectividade entre o cliente e a empresa. E fácil conectar-se com a ISPMAIS Telecom para orientações, solucionar problemas e tirar dúvidas. A empresa está periodicamente em contato com seus clientes a fim de obter feedback e opiniões para melhoria nos serviços e produtos. O objetivo é garantir que além da conexão, o cliente terá conectividade com seus objetivos pessoais e profissionais, não importando o tempo, nem a distância. A ISPMAIS Telecom oferece conectividade a seus clientes, conectando soluções.','Fornecer serviços de acesso à internet e nas suas mais atualizadas tecnologias, oferecendo qualidade.','Ser reconhecida no mercado como uma das melhores empresas no setor de Telecomunicações e atender as necessidades de cada cliente, de forma a obter a satisfação dos mesmos.','Ética, Aperfeiçoamento profissional, Comprometimento com os clientes, Compromisso com os resultados, Busca constante pela qualidade','A ISPMAIS Telecom');
/*!40000 ALTER TABLE `site_ourilandia_pagina_sobre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_ourilandia_planos_empresariais`
--

DROP TABLE IF EXISTS `site_ourilandia_planos_empresariais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_ourilandia_planos_empresariais` (
  `id_plano_empresarial` int(11) NOT NULL,
  `nome_plano_empresarial` varchar(45) DEFAULT NULL,
  `descricao_plano_empresarial` varchar(512) DEFAULT NULL,
  `velocidade_plano_empresarial` varchar(5) DEFAULT NULL,
  `tipoVelocidade_plano_empresarial` varchar(5) DEFAULT NULL,
  `preco_plano_empresarial` decimal(10,2) DEFAULT NULL,
  `destaque_plano_empresarial` enum('0','1') DEFAULT NULL,
  `ativo_plano_empresarial` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_plano_empresarial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_ourilandia_planos_empresariais`
--

LOCK TABLES `site_ourilandia_planos_empresariais` WRITE;
/*!40000 ALTER TABLE `site_ourilandia_planos_empresariais` DISABLE KEYS */;
INSERT INTO `site_ourilandia_planos_empresariais` VALUES (1,'Empresarial 1','Empresarial 1','100','MB',59.90,'0','1'),(2,'Empresarial 2','Empresarial 2','150','MB',89.90,'0','1'),(3,'Empresarial 3','Empresarial 3','200','MB',109.90,'0','1'),(4,'Empresarial 4','Empresarial 4','250','MB',149.90,'1','1'),(5,'asdas','dasdasd','200','MB',12.00,'0','1'),(6,'asdasd','asdasd','800','MB',500.50,'0','1');
/*!40000 ALTER TABLE `site_ourilandia_planos_empresariais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_ourilandia_planos_residenciais`
--

DROP TABLE IF EXISTS `site_ourilandia_planos_residenciais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_ourilandia_planos_residenciais` (
  `id_plano_residencial` int(11) NOT NULL,
  `nome_plano_residencial` varchar(45) DEFAULT NULL,
  `descricao_plano_residencial` varchar(512) DEFAULT NULL,
  `velocidade_plano_residencial` varchar(5) DEFAULT NULL,
  `tipoVelocidade_plano_residencial` varchar(5) DEFAULT NULL,
  `preco_plano_residencial` varchar(10) DEFAULT NULL,
  `destaque_plano_residencial` enum('0','1') DEFAULT NULL,
  `ativo_plano_residencial` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_plano_residencial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_ourilandia_planos_residenciais`
--

LOCK TABLES `site_ourilandia_planos_residenciais` WRITE;
/*!40000 ALTER TABLE `site_ourilandia_planos_residenciais` DISABLE KEYS */;
INSERT INTO `site_ourilandia_planos_residenciais` VALUES (1,'Plano Residencia 1','Plano 1','20','MB','29.90','0','1'),(2,'Plano Residencia 2','Plano 2','50','MB','59.90','1','1'),(3,'Plano Residencia 3','Plano 3','80','MB','89.90','0','1'),(4,'Plano Residencia 4','Plano 4','110','MB','119.90','0','1');
/*!40000 ALTER TABLE `site_ourilandia_planos_residenciais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_ourilandia_seo`
--

DROP TABLE IF EXISTS `site_ourilandia_seo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_ourilandia_seo` (
  `id_seo` int(11) NOT NULL,
  `seo_tituloSite` text,
  `seo_nomeSite` text,
  `seo_corTema` varchar(10) DEFAULT NULL,
  `seo_css` longtext,
  `seo_js` longtext,
  `seo_js_body` longtext,
  `seo_keywords` text,
  `seo_description1` text,
  `seo_description2` text,
  `seo_description3` text,
  `seo_coordenadasMaps` varchar(80) DEFAULT NULL,
  `seo_cidade` varchar(80) DEFAULT NULL,
  `seo_estado` varchar(80) DEFAULT NULL,
  `seo_googleVerificacao` text,
  `seo_imagem` varchar(145) DEFAULT 'logo_default.png',
  PRIMARY KEY (`id_seo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_ourilandia_seo`
--

LOCK TABLES `site_ourilandia_seo` WRITE;
/*!40000 ALTER TABLE `site_ourilandia_seo` DISABLE KEYS */;
INSERT INTO `site_ourilandia_seo` VALUES (1,'ISPMAIS Telecon | Internet com fibra óptica com a melhor velocidade de Ourilândia do Norte!','Internet banda larga via fibra optica e radio (com wifi) de qualidade e velocidade super rápida em Ourilândia do Norte. Os melhores planos e preços são com a ISPMAIS Telecom.','#ff722d','.input-form-ligar \r\n        {   \r\n            color: #ffffff;\r\n            font-size: 13px; \r\n            background: #1c2746;\r\n            border: 0; \r\n            order-radius: 5px; \r\n            box-shadow: inset 0 0 0; \r\n            height: 47px; \r\n            transition: .3s linear; \r\n            margin: 10px 0;\r\n        }','<!-- Global site tag (gtag.js) - Google Analytics -->\r\n<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-35963898-6\"></script>\r\n\r\n<script>\r\n        window.dataLayer = window.dataLayer || [];\r\n        function gtag(){dataLayer.push(arguments);}\r\n        gtag(\'js\', new Date());\r\n\r\n        gtag(\'config\', \'UA-35963898-6\');\r\n    </script>','','Internet Ourilândia do Norte, Internet banda larga em Ourilândia do Norte, banda larga em Ourilândia do Norte, provedor em Ourilândia do Norte, internet wifi em Ourilândia do Norte, planos de internet em Ourilândia do Norte, internet fibra optica em Ourilândia do Norte, internet para empresas em Ourilândia do Norte, internet empresarial em Ourilândia do Norte, internet residencial em Ourilândia do Norte, internet rápida em Ourilândia do Norte, Ourilândia do Norte, ultra velocidade de internet em Ourilândia do Norte, banda larga ultra rápida em Ourilândia do Norte, NET em Ourilândia do Norte, Fibra optica em Ourilândia do Norte','Soluções Completas em Internet para sua Casa e Empresa em Ourilândia do Norte (94) 3434-2444, (94) 9 9304-009 . Alta Velocidade e Disponibilidade. Qualidade e Economia. Atendimento Diferenciado.','Soluções Completas em Internet para sua Casa e Empresa em Ourilândia do Norte (94) 3434-2444, (94) 9 9304-009 . Alta Velocidade e Disponibilidade. Qualidade e Economia. Atendimento Diferenciado.','Soluções Completas em Internet para sua Casa e Empresa em Ourilândia do Norte (94) 3434-2444, (94) 9 9304-009 . Alta Velocidade e Disponibilidade. Qualidade e Economia. Atendimento Diferenciado.','-6.752005, -51.076337','Ourilândia do Norte, Pará','PA-BR','asd','img_seo_1162356640.png');
/*!40000 ALTER TABLE `site_ourilandia_seo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_ourilandia_solicitacoes_ligacoes`
--

DROP TABLE IF EXISTS `site_ourilandia_solicitacoes_ligacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_ourilandia_solicitacoes_ligacoes` (
  `id_solicitacoes_ligacoes` int(11) NOT NULL,
  `nome_solicitacoes_ligacoes` varchar(90) DEFAULT NULL,
  `telefone_solicitacoes_ligacoes` varchar(15) DEFAULT NULL,
  `email_solicitacoes_ligacoes` varchar(120) DEFAULT NULL,
  `plano_solicitacoes_ligacoes` varchar(90) DEFAULT NULL,
  `valorPlano_solicitacoes_ligacoes` decimal(10,2) DEFAULT NULL,
  `cepEndereco_solicitacoes_ligacoes` varchar(11) DEFAULT NULL,
  `ruaEndereco_solicitacoes_ligacoes` varchar(145) DEFAULT NULL,
  `complementoEndereco_solicitacoes_ligacoes` varchar(145) DEFAULT NULL,
  `numeroEndereco_solicitacoes_ligacoes` varchar(10) DEFAULT NULL,
  `bairroEndereco_solicitacoes_ligacoes` varchar(90) DEFAULT NULL,
  `cidadeEndereco_solicitacoes_ligacoes` varchar(90) DEFAULT NULL,
  `estadoEndereco_solicitacoes_ligacoes` varchar(3) DEFAULT NULL,
  `observacoes_solicitacoes_ligacoes` longtext,
  `operador_solicitacoes_ligacoes` varchar(45) DEFAULT NULL,
  `data_solicitacoes_ligacoes` varchar(10) DEFAULT NULL,
  `hora_solicitacoes_ligacoes` varchar(6) DEFAULT NULL,
  `status_solicitacoes_ligacoes` enum('0','1','2','3','4') DEFAULT NULL COMMENT '0=pendente\\n1=atendimento\\n2=não atendida\\n3=venda realizada\\n4=venda não realizada',
  PRIMARY KEY (`id_solicitacoes_ligacoes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_ourilandia_solicitacoes_ligacoes`
--

LOCK TABLES `site_ourilandia_solicitacoes_ligacoes` WRITE;
/*!40000 ALTER TABLE `site_ourilandia_solicitacoes_ligacoes` DISABLE KEYS */;
INSERT INTO `site_ourilandia_solicitacoes_ligacoes` VALUES (1,'asdasd','(66) 6 6666-666','66666@gmail.com','Plano Residencia 4',119.90,'74.555-270','Rua 15','sdsdfs','df7','Vila Santa Helena','Goiânia','GO','asdasdasd','1','20200801','15:09','3'),(2,'asdas','(12) 3 1231-231','asdsa@gmail.com','Plano Residencia 1',29.90,'74.555-270','Rua 15','asdas','12312','Vila Santa Helena','Goiânia','GO','asdasdasd','1','20200801','15:35','2'),(3,'asdasdas','(12) 3 1231-231','3123sdas@gmai.com','Plano Residencia 2',59.90,'74.555-270','Rua 15','asdasdasd','sn','Vila Santa Helena','Goiânia','GO','sdasdasd','1','20201103','16:06','1'),(4,'asdasdas','(32) 1 2132-132','231sda32s1d32a1@gmail.com','Plano Residencia 3',89.90,'74.555-250','Rua 13','asdasd','sn','Vila Santa Helena','Goiânia','GO','asdasdasd','1','20201103','16:06','1');
/*!40000 ALTER TABLE `site_ourilandia_solicitacoes_ligacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_ourilandia_solicitacoes_planoEmpresarial`
--

DROP TABLE IF EXISTS `site_ourilandia_solicitacoes_planoEmpresarial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_ourilandia_solicitacoes_planoEmpresarial` (
  `id_solicitacoes_planoEmpresarial` int(11) NOT NULL,
  `nome_solicitacoes_planoEmpresarial` varchar(90) DEFAULT NULL,
  `telefone_solicitacoes_planoEmpresarial` varchar(15) DEFAULT NULL,
  `telefoneFixo_solicitacoes_planoEmpresarial` varchar(15) DEFAULT NULL,
  `email_solicitacoes_planoEmpresarial` varchar(120) DEFAULT NULL,
  `cnpj_solicitacoes_planoEmpresarial` varchar(15) DEFAULT NULL,
  `nomeFantasia_planoEmpresarial` varchar(145) DEFAULT NULL,
  `cep_solicitacoes_planoEmpresarial` varchar(11) DEFAULT NULL,
  `rua_solicitacoes_planoEmpresarial` varchar(145) DEFAULT NULL,
  `complemento_solicitacoes_planoEmpresarial` varchar(145) DEFAULT NULL,
  `numero_solicitacoes_planoEmpresarial` varchar(10) DEFAULT NULL,
  `bairro_solicitacoes_planoEmpresarial` varchar(90) DEFAULT NULL,
  `cidade_solicitacoes_planoEmpresarial` varchar(90) DEFAULT NULL,
  `estado_solicitacoes_planoEmpresarial` varchar(3) DEFAULT NULL,
  `observacao_solicitacoes_planoEmpresarial` longtext,
  `nomePlano_solicitacoes_planoEmpresarial` varchar(45) DEFAULT NULL,
  `valorPlano_solicitacoes_planoEmpresarial` decimal(10,2) DEFAULT NULL,
  `velocidadePlano_solicitacoes_planoEmpresarial` varchar(10) DEFAULT NULL,
  `operador_solicitacoes_planoEmpresarial` varchar(45) DEFAULT NULL,
  `status_solicitacoes_planoEmpresarial` enum('0','1','2','3','4') DEFAULT NULL,
  `data_solicitacoes_planoEmpresarial` varchar(10) DEFAULT NULL,
  `hora_solicitacoes_planoEmpresarial` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`id_solicitacoes_planoEmpresarial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_ourilandia_solicitacoes_planoEmpresarial`
--

LOCK TABLES `site_ourilandia_solicitacoes_planoEmpresarial` WRITE;
/*!40000 ALTER TABLE `site_ourilandia_solicitacoes_planoEmpresarial` DISABLE KEYS */;
INSERT INTO `site_ourilandia_solicitacoes_planoEmpresarial` VALUES (1,'Josheffer','(77) 7 7777-777','(77) 7777-7777','josjo@gmail.com','75.371.545/115','asdasd','74555270','Rua 15','asdasda','123','Vila Santa Helena','Goiânia','GO','asdasd','Empresarial 3',109.90,'200MB','2','3','20200717','11:59'),(2,'Josheffer','(77) 7 7777-777','(77) 7777-7777','josjo@gmail.com','75.371.545/115','asdasd','74555270','Rua 15','asdasda','123','Vila Santa Helena','Goiânia','GO','asdasd','Empresarial 3',109.90,'200MB','1','3','20200717','11:59'),(3,'sdasdasdasdasdasd','(13) 2 1321-321','(23) 1231-3213','asd1@gmail.com','75.154.545/1511','asd4a65s4da6s4','74555270','Rua 15','asdasdasd','sn','Vila Santa Helena','Goiânia','GO','asdasdasdasd','Empresarial 2',89.90,'150MB','1','1','20201104','09:08');
/*!40000 ALTER TABLE `site_ourilandia_solicitacoes_planoEmpresarial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_ourilandia_solicitacoes_planoResidencial`
--

DROP TABLE IF EXISTS `site_ourilandia_solicitacoes_planoResidencial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_ourilandia_solicitacoes_planoResidencial` (
  `id_solicitacoes_planoResidencial` int(11) NOT NULL,
  `nome_solicitacoes_planoResidencial` varchar(90) DEFAULT NULL,
  `telefone_solicitacoes_planoResidencial` varchar(15) DEFAULT NULL,
  `email_solicitacoes_planoResidencial` varchar(120) DEFAULT NULL,
  `cpf_solicitacoes_planoResidencial` varchar(15) DEFAULT NULL,
  `cep_solicitacoes_planoResidencial` varchar(11) DEFAULT NULL,
  `rua_solicitacoes_planoResidencial` varchar(145) DEFAULT NULL,
  `complemento_solicitacoes_planoResidencial` varchar(145) DEFAULT NULL,
  `numero_solicitacoes_planoResidencial` varchar(10) DEFAULT NULL,
  `bairro_solicitacoes_planoResidencial` varchar(90) DEFAULT NULL,
  `cidade_solicitacoes_planoResidencial` varchar(90) DEFAULT NULL,
  `estado_solicitacoes_planoResidencial` varchar(3) DEFAULT NULL,
  `observacao_solicitacoes_planoResidencial` longtext,
  `nomePlano_solicitacoes_planoResidencial` varchar(45) DEFAULT NULL,
  `valorPlano_solicitacoes_planoResidencial` decimal(10,2) DEFAULT NULL,
  `velocidadePlano_solicitacoes_planoResidencial` varchar(10) DEFAULT NULL,
  `operador_solicitacoes_planoResidencial` varchar(45) DEFAULT NULL,
  `status_solicitacoes_planoResidencial` enum('0','1','2','3','4') DEFAULT NULL,
  `data_solicitacoes_planoResidencial` varchar(10) DEFAULT NULL,
  `hora_solicitacoes_planoResidencial` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`id_solicitacoes_planoResidencial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_ourilandia_solicitacoes_planoResidencial`
--

LOCK TABLES `site_ourilandia_solicitacoes_planoResidencial` WRITE;
/*!40000 ALTER TABLE `site_ourilandia_solicitacoes_planoResidencial` DISABLE KEYS */;
INSERT INTO `site_ourilandia_solicitacoes_planoResidencial` VALUES (1,'asdlkjasldkj','(55) 5 5555-555','asdkljas@gmail.com','222.222.222-22','74.555-270','Rua 15','askjdhajksh','askjdh','Vila Santa Helena','Goiânia','GO','askjdhasjkhd','Plano Residencia 3',89.90,'80MB','1','3','20200717','12:09'),(2,'asdasdasd','(33) 3 3333-333','asdasds@gmail.com','750.000.000-00','74.555-270','Rua 15','ssdasdasd','sn','Vila Santa Helena','Goiânia','GO','asdasdasd','Plano Residencia 3',89.90,'80MB','1','1','20201104','09:06');
/*!40000 ALTER TABLE `site_ourilandia_solicitacoes_planoResidencial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_regioes`
--

DROP TABLE IF EXISTS `site_regioes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_regioes` (
  `id_regioes` int(11) NOT NULL,
  `nome_regioes` varchar(145) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_regioes` text COLLATE utf8_unicode_ci,
  `status_regioes` enum('0','1') COLLATE utf8_unicode_ci DEFAULT NULL,
  `endereco_regioes` varchar(145) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_regioes` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foneFixoTexto_regioes` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foneWhatsappTexto_regioes` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkWhatsapp_regioes` varchar(600) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativoSite_regioes` enum('0','1') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_regioes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_regioes`
--

LOCK TABLES `site_regioes` WRITE;
/*!40000 ALTER TABLE `site_regioes` DISABLE KEYS */;
INSERT INTO `site_regioes` VALUES (1,'São Felix do Xingú','site/sao_felix_do_xingu','0','Av. Rio Xingu, 12A Sala A – Centro','xingu@ispmais.com','(94) 9 8104-275','(94) 9 8193-122','https://api.whatsapp.com/send?phone=5594981931224&text=Ol%C3%A1%2C%20sou%20da%20Regi%C3%A3o%20de%20*S%C3%A3o%20Felix%20do%20Xing%C3%BA*%2C%20vi%20o%20contato%20no%20site%20e%20quero%20mais%20informa%C3%A7%C3%B5es!!','1'),(2,'Ourilândia do Norte','site/ourilandia_do_norte','1','Av. das Nações, 2235 – Centro','contato@ispmais.com','(94) 3434-2444','(94) 9 9304-009','https://api.whatsapp.com/send?phone=5594993040097&text=Ol%C3%A1%2C%20sou%20da%20Regi%C3%A3o%20de%20*Ouril%C3%A2ndia%20do%20Norte*%2C%20vi%20o%20contato%20no%20site%20e%20quero%20mais%20informa%C3%A7%C3%B5es!!','1'),(3,'Tucumã','site/tucuma','1','Rua Canavial','contato@ispmais.com','(94) 9 9102-857','(94) 9 9102-857','https://api.whatsapp.com/send?phone=5594991028571&text=Ol%C3%A1%2C%20sou%20da%20Regi%C3%A3o%20de%20*Tucum%C3%A3*%2C%20vi%20o%20contato%20no%20site%20e%20quero%20mais%20informa%C3%A7%C3%B5es!!','1');
/*!40000 ALTER TABLE `site_regioes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_visitas_intro`
--

DROP TABLE IF EXISTS `site_visitas_intro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_visitas_intro` (
  `id_visita` int(11) NOT NULL,
  `ip_visita` varchar(45) DEFAULT NULL,
  `pagina_visita` text,
  `pais_visita` varchar(45) DEFAULT NULL,
  `estado_visita` varchar(45) DEFAULT NULL,
  `cidade_visita` varchar(60) DEFAULT NULL,
  `navegador_visita` varchar(145) DEFAULT NULL,
  `dispositivo_visita` varchar(45) DEFAULT NULL,
  `so_visita` varchar(145) DEFAULT NULL,
  `data_visita` varchar(18) DEFAULT NULL,
  `hora_visita` varchar(10) DEFAULT NULL,
  `token_visita` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`id_visita`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_visitas_intro`
--

LOCK TABLES `site_visitas_intro` WRITE;
/*!40000 ALTER TABLE `site_visitas_intro` DISABLE KEYS */;
INSERT INTO `site_visitas_intro` VALUES (1,'186.229.177.35','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200820','13:10','apsnj2pivpibibvetublh1it7c64vsq4'),(2,'0','ispmais.net.br/',NULL,NULL,NULL,'Chrome','0','Windows 8.1','20200820','14:17','40fiqgollokf692t3rlcko33fhg86gep'),(3,'186.229.177.35','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200820','17:04','abonp8did0de6f4hob9um1bjr21t4pe8'),(4,'177.30.105.219','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200821','08:48','nu75igo4huajqufbp85rcuv8ug0l4c1i'),(5,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20200821','14:13','2sn6a34jk97eon0a9antnvk15m6ke3o0'),(6,'177.30.111.114','projeto1-net.com.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200826','13:25','d2f65dpbld3f2t8shvjt80o46rmnba62'),(7,'177.30.111.114','projeto1-net.com.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200826','13:35','r91itaams178eo0lr6ot8mj99sosltjp'),(8,'177.30.111.114','projeto1-net.com.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200826','13:40','spv9kf7dtlo65l7pp0dqniknjsvkvpdd'),(9,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20200826','16:40','jdh1mr82v3tgvpnpmak28o8jf6r0h0p5'),(10,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200901','09:24','5a30lj12r5bcegebol2sdl4rhd495f0v'),(11,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200901','12:43','pn67th50uioima66pn7v2p3heh77v8jr'),(12,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200901','13:49','p9urtr0d5lmpms7qim13edngilff96jj'),(13,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200902','08:11','rtdftgbjhlndrqiq977s3co466ju62k8'),(14,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20200908','17:13','9c5opi6lncbulblbgh8upb52ucevb8lv'),(15,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20200908','17:23','m710s5ujsaqhrtn7c8eqj5qpipq2rj4j'),(16,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200909','14:02','dm75bvsfospuslq56hsjhfcr78p04jag'),(17,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200909','14:02','oc7h5ov27en53nktalos859v81g23mvs'),(18,'177.30.111.114','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200909','14:03','1co9fa63cpd7v5sqrkt3el80v0d10mpl'),(19,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201008','11:15','f0183em3qgl54bu3ovhfclr7rislfldm'),(20,'177.30.111.114','ispmais.net.br/bio','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201008','11:20','pjeqqhej0iq9fvc62f1r6om7ob41ubgk'),(21,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201009','16:42','jempp9mkal4ipjovle5apqc15rghrnic'),(22,'0','ispmais.net.br/',NULL,NULL,NULL,'Opera','0','Windows 8.1','20201023','10:03','pbgjdpld1uqgombsngkedfm192h2of71'),(23,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201023','10:09','j90l8qelj8grt4io4fs9fgrk0nql7v13'),(24,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201023','13:02','4ud8vl2mqcvotgjjl4uolbar7hiei6s3'),(25,'177.30.111.114','ispmais.net.br/bio','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201023','13:48','jsv7kch9pgns3hsmjm9el4suupna5g6k'),(26,'177.30.107.204','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201024','00:10','n9fsmb8b3cn4liv0tq7kuev498oqqpni'),(27,'177.30.107.204','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201024','16:42','cc388hcasjj0nv8susekaqfth36q8bcv'),(28,'177.30.107.204','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201024','22:34','qo0ta61adt06km921lrcn3rcq1dch05f'),(29,'177.30.90.187','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','19:49','918gfhthm8eogn5fp17b0p5mkqqq2j08'),(30,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','20:23','bh75d5ug2v5h6p3cdacjejmntgn4k4vs'),(31,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','21:39','sr0g95usblm9693lqvilkfgro158jobg'),(32,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','21:43','ma4tom0fhjcruir8n55vnqoq86otre63'),(33,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','21:45','20djptfqqiufu0pibctpfhe4o6si7mck'),(34,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','21:51','rcs16igsu98du31lfhsfghbspigqn56i'),(35,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','21:57','3afcupk7cq0i1f4j4sg48sbng010kq6m'),(36,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','22:04','joov2ac3a4bml73rn0sfk53ob4u1lsjm'),(37,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','22:09','4kl6jmje2shq6ec70m9c64g9413dk654'),(38,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','22:18','l0v7pp0gbouhmlironcltbmf4gfhc6to'),(39,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','22:36','j72gc972168l6t3hmqd41r76d3ugu2d0'),(40,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','22:45','i2jqbk3433ekatjdm458nkeq8im0odaa'),(41,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','22:53','dlej0od1br682vjj45ur0u60s8lkhrph'),(42,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','22:54','n9fmq0mh24p3hltl4cjdjjpehstd57kn'),(43,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','23:00','tik7ep2qtcatp212d89vp4kvc6hrmn4r'),(44,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','23:05','pshejrbbttehml7ks5analioool28rko'),(45,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','23:13','ammonql2g8vl8b7u093ge5ljip8ai4fu'),(46,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201028','23:18','bcuaoksdotuuohlptcsvahc6boeu0sdu'),(47,'177.30.90.187','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201029','18:56','v085fcrcgifnrjttr3t39o31a8u3h59c'),(48,'177.30.90.187','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201031','13:48','u6p5co85ukj27gc5fegtice8ctcvr07h'),(49,'177.30.90.187','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201101','11:54','2m5ujrjm1ntrv6i1944esl85c4rlpbuu'),(50,'177.30.90.187','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201101','21:52','s2g4btgduvam1klenlihvis60ub6jhov'),(51,'177.30.87.83','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201102','17:02','pftla47lphdfsl9bn0upo3s7rgml3v8h'),(52,'177.30.87.83','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201102','17:04','arkek81vc2bq3nbifqmb07rvgu11rmuv'),(53,'177.30.87.83','ispmais.net.br/bio','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201102','17:09','3m2vspnqem5oc967gbb07jofoltn76gd'),(54,'177.30.87.83','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201102','17:15','jel2iqn6jqoqbbm12buie1m92006a2ej'),(55,'177.30.87.83','ispmais.net.br/','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201102','18:02','uo6j1h6m3a1fvdkkoshahd39o8c80iv6'),(56,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201103','08:44','lpl7lqghe8atcjp249jtj9p5vr9reo7f'),(57,'177.30.111.114','ispmais.net.br/bio','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201103','10:59','ibia50j2o9oeu8akplnjfdomrukau8qq'),(58,'177.30.111.114','ispmais.net.br/bio','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201103','11:13','3pths4ied8m3fe6nj06vho9ri5o9fth5'),(59,'177.30.111.114','ispmais.net.br/','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201103','13:06','k2beqnstobmefimnn7au70q9bhvmssab'),(60,'177.30.111.114','ispmais.net.br/index.php','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201104','14:06','5ddi0657iblo2f9upq4v7ndfsbenim19');
/*!40000 ALTER TABLE `site_visitas_intro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_visitas_ourilandia`
--

DROP TABLE IF EXISTS `site_visitas_ourilandia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_visitas_ourilandia` (
  `id_visita` int(11) NOT NULL,
  `ip_visita` varchar(45) DEFAULT NULL,
  `pagina_visita` text,
  `pais_visita` varchar(45) DEFAULT NULL,
  `estado_visita` varchar(45) DEFAULT NULL,
  `cidade_visita` varchar(60) DEFAULT NULL,
  `navegador_visita` varchar(145) DEFAULT NULL,
  `dispositivo_visita` varchar(45) DEFAULT NULL,
  `so_visita` varchar(145) DEFAULT NULL,
  `data_visita` varchar(18) DEFAULT NULL,
  `hora_visita` varchar(10) DEFAULT NULL,
  `token_visita` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`id_visita`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_visitas_ourilandia`
--

LOCK TABLES `site_visitas_ourilandia` WRITE;
/*!40000 ALTER TABLE `site_visitas_ourilandia` DISABLE KEYS */;
INSERT INTO `site_visitas_ourilandia` VALUES (1,'186.229.177.35','ispmais.net.br/site/ourilandia_do_norte/sobre','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200820','13:08','eun8ifvninmmkgd3ingofhfgongliccs'),(2,'186.229.177.35','ispmais.net.br/site/ourilandia_do_norte','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200820','13:10','apsnj2pivpibibvetublh1it7c64vsq4'),(3,'186.229.177.35','ispmais.net.br/site/ourilandia_do_norte/sobre','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200820','13:15','ujemnqjg45okjv0brfsbnvcs0pbfr0s3'),(4,'186.229.177.35','ispmais.net.br/site/ourilandia_do_norte/sobre','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200820','13:21','jksafi5k3hslsei610hl79ot1759ge8q'),(5,'186.229.177.35','ispmais.net.br/site/ourilandia_do_norte/sobre','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200821','00:16','tee4n5m4k2pslstm2156tdtgrmtn16ce'),(6,'186.229.177.35','ispmais.net.br/site/ourilandia_do_norte','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200821','00:24','385iri30ievsf64qmqmgmkg2e7fricfr'),(7,'177.30.105.219','ispmais.net.br/site/ourilandia_do_norte','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200821','08:48','nu75igo4huajqufbp85rcuv8ug0l4c1i'),(8,'177.30.105.219','ispmais.net.br/site/ourilandia_do_norte/sobre','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200821','09:48','dos0upgaelr2c945m6s8akti8rsj9avj'),(9,'177.30.105.219','ispmais.net.br/site/ourilandia_do_norte/sobre','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200821','09:53','urtlbgcdce027m8kg7uu42e4ie2v0utf'),(10,'177.30.105.219','ispmais.net.br/site/ourilandia_do_norte/sobre','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200821','11:32','jm3eh29q47562svlm8i89f26ru8u84hc'),(11,'177.30.105.219','ispmais.net.br/site/ourilandia_do_norte/sobre','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200821','11:37','kj33q2a2q0lvo7bnhc34e7a35r8gs07p'),(12,'177.30.111.114','ispmais.net.br/site/ourilandia_do_norte','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20200821','14:13','2sn6a34jk97eon0a9antnvk15m6ke3o0'),(13,'177.30.111.114','ispmais.net.br/site/ourilandia_do_norte','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20200909','14:02','dm75bvsfospuslq56hsjhfcr78p04jag'),(14,'177.30.111.114','ispmais.net.br/site/ourilandia_do_norte','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201008','11:15','f0183em3qgl54bu3ovhfclr7rislfldm'),(15,'0','ispmais.net.br/site/ourilandia_do_norte',NULL,NULL,NULL,'Opera','0','Windows 8.1','20201023','10:04','pbgjdpld1uqgombsngkedfm192h2of71'),(16,'177.30.111.114','ispmais.net.br/site/ourilandia_do_norte/sobre','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201028','16:53','jer1gikgk2i5j1j78haoubqbmnm9rpeo'),(17,'177.30.87.83','ispmais.net.br/site/ourilandia_do_norte','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201102','17:10','3m2vspnqem5oc967gbb07jofoltn76gd'),(18,'177.30.87.83','ispmais.net.br/site/ourilandia_do_norte/sobre','Brazil','Goias','Goiânia','Chrome','0','Windows 8.1','20201102','17:13','jel2iqn6jqoqbbm12buie1m92006a2ej'),(19,'177.30.111.114','ispmais.net.br/site/ourilandia_do_norte/sobre','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201103','09:59','t0pa1oule6drcp2g9n7kki6k1ivbm94r'),(20,'177.30.111.114','ispmais.net.br/site/ourilandia_do_norte','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201103','13:18','06kfb7apjfakas4qti9l2snlns1b58kp'),(21,'177.30.111.114','ispmais.net.br/site/ourilandia_do_norte/','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201103','15:29','glnbvkg960foa4d60dkjnl9asghnh6bc'),(22,'177.30.111.114','ispmais.net.br/site/ourilandia_do_norte/','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201103','15:41','pumkgi4190u39d8iqd1209r0euk33kvb'),(23,'177.30.111.114','ispmais.net.br/site/ourilandia_do_norte/','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201103','15:46','b42oplpc9a8fiogsi44nvujubpj8idg5'),(24,'177.30.111.114','ispmais.net.br/site/ourilandia_do_norte/','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201103','15:51','7kbqo8hvf1kaku6j5j5sit27qaliq5hl'),(25,'177.30.111.114','ispmais.net.br/site/ourilandia_do_norte/','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201103','16:13','fv7d59qgjk9sqlosvsus3vb0mgq8u6ah'),(26,'177.30.111.114','ispmais.net.br/site/ourilandia_do_norte/','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201103','16:31','kqqgei1cp8v354047g4smbmt34dp8iel'),(27,'177.30.111.114','ispmais.net.br/site/ourilandia_do_norte/sobre','Brazil','Goias','Goiânia','Opera','0','Windows 8.1','20201104','09:05','3827a5lspsuvkjsvf7f7jsidbc1vjb46');
/*!40000 ALTER TABLE `site_visitas_ourilandia` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-05 17:53:57
