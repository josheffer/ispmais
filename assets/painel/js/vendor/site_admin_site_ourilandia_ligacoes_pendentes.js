var site = 'ispmais';

listarligacoes_pendentes();

function listarligacoes_pendentes() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarligacoes_pendentes",

        ajax: 'data.json',

        dataType: 'json',

        success: function(data) {

            var dados = data;

            dadosGlobais = dados;
            idUsuario = dados.dadosUsuario

            $('#tabelaListarSolicitacoesLigacoes').html('');
            $('#atualizaDiv').html('<a type="button" onclick="listarligacoes_pendentes();" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>');

            if (parseInt(dados.ligacoes.length) > 0) {

                for (var i = 0; i < dados.ligacoes.length; i++) {

                    var telefonePessoa = dados.ligacoes[i].telefone_solicitacoes_ligacoes;
                    var NomePessoa = dados.ligacoes[i].nome_solicitacoes_ligacoes;

                    var numeroWhats = telefonePessoa.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '');

                    if (dados.ligacoes[i].status_solicitacoes_ligacoes === '0') {

                        var botaoWhats = '<a class="btn btn-bg-danger">' +
                            '<button type="button" disabled class="btn btn-block btn-success btn-rounded waves-effect waves-light">' +
                            '<span class="btn-label">' +
                            '<i class="mdi mdi-eye-off"></i>' +
                            '</span> <i class="mdi mdi-link-variant-off"></i>' +
                            '</button>' +
                            '</a>';

                        var statusBotao = '<button type="button" onclick="modalRecepcionar(' + i + ')" class="btn btn-block btn-xs btn-primary waves-effect waves-light">Recepcionar</button>';

                    } else {

                        if (dadosGlobais.ligacoes[i].operador_solicitacoes_ligacoes === idUsuario) {

                            botaoWhats = '<a target="_blank" href="https://api.whatsapp.com/send?phone=55' + numeroWhats + '&text=Olá ' + NomePessoa + ', recebemos seu pedido de ligação através do site, em que posso ajudar?" class="btn btn-bg-danger">' +
                                '<button type="button" class="btn btn-block btn-success btn-rounded waves-effect waves-light">' +
                                '<span class="btn-label">' +
                                '<i class="mdi mdi-whatsapp"></i>' +
                                '</span>' +
                                dados.ligacoes[i].telefone_solicitacoes_ligacoes +
                                '</button>' +
                                '</a>';

                            var statusBotao = '<button type="button" onclick="modalRecepcionar(' + i + ')" class="btn btn-block btn-xs btn-warning waves-effect waves-light">Em atendimento..</button>';

                        } else {

                            var botaoWhats = '<a class="btn btn-bg-danger">' +
                                '<button type="button" disabled class="btn btn-block btn-success btn-rounded waves-effect waves-light">' +
                                '<span class="btn-label">' +
                                '<i class="mdi mdi-eye-off"></i>' +
                                '</span> <i class="mdi mdi-link-variant-off"></i>' +
                                '</button>' +
                                '</a>';

                            var statusBotao = '<button type="button" class="disabled btn btn-block btn-xs btn-warning waves-effect waves-light">Em atendimento..</button>';

                        }
                    }

                    var dataLigacoes = moment(dados.ligacoes[i].data_solicitacoes_ligacoes).format('DD/MM/YYYY');
                    var horaLigacoes = dados.ligacoes[i].hora_solicitacoes_ligacoes;

                    var dataHoraligacoes = '<span class="badge badge-light">' + dataLigacoes + ' às ' + horaLigacoes + '</span>';

                    $('#tabelaListarSolicitacoesLigacoes').append(

                        '<tr>' +

                        '<td class="text-center"> ' + dados.ligacoes[i].id_solicitacoes_ligacoes + ' </td>' +

                        '<td class="text-center"> ' + dados.ligacoes[i].nome_solicitacoes_ligacoes + ' </td>' +

                        '<td class="text-center"> <h5>' + dataHoraligacoes + '</h5></td>' +

                        '<td class="text-center">' + botaoWhats + '</td>' +

                        '<td class="text-center">' +
                        '<div class="button-list">' +
                        statusBotao +
                        '</div>' +
                        '</td>' +

                        '</tr> '

                    );
                }

            } else {

                $('#tabelaListarSolicitacoesLigacoes').append(
                    '<td colspan="5">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhuma ligação pendente!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }

        }

    });
}

function modalRecepcionar(abrirModal) {

    if (dadosGlobais.ligacoes[abrirModal].status_solicitacoes_ligacoes === '1') {

        $('#dadosSolicitacao').html('');

        $('#abrirModalRecepcionar').modal('show');

        $('#tituloRecepcionar').html('Você está recepcionando a ligação de: <strong><u><i><span class="text-warning">' + dadosGlobais.ligacoes[abrirModal].nome_solicitacoes_ligacoes + '</span></i></u></strong>');

        $('#mensagemRecepcionar').html(

            '<form>' +

            '<div class="form-row">' +

            '<div class="form-group col-md-4">' +
            '<label class="col-form-label">Nome</label>' +
            '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[abrirModal].nome_solicitacoes_ligacoes + '">' +
            '</div>' +

            '<div class="form-group col-md-4">' +
            '<label class="col-form-label">Telefone</label>' +
            '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[abrirModal].telefone_solicitacoes_ligacoes + '">' +
            '</div>' +

            '<div class="form-group col-md-4">' +
            '<label class="col-form-label">E-mail</label>' +
            '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[abrirModal].email_solicitacoes_ligacoes + '">' +
            '</div>' +

            '<div class="form-group col-md-2">' +
            '<label class="col-form-label">Plano</label>' +
            '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[abrirModal].plano_solicitacoes_ligacoes + '">' +
            '</div>' +

            '<div class="form-group col-md-2">' +
            '<label class="col-form-label">Valor</label>' +
            '<input type="text" class="form-control" disabled value="R$ ' + dadosGlobais.ligacoes[abrirModal].valorPlano_solicitacoes_ligacoes + '">' +
            '</div>' +

            '<div class="form-group col-md-2">' +
            '<label class="col-form-label">CEP</label>' +
            '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[abrirModal].cepEndereco_solicitacoes_ligacoes + '">' +
            '</div>' +

            '<div class="form-group col-md-3">' +
            '<label class="col-form-label">Rua</label>' +
            '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[abrirModal].ruaEndereco_solicitacoes_ligacoes + '">' +
            '</div>' +

            '<div class="form-group col-md-3">' +
            '<label class="col-form-label">Complemento</label>' +
            '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[abrirModal].complementoEndereco_solicitacoes_ligacoes + '">' +
            '</div>' +

            '<div class="form-group col-md-3">' +
            '<label class="col-form-label">Número</label>' +
            '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[abrirModal].numeroEndereco_solicitacoes_ligacoes + '">' +
            '</div>' +

            '<div class="form-group col-md-3">' +
            '<label class="col-form-label">Bairro</label>' +
            '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[abrirModal].bairroEndereco_solicitacoes_ligacoes + '">' +
            '</div>' +

            '<div class="form-group col-md-3">' +
            '<label class="col-form-label">Cidade</label>' +
            '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[abrirModal].cidadeEndereco_solicitacoes_ligacoes + '">' +
            '</div>' +

            '<div class="form-group col-md-3">' +
            '<label class="col-form-label">Estado</label>' +
            '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[abrirModal].estadoEndereco_solicitacoes_ligacoes + '">' +
            '</div>' +

            '<div class="form-group col-md-12">' +
            '<label class="col-form-label">Observação</label>' +
            '<textarea class="form-control" rows="5" disabled>' + dadosGlobais.ligacoes[abrirModal].observacoes_solicitacoes_ligacoes + '</textarea>' +
            '</div>' +

            '</div>' +

            '</form>' +

            '<div class="row mb-3 mt-3 text-center">' +

            '<div class="col-md-3">' +
            '<button type="submit" onclick="javascript:devolverLigacao(' + abrirModal + ');" class="btn btn-block btn-purple waves-effect" >Devolver para pendentes</button>' +
            '</div>' +

            '<div class="col-md-3">' +
            '<button type="submit" onclick="javascript:NaoAtendidaLigacao(' + abrirModal + ');" class="btn btn-block btn-danger waves-effect" >Não atendida</button>' +
            '</div>' +

            '<div class="col-md-3">' +
            '<button type="submit" onclick="javascript:vendaRealizadaLigacao(' + abrirModal + ');" class="btn btn-block btn-success waves-effect" >Venda realizada</button>' +
            '</div>' +

            '<div class="col-md-3">' +
            '<button type="submit" onclick="javascript:vendaNaoRealizada(' + abrirModal + ');" class="btn btn-block btn-warning waves-effect" >Venda não realizada</button>' +
            '</div>' +

            '</div>'

        );
    }

    if (dadosGlobais.ligacoes[abrirModal].status_solicitacoes_ligacoes === '0') {

        $('#abrirModalRecepcionar').modal('show');
        $('#dadosSolicitacao').html('');

        $('#tituloRecepcionar').html('Recepcionar ligação de: <strong><u><i><span class="text-warning">' + dadosGlobais.ligacoes[abrirModal].nome_solicitacoes_ligacoes + '</span></i></u></strong>');

        $('#mensagemRecepcionar').html(
            '<h4 class="text-center">Deseja recepcionar a solicitação de <strong><u><i><span class="text-warning">' + dadosGlobais.ligacoes[abrirModal].nome_solicitacoes_ligacoes + '</span></i></u></strong> ?</h4>' +
            '<div class="col-md-12 text-center">' +
            '<br>' +
            '<button type="submit" onclick="javascript:recepcionarLigacao(' + abrirModal + ');" class="btn btn-primary waves-effect">Sim, quero!</button>' +
            '</div>'
        );

    }


}

function recepcionarLigacao(attrecepcionarLig) {

    $.post({

        type: "POST",

        data: {
            idLigacao: dadosGlobais.ligacoes[attrecepcionarLig].id_solicitacoes_ligacoes,
            idOperador: idUsuario,
            statusLigacao: '1'
        },

        url: "/dashboard/site_admin/ourilandia/recepcionarLigacao",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#mensagemRecepcionar').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                listarligacoes_pendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarligacoes_pendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#mensagemRecepcionar').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    'Você agora é responsável por este pedido de ligação de <strong class="text-light">' + dadosGlobais.ligacoes[attrecepcionarLig].nome_solicitacoes_ligacoes + '</strong> !' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#tituloRecepcionar').html('Você está recepcionando a ligação de: <strong><u><i><span class="text-warning">' + dadosGlobais.ligacoes[attrecepcionarLig].nome_solicitacoes_ligacoes + '</span></i></u></strong>');

                $('#dadosSolicitacao').append(

                    '<form>' +

                    '<div class="form-row">' +

                    '<div class="form-group col-md-4">' +
                    '<label class="col-form-label">Nome</label>' +
                    '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[attrecepcionarLig].nome_solicitacoes_ligacoes + '">' +
                    '</div>' +

                    '<div class="form-group col-md-4">' +
                    '<label class="col-form-label">Telefone</label>' +
                    '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[attrecepcionarLig].telefone_solicitacoes_ligacoes + '">' +
                    '</div>' +

                    '<div class="form-group col-md-4">' +
                    '<label class="col-form-label">E-mail</label>' +
                    '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[attrecepcionarLig].email_solicitacoes_ligacoes + '">' +
                    '</div>' +

                    '<div class="form-group col-md-2">' +
                    '<label class="col-form-label">Plano</label>' +
                    '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[attrecepcionarLig].plano_solicitacoes_ligacoes + '">' +
                    '</div>' +

                    '<div class="form-group col-md-2">' +
                    '<label class="col-form-label">Valor</label>' +
                    '<input type="text" class="form-control" disabled value="R$ ' + dadosGlobais.ligacoes[attrecepcionarLig].valorPlano_solicitacoes_ligacoes + '">' +
                    '</div>' +

                    '<div class="form-group col-md-2">' +
                    '<label class="col-form-label">CEP</label>' +
                    '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[attrecepcionarLig].cepEndereco_solicitacoes_ligacoes + '">' +
                    '</div>' +

                    '<div class="form-group col-md-3">' +
                    '<label class="col-form-label">Rua</label>' +
                    '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[attrecepcionarLig].ruaEndereco_solicitacoes_ligacoes + '">' +
                    '</div>' +

                    '<div class="form-group col-md-3">' +
                    '<label class="col-form-label">Complemento</label>' +
                    '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[attrecepcionarLig].complementoEndereco_solicitacoes_ligacoes + '">' +
                    '</div>' +

                    '<div class="form-group col-md-3">' +
                    '<label class="col-form-label">Número</label>' +
                    '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[attrecepcionarLig].numeroEndereco_solicitacoes_ligacoes + '">' +
                    '</div>' +

                    '<div class="form-group col-md-3">' +
                    '<label class="col-form-label">Bairro</label>' +
                    '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[attrecepcionarLig].bairroEndereco_solicitacoes_ligacoes + '">' +
                    '</div>' +

                    '<div class="form-group col-md-3">' +
                    '<label class="col-form-label">Cidade</label>' +
                    '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[attrecepcionarLig].cidadeEndereco_solicitacoes_ligacoes + '">' +
                    '</div>' +

                    '<div class="form-group col-md-3">' +
                    '<label class="col-form-label">Estado</label>' +
                    '<input type="text" class="form-control" disabled value="' + dadosGlobais.ligacoes[attrecepcionarLig].estadoEndereco_solicitacoes_ligacoes + '">' +
                    '</div>' +

                    '<div class="form-group col-md-12">' +
                    '<label class="col-form-label">Observação</label>' +
                    '<textarea class="form-control" rows="5" disabled>' + dadosGlobais.ligacoes[attrecepcionarLig].observacoes_solicitacoes_ligacoes + '</textarea>' +
                    '</div>' +

                    '</div>' +

                    '</form>' +

                    '<div class="row mb-3 mt-3 text-center">' +

                    '<div class="col-md-3">' +
                    '<button type="submit" onclick="javascript:devolverLigacao(' + attrecepcionarLig + ');" class="btn btn-block btn-purple waves-effect" >Devolver para pendentes</button>' +
                    '</div>' +

                    '<div class="col-md-3">' +
                    '<button type="submit" onclick="javascript:NaoAtendidaLigacao(' + attrecepcionarLig + ');" class="btn btn-block btn-danger waves-effect" >Não atendida</button>' +
                    '</div>' +

                    '<div class="col-md-3">' +
                    '<button type="submit" onclick="javascript:vendaRealizadaLigacao(' + attrecepcionarLig + ');" class="btn btn-block btn-success waves-effect" >Venda realizada</button>' +
                    '</div>' +

                    '<div class="col-md-3">' +
                    '<button type="submit" onclick="javascript:vendaNaoRealizada(' + attrecepcionarLig + ');" class="btn btn-block btn-warning waves-effect" >Venda não realizada</button>' +
                    '</div>' +

                    '</div>'

                );


            }
        }


    });

}

function devolverLigacao(devolverLigacao) {

    $.post({

        type: "POST",

        data: {
            idLigacao: dadosGlobais.ligacoes[devolverLigacao].id_solicitacoes_ligacoes,
            idOperador: "",
            statusLigacao: '0'
        },

        url: "/dashboard/site_admin/ourilandia/recepcionarLigacao",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroEditarLinksBarraSupFixa').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarligacoes_pendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#abrirModalRecepcionar').modal('hide');


            }
        }


    });

}

function NaoAtendidaLigacao(naoAtendLigacao) {

    $.post({

        type: "POST",

        data: {
            idLigacao: dadosGlobais.ligacoes[naoAtendLigacao].id_solicitacoes_ligacoes,
            idOperador: idUsuario,
            statusLigacao: '2'
        },

        url: "/dashboard/site_admin/ourilandia/recepcionarLigacao",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroEditarLinksBarraSupFixa').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarligacoes_pendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#abrirModalRecepcionar').modal('hide');


            }
        }


    });

}

function vendaRealizadaLigacao(vendaRealizadaLigacao) {

    $.post({

        type: "POST",

        data: {
            idLigacao: dadosGlobais.ligacoes[vendaRealizadaLigacao].id_solicitacoes_ligacoes,
            idOperador: idUsuario,
            statusLigacao: '3'
        },

        url: "/dashboard/site_admin/ourilandia/recepcionarLigacao",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroEditarLinksBarraSupFixa').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarligacoes_pendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#abrirModalRecepcionar').modal('hide');


            }
        }


    });

}

function vendaNaoRealizada(vendaNaoRealizadaLigacao) {

    $.post({

        type: "POST",

        data: {
            idLigacao: dadosGlobais.ligacoes[vendaNaoRealizadaLigacao].id_solicitacoes_ligacoes,
            idOperador: idUsuario,
            statusLigacao: '4'
        },

        url: "/dashboard/site_admin/ourilandia/recepcionarLigacao",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroEditarLinksBarraSupFixa').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarligacoes_pendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#abrirModalRecepcionar').modal('hide');


            }
        }


    });

}