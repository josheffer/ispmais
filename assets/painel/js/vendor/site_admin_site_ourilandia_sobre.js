var site = "ispmais";

listarDadosPaginaSobre(); 

function listarDadosPaginaSobre() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarDadosPaginaSobre",

        ajax: 'data.json',

        dataType: 'json',

        success: function(data) {

            var dados = data;
            
            $('#titulo_paginaSobre').val(dados.titulo_sobre);
            $('#descricao_paginaSobre').val(dados.descricao_sobre);
            $('#nomeEmpresa_paginaSobre').val(dados.nomeEmpresa_sobre);
            $('#textoSobre_paginaSobre').val(dados.texto_sobre);
            $('#textoMissao_paginaSobre').val(dados.missao_sobre);
            $('#textoVisao_paginaSobre').val(dados.visao_sobre);
            $('#textoValores_paginaSobre').val(dados.valores_sobre);
            
            $('#verImagemSobre').attr("src", "/assets/sites/" + site + "/img/sobre/" + dados.imagem_sobre);
            
        }

    });
}

$('#formPaginaSObre').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = atualizar_dadosSobre(self);

});

function atualizar_dadosSobre(dados) {

    $.ajax({

        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/site/atualizar/atualizar_dadosSobre",
        dataType: 'json',
        enctype: 'multipart/form-data',
        data: new FormData($('#formPaginaSObre')[0]),
        cache: false,
        contentType: false,
        processData: false,

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarDadosPaginaSobre();

            }
        }
    });
}
