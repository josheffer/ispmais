var site = 'ispmais';

listarNewsletter();

function listarNewsletter() {
    
    $.ajax({

        type: "POST",

        url: "/dashboard/site_admin/ourilandia/listarNewsletter",

        ajax: 'data.json',

        dataType: 'json', 
        
        success: function(data) {

            console.log(data);

            var dados = data;

            dadosGlobais = dados;
            
            $('#tabelaListarNewsletter').html('');

            $('#atualizaDivNewsletter').html('<a type="button" onclick="listarNewsletter();" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>');

            if (parseInt(dados.length) > 0) {

                for (var i = 0; i < dados.length; i++) {

                    var dataNews = moment(dados[i].data_newsletter).format('DD/MM/YYYY');
                    var horaNews = dados[i].hora_newsletter;

                    var dataHoraNews = '<span class="badge badge-light">' + dataNews + ' às ' + horaNews + '</span>';

                    var telefonePessoa = dados[i].telefoneCliente_newsletter;
                    var NomePessoa = dados[i].nomeCliente_newsletter;

                    var numeroWhats = telefonePessoa.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '');

                    var botaoWhats = '<a target="_blank" href="https://api.whatsapp.com/send?phone=55' + numeroWhats + '&text=Olá *' + NomePessoa + '*, recebemos seu pedido de ligação através do site, em que posso ajudar?" class="btn btn-bg-danger">' +
                                '<button type="button" class="btn btn-block btn-success btn-rounded waves-effect waves-light">' +
                                '<span class="btn-label">' +
                                '<i class="mdi mdi-whatsapp"></i>' +
                                '</span>' +
                                dados[i].telefoneCliente_newsletter +
                                '</button>' +
                                '</a>';

                    
                    $('#tabelaListarNewsletter').append(

                        '<tr>' +

                        '<td class="text-center"> ' + dados[i].id_newsletter + ' </td>' +

                        '<td class="text-center"> ' + dados[i].nomeCliente_newsletter + ' </td>' +

                        '<td class="text-center"> ' + dados[i].emailCliente_newsletter + ' </td>' +

                        '<td class="text-center"> <h5>' + dataHoraNews + '</h5></td>' +

                        '<td class="text-center">' + botaoWhats + '</td>' +
                        
                        '</tr> '

                    );
                }

            } else {

                $('#tabelaListarNewsletter').append(
                    '<td colspan="6">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhum lcadastro de newsletter!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }

        }

    });
}

