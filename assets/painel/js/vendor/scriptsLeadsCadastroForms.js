$('#leadsForm1').submit(function(e){
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = leadsCadastroForm1(self);
  
});

function leadsCadastroForm1(dados){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
      {
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/cadastro/leadsCadastroForm1/"+numModelo[4],
        dataType: 'json',

        beforeSend: function(){
          
          $("#leadsBotaoForm1").val("Cadastrando...").prop("disabled", true);
          $("#ondeNosConheceu").prop("disabled", true);
          $("#emailVisitante").prop("disabled", true);
          $("#telefoneVisitante").prop("disabled", true);
          $("#nomeVisitante").prop("disabled", true);

        },

        success: function(retorno){
            
          if(retorno.ret === false) {
  
            $('#erroMsg1').html(
                '<div class="alert alert-danger alert-dismissible bg-danger mb-0 text-white border-0 fade show text-center" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Erro! '+ retorno.msg +
                '</div>' 
            );

            $("#leadsBotaoForm1").val("Tentar novamente!").prop("disabled", false);
            $("#ondeNosConheceu").prop("disabled", false);
            $("#emailVisitante").prop("disabled", false);
            $("#telefoneVisitante").prop("disabled", false);
            $("#nomeVisitante").prop("disabled", false);
  
            $(".alert").delay(5000).slideUp(1000, function() { $(this).alert('close'); });
            
          } else {
              
            window.location = "/landingpage/"+numModelo[4]+"/sucesso/";

          }
        }
      }
    );
  }

$('#leadsFormRS').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = leadsCadastroFormRS(self);
});

function leadsCadastroFormRS(dados){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");
  
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/cadastro/leadsCadastroForm1/"+numModelo[4],
      dataType: 'json',

      beforeSend: function(){
        
        $("#leadsBotaoFormRS").val("Cadastrando...").prop("disabled", true);
        $("#ondeNosConheceuRS").prop("disabled", true);
        $("#emailVisitanteRS").prop("disabled", true);
        $("#telefoneVisitanteRS").prop("disabled", true);
        $("#nomeVisitanteRS").prop("disabled", true);

      },

      success: function(retorno){
          
        if(retorno.ret === false) {

          $('#erroMsgRS').html(
              '<div class="alert alert-danger alert-dismissible bg-danger mb-0 text-white border-0 fade show text-center" role="alert">' +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                      '<span aria-hidden="true">×</span>' +
                  '</button>' +
                  '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Erro! '+ retorno.msg +
              '</div>' 
          );

          $("#leadsBotaoFormRS").val("Tentar novamente!").prop("disabled", false);
          $("#ondeNosConheceuRS").prop("disabled", false);
          $("#emailVisitanteRS").prop("disabled", false);
          $("#telefoneVisitanteRS").prop("disabled", false);
          $("#nomeVisitanteRS").prop("disabled", false);

          $(".alert").delay(5000).slideUp(1000, function() { $(this).alert('close'); });
          
        } else {
            
          window.location = "/landingpage/"+numModelo[4]+"/sucesso/";

        }
      }
    }
  );
}

$('#leadsForm2').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = leadsCadastroForm2(self);
});

function leadsCadastroForm2(dados){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");
  
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/cadastro/leadsCadastroForm1/"+numModelo[4],
      dataType: 'json',

      beforeSend: function(){
        
        $("#leadsBotaoForm2").val("Cadastrando...").prop("disabled", true);
        $("#ondeNosConheceu2").prop("disabled", true);
        $("#emailVisitante2").prop("disabled", true);
        $("#telefoneVisitante2").prop("disabled", true);
        $("#nomeVisitante2").prop("disabled", true);

      },

      success: function(retorno){
          
        if(retorno.ret === false) {

          $('#erroMsg2').html(
              '<div class="alert alert-danger alert-dismissible bg-danger mb-0 text-white border-0 fade show text-center" role="alert">' +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                      '<span aria-hidden="true">×</span>' +
                  '</button>' +
                  '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Erro! '+ retorno.msg +
              '</div>' 
          );

          $("#leadsBotaoForm2").val("Tentar novamente!").prop("disabled", false);
          $("#ondeNosConheceu2").prop("disabled", false);
          $("#emailVisitante2").prop("disabled", false);
          $("#telefoneVisitante2").prop("disabled", false);
          $("#nomeVisitante2").prop("disabled", false);

          $(".alert").delay(5000).slideUp(1000, function() { $(this).alert('close'); });
          
        } else {
            
          window.location = "/landingpage/"+numModelo[4]+"/sucesso/";

        }
      }
    }
  );
}

$('#leadsForm3').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = leadsCadastroForm3(self);
});

function leadsCadastroForm3(dados){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");
  
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/cadastro/leadsCadastroForm1/"+numModelo[4],
      dataType: 'json',

      beforeSend: function(){
        
        $("#leadsBotaoForm3").val("Cadastrando...").prop("disabled", true);
        $("#ondeNosConheceu3").prop("disabled", true);
        $("#emailVisitante3").prop("disabled", true);
        $("#telefoneVisitante3").prop("disabled", true);
        $("#nomeVisitante3").prop("disabled", true);

      },

      success: function(retorno){
          
        if(retorno.ret === false) {

          $('#erroMsg3').html(
              '<div class="alert alert-danger alert-dismissible bg-danger mb-0 text-white border-0 fade show text-center" role="alert">' +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                      '<span aria-hidden="true">×</span>' +
                  '</button>' +
                  '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Erro! '+ retorno.msg +
              '</div>' 
          );

          $("#leadsBotaoForm3").val("Tentar novamente!").prop("disabled", false);
          $("#ondeNosConheceu3").prop("disabled", false);
          $("#emailVisitante3").prop("disabled", false);
          $("#telefoneVisitante3").prop("disabled", false);
          $("#nomeVisitante3").prop("disabled", false);

          $(".alert").delay(5000).slideUp(1000, function() { $(this).alert('close'); });
          
        } else {
            
          window.location = "/landingpage/"+numModelo[4]+"/sucesso/";

        }
      }
    }
  );
}

$('#leadsForm4').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = leadsCadastroForm4(self);
});

function leadsCadastroForm4(dados){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");
  
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/cadastro/leadsCadastroForm1/"+numModelo[4],
      dataType: 'json',

      beforeSend: function(){
        
        $("#leadsBotaoForm4").val("Cadastrando...").prop("disabled", true);
        $("#ondeNosConheceu4").prop("disabled", true);
        $("#emailVisitante4").prop("disabled", true);
        $("#telefoneVisitante4").prop("disabled", true);
        $("#nomeVisitante4").prop("disabled", true);

      },

      success: function(retorno){
          
        if(retorno.ret === false) {

          $('#erroMsg4').html(
              '<div class="alert alert-danger alert-dismissible bg-danger mb-0 text-white border-0 fade show text-center" role="alert">' +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                      '<span aria-hidden="true">×</span>' +
                  '</button>' +
                  '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Erro! '+ retorno.msg +
              '</div>' 
          );

          $("#leadsBotaoForm4").val("Tentar novamente!").prop("disabled", false);
          $("#ondeNosConheceu4").prop("disabled", false);
          $("#emailVisitante4").prop("disabled", false);
          $("#telefoneVisitante4").prop("disabled", false);
          $("#nomeVisitante4").prop("disabled", false);

          $(".alert").delay(5000).slideUp(1000, function() { $(this).alert('close'); });
          
        } else {
            
          window.location = "/landingpage/"+numModelo[4]+"/sucesso/";

        }
      }
    }
  );
}

$('#leadsForm5').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = leadsCadastroForm5(self);
});

function leadsCadastroForm5(dados){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");
  
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/cadastro/leadsCadastroForm1/"+numModelo[4],
      dataType: 'json',

      beforeSend: function(){
        
        $("#leadsBotaoForm5").val("Cadastrando...").prop("disabled", true);
        $("#ondeNosConheceu5").prop("disabled", true);
        $("#emailVisitante5").prop("disabled", true);
        $("#telefoneVisitante5").prop("disabled", true);
        $("#nomeVisitante5").prop("disabled", true);

      },

      success: function(retorno){
          
        if(retorno.ret === false) {

          $('#erroMsg5').html(
              '<div class="alert alert-danger alert-dismissible bg-danger mb-0 text-white border-0 fade show text-center" role="alert">' +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                      '<span aria-hidden="true">×</span>' +
                  '</button>' +
                  '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Erro! '+ retorno.msg +
              '</div>' 
          );

          $("#leadsBotaoForm5").val("Tentar novamente!").prop("disabled", false);
          $("#ondeNosConheceu5").prop("disabled", false);
          $("#emailVisitante5").prop("disabled", false);
          $("#telefoneVisitante5").prop("disabled", false);
          $("#nomeVisitante5").prop("disabled", false);

          $(".alert").delay(5000).slideUp(1000, function() { $(this).alert('close'); });
          
        } else {
            
          window.location = "/landingpage/"+numModelo[4]+"/sucesso/";

        }
      }
    }
  );
}

$('#leadsForm6').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = leadsCadastroForm6(self);
});

function leadsCadastroForm6(dados){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");
  
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/cadastro/leadsCadastroForm1/"+numModelo[4],
      dataType: 'json',

      beforeSend: function(){
        
        $("#leadsBotaoForm6").val("Cadastrando...").prop("disabled", true);
        $("#ondeNosConheceu6").prop("disabled", true);
        $("#emailVisitante6").prop("disabled", true);
        $("#telefoneVisitante6").prop("disabled", true);
        $("#nomeVisitante6").prop("disabled", true);

      },

      success: function(retorno){
          
        if(retorno.ret === false) {

          $('#erroMsg6').html(
              '<div class="alert alert-danger alert-dismissible bg-danger mb-0 text-white border-0 fade show text-center" role="alert">' +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                      '<span aria-hidden="true">×</span>' +
                  '</button>' +
                  '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Erro! '+ retorno.msg +
              '</div>' 
          );

          $("#leadsBotaoForm6").val("Tentar novamente!").prop("disabled", false);
          $("#ondeNosConheceu6").prop("disabled", false);
          $("#emailVisitante6").prop("disabled", false);
          $("#telefoneVisitante6").prop("disabled", false);
          $("#nomeVisitante6").prop("disabled", false);

          $(".alert").delay(5000).slideUp(1000, function() { $(this).alert('close'); });
          
        } else {
            
          window.location = "/landingpage/"+numModelo[4]+"/sucesso/";

        }
      }
    }
  );
}

$('#leadsForm7').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = leadsCadastroForm7(self);
});

function leadsCadastroForm7(dados){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");
  
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/cadastro/leadsCadastroForm1/"+numModelo[4],
      dataType: 'json',

      beforeSend: function(){
        
        $("#leadsBotaoForm7").val("Cadastrando...").prop("disabled", true);
        $("#ondeNosConheceu7").prop("disabled", true);
        $("#emailVisitante7").prop("disabled", true);
        $("#telefoneVisitante7").prop("disabled", true);
        $("#nomeVisitante7").prop("disabled", true);

      },

      success: function(retorno){
          
        if(retorno.ret === false) {

          $('#erroMsg7').html(
              '<div class="alert alert-danger alert-dismissible bg-danger mb-0 text-white border-0 fade show text-center" role="alert">' +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                      '<span aria-hidden="true">×</span>' +
                  '</button>' +
                  '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Erro! '+ retorno.msg +
              '</div>' 
          );

          $("#leadsBotaoForm7").val("Tentar novamente!").prop("disabled", false);
          $("#ondeNosConheceu7").prop("disabled", false);
          $("#emailVisitante7").prop("disabled", false);
          $("#telefoneVisitante7").prop("disabled", false);
          $("#nomeVisitante7").prop("disabled", false);

          $(".alert").delay(5000).slideUp(1000, function() { $(this).alert('close'); });
          
        } else {
            
          window.location = "/landingpage/"+numModelo[4]+"/sucesso/";

        }
      }
    }
  );
}

$('#leadsForm8').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = leadsCadastroForm8(self);
});

function leadsCadastroForm8(dados){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");
  
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/cadastro/leadsCadastroForm1/"+numModelo[4],
      dataType: 'json',

      beforeSend: function(){
        
        $("#leadsBotaoForm8").val("Cadastrando...").prop("disabled", true);
        $("#ondeNosConheceu8").prop("disabled", true);
        $("#emailVisitante8").prop("disabled", true);
        $("#telefoneVisitante8").prop("disabled", true);
        $("#nomeVisitante8").prop("disabled", true);

      },

      success: function(retorno){
          
        if(retorno.ret === false) {

          $('#erroMsg8').html(
              '<div class="alert alert-danger alert-dismissible bg-danger mb-0 text-white border-0 fade show text-center" role="alert">' +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                      '<span aria-hidden="true">×</span>' +
                  '</button>' +
                  '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Erro! '+ retorno.msg +
              '</div>' 
          );

          $("#leadsBotaoForm8").val("Tentar novamente!").prop("disabled", false);
          $("#ondeNosConheceu8").prop("disabled", false);
          $("#emailVisitante8").prop("disabled", false);
          $("#telefoneVisitante8").prop("disabled", false);
          $("#nomeVisitante8").prop("disabled", false);

          $(".alert").delay(5000).slideUp(1000, function() { $(this).alert('close'); });
          
        } else {
            
          window.location = "/landingpage/"+numModelo[4]+"/sucesso/";

        }
      }
    }
  );
}