var site = 'ispmais';

listarBioInfos();
listarSites();
listarCanaisAtendimento();
listarRedesSociais();
listarCoresEstilo();

function listarBioInfos() {

    $.ajax({

        url: "/dashboard/listagens/listarBioInfos",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            $('#nome_bio').val(dados.nome_infos_bio);
            $('#descricao_bio').val(dados.descricao_infos_bio);
            $('#anoEmpresa_bio').val(dados.anoEmpresa_infos_bio);
            $('#TextoEmpresa_bio').val(dados.textoEmpresa_infos_bio);

            

            
            $('#verImagemDaBio').attr("src", "/assets/sites/" + site + "/img/bio/" + dados.imagem_infos_bio);
        }

    });
}

$('#formEditarInfosBio').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = editarInfosBio(self);

});

function editarInfosBio(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/atualizar/editarInfosBio",
        dataType: 'json',
        enctype: 'multipart/form-data',
        data: new FormData($('#formEditarInfosBio')[0]),
        cache: false,
        contentType: false,
        processData: false,

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#formEditarInfosBio').each(function() {
                    this.reset();
                });

                listarBioInfos();

            }
        }
    });
}

$('#formCadastrarSite').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = cadastrarSiteBio(self);
});

function cadastrarSiteBio(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/cadastro/cadastrarSiteBio",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#formCadastrarSite').each(function() {
                    this.reset();
                });

                listarSites();

            }
        }
    });
}

function listarSites() {

    $.ajax({

        url: "/dashboard/listagens/listarSites",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            dadosGlobaisSite = dados;

            $('#tabelaSitesBio').html('');

            if (dados.length > 0) {

                for (var i = 0; i < dados.length; i++) {

                    if (dados[i].status_sites_bio === '1') {

                        var btnStatus = '<button type="submit" onclick="javascript:statusBioSite(' + i + ');"  class="btn btn-block btn-xs btn-success waves-effect waves-light">Ativado</button>';

                    } else {

                        var btnStatus = '<button type="submit" onclick="javascript:statusBioSite(' + i + ');"  class="btn btn-block btn-xs btn-danger waves-effect waves-light">Desativado</button>';

                    }
                    
                    $('#tabelaSitesBio').append(

                        '<tr>' +

                            '<td>' + dados[i].id_sites_bio + '</td>' +

                            '<td>' + dados[i].nome_sites_bio + '</td>' +

                            '<td>' + btnStatus + '</td>' +

                            '<td>' +
                                '<div class="button-list">' +
                                    '<button type="button" onclick="javascript:EditarSiteBio(' + i + ');" class="btn btn-xs btn-warning waves-effect waves-light"><i class="mdi mdi-pencil"></i></button>' +
                                    '<button type="button" onclick="javascript:ExcluirSiteBio(' + i + ');" class="btn btn-xs btn-danger waves-effect waves-light"><i class="mdi mdi-delete"></i></button>' +
                                '</div>' +
                            '</td>' +

                        '</tr> '

                    );
                }

            
            } else {

                $('#tabelaSitesBio').append(
                    '<td colspan="4">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhum site cadastrado!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }
        }
    });
}

function statusBioSite(status) {

    $.post({
        type: "POST",

        data: {
            id_site_bio: dadosGlobaisSite[status].id_sites_bio
        },

        url: "/dashboard/atualizar/statusBioSite",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarSites();

            }
        }
    });

}

function EditarSiteBio(alterarSiteBio) {
    showModalEditarSiteBio(alterarSiteBio);
}

function showModalEditarSiteBio(alterarSiteBio) {

    $('#alterarInfosSIteBio').modal('show');

    $("#idEditarSiteBio").val(dadosGlobaisSite[alterarSiteBio].id_sites_bio);
    $("#alterarTituloNomeSite").html(dadosGlobaisSite[alterarSiteBio].nome_sites_bio);
    $("#alterarNomeSiteBio").val(dadosGlobaisSite[alterarSiteBio].nome_sites_bio);
    $("#alterarLinkSiteBio").val(dadosGlobaisSite[alterarSiteBio].url_sites_bio);

}

$('#formEditarSiteBio').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = editarSiteBioModal(self);
});

function editarSiteBioModal(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/atualizar/editarSiteBio",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#msgErroEditarSite').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#alterarInfosSIteBio').modal('hide');

                listarSites();

            }
        }
    });
}

function ExcluirSiteBio(deletarSiteBio) {
    showModalExcluirSiteBio(deletarSiteBio);
}

function showModalExcluirSiteBio(deletarSiteBio) {

    $('#ModalExcluiSiteBio').modal('show');

    $("#idExcluirSiteBio").val(dadosGlobaisSite[deletarSiteBio].id_sites_bio);
    $("#excluirNomeSiteBio").html(dadosGlobaisSite[deletarSiteBio].nome_sites_bio);
    $("#tituloExcluirSiteBio").html(dadosGlobaisSite[deletarSiteBio].nome_sites_bio);

}

$('#formExcluirSiteBio').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = excluirSiteBioModal(self);
});

function excluirSiteBioModal(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/excluir/excluirSiteBio",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#msgERROExcluirSiteBio').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#ModalExcluiSiteBio').modal('hide');

                listarSites();

            }
        }
    });
}

$('#formCadastrarcanalAtendimento').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = cadastrarCanalAtendimentoBio(self);
});

function cadastrarCanalAtendimentoBio(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/cadastrar/cadastrarCanalAtendimentoBio",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#ModalExcluiSiteBio').modal('hide');

                listarCanaisAtendimento();

            }
        }
    });
}

function listarCanaisAtendimento() {

    $.ajax({

        url: "/dashboard/listagens/listarCanaisAtendimento",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            dadosGlobaisCanaisAtendimento = dados;

            $('#tabelaListarCanaisAtendimento').html('');

            if (dados.length > 0) {

                for (var i = 0; i < dados.length; i++) {

                    if (dados[i].ativo_canal_atendimento === '1') {

                        var btnStatus = '<button type="submit" onclick="javascript:statusBioCanalAtendimento(' + i + ');"  class="btn btn-block btn-xs btn-success waves-effect waves-light">Ativado</button>';

                    } else {

                        var btnStatus = '<button type="submit" onclick="javascript:statusBioCanalAtendimento(' + i + ');"  class="btn btn-block btn-xs btn-danger waves-effect waves-light">Desativado</button>';

                    }
                    
                    $('#tabelaListarCanaisAtendimento').append(

                        '<tr>' +

                            '<td>' + dados[i].id_canal_atendimento + '</td>' +
                            
                            '<td>' + dados[i].nome_canal_atendimento + '</td>' +

                            '<td>' + btnStatus + '</td>' +
                            
                            '<td>' +
                            '<div class="button-list">' +
                            '<button type="button" onclick="javascript:EditarCanalAtendimento(' + i + ');" class="btn btn-xs btn-warning waves-effect waves-light"><i class="mdi mdi-pencil"></i></button>' +
                            '<button type="button" onclick="javascript:ExcluirCanalAtendimento(' + i + ');" class="btn btn-xs btn-danger waves-effect waves-light"><i class="mdi mdi-delete"></i></button>' +
                            '</div>' +
                            '</td>' +
                            
                            '<td>' + 
                                '<a href="' + dados[i].url_canal_atendimento + '" target="_blank">' +
                                    '<button type="button" class="btn btn-xs btn-info waves-effect waves-light"><i class="' + dados[i].icone_canal_atendimento + '"></i></button>' + 
                                '</a>' +
                            '</td>' +

                        '</tr> '

                    );
                }

            
            } else {

                $('#tabelaListarCanaisAtendimento').append(
                    '<td colspan="5">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhum canal de atendimento cadastrado!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }
        }
    });
}

function statusBioCanalAtendimento(CanalAtendimento) {

    $.post({
        type: "POST",

        data: {
            id_site_bio_canaisAtendimento: dadosGlobaisCanaisAtendimento[CanalAtendimento].id_canal_atendimento
        },

        url: "/dashboard/atualizar/statusBioCanalAtendimento",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarCanaisAtendimento();

            }
        }
    });

}

function EditarCanalAtendimento(EditarCanal) {

    $('#formEditarCanaisDeAtendimento').each(function() {
        this.reset();
    });

    showModalEditarCanal(EditarCanal);
}

function showModalEditarCanal(EditarCanal) {

    $('#alterarInfosCanalAtendimento').modal('show');

    $("#idEditarCanalAtendimento").val(dadosGlobaisCanaisAtendimento[EditarCanal].id_canal_atendimento);
    $("#alterarTituloNomeCanalAtendimento").html(dadosGlobaisCanaisAtendimento[EditarCanal].nome_canal_atendimento);
    $("#alterarNomeCanalAtendimento").val(dadosGlobaisCanaisAtendimento[EditarCanal].nome_canal_atendimento);
    $("#alterarLinkCanalAtendimento").val(dadosGlobaisCanaisAtendimento[EditarCanal].url_canal_atendimento);

}

$('#formEditarCanaisDeAtendimento').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = editarInfosCanalAtendimentoModal(self);
});

function editarInfosCanalAtendimentoModal(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/atualizar/editarInfosCanalAtendimentoModal",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#msgErroEditarInfosCanaisAtendimento').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#alterarInfosCanalAtendimento').modal('hide');

                $('#formEditarCanaisDeAtendimento').each(function() {
                    this.reset();
                });
                
                listarCanaisAtendimento();

            }
        }
    });
}

function ExcluirCanalAtendimento(deletarCanalAtendimento) {
    showModalExcluirCanalAtendimento(deletarCanalAtendimento);
}

function showModalExcluirCanalAtendimento(deletarCanalAtendimento) {

    $('#ModalExcluiCanalAtendimento').modal('show');

    $("#idExcluirCanalAtendimento").val(dadosGlobaisCanaisAtendimento[deletarCanalAtendimento].id_canal_atendimento);
    $("#excluirNomeCanalAtendimento").html(dadosGlobaisCanaisAtendimento[deletarCanalAtendimento].nome_canal_atendimento);
    $("#tituloExcluirCanalAtendimento").html(dadosGlobaisCanaisAtendimento[deletarCanalAtendimento].nome_canal_atendimento);

}

$('#formExcluirCanalAtendimento').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = excluirCanalAtendimentoBioModal(self);
});

function excluirCanalAtendimentoBioModal(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/excluir/excluirCanalAtendimentoBioModal",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#msgERROExcluirCanalAtendimento').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#ModalExcluiCanalAtendimento').modal('hide');

                listarCanaisAtendimento();

            }
        }
    });
}

$('#formCadastrarRedeSocial').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = cadastrarRedeSocialBio(self);
});

function cadastrarRedeSocialBio(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/cadastro/cadastrarRedeSocialBio",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#formCadastrarRedeSocial').each(function() {
                    this.reset();
                });

                listarRedesSociais();

            }
        }
    });
}

function listarRedesSociais() {

    $.ajax({

        url: "/dashboard/listagens/listarRedesSociaisBio",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            dadosGlobaisRedesSociais = dados;

            $('#tabelaListarRedesSociais').html('');

            if (dados.length > 0) {

                for (var i = 0; i < dados.length; i++) {

                    if (dados[i].ativo_redes_sociais === '1') {

                        var btnStatus = '<button type="submit" onclick="javascript:statusBioRedesSociais(' + i + ');"  class="btn btn-block btn-xs btn-success waves-effect waves-light">Ativado</button>';

                    } else {

                        var btnStatus = '<button type="submit" onclick="javascript:statusBioRedesSociais(' + i + ');"  class="btn btn-block btn-xs btn-danger waves-effect waves-light">Desativado</button>';

                    }
                    
                    $('#tabelaListarRedesSociais').append(

                        '<tr>' +

                            '<td>' + dados[i].id_redes_sociais + '</td>' +
                            
                            '<td>' + dados[i].nome_redes_sociais + '</td>' +

                            '<td>' + btnStatus + '</td>' +
                            
                            '<td>' +
                            '<div class="button-list">' +
                            '<button type="button" onclick="javascript:editarRedesSociais(' + i + ');" class="btn btn-xs btn-warning waves-effect waves-light"><i class="mdi mdi-pencil"></i></button>' +
                            '<button type="button" onclick="javascript:excluirRedesSociais(' + i + ');" class="btn btn-xs btn-danger waves-effect waves-light"><i class="mdi mdi-delete"></i></button>' +
                            '</div>' +
                            '</td>' +
                            
                            '<td>' + 
                                '<a href="' + dados[i].url_redes_sociais + '" target="_blank">' +
                                    '<button type="button" class="btn btn-xs btn-info waves-effect waves-light"><i class="' + dados[i].icone_redes_sociais + '"></i></button>' + 
                                '</a>' +
                            '</td>' +

                        '</tr> '

                    );
                }

            
            } else {

                $('#tabelaListarRedesSociais').append(
                    '<td colspan="4">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhum canal de atendimento cadastrado!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }
        }
    });
}

function editarRedesSociais(alterarRedeSociais) {
    showModalAlterarRedesSociais(alterarRedeSociais);
}

function showModalAlterarRedesSociais(alterarRedeSociais) {

    $('#alterarInfosRedesSociais').modal('show');
    
    $("#idEditarRedesSociaisBio").val(dadosGlobaisRedesSociais[alterarRedeSociais].id_redes_sociais);
    $("#alterarNomeRedesSociaisBio").val(dadosGlobaisRedesSociais[alterarRedeSociais].nome_redes_sociais);
    $("#alterarLinkRedesSociaisBio").val(dadosGlobaisRedesSociais[alterarRedeSociais].url_redes_sociais);
    $("#alterarTituloNomeRedeSocial").html(dadosGlobaisRedesSociais[alterarRedeSociais].nome_redes_sociais);

}

$('#formEditarRedesSociaisBio').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = editarInfosRedesSociaisBio(self);
});

function editarInfosRedesSociaisBio(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/atualizar/editarInfosRedesSociaisBio",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#msgErroEditarRedesSociais').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#formEditarRedesSociaisBio').each(function() {
                    this.reset();
                });

                $('#alterarInfosRedesSociais').modal('hide');

                listarRedesSociais();

            }
        }
    });
}

function statusBioRedesSociais(statusBioRedesSociais) {

    $.post({
        type: "POST",

        data: {
            id_site_bio_redesSociais: dadosGlobaisRedesSociais[statusBioRedesSociais].id_redes_sociais
        },

        url: "/dashboard/atualizar/statusBioRedesSociais",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarRedesSociais();

            }
        }
    });

}

function excluirRedesSociais(excluirRedeSociais) {
    showModalExcluirRedesSociais(excluirRedeSociais);
}

function showModalExcluirRedesSociais(excluirRedeSociais) {

    $('#ModalExcluiRedeSocial').modal('show');
    
    $("#idExcluirRedeSocial").val(dadosGlobaisRedesSociais[excluirRedeSociais].id_redes_sociais);
    $("#excluirNomeRedeSocial").html(dadosGlobaisRedesSociais[excluirRedeSociais].nome_redes_sociais);
    // $("#alterarLinkRedesSociaisBio").val(dadosGlobaisRedesSociais[excluirRedeSociais].url_redes_sociais);
    $("#tituloExcluirRedeSocial").html(dadosGlobaisRedesSociais[excluirRedeSociais].nome_redes_sociais);

}

$('#formExcluirRedeSocial').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = excluirRedesSociaisBio(self);
});

function excluirRedesSociaisBio(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/excluir/excluirRedesSociaisBio",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#msgERROExcluirRedeSocial').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#ModalExcluiRedeSocial').modal('hide');

                listarRedesSociais();

            }
        }
    });
}

function listarCoresEstilo() {

    $.ajax({

        url: "/dashboard/listagens/listarCoresEstilo",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            $('#corBgBio').val(dados.corBg_cores_estilos);
            $('#corBoxMeioBio').val(dados.corBoxMeio_cores_estilos);
            $('#corFaixasBoxMeioBio').val(dados.CorFaixas_cores_estilos);
            $('#corTextosFaixaBio').val(dados.corTextoFaixa_cores_estilos);
            $('#corTextosBio').val(dados.CorTextos_cores_estilos);
            $('#corTextosBotoesBio').val(dados.corTextosBotoes_cores_estilos);
            $('#corTextosBotoesHoverBio').val(dados.corTextosBotoesHover_cores_estilos);
            $('#corBotoesBio').val(dados.corBgBotoes_cores_estilos);
            $('#corBotoeHoversBio').val(dados.corBgBotoesHover_cores_estilos);
            $('#corTextoRodapeBio').val(dados.corTextoRodape_cores_estilos);
            
        }

    });
}

$('#formAtualizarCoresEstiloBio').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = atualizarCoresEstiloBio(self);
});

function atualizarCoresEstiloBio(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/atualizar/atualizarCoresEstiloBio",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
                
                listarCoresEstilo();

            }
        }
    });
}


