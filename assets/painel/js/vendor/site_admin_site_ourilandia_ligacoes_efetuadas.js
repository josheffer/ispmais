var site = 'ispmais';

listarligacoes_efetuadas();

function listarligacoes_efetuadas() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarligacoes_efetuadas",

        ajax: 'data.json',

        dataType: 'json',

        success: function(data) {

            var dados = data;

            dadosGlobais = dados;
            idUsuario = dados.dadosUsuario

            $('#tabelaListarSolicitacoesLigacoesEfetuadas').html('');
            $('#atualizaDiv').html('<a type="button" onclick="listarligacoes_efetuadas();" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>');

            if (parseInt(dados.ligacoes.length) > 0) {

                for (var i = 0; i < dados.ligacoes.length; i++) {

                    if (dados.ligacoes[i].status_solicitacoes_ligacoes === '2') {

                        var statusBotao = '<button type="button" class="btn btn-xs btn-block btn-danger waves-effect waves-light">Não atendida</button>';

                    }

                    if (dados.ligacoes[i].status_solicitacoes_ligacoes === '3') {

                        var statusBotao = '<button type="button" class="btn btn-xs btn-block btn-success waves-effect waves-light">Venda realizada</button>';

                    }

                    if (dados.ligacoes[i].status_solicitacoes_ligacoes === '4') {

                        var statusBotao = '<button type="button" class="btn btn-xs btn-block btn-warning waves-effect waves-light">Venda Não realizada</button>';

                    }

                    var statusDevolver = '<button type="button" disabled onclick="devolverLigacao(' + i + ')" class="btn btn-xs btn-block btn-primary waves-effect waves-light"> <i class="mdi mdi-arrow-left-bold-circle"></i> Devolver</button>';

                    var dataLigacoes = moment(dados.ligacoes[i].data_solicitacoes_ligacoes).format('DD/MM/YYYY');
                    var horaLigacoes = dados.ligacoes[i].hora_solicitacoes_ligacoes;

                    var dataHoraligacoes = '<span class="badge badge-light">' + dataLigacoes + ' às ' + horaLigacoes + '</span>';
                    var nomeOperador = '<span class="badge badge-info text-light">' + dados.ligacoes[i].nome_usuario + '</span>';

                    if (dados.ligacoes[i].operador_solicitacoes_ligacoes === idUsuario) {

                        $('#tabelaListarSolicitacoesLigacoesEfetuadas').append(

                            '<tr>' +

                            '<td class="text-center"> <h5>' + dados.ligacoes[i].id_solicitacoes_ligacoes + ' </h5></td>' +

                            '<td class="text-center"> <h5>' + dados.ligacoes[i].nome_solicitacoes_ligacoes + ' </h5></td>' +

                            '<td class="text-center"> <h5>' + dataHoraligacoes + '</h5></td>' +

                            '<td class="text-center"> <h5>' + nomeOperador + '</h5></td>' +

                            '<td class="text-center"> <h5>' + statusBotao + '</h5></td>' +

                            '<td class="text-center"> <h5>' + statusDevolver + '</h5></td>' +

                            '</tr> '

                        );

                    }
                }

            } else {

                $('#tabelaListarSolicitacoesLigacoesEfetuadas').html(
                    '<td colspan="6">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhuma ligação efetuada!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }

        }

    });
}

function devolverLigacao(devolverLigacao) {

    $.post({

        type: "POST",

        data: {
            idLigacao: dadosGlobais.ligacoes[devolverLigacao].id_solicitacoes_ligacoes,
            idOperador: "",
            statusLigacao: '0'
        },

        url: "/dashboard/site_admin/ourilandia/recepcionarLigacao",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarligacoes_efetuadas();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

            }
        }


    });

}