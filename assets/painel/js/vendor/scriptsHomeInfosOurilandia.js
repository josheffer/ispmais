informacoesLigacoes();
informacoesResidenciais();
informacoesEmpresariais();

function informacoesLigacoes() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarInfosLigacoes",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#atualizaDivLigacoes').html(
                '<a type="button" onclick="informacoesLigacoes();" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>' +
                '<a data-toggle="collapse" href="#solicitacoesLigacoes" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>'
            );

            $('#ligacoesDisponiveis').html(dados.ligacoes.pendentes);
            $('#ligacoesEmAndamento').html(dados.ligacoes.andamento);
            $('#ligacoesRealizadas').html(dados.ligacoes.realizadas);

        }

    });

}

function informacoesResidenciais() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarInfosResidenciais",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#atualizaDivResidenciais').html(
                '<a type="button" onclick="informacoesResidenciais();" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>' +
                '<a data-toggle="collapse" href="#solicitacoesResidencias" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>'
            );

            $('#residenciaisDisponiveis').html(dados.ligacoes.pendentes);
            $('#residenciaisEmAndamento').html(dados.ligacoes.andamento);
            $('#residenciaisRealizadas').html(dados.ligacoes.realizadas);

        }

    });

}

function informacoesEmpresariais() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarInfosEmpresariais",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#atualizaDivEmpresariais').html(
                '<a type="button" onclick="informacoesEmpresariais();" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>' +
                '<a data-toggle="collapse" href="#solicitacoesEmpresariais" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>'
            );

            $('#empresariaisDisponiveis').html(dados.ligacoes.pendentes);
            $('#empresariaisEmAndamento').html(dados.ligacoes.andamento);
            $('#empresariaisRealizadas').html(dados.ligacoes.realizadas);

        }

    });

}