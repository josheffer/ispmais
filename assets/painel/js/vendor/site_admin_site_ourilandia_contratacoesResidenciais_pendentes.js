var site = 'ispmais';

listarContratacoesPendentes();

function listarContratacoesPendentes() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarContratacoes_pendentes",

        ajax: 'data.json',

        dataType: 'json',

        success: function(data) {

            var dados = data;

            dadosGlobais = dados;
            idUsuario = dados.dadosUsuario;

            $('#tabelaListarContratacoesResidenciais').html('');

            $('#atualizaDiv').html('<a type="button" onclick="listarContratacoesPendentes();" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>');

            if (parseInt(dados.residenciais.length) > 0) {

                for (var i = 0; i < dados.residenciais.length; i++) {

                    var dataResidenciais = moment(dados.residenciais[i].data_solicitacoes_planoResidencial).format('DD/MM/YYYY');
                    var horaResidenciais = dados.residenciais[i].hora_solicitacoes_planoResidencial;

                    var dataHoraResidenciais = '<span class="badge badge-light">' + dataResidenciais + ' às ' + horaResidenciais + '</span>';

                    if (dados.residenciais[i].status_solicitacoes_planoResidencial === '0') {

                        var btnVerInfos = '<button type="button" class="disabled btn btn-info waves-effect waves-light"><i class="mdi mdi-eye-off-outline"></i></button>';

                        var btnRecepcionar = '<button type="button" onclick="abrirModalRecepcionar(' + i + ')" class="btn btn-block btn-xs btn-primary waves-effect waves-light">Recepcionar</button>';


                    } else {

                        if (dadosGlobais.residenciais[i].operador_solicitacoes_planoResidencial === idUsuario) {

                            var btnVerInfos = '<button type="button" onclick="abrirModalVerInfos(' + i + ')" class="btn btn-purple waves-effect waves-light"><i class="mdi mdi-eye-outline"></i></button>';

                            var btnRecepcionar = '<button type="button" onclick="abrirModalRecepcionar(' + i + ')" class="btn btn-block btn-xs btn-warning waves-effect waves-light">Em atendimento..</button>';

                        } else {

                            var btnVerInfos = '<button type="button" class="disabled btn btn-info waves-effect waves-light"><i class="mdi mdi-eye-off-outline"></i></button>';

                            var btnRecepcionar = '<a href="javascript: void(0);">' +
                                '<button type="button" class="disabled btn btn-block btn-xs btn-warning waves-effect waves-light">Em atendimento..</button>' +
                                '</a>';

                        }
                    }


                    $('#tabelaListarContratacoesResidenciais').append(

                        '<tr>' +

                        '<td class="text-center"> ' + dados.residenciais[i].id_solicitacoes_planoResidencial + ' </td>' +

                        '<td class="text-center"> ' + dados.residenciais[i].nome_solicitacoes_planoResidencial + ' </td>' +

                        '<td class="text-center"> <h5> ' + dataHoraResidenciais + ' </h5></td>' +

                        '<td class="text-center">' + btnVerInfos + '</td>' +

                        '<td class="text-center">' +
                        '<div class="button-list">' + btnRecepcionar + '</div>' +
                        '</td>' +

                        '</tr> '

                    );
                }

            } else {

                $('#tabelaListarContratacoesResidenciais').append(
                    '<td colspan="5">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhuma ligação pendente!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }

        }

    });
}

function abrirModalVerInfos(abrirModalVerInfos) {

    $('#abrirModalVerInfos').modal('show');
    $('#tituloVerInfos').html('Solicitação de contratação de plano de: <strong class="text-warning"><i><u>' + dadosGlobais.residenciais[abrirModalVerInfos].nome_solicitacoes_planoResidencial + '</u></i></strong> - Plano: <strong class="text-warning"><i><u>' + dadosGlobais.residenciais[abrirModalVerInfos].nomePlano_solicitacoes_planoResidencial + '</u></i></strong> no valor de: <strong class="text-warning"><i><u>' + dadosGlobais.residenciais[abrirModalVerInfos].valorPlano_solicitacoes_planoResidencial + '</u></i></strong> com velocidade de: <strong class="text-warning"><i><u>' + dadosGlobais.residenciais[abrirModalVerInfos].velocidadePlano_solicitacoes_planoResidencial + '</u></i></strong>');
    $('#nomeVerInfos').val(dadosGlobais.residenciais[abrirModalVerInfos].nome_solicitacoes_planoResidencial);
    $('#telefoneVerInfos').val(dadosGlobais.residenciais[abrirModalVerInfos].telefone_solicitacoes_planoResidencial);
    $('#emailVerInfos').val(dadosGlobais.residenciais[abrirModalVerInfos].email_solicitacoes_planoResidencial);
    $('#cpfVerInfos').val(dadosGlobais.residenciais[abrirModalVerInfos].cpf_solicitacoes_planoResidencial);
    $('#cepVerInfos').val(dadosGlobais.residenciais[abrirModalVerInfos].cep_solicitacoes_planoResidencial);
    $('#ruaVerInfos').val(dadosGlobais.residenciais[abrirModalVerInfos].rua_solicitacoes_planoResidencial);
    $('#complementoVerInfos').val(dadosGlobais.residenciais[abrirModalVerInfos].complemento_solicitacoes_planoResidencial);
    $('#numeroVerInfos').val(dadosGlobais.residenciais[abrirModalVerInfos].numero_solicitacoes_planoResidencial);
    $('#bairroVerInfos').val(dadosGlobais.residenciais[abrirModalVerInfos].bairro_solicitacoes_planoResidencial);
    $('#cidadeVerInfos').val(dadosGlobais.residenciais[abrirModalVerInfos].cidade_solicitacoes_planoResidencial);
    $('#estadoVerInfos').val(dadosGlobais.residenciais[abrirModalVerInfos].estado_solicitacoes_planoResidencial);
    $('#observacaoVerInfos').val(dadosGlobais.residenciais[abrirModalVerInfos].observacao_solicitacoes_planoResidencial);

}

function abrirModalRecepcionar(recepcionar) {

    if (dadosGlobais.residenciais[recepcionar].status_solicitacoes_planoResidencial === '1') {

        $('#dadosSolicitacao').html('');

        $('#abrirModalRecepcionar').modal('show');

        $('#tituloRecepcionar').html('Você está recepcionando a solicitação de contratação de: <strong><u><i><span class="text-warning">' + dadosGlobais.residenciais[recepcionar].nome_solicitacoes_planoResidencial + '</span></i></u></strong>');

        $('#mensagemRecepcionar').html(
            '<form>' +

            '<div class="form-row">' +

            '<div class="form-group col-md-4">' +
            '<label for="nomeVerInfos" class="col-form-label">Nome</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[recepcionar].nome_solicitacoes_planoResidencial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-4">' +
            '<label for="telefoneVerInfos" class="col-form-label">Telefone</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[recepcionar].telefone_solicitacoes_planoResidencial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-4">' +
            '<label for="emailVerInfos" class="col-form-label">E-mail</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[recepcionar].email_solicitacoes_planoResidencial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-2">' +
            '<label for="cpfVerInfos" class="col-form-label">Valor</label>' +
            '<input type="text" class="form-control" value="R$ ' + dadosGlobais.residenciais[recepcionar].valorPlano_solicitacoes_planoResidencial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-3">' +
            '<label for="cpfVerInfos" class="col-form-label">C.P.F</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[recepcionar].cpf_solicitacoes_planoResidencial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-2">' +
            '<label for="cepVerInfos" class="col-form-label">CEP</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[recepcionar].cep_solicitacoes_planoResidencial + '"" disabled>' +
            '</div>' +

            '<div class="form-group col-md-5">' +
            '<label for="ruaVerInfos" class="col-form-label">Rua</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[recepcionar].rua_solicitacoes_planoResidencial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-10">' +
            '<label for="complementoVerInfos" class="col-form-label">Complemento</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[recepcionar].complemento_solicitacoes_planoResidencial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-2">' +
            '<label for="numeroVerInfos" class="col-form-label">Número</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[recepcionar].numero_solicitacoes_planoResidencial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-4">' +
            '<label for="bairroVerInfos" class="col-form-label">Bairro</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[recepcionar].bairro_solicitacoes_planoResidencial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-4">' +
            '<label for="cidadeVerInfos" class="col-form-label">Cidade</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[recepcionar].cidade_solicitacoes_planoResidencial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-4">' +
            '<label for="estadoVerInfos" class="col-form-label">Estado</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[recepcionar].estado_solicitacoes_planoResidencial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-12">' +
            '<label for="observacaoVerInfos" class="col-form-label">Observação</label>' +
            '<textarea class="form-control" rows="4" value="' + dadosGlobais.residenciais[recepcionar].observacao_solicitacoes_planoResidencial + '" disabled></textarea>' +
            '</div>' +

            '</div>' +

            '</form>' +

            '<div class="row mb-3 mt-3 text-center">' +

            '<div class="col-md-3">' +
            '<button type="submit" onclick="javascript:devolverSolicitacaoContratacaoResidencial(' + recepcionar + ');" class="btn btn-block btn-purple waves-effect" >Devolver para pendentes</button>' +
            '</div>' +

            '<div class="col-md-3">' +
            '<button type="submit" onclick="javascript:naoAtendidaContratacaoResidencial(' + recepcionar + ');" class="btn btn-block btn-danger waves-effect" >Não atendida</button>' +
            '</div>' +

            '<div class="col-md-3">' +
            '<button type="submit" onclick="javascript:vendaRealizadaContratacaoResidencial(' + recepcionar + ');" class="btn btn-block btn-success waves-effect" >Venda realizada</button>' +
            '</div>' +

            '<div class="col-md-3">' +
            '<button type="submit" onclick="javascript:vendaNaoRealizadaContratacaoResidencial(' + recepcionar + ');" class="btn btn-block btn-warning waves-effect" >Venda não realizada</button>' +
            '</div>' +

            '</div>'
        );
    }

    if (dadosGlobais.residenciais[recepcionar].status_solicitacoes_planoResidencial === '0') {

        $('#abrirModalRecepcionar').modal('show');
        $('#dadosSolicitacao').html('');

        $('#tituloRecepcionar').html('Recepcionar solicitação de contratação de: <strong><u><i><span class="text-warning">' + dadosGlobais.residenciais[recepcionar].nome_solicitacoes_planoResidencial + '</span></i></u></strong>');

        $('#mensagemRecepcionar').html(
            '<h4 class="text-center">Deseja recepcionar a solicitação de contratação de <strong><u><i><span class="text-warning">' + dadosGlobais.residenciais[recepcionar].nome_solicitacoes_planoResidencial + '</span></i></u></strong> ?</h4>' +
            '<div class="col-md-12 text-center">' +
            '<br>' +
            '<button type="submit" onclick="javascript:recepcionarContratacaoResidencial(' + recepcionar + ');" class="btn btn-primary waves-effect">Sim, quero!</button>' +
            '</div>'
        );

    }


}

function recepcionarContratacaoResidencial(attRecepContrResi) {

    $.post({

        type: "POST",

        data: {
            idContratarPlanoResidencial: dadosGlobais.residenciais[attRecepContrResi].id_solicitacoes_planoResidencial,
            idOperador: idUsuario,
            statusContratarPlanoResidencial: '1'
        },

        url: "/dashboard/site_admin/ourilandia/recepcionarContratarPlanoresidencial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#mensagemRecepcionar').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                listarContratacoesPendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarContratacoesPendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#mensagemRecepcionar').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#tituloRecepcionar').html('Você está recepcionando a solicitação de contratação de: <strong><u><i><span class="text-warning">' + dadosGlobais.residenciais[attRecepContrResi].nome_solicitacoes_planoResidencial + '</span></i></u></strong>');

                $('#dadosSolicitacao').append(

                    '<form>' +

                    '<div class="form-row">' +

                    '<div class="form-group col-md-4">' +
                    '<label for="nomeVerInfos" class="col-form-label">Nome</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[attRecepContrResi].nome_solicitacoes_planoResidencial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-4">' +
                    '<label for="telefoneVerInfos" class="col-form-label">Telefone</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[attRecepContrResi].telefone_solicitacoes_planoResidencial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-4">' +
                    '<label for="emailVerInfos" class="col-form-label">E-mail</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[attRecepContrResi].email_solicitacoes_planoResidencial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-2">' +
                    '<label for="cpfVerInfos" class="col-form-label">Valor</label>' +
                    '<input type="text" class="form-control" value="R$ ' + dadosGlobais.residenciais[attRecepContrResi].valorPlano_solicitacoes_planoResidencial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-3">' +
                    '<label for="cpfVerInfos" class="col-form-label">C.P.F</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[attRecepContrResi].cpf_solicitacoes_planoResidencial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-2">' +
                    '<label for="cepVerInfos" class="col-form-label">CEP</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[attRecepContrResi].cep_solicitacoes_planoResidencial + '"" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-5">' +
                    '<label for="ruaVerInfos" class="col-form-label">Rua</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[attRecepContrResi].rua_solicitacoes_planoResidencial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-10">' +
                    '<label for="complementoVerInfos" class="col-form-label">Complemento</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[attRecepContrResi].complemento_solicitacoes_planoResidencial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-2">' +
                    '<label for="numeroVerInfos" class="col-form-label">Número</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[attRecepContrResi].numero_solicitacoes_planoResidencial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-4">' +
                    '<label for="bairroVerInfos" class="col-form-label">Bairro</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[attRecepContrResi].bairro_solicitacoes_planoResidencial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-4">' +
                    '<label for="cidadeVerInfos" class="col-form-label">Cidade</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[attRecepContrResi].cidade_solicitacoes_planoResidencial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-4">' +
                    '<label for="estadoVerInfos" class="col-form-label">Estado</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.residenciais[attRecepContrResi].estado_solicitacoes_planoResidencial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-12">' +
                    '<label for="observacaoVerInfos" class="col-form-label">Observação</label>' +
                    '<textarea class="form-control" rows="4" value="' + dadosGlobais.residenciais[attRecepContrResi].observacao_solicitacoes_planoResidencial + '" disabled></textarea>' +
                    '</div>' +

                    '</div>' +

                    '</form>' +

                    '<div class="row mb-3 mt-3 text-center">' +

                    '<div class="col-md-3">' +
                    '<button type="submit" onclick="javascript:devolverSolicitacaoContratacaoResidencial(' + attRecepContrResi + ');" class="btn btn-block btn-purple waves-effect" >Devolver para pendentes</button>' +
                    '</div>' +

                    '<div class="col-md-3">' +
                    '<button type="submit" onclick="javascript:naoAtendidaContratacaoResidencial(' + attRecepContrResi + ');" class="btn btn-block btn-danger waves-effect" >Não atendida</button>' +
                    '</div>' +

                    '<div class="col-md-3">' +
                    '<button type="submit" onclick="javascript:vendaRealizadaContratacaoResidencial(' + attRecepContrResi + ');" class="btn btn-block btn-success waves-effect" >Venda realizada</button>' +
                    '</div>' +

                    '<div class="col-md-3">' +
                    '<button type="submit" onclick="javascript:vendaNaoRealizadaContratacaoResidencial(' + attRecepContrResi + ');" class="btn btn-block btn-warning waves-effect" >Venda não realizada</button>' +
                    '</div>' +

                    '</div>'

                );


            }
        }


    });

}

function devolverSolicitacaoContratacaoResidencial(devolSolicit) {

    $.post({

        type: "POST",

        data: {
            idContratarPlanoResidencial: dadosGlobais.residenciais[devolSolicit].id_solicitacoes_planoResidencial,
            idOperador: "",
            statusContratarPlanoResidencial: '0'
        },

        url: "/dashboard/site_admin/ourilandia/recepcionarContratarPlanoresidencial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#mensagemRecepcionar').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarContratacoesPendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#abrirModalRecepcionar').modal('hide');


            }
        }


    });

}

function naoAtendidaContratacaoResidencial(naoAtendidaContratacaoResi) {

    $.post({

        type: "POST",

        data: {
            idContratarPlanoResidencial: dadosGlobais.residenciais[naoAtendidaContratacaoResi].id_solicitacoes_planoResidencial,
            idOperador: idUsuario,
            statusContratarPlanoResidencial: '2'
        },

        url: "/dashboard/site_admin/ourilandia/recepcionarContratarPlanoresidencial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#mensagemRecepcionar').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarContratacoesPendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#abrirModalRecepcionar').modal('hide');

            }
        }


    });

}

function vendaRealizadaContratacaoResidencial(vendaRealizadaLigacao) {

    $.post({

        type: "POST",

        data: {

            idContratarPlanoResidencial: dadosGlobais.residenciais[vendaRealizadaLigacao].id_solicitacoes_planoResidencial,
            idOperador: idUsuario,
            statusContratarPlanoResidencial: '3'

        },

        url: "/dashboard/site_admin/ourilandia/recepcionarContratarPlanoresidencial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroEditarLinksBarraSupFixa').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarContratacoesPendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#abrirModalRecepcionar').modal('hide');


            }
        }


    });

}

function vendaNaoRealizadaContratacaoResidencial(vendaNaoRealizadaLigacao) {

    $.post({

        type: "POST",

        data: {
            idContratarPlanoResidencial: dadosGlobais.residenciais[vendaNaoRealizadaLigacao].id_solicitacoes_planoResidencial,
            idOperador: idUsuario,
            statusContratarPlanoResidencial: '4'
        },

        url: "/dashboard/site_admin/ourilandia/recepcionarContratarPlanoresidencial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroEditarLinksBarraSupFixa').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarContratacoesPendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#abrirModalRecepcionar').modal('hide');


            }
        }


    });

}