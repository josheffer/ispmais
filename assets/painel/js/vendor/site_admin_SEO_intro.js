var site = 'ispmais';

listarConfigsSeoIntro();

function listarConfigsSeoIntro() {

    $.ajax({

        url: "/dashboard/listagens/listarConfigsSeo_intro",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            $('#css_seo_intro').val(dados.intro_seo_css);
            $('#js_seo_intro').val(dados.intro_seo_js);
            $('#jsBody_seo_intro').val(dados.intro_seo_js_body);
            $('#keywords_seo_intro').val(dados.intro_seo_keywords);
            $('#description1_seo_intro').val(dados.intro_seo_description1);
            $('#description2_seo_intro').val(dados.intro_seo_description2);
            $('#description3_seo_intro').val(dados.intro_seo_description3);
            $('#coordenadas_seo_intro').val(dados.intro_seo_coordenadasMaps);
            $('#cidadeEstado_seo_intro').val(dados.intro_seo_cidade);
            $('#estadoPais_seo_intro').val(dados.intro_seo_estado);
            $('#titulo_seo_intro').val(dados.intro_seo_tituloSite);
            $('#nome_seo_intro').val(dados.intro_seo_nomeSite);
            $('#codigoGoogle_seo_intro').val(dados.intro_seo_googleVerificacao);
            $('#corTema_seo_intro').val(dados.intro_seo_corTema);
            
            $('#verImagemDoSEOIntro').attr("src", "/assets/sites/" + site + "/img/seo/" + dados.intro_seo_imagem);
        }

    });
}

$('#formSEOIntro').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = atualizarSEO_site(self);

});

function atualizarSEO_site(dados) {

    $.ajax({

        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/site/atualizar/atualizarSEO_site",
        dataType: 'json',
        enctype: 'multipart/form-data',
        data: new FormData($('#formSEOIntro')[0]),
        cache: false,
        contentType: false,
        processData: false,

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarConfigsSeo();

            }
        }
    });
}