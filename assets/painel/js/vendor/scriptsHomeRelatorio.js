listarDadosCharts();
listarDadosChartsGeral();
listarValores();

function listarDadosChartsGeral() {

    $.ajax({
        type: "POST",

        url: "/dashboard/site_admin/ourilandia/listarRelatorioChatjsGeral",

        ajax: 'data.json',

        success: function(data) {

            $('#atualizaDivGraficoOurilandiaGeral').html(
                '<a type="button" onclick="listarDadosChartsGeral();" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>'
            );

            var dados = JSON.parse(data);

            var ligacoes = dados.ligacoes;
            var residenciais = dados.residenciais;
            var empresariais = dados.empresariais;

            // =======================================
            var ligacoesNaoAtendida = [
                ligacoes[1].janeiro_naoAtendida,
                ligacoes[2].fevereiro_naoAtendida,
                ligacoes[3].marco_naoAtendida,
                ligacoes[4].abril_naoAtendida,
                ligacoes[5].maio_naoAtendida,
                ligacoes[6].junho_naoAtendida,
                ligacoes[7].julho_naoAtendida,
                ligacoes[8].agosto_naoAtendida,
                ligacoes[9].setembro_naoAtendida,
                ligacoes[10].outubro_naoAtendida,
                ligacoes[11].novembro_naoAtendida,
                ligacoes[12].dezembro_naoAtendida,
            ];

            var ligacoesVendaRealizada = [
                ligacoes[1].janeiro_vendaRealizada,
                ligacoes[2].fevereiro_vendaRealizada,
                ligacoes[3].marco_vendaRealizada,
                ligacoes[4].abril_vendaRealizada,
                ligacoes[5].maio_vendaRealizada,
                ligacoes[6].junho_vendaRealizada,
                ligacoes[7].julho_vendaRealizada,
                ligacoes[8].agosto_vendaRealizada,
                ligacoes[9].setembro_vendaRealizada,
                ligacoes[10].outubro_vendaRealizada,
                ligacoes[11].novembro_vendaRealizada,
                ligacoes[12].dezembro_vendaRealizada,
            ];

            var ligacoesVendaNaoRealizada = [
                ligacoes[1].janeiro_vendaNaoRealizada,
                ligacoes[2].fevereiro_vendaNaoRealizada,
                ligacoes[3].marco_vendaNaoRealizada,
                ligacoes[4].abril_vendaNaoRealizada,
                ligacoes[5].maio_vendaNaoRealizada,
                ligacoes[6].junho_vendaNaoRealizada,
                ligacoes[7].julho_vendaNaoRealizada,
                ligacoes[8].agosto_vendaNaoRealizada,
                ligacoes[9].setembro_vendaNaoRealizada,
                ligacoes[10].outubro_vendaNaoRealizada,
                ligacoes[11].novembro_vendaNaoRealizada,
                ligacoes[12].dezembro_vendaNaoRealizada,
            ];
            // =======================================

            // =======================================
            var residenciaisNaoAtendida = [
                residenciais[1].janeiro_naoAtendida,
                residenciais[2].fevereiro_naoAtendida,
                residenciais[3].marco_naoAtendida,
                residenciais[4].abril_naoAtendida,
                residenciais[5].maio_naoAtendida,
                residenciais[6].junho_naoAtendida,
                residenciais[7].julho_naoAtendida,
                residenciais[8].agosto_naoAtendida,
                residenciais[9].setembro_naoAtendida,
                residenciais[10].outubro_naoAtendida,
                residenciais[11].novembro_naoAtendida,
                residenciais[12].dezembro_naoAtendida,
            ];

            var residenciaisVendaRealizada = [
                residenciais[1].janeiro_vendaRealizada,
                residenciais[2].fevereiro_vendaRealizada,
                residenciais[3].marco_vendaRealizada,
                residenciais[4].abril_vendaRealizada,
                residenciais[5].maio_vendaRealizada,
                residenciais[6].junho_vendaRealizada,
                residenciais[7].julho_vendaRealizada,
                residenciais[8].agosto_vendaRealizada,
                residenciais[9].setembro_vendaRealizada,
                residenciais[10].outubro_vendaRealizada,
                residenciais[11].novembro_vendaRealizada,
                residenciais[12].dezembro_vendaRealizada,
            ];

            var residenciaisVendaNaoRealizada = [
                residenciais[1].janeiro_vendaNaoRealizada,
                residenciais[2].fevereiro_vendaNaoRealizada,
                residenciais[3].marco_vendaNaoRealizada,
                residenciais[4].abril_vendaNaoRealizada,
                residenciais[5].maio_vendaNaoRealizada,
                residenciais[6].junho_vendaNaoRealizada,
                residenciais[7].julho_vendaNaoRealizada,
                residenciais[8].agosto_vendaNaoRealizada,
                residenciais[9].setembro_vendaNaoRealizada,
                residenciais[10].outubro_vendaNaoRealizada,
                residenciais[11].novembro_vendaNaoRealizada,
                residenciais[12].dezembro_vendaNaoRealizada,
            ];
            // =======================================

            // =======================================
            var empresariaisNaoAtendida = [
                empresariais[1].janeiro_naoAtendida,
                empresariais[2].fevereiro_naoAtendida,
                empresariais[3].marco_naoAtendida,
                empresariais[4].abril_naoAtendida,
                empresariais[5].maio_naoAtendida,
                empresariais[6].junho_naoAtendida,
                empresariais[7].julho_naoAtendida,
                empresariais[8].agosto_naoAtendida,
                empresariais[9].setembro_naoAtendida,
                empresariais[10].outubro_naoAtendida,
                empresariais[11].novembro_naoAtendida,
                empresariais[12].dezembro_naoAtendida,
            ];

            var empresariaisVendaRealizada = [
                empresariais[1].janeiro_vendaRealizada,
                empresariais[2].fevereiro_vendaRealizada,
                empresariais[3].marco_vendaRealizada,
                empresariais[4].abril_vendaRealizada,
                empresariais[5].maio_vendaRealizada,
                empresariais[6].junho_vendaRealizada,
                empresariais[7].julho_vendaRealizada,
                empresariais[8].agosto_vendaRealizada,
                empresariais[9].setembro_vendaRealizada,
                empresariais[10].outubro_vendaRealizada,
                empresariais[11].novembro_vendaRealizada,
                empresariais[12].dezembro_vendaRealizada,
            ];

            var empresariaisVendaNaoRealizada = [
                empresariais[1].janeiro_vendaNaoRealizada,
                empresariais[2].fevereiro_vendaNaoRealizada,
                empresariais[3].marco_vendaNaoRealizada,
                empresariais[4].abril_vendaNaoRealizada,
                empresariais[5].maio_vendaNaoRealizada,
                empresariais[6].junho_vendaNaoRealizada,
                empresariais[7].julho_vendaNaoRealizada,
                empresariais[8].agosto_vendaNaoRealizada,
                empresariais[9].setembro_vendaNaoRealizada,
                empresariais[10].outubro_vendaNaoRealizada,
                empresariais[11].novembro_vendaNaoRealizada,
                empresariais[12].dezembro_vendaNaoRealizada,
            ];
            // =======================================

            now = new Date;

            var ctx = document.getElementsByClassName('grafico-bar-geral-Ourilandia');

            var chartGraph = new Chart(ctx, {

                type: 'bar',

                data: {
                    labels: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],

                    datasets: [{
                            label: "Ligações não atendidas",
                            data: ligacoesNaoAtendida,
                            borderWidth: 1,
                            borderColor: 'rgba(28,28,287,1)',
                            backgroundColor: 'rgba(28,28,287,0.4)'

                        },
                        {
                            label: "Ligações vendas não realizadas",
                            data: ligacoesVendaNaoRealizada,
                            borderWidth: 1,
                            borderColor: 'rgba(220,220,220,1)',
                            backgroundColor: 'rgba(220,220,220,0.4)'

                        },
                        {
                            label: "Ligações vendas realizadas",
                            data: ligacoesVendaRealizada,
                            borderWidth: 1,
                            borderColor: 'rgba(79,79,79,1)',
                            backgroundColor: 'rgba(79,79,79,0.4)'

                        },
                        {
                            label: "Residenciais não atendidas",
                            data: residenciaisNaoAtendida,
                            borderWidth: 1,
                            borderColor: 'rgba(72,61,139,1)',
                            backgroundColor: 'rgba(72,61,139,0.4)'

                        },
                        {
                            label: "Residenciais vendas não realizadas",
                            data: residenciaisVendaNaoRealizada,
                            borderWidth: 1,
                            borderColor: 'rgba(105,89,205,1)',
                            backgroundColor: 'rgba(105,89,205,0.4)'

                        },
                        {
                            label: "Residenciais vendas realizadas",
                            data: residenciaisVendaRealizada,
                            borderWidth: 1,
                            borderColor: 'rgba(131,111,255,1)',
                            backgroundColor: 'rgba(131,111,255,0.4)'

                        },
                        {
                            label: "Empresariais não atendidas",
                            data: empresariaisNaoAtendida,
                            borderWidth: 1,
                            borderColor: 'rgba(143,188,143,1)',
                            backgroundColor: 'rgba(143,188,143,0.4)'

                        },
                        {
                            label: "Empresariais vendas não realizadas",
                            data: empresariaisVendaNaoRealizada,
                            borderWidth: 1,
                            borderColor: 'rgba(46,139,87,1)',
                            backgroundColor: 'rgba(46,139,87,0.4)'

                        },
                        {
                            label: "Empresariais vendas realizadas",
                            data: empresariaisVendaRealizada,
                            borderWidth: 1,
                            borderColor: 'rgba(0,100,0,1)',
                            backgroundColor: 'rgba(0,100,0,0.4)'

                        }
                    ]
                },

                options: {
                    title: {
                        display: true,
                        fontColor: '#ffffff',
                        fontSize: 15,
                        text: "RELATÓRIO ANUAL " + now.getFullYear() + " - OURILÂNDIA DO NORTE"
                    },
                    labels: {
                        fontStyle: "bold",
                    },
                    responsive: true,
                    maintainAspectRatio: false
                }

            });

        }
    });

}

function listarDadosCharts() {

    $.ajax({
        type: "POST",

        url: "/dashboard/site_admin/ourilandia/listarRelatorioChatjs",

        ajax: 'data.json',

        success: function(data) {

            $('#atualizaDivGraficoOurilandia').html(
                '<a type="button" onclick="listarDadosCharts();" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>'
            );

            var dados = JSON.parse(data);

            var ligacoes = dados.ligacoes;
            var residenciais = dados.residenciais;
            var empresariais = dados.empresariais;

            // =======================================
            var ligacoesNaoAtendida = [
                ligacoes[1].janeiro_naoAtendida,
                ligacoes[2].fevereiro_naoAtendida,
                ligacoes[3].marco_naoAtendida,
                ligacoes[4].abril_naoAtendida,
                ligacoes[5].maio_naoAtendida,
                ligacoes[6].junho_naoAtendida,
                ligacoes[7].julho_naoAtendida,
                ligacoes[8].agosto_naoAtendida,
                ligacoes[9].setembro_naoAtendida,
                ligacoes[10].outubro_naoAtendida,
                ligacoes[11].novembro_naoAtendida,
                ligacoes[12].dezembro_naoAtendida,
            ];

            var ligacoesVendaRealizada = [
                ligacoes[1].janeiro_vendaRealizada,
                ligacoes[2].fevereiro_vendaRealizada,
                ligacoes[3].marco_vendaRealizada,
                ligacoes[4].abril_vendaRealizada,
                ligacoes[5].maio_vendaRealizada,
                ligacoes[6].junho_vendaRealizada,
                ligacoes[7].julho_vendaRealizada,
                ligacoes[8].agosto_vendaRealizada,
                ligacoes[9].setembro_vendaRealizada,
                ligacoes[10].outubro_vendaRealizada,
                ligacoes[11].novembro_vendaRealizada,
                ligacoes[12].dezembro_vendaRealizada,
            ];

            var ligacoesVendaNaoRealizada = [
                ligacoes[1].janeiro_vendaNaoRealizada,
                ligacoes[2].fevereiro_vendaNaoRealizada,
                ligacoes[3].marco_vendaNaoRealizada,
                ligacoes[4].abril_vendaNaoRealizada,
                ligacoes[5].maio_vendaNaoRealizada,
                ligacoes[6].junho_vendaNaoRealizada,
                ligacoes[7].julho_vendaNaoRealizada,
                ligacoes[8].agosto_vendaNaoRealizada,
                ligacoes[9].setembro_vendaNaoRealizada,
                ligacoes[10].outubro_vendaNaoRealizada,
                ligacoes[11].novembro_vendaNaoRealizada,
                ligacoes[12].dezembro_vendaNaoRealizada,
            ];
            // =======================================

            // =======================================
            var residenciaisNaoAtendida = [
                residenciais[1].janeiro_naoAtendida,
                residenciais[2].fevereiro_naoAtendida,
                residenciais[3].marco_naoAtendida,
                residenciais[4].abril_naoAtendida,
                residenciais[5].maio_naoAtendida,
                residenciais[6].junho_naoAtendida,
                residenciais[7].julho_naoAtendida,
                residenciais[8].agosto_naoAtendida,
                residenciais[9].setembro_naoAtendida,
                residenciais[10].outubro_naoAtendida,
                residenciais[11].novembro_naoAtendida,
                residenciais[12].dezembro_naoAtendida,
            ];

            var residenciaisVendaRealizada = [
                residenciais[1].janeiro_vendaRealizada,
                residenciais[2].fevereiro_vendaRealizada,
                residenciais[3].marco_vendaRealizada,
                residenciais[4].abril_vendaRealizada,
                residenciais[5].maio_vendaRealizada,
                residenciais[6].junho_vendaRealizada,
                residenciais[7].julho_vendaRealizada,
                residenciais[8].agosto_vendaRealizada,
                residenciais[9].setembro_vendaRealizada,
                residenciais[10].outubro_vendaRealizada,
                residenciais[11].novembro_vendaRealizada,
                residenciais[12].dezembro_vendaRealizada,
            ];

            var residenciaisVendaNaoRealizada = [
                residenciais[1].janeiro_vendaNaoRealizada,
                residenciais[2].fevereiro_vendaNaoRealizada,
                residenciais[3].marco_vendaNaoRealizada,
                residenciais[4].abril_vendaNaoRealizada,
                residenciais[5].maio_vendaNaoRealizada,
                residenciais[6].junho_vendaNaoRealizada,
                residenciais[7].julho_vendaNaoRealizada,
                residenciais[8].agosto_vendaNaoRealizada,
                residenciais[9].setembro_vendaNaoRealizada,
                residenciais[10].outubro_vendaNaoRealizada,
                residenciais[11].novembro_vendaNaoRealizada,
                residenciais[12].dezembro_vendaNaoRealizada,
            ];
            // =======================================

            // =======================================
            var empresariaisNaoAtendida = [
                empresariais[1].janeiro_naoAtendida,
                empresariais[2].fevereiro_naoAtendida,
                empresariais[3].marco_naoAtendida,
                empresariais[4].abril_naoAtendida,
                empresariais[5].maio_naoAtendida,
                empresariais[6].junho_naoAtendida,
                empresariais[7].julho_naoAtendida,
                empresariais[8].agosto_naoAtendida,
                empresariais[9].setembro_naoAtendida,
                empresariais[10].outubro_naoAtendida,
                empresariais[11].novembro_naoAtendida,
                empresariais[12].dezembro_naoAtendida,
            ];

            var empresariaisVendaRealizada = [
                empresariais[1].janeiro_vendaRealizada,
                empresariais[2].fevereiro_vendaRealizada,
                empresariais[3].marco_vendaRealizada,
                empresariais[4].abril_vendaRealizada,
                empresariais[5].maio_vendaRealizada,
                empresariais[6].junho_vendaRealizada,
                empresariais[7].julho_vendaRealizada,
                empresariais[8].agosto_vendaRealizada,
                empresariais[9].setembro_vendaRealizada,
                empresariais[10].outubro_vendaRealizada,
                empresariais[11].novembro_vendaRealizada,
                empresariais[12].dezembro_vendaRealizada,
            ];

            var empresariaisVendaNaoRealizada = [
                empresariais[1].janeiro_vendaNaoRealizada,
                empresariais[2].fevereiro_vendaNaoRealizada,
                empresariais[3].marco_vendaNaoRealizada,
                empresariais[4].abril_vendaNaoRealizada,
                empresariais[5].maio_vendaNaoRealizada,
                empresariais[6].junho_vendaNaoRealizada,
                empresariais[7].julho_vendaNaoRealizada,
                empresariais[8].agosto_vendaNaoRealizada,
                empresariais[9].setembro_vendaNaoRealizada,
                empresariais[10].outubro_vendaNaoRealizada,
                empresariais[11].novembro_vendaNaoRealizada,
                empresariais[12].dezembro_vendaNaoRealizada,
            ];
            // =======================================

            now = new Date;

            var ctx = document.getElementsByClassName('line-chart');

            var chartGraph = new Chart(ctx, {

                type: 'bar',

                data: {
                    labels: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],

                    datasets: [{
                            label: "Ligações não atendidas",
                            data: ligacoesNaoAtendida,
                            borderWidth: 1,
                            borderColor: 'rgba(28,28,287,1)',
                            backgroundColor: 'rgba(28,28,287,0.4)'

                        },
                        {
                            label: "Ligações vendas não realizadas",
                            data: ligacoesVendaNaoRealizada,
                            borderWidth: 1,
                            borderColor: 'rgba(220,220,220,1)',
                            backgroundColor: 'rgba(220,220,220,0.4)'

                        },
                        {
                            label: "Ligações vendas realizadas",
                            data: ligacoesVendaRealizada,
                            borderWidth: 1,
                            borderColor: 'rgba(79,79,79,1)',
                            backgroundColor: 'rgba(79,79,79,0.4)'

                        },
                        {
                            label: "Residenciais não atendidas",
                            data: residenciaisNaoAtendida,
                            borderWidth: 1,
                            borderColor: 'rgba(72,61,139,1)',
                            backgroundColor: 'rgba(72,61,139,0.4)'

                        },
                        {
                            label: "Residenciais vendas não realizadas",
                            data: residenciaisVendaNaoRealizada,
                            borderWidth: 1,
                            borderColor: 'rgba(105,89,205,1)',
                            backgroundColor: 'rgba(105,89,205,0.4)'

                        },
                        {
                            label: "Residenciais vendas realizadas",
                            data: residenciaisVendaRealizada,
                            borderWidth: 1,
                            borderColor: 'rgba(131,111,255,1)',
                            backgroundColor: 'rgba(131,111,255,0.4)'

                        },
                        {
                            label: "Empresariais não atendidas",
                            data: empresariaisNaoAtendida,
                            borderWidth: 1,
                            borderColor: 'rgba(143,188,143,1)',
                            backgroundColor: 'rgba(143,188,143,0.4)'

                        },
                        {
                            label: "Empresariais vendas não realizadas",
                            data: empresariaisVendaNaoRealizada,
                            borderWidth: 1,
                            borderColor: 'rgba(46,139,87,1)',
                            backgroundColor: 'rgba(46,139,87,0.4)'

                        },
                        {
                            label: "Empresariais vendas realizadas",
                            data: empresariaisVendaRealizada,
                            borderWidth: 1,
                            borderColor: 'rgba(0,100,0,1)',
                            backgroundColor: 'rgba(0,100,0,0.4)'

                        }
                    ]
                },

                options: {
                    title: {
                        display: true,
                        fontColor: '#ffffff',
                        fontSize: 15,
                        text: "RELATÓRIO ANUAL " + now.getFullYear() + " - OURILÂNDIA DO NORTE"
                    },
                    labels: {
                        fontStyle: "bold",
                    },
                    responsive: true,
                    maintainAspectRatio: false
                }

            });

        }
    });

}

function listarValores() {

    $.ajax({
        type: "POST",

        url: "/dashboard/site_admin/ourilandia/listarValores",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            console.log(dados);

            $('#divAtualizarValores').html(
                '<a type="button" onclick="listarValores();" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>'
            );

            var ligacoesPerdidas = dados.ligacoesPerdidas[0].totalLigacoesPerdidas;
            var ligacoesVendas = dados.ligacoesVendas[0].totalLigacoesVendas;
            var residenciaisPerdidas = dados.residenciaisPerdidas[0].totalResidenciaisPerdidas;
            var residenciaisVendas = dados.residenciaisVendas[0].totalResidenciaisVendas;
            var empresariaisPerdidas = dados.empresariaisPerdidas[0].totalEmpresariaisPerdidas;
            var empresariaisVendas = dados.empresariaisVendas[0].totalEmpresariaisVendas;

            if (ligacoesPerdidas        === null) { $('#ligacoesPerdidas').html('0.00'); }      else { $('#ligacoesPerdidas').html(ligacoesPerdidas); }
            if (ligacoesVendas          === null) { $('#ligacoesVendas').html('0.00'); }        else { $('#ligacoesVendas').html(ligacoesVendas); }
            if (residenciaisPerdidas    === null) { $('#residenciaisPerdidas').html('0.00'); }  else { $('#residenciaisPerdidas').html(residenciaisPerdidas); }
            if (residenciaisVendas      === null) { $('#residenciaisVendas').html('0.00'); }    else { $('#residenciaisVendas').html(residenciaisVendas); }
            if (empresariaisPerdidas    === null) { $('#empresariaisPerdidas').html('0.00'); }  else { $('#empresariaisPerdidas').html(empresariaisPerdidas); }
            if (empresariaisVendas      === null) { $('#empresariaisVendas').html('0.00'); }    else { $('#empresariaisVendas').html(empresariaisVendas); }
            
        }
    });

}