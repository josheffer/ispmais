// $(document).ready(function() {
//     $("#menuNavegacaoPainel").delay(2000).slideUp("slow", function(){$(this).collapse('hide');});
// });

var site = 'ispmais';

listarConfigsSeo();
listarAvisoGlobal();
listarInfosBarraSuperiorFixa();
listarLinksBarraSuperiorFixa();
listarSlidesCarousel();
listarCoberturaAtendimento();
listarPlanosResidenciais();
listarPlanosEmpresariais();

function listarConfigsSeo() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarConfigsSeo",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#css_seo_ourilandia').val(dados.seo_css);
            $('#js_seo_ourilandia').val(dados.seo_js);
            $('#jsBody_seo_ourilandia').val(dados.seo_js_body);
            $('#keywords_seo_ourilandia').val(dados.seo_keywords);
            $('#description1_seo_ourilandia').val(dados.seo_description1);
            $('#description2_seo_ourilandia').val(dados.seo_description2);
            $('#description3_seo_ourilandia').val(dados.seo_description3);
            $('#coordenadas_seo_ourilandia').val(dados.seo_coordenadasMaps);
            $('#cidadeEstado_seo_ourilandia').val(dados.seo_cidade);
            $('#estadoPais_seo_ourilandia').val(dados.seo_estado);
            $('#titulo_seo_ourilandia').val(dados.seo_tituloSite);
            $('#nome_seo_ourilandia').val(dados.seo_nomeSite);
            $('#corTema_seo_ourilandia').val(dados.seo_corTema);
            $('#codigoGoogle_seo_ourilandia').val(dados.seo_googleVerificacao);

            $('#verImagemDoSEO').attr("src", "/assets/sites/" + site + "/img/seo/" + dados.seo_imagem);
        }

    });
}

$('#formAttAConfigSEO').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = atualizarSEO_ourilandia(self);

});

function atualizarSEO_ourilandia(dados) {

    $.ajax({

        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/atualizarSEO_ourilandia",
        dataType: 'json',
        enctype: 'multipart/form-data',
        data: new FormData($('#formAttAConfigSEO')[0]),
        cache: false,
        contentType: false,
        processData: false,

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarConfigsSeo();

            }
        }
    });
}

function listarAvisoGlobal() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarAvisoGlobal",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#textoAvisoGlobal').val(dados.texto_aviso);
            if (dados.ativo_aviso === '1') { $('#statusAvisoGlobalOurilandia').val(1).attr("checked", "checked"); } else { $('#statusAvisoGlobalOurilandia').val(1); }

        }

    });
}

function listarInfosBarraSuperiorFixa() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarInfosBarraSuperiorFixa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#mensagem_BarraSupFixa').val(dados.mensagem_barraSuperiorFixa);
            $('#email_BarraSupFixa').val(dados.email_barraSuperiorFixa);
            $('#telefone_BarraSupFixa').val(dados.telefone_barraSuperiorFixa);
            $('#facebook_BarraSupFixa').val(dados.facebook_barraSuperiorFixa);
            $('#instagram_BarraSupFixa').val(dados.instagram_barraSuperiorFixa);

            if (dados.ativo_minhaConta_barraSuperiorFixa === '1') { $('#ativoMinhaConta_BarraSupFixa').val(1).attr("checked", "checked"); } else { $('#ativoMinhaConta_BarraSupFixa').val(1); }
            if (dados.ativo_facebook_barraSuperiorFixa === '1') { $('#ativoFacebook_BarraSupFixa').val(1).attr("checked", "checked"); } else { $('#ativoFacebook_BarraSupFixa').val(1); }
            if (dados.ativo_instagram_barraSuperiorFixa === '1') { $('#ativoInstagram_BarraSupFixa').val(1).attr("checked", "checked"); } else { $('#ativoInstagram_BarraSupFixa').val(1); }

        }

    });
}

$('#formAttAvisoGlobal').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = atualizarAvisoGlobal_ourilandia(self);

});

function atualizarAvisoGlobal_ourilandia(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/atualizarAvisoGlobal_ourilandia",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#accordionAvisoGlobal').collapse('hide');

            }
        }
    });
}

$('#formAttBarraSupFixa').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = atualizarBarraSupFixa_ourilandia(self);

});

function atualizarBarraSupFixa_ourilandia(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/atualizarBarraSupFixa_ourilandia",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#barraFixaSuperior').collapse('hide');

            }
        }
    });
}

$('#formNovoLinkBarraSuperior').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = cadastrarNovoLinkBarraSupFixa(self);

});

function cadastrarNovoLinkBarraSupFixa(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/cadastrarNovoLinkBarraSupFixa",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarLinksBarraSuperiorFixa();

                $('#formNovoLinkBarraSuperior').each(function() {
                    this.reset();
                });

            }
        }
    });
}

function listarLinksBarraSuperiorFixa() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarLinksBarraSuperiorFixa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            dadosGlobaisLinksBarraSupFixa = dados;

            $('#tabelaLinksBarraSupFixa').html('');

            if (dados.length > 0) {

                for (var i = 0; i < dados.length; i++) {

                    if (dados[i].status_links_barraSuperiorFixa === '1') {

                        var btnStatusLinksBarraSupFixa = '<button type="submit" onclick="javascript:liberarBloquearStatusLinksBarraSupFixa(' + i + ');" class="btn btn-block btn-xs btn-purple waves-effect waves-light">Ativado</button>';

                    } else {

                        var btnStatusLinksBarraSupFixa = '<button type="submit" onclick="javascript:liberarBloquearStatusLinksBarraSupFixa(' + i + ');"  class="btn btn-block btn-xs btn-danger waves-effect waves-light">Desativado</button>';

                    }

                    if (dados[i].target_links_barraSuperiorFixa === '1') {

                        var novaAba = '<button type="submit" onclick="javascript:abrirLinkAba(' + i + ');" class="btn btn-block btn-xs bg-soft-red waves-effect waves-light">Sim</button>';

                    } else {

                        var novaAba = '<button type="submit" onclick="javascript:abrirLinkAba(' + i + ');"  class="btn btn-block btn-xs bg-secondary text-light waves-effect waves-light"><strong>Não</strong></button>';

                    }

                    $('#tabelaLinksBarraSupFixa').append(

                        '<tr>' +
                        '<td>' + dados[i].id_links_barraSuperiorFixa + '</td>' +
                        '<td>' + dados[i].nome_links_barraSuperiorFixa + '</td>' +
                        '<td>' + novaAba + '</td>' +
                        '<td>' + btnStatusLinksBarraSupFixa + '</td>' +
                        '<td>' +
                        '<div class="button-list">' +
                        '<button type="button" onclick="javascript:editarDadosLinkBarraSupFixa(' + i + ');" class="btn btn-xs btn-info waves-effect waves-light"><i class=" mdi mdi-lead-pencil"></i></button>' +
                        '<button type="button" onclick="javascript:ExcluirLinkBarraSupFixa(' + i + ');" class="btn btn-xs btn-danger waves-effect waves-light"><i class="mdi mdi-delete"></i></button>' +
                        '</div>' +
                        '</td>' +
                        '</tr> '

                    );
                }

            } else {

                $('#tabelaLinksBarraSupFixa').append(
                    '<td colspan="4">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhum link cadastrado!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }

        }

    });
}

function liberarBloquearStatusLinksBarraSupFixa(statusLinksBarraSupFixa) {

    $.post({
        type: "POST",

        data: {
            id_link: dadosGlobaisLinksBarraSupFixa[statusLinksBarraSupFixa].id_links_barraSuperiorFixa
        },

        url: "/dashboard/site_admin/ourilandia/liberarBloquearStatusLinksBarraSupFixa",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarLinksBarraSuperiorFixa();

            }
        }
    });

}

function abrirLinkAba(alterarAbrirAba) {

    $.post({
        type: "POST",

        data: {
            id_link: dadosGlobaisLinksBarraSupFixa[alterarAbrirAba].id_links_barraSuperiorFixa
        },

        url: "/dashboard/site_admin/ourilandia/abrirLinkAba_ourilandia",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarLinksBarraSuperiorFixa();

            }
        }
    });

}

function editarDadosLinkBarraSupFixa(attLinksBarraSupFixa) {

    $('#formAlterarLinksBarraSupFixa').each(function() {
        this.reset();
    });

    showModalEditarDadosLinkBarraSupFixa(attLinksBarraSupFixa);
}

function showModalEditarDadosLinkBarraSupFixa(attLinksBarraSupFixa) {

    $('#alterarInfosLinksBarraSupFixa').modal('show');

    $("#idLinksBarraSupFixa").val(dadosGlobaisLinksBarraSupFixa[attLinksBarraSupFixa].id_links_barraSuperiorFixa);
    $("#alterarNomeLinksBarraSupFixa").val(dadosGlobaisLinksBarraSupFixa[attLinksBarraSupFixa].nome_links_barraSuperiorFixa);
    $("#alterarLinksBarraSupFixa").val(dadosGlobaisLinksBarraSupFixa[attLinksBarraSupFixa].link_links_barraSuperiorFixa);
    $("#alterarTituloNomeLink").html(dadosGlobaisLinksBarraSupFixa[attLinksBarraSupFixa].nome_links_barraSuperiorFixa);

}

$('#formAlterarLinksBarraSupFixa').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = atualizarLinksBarraSuperiorFixa(self);
});

function atualizarLinksBarraSuperiorFixa(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/atualizarLinksBarraSuperiorFixa",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroEditarLinksBarraSupFixa').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#formAlterarLinksBarraSupFixa').each(function() {
                    this.reset();
                });

                $('#alterarInfosLinksBarraSupFixa').modal('hide');

                listarLinksBarraSuperiorFixa();

            }
        }
    });
}

function ExcluirLinkBarraSupFixa(excluirLinksBarraSuperiorFixa) {

    $('#formExcluirBarraSuperiorFixa').each(function() {
        this.reset();
    });

    showModalExcluirLinkBarraSupFixa(excluirLinksBarraSuperiorFixa);
}

function showModalExcluirLinkBarraSupFixa(excluirLinksBarraSuperiorFixa) {

    $('#ModalExcluirLinksBarraSuperiorFixa').modal('show');

    $("#idExcluirBarraSuperiorFixa").val(dadosGlobaisLinksBarraSupFixa[excluirLinksBarraSuperiorFixa].id_links_barraSuperiorFixa);
    $("#excluirNomeBarraSuperiorFixa").html(dadosGlobaisLinksBarraSupFixa[excluirLinksBarraSuperiorFixa].nome_links_barraSuperiorFixa);
    $("#tituloExcluirLinkBarraSuperiorFixa").html(dadosGlobaisLinksBarraSupFixa[excluirLinksBarraSuperiorFixa].nome_links_barraSuperiorFixa);

}

$('#formExcluirBarraSuperiorFixa').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = excluirLinkBarraSuperiorFixa(self);
});

function excluirLinkBarraSuperiorFixa(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/excluirLinkBarraSuperiorFixa",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgERROExcluirLinksBarraSuperiorFixa').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#formExcluirBarraSuperiorFixa').each(function() {
                    this.reset();
                });

                $('#ModalExcluirLinksBarraSuperiorFixa').modal('hide');

                listarLinksBarraSuperiorFixa();

            }
        }
    });
}

$('#formCadastrarNovoSlide').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    var retorno = cadastrarNovoSlide(self);
});

function cadastrarNovoSlide(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/cadastrarNovoSlide",
        dataType: 'json',
        data: new FormData($('#formCadastrarNovoSlide')[0]),
        cache: false,
        contentType: false,
        processData: false,

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#formCadastrarNovoSlide').each(function() {
                    this.reset();
                });

                listarSlidesCarousel();

            }
        }
    });
}

function listarSlidesCarousel() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarSlidesCarousel",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            dadosGlobaisSlideCarousel = dados;

            $('#tabelaListarSlideCarousel').html('');

            if (dados.length > 0) {

                for (var i = 0; i < dados.length; i++) {

                    if (dados[i].ativo_carousel === '1') {

                        var btnStatusBannerSlide = '<button type="submit" onclick="javascript:liberarStatusItemCarouselSlide(' + i + ');" class="btn btn-block btn-xs btn-purple waves-effect waves-light">Ativado</button>';

                    } else {

                        var btnStatusBannerSlide = '<button type="submit" onclick="javascript:liberarStatusItemCarouselSlide(' + i + ');"  class="btn btn-block btn-xs btn-danger waves-effect waves-light">Desativado</button>';

                    }

                    $('#tabelaListarSlideCarousel').append(

                        '<tr>' +

                        '<td> ' +
                        '<h5 class="m-0 font-weight-normal"> ' + dados[i].id_carousel + ' </h5> ' +
                        '</td>' +

                        '<td>' +
                        '<a href="javascript: void(0);" onclick="javascript:abrirModalVerImagemCarouselSlide(' + i + ');" class="btn btn-xs btn-warning mr-1"><i class="fas fa-file-image"></i> Ver imagem</a>' +
                        '</td>' +

                        '<td>' + btnStatusBannerSlide + '</td>' +

                        '<td>' +

                        '<a href="javascript: void(0);" onclick="javascript:editarImagemCarouselSlide(' + i + ');" class="btn btn-xs btn-primary mr-1">' +
                        '<i class="mdi mdi-pencil-outline"></i>' +
                        '</a>' +

                        '<a href="javascript: void(0);" onclick="javascript:deletarImagemCarouselSlide(' + i + ');" class="btn btn-xs btn-danger">' +
                        '<i class="fe-trash-2"></i>' +
                        '</a>' +

                        '</td>' +

                        '</tr>'

                    );
                }

            } else {

                $('#tabelaListarSlideCarousel').append(
                    '<td colspan="4">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhum banner cadastrado!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }

        }

    });
}

function abrirModalVerImagemCarouselSlide(verItemSlideCarousel) {

    $('#modalVerImagemSlideCarousel').modal('show');

    $("#idItemSlideCarousel").html("#" + dadosGlobaisSlideCarousel[verItemSlideCarousel].id_carousel);
    $('#imagemSlideModal').attr("src", "/assets/sites/" + site + "/img/banners/" + dadosGlobaisSlideCarousel[verItemSlideCarousel].imagem_carousel);

}

function liberarStatusItemCarouselSlide(liberarItemCarousel) {

    $.post({

        type: "POST",

        data: {
            id_itemCarouselSlide: dadosGlobaisSlideCarousel[liberarItemCarousel].id_carousel
        },

        url: "/dashboard/site_admin/ourilandia/liberarStatusItemCarouselSlide",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarSlidesCarousel();

            }
        }
    });

}

function editarImagemCarouselSlide(editarItemCarouselSlide) {

    $('#modalVEditarItemCarousel').modal('show');

    $("#tituloIdItemSlideCarousel").html("#" + dadosGlobaisSlideCarousel[editarItemCarouselSlide].id_carousel);
    $("#editarIdItemSlide").val(dadosGlobaisSlideCarousel[editarItemCarouselSlide].id_carousel);

    $("#descricaoEditarItemCarousel").val(dadosGlobaisSlideCarousel[editarItemCarouselSlide].descricao_carousel);
    $('#imagemSlideModalEditar').attr("src", "/assets/sites/" + site + "/img/banners/" + dadosGlobaisSlideCarousel[editarItemCarouselSlide].imagem_carousel);

}

$('#formEditarItemSlide').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = editarItemSlideCarousel(self);
});

function editarItemSlideCarousel(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/editarItemSlideCarousel",
        dataType: 'json',
        data: new FormData($('#formEditarItemSlide')[0]),
        cache: false,
        contentType: false,
        processData: false,

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroEditarItemSlide').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#formCadastrarNovoSlide').each(function() {
                    this.reset();
                });

                $('#modalVEditarItemCarousel').modal('hide');

                listarSlidesCarousel();

            }
        }
    });
}

function deletarImagemCarouselSlide(excluirItemCarouselSlide) {

    $('#modalExcluirItemSlide').modal('show');

    $("#idExluirImagemSlide").val(dadosGlobaisSlideCarousel[excluirItemCarouselSlide].id_carousel);
    $("#tituloIdExcluirImagem").html("#" + dadosGlobaisSlideCarousel[excluirItemCarouselSlide].id_carousel);
    $("#excluirNomeSlideCarousel").html("#" + dadosGlobaisSlideCarousel[excluirItemCarouselSlide].id_carousel);
    $('#imagemSlideModalExcluir').attr("src", "/assets/sites/" + site + "/img/banners/" + dadosGlobaisSlideCarousel[excluirItemCarouselSlide].imagem_carousel);

}

$('#formExcluirItemSlide').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = excluirItemSlideCarousel(self);
});

function excluirItemSlideCarousel(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/excluirItemSlideCarousel",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgERROExcluirImagemSlide').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#modalExcluirItemSlide').modal('hide');

                listarSlidesCarousel();

            }
        }
    });
}

function listarCoberturaAtendimento() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarCoberturaAtendimento",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            dadosGlobaisCoberturaAtendimento = dados;

            if (dados.ativo_cobertura === '1') {

                var botaoStatus = '<button type="submit" onclick="javascript:attStatusCoberturaAtendimento();" class="btn btn-block btn-sm btn-success waves-effect waves-light">visível</button>';

            } else {

                var botaoStatus = '<button type="submit" onclick="javascript:attStatusCoberturaAtendimento();" class="btn btn-block btn-sm btn-danger waves-effect waves-light">Oculto</button>';

            }

            $('#botaoStatusCoberturaAtendimento').html(botaoStatus);
            $('#tituloCoberturaAtendimento').val(dados.titulo_cobertura);
            $('#descricaoCoberturaAtendimento').val(dados.descricao_cobertura);
            $('#textoBotao1CoberturaAtendimento').val(dados.textoBotao1_cobertura);
            $('#textoBotao2CoberturaAtendimento').val(dados.textoBotao2_cobertura);
            $('#LinkBotao1CoberturaAtendimento').val(dados.linkBotao1_cobertura);

            if (dados.ativoBotao1_cobertura === '1') { $('#ativoBotao1CoberturaAtendimento').val(1).attr("checked", "checked"); } else { $('#ativoBotao1CoberturaAtendimento').val(1); }
            if (dados.ativoBotao2_cobertura === '1') { $('#ativoBotao2CoberturaAtendimento').val(1).attr("checked", "checked"); } else { $('#ativoBotao2CoberturaAtendimento').val(1); }

        }

    });
}

function attStatusCoberturaAtendimento(attStatusCoberAtend) {

    $.post({
        type: "POST",

        data: {
            id_coberturaAtendimento: 1
        },

        url: "/dashboard/site_admin/ourilandia/liberarStatusCoberturaAtendimento",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarCoberturaAtendimento();

            }
        }
    });

}

$('#formAtualizarCoberturaAtendimento').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = atualizarCoberturaAtendimento(self);
});

function atualizarCoberturaAtendimento(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/atualizarCoberturaAtendimento",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarCoberturaAtendimento();

            }
        }
    });
}

$('#formPlanosResidenciais').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = cadastrarNovoPlanoResidencial(self);
});

function cadastrarNovoPlanoResidencial(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/cadastrarNovoPlanoResidencial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#formPlanosResidenciais').each(function() {
                    this.reset();
                });

                listarPlanosResidenciais();

            }
        }
    });
}

function listarPlanosResidenciais() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarPlanosResidenciais",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            dadosGlobaisPlanosResidenciais = dados;

            $('#tabelaPlanosResidenciais').html('');

            if (dados.length > 0) {

                for (var i = 0; i < dados.length; i++) {

                    if (dados[i].destaque_plano_residencial === '1') {
                        var destaque = 'class="text-warning"';
                        var botaoDestaque = '<button type="button" onclick="javascript:atualizarPlanoDestaqueResidencial(' + i + ');" class="btn btn-block btn-xs bg-warning text-light waves-effect waves-light"><i class="fas fa-heart"></i></button>';


                    } else {

                        var destaque = '';
                        var botaoDestaque = '<button type="button" onclick="javascript:atualizarPlanoDestaqueResidencial(' + i + ');" class="btn btn-block btn-xs btn-light waves-effect waves-light"><i class="far fa-heart"></i></button>';

                    }

                    if (dados[i].ativo_plano_residencial === '1') {

                        var botaoStatus = '<button type="button" onclick="javascript:atualizarStatusPlanoResidencial(' + i + ');" class="btn btn-block btn-xs bg-primary waves-effect waves-light">Ativado</button>';

                    } else {

                        var botaoStatus = '<button type="button" onclick="javascript:atualizarStatusPlanoResidencial(' + i + ');" class="btn btn-block btn-xs btn-danger waves-effect waves-light">Desativado</button>';

                    }


                    $('#tabelaPlanosResidenciais').append(

                        '<tr>' +
                        '<td> ' +
                        '<h5 class="m-0 font-weight-normal">' + dados[i].id_plano_residencial + '</h5> ' +
                        '</td>' +

                        '<td ' + destaque + '>' + dados[i].nome_plano_residencial + '</td>' +

                        '<td ' + destaque + '>R$ ' + dados[i].preco_plano_residencial + '</td>' +

                        '<td ' + destaque + '>' + dados[i].velocidade_plano_residencial + dados[i].tipoVelocidade_plano_residencial + '</td>' +

                        '<td>' + botaoDestaque + '</td>' +

                        '<td>' + botaoStatus + '</td>' +

                        '<td>' +

                        '<a href="javascript:void(0);" onclick="javascript:showModalEditarPlanoResidencial(' + i + ');" class="btn btn-xs btn-primary mr-1">' +
                        '<i class="mdi mdi-pencil-outline"></i>' +
                        '</a>' +

                        '<a href="javascript:void(0);" onclick="javascript:showModalExcluirPlanoResidencial(' + i + ');" class="btn btn-xs btn-danger">' +
                        '<i class="fe-trash-2"></i>' +
                        '</a>' +

                        '</td>' +

                        '</tr>'

                    );
                }

            } else {

                $('#tabelaPlanosResidenciais').append(
                    '<td colspan="7">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhum plano residencial cadastrado!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }

        }

    });
}

function atualizarStatusPlanoResidencial(planoStatusRes) {

    $.post({
        type: "POST",

        data: {
            id_plano_residencial: dadosGlobaisPlanosResidenciais[planoStatusRes].id_plano_residencial
        },

        url: "/dashboard/site_admin/ourilandia/atualizarPlanoStatusResidencial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarPlanosResidenciais();

            }
        }
    });

}

function atualizarPlanoDestaqueResidencial(planoDestaqueRes) {

    $.post({
        type: "POST",

        data: {
            id_plano_residencial: dadosGlobaisPlanosResidenciais[planoDestaqueRes].id_plano_residencial
        },

        url: "/dashboard/site_admin/ourilandia/atualizarPlanoDestaqueResidencial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarPlanosResidenciais();

            }
        }
    });

}

function showModalEditarPlanoResidencial(attPlanosResidenciais) {

    $('#modalEditarPlanosResidenciais').modal('show');

    $("#idPlanoResidencial").val(dadosGlobaisPlanosResidenciais[attPlanosResidenciais].id_plano_residencial);
    $("#alterarDescricaoPlanoResidencial").val(dadosGlobaisPlanosResidenciais[attPlanosResidenciais].descricao_plano_residencial);
    $("#alterarTipoVelocidadePlanoResidencial").val(dadosGlobaisPlanosResidenciais[attPlanosResidenciais].tipoVelocidade_plano_residencial);
    $("#alterarVelocidadePlanoResidencial").val(dadosGlobaisPlanosResidenciais[attPlanosResidenciais].velocidade_plano_residencial);
    $("#alterarPrecoPlanoResidencial").val(dadosGlobaisPlanosResidenciais[attPlanosResidenciais].preco_plano_residencial);
    $("#alterarNomePlanoResidencial").val(dadosGlobaisPlanosResidenciais[attPlanosResidenciais].nome_plano_residencial);
    $("#alterarTituloNomePlano").html(dadosGlobaisPlanosResidenciais[attPlanosResidenciais].nome_plano_residencial);

}

$('#formAlterarPlanoResidencial').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = atualizarPlanoResidencial(self);
});

function atualizarPlanoResidencial(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/atualizarPlanoResidencial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroEditarPlanoResidencial').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#formAlterarPlanoResidencial').each(function() {
                    this.reset();
                });

                $('#modalEditarPlanosResidenciais').modal('hide');

                listarPlanosResidenciais();

            }
        }
    });
}

function showModalExcluirPlanoResidencial(excluirPlanoResidencial) {

    $('#modalExcluirPlanoResidencial').modal('show');

    $("#idExluirPlanoResidencial").val(dadosGlobaisPlanosResidenciais[excluirPlanoResidencial].id_plano_residencial);
    $("#tituloExcluirPlanoResidencial").html(dadosGlobaisPlanosResidenciais[excluirPlanoResidencial].nome_plano_residencial);
    $("#excluirNomePlanoResidencial").html(dadosGlobaisPlanosResidenciais[excluirPlanoResidencial].nome_plano_residencial);

}

$('#formExcluirPlanoResidencial').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = excluirPlanoResidencial(self);
});

function excluirPlanoResidencial(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/excluirPlanoResidencial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgERROExcluirPlanoResidencial').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#modalExcluirPlanoResidencial').modal('hide');

                listarPlanosResidenciais();

            }
        }
    });
}

function listarPlanosEmpresariais() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarPlanosEmpresariais",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            dadosGlobaisPlanosEmpresariais = dados;

            $('#tabelaPlanosEmpresariais').html('');

            if (dados.length > 0) {

                for (var i = 0; i < dados.length; i++) {

                    if (dados[i].destaque_plano_empresarial === '1') {
                        var destaqueEmp = 'class="text-warning"';
                        var botaoDestaqueEmp = '<button type="button" onclick="javascript:atualizarPlanoDestaqueEmpresarial(' + i + ');" class="btn btn-block btn-xs bg-warning text-light waves-effect waves-light"><i class="fas fa-heart"></i></button>';


                    } else {

                        var destaqueEmp = '';
                        var botaoDestaqueEmp = '<button type="button" onclick="javascript:atualizarPlanoDestaqueEmpresarial(' + i + ');" class="btn btn-block btn-xs btn-light waves-effect waves-light"><i class="far fa-heart"></i></button>';

                    }

                    if (dados[i].ativo_plano_empresarial === '1') {

                        var botaoStatusEmp = '<button type="button" onclick="javascript:atualizarStatusPlanoEmpresarial(' + i + ');" class="btn btn-block btn-xs bg-primary waves-effect waves-light">Ativado</button>';

                    } else {

                        var botaoStatusEmp = '<button type="button" onclick="javascript:atualizarStatusPlanoEmpresarial(' + i + ');" class="btn btn-block btn-xs btn-danger waves-effect waves-light">Desativado</button>';

                    }


                    $('#tabelaPlanosEmpresariais').append(

                        '<tr>' +
                        '<td> ' +
                        '<h5 class="m-0 font-weight-normal">' + dados[i].id_plano_empresarial + '</h5> ' +
                        '</td>' +

                        '<td ' + destaqueEmp + '>' + dados[i].nome_plano_empresarial + '</td>' +

                        '<td ' + destaqueEmp + '>R$ ' + dados[i].preco_plano_empresarial + '</td>' +

                        '<td ' + destaqueEmp + '>' + dados[i].velocidade_plano_empresarial + dados[i].tipoVelocidade_plano_empresarial + '</td>' +

                        '<td>' + botaoDestaqueEmp + '</td>' +

                        '<td>' + botaoStatusEmp + '</td>' +

                        '<td>' +

                        '<a href="javascript:void(0);" onclick="javascript:showModalEditarPlanoEmpresarial(' + i + ');" class="btn btn-xs btn-primary mr-1">' +
                        '<i class="mdi mdi-pencil-outline"></i>' +
                        '</a>' +

                        '<a href="javascript:void(0);" onclick="javascript:showModalExcluirPlanoEmpresarial(' + i + ');" class="btn btn-xs btn-danger">' +
                        '<i class="fe-trash-2"></i>' +
                        '</a>' +

                        '</td>' +

                        '</tr>'

                    );
                }

            } else {

                $('#tabelaPlanosEmpresariais').append(
                    '<td colspan="7">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhum plano empresarial cadastrado!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }

        }

    });
}

$('#formPlanosEmpresariais').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = cadastrarNovoPlanoEmpresarial(self);
});

function cadastrarNovoPlanoEmpresarial(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/cadastrarNovoPlanoEmpresarial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#formPlanosEmpresariais').each(function() {
                    this.reset();
                });

                listarPlanosEmpresariais();

            }
        }
    });
}

function atualizarPlanoDestaqueEmpresarial(planoDestaqueEmp) {

    $.post({
        type: "POST",

        data: {
            id_plano_Empresarial: dadosGlobaisPlanosEmpresariais[planoDestaqueEmp].id_plano_empresarial
        },

        url: "/dashboard/site_admin/ourilandia/atualizarPlanoDestaqueEmpresarial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarPlanosEmpresariais();

            }
        }
    });

}

function atualizarStatusPlanoEmpresarial(planoStatusEmp) {

    $.post({
        type: "POST",

        data: {
            id_plano_empresarial: dadosGlobaisPlanosEmpresariais[planoStatusEmp].id_plano_empresarial
        },

        url: "/dashboard/site_admin/ourilandia/atualizarStatusPlanoEmpresarial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarPlanosEmpresariais();

            }
        }
    });

}

function showModalEditarPlanoEmpresarial(attPlanosEmpresariais) {

    $('#modalEditarPlanosEmpresariais').modal('show');

    $("#idPlanoEmpresarial").val(dadosGlobaisPlanosEmpresariais[attPlanosEmpresariais].id_plano_empresarial);
    $("#alterarDescricaoPlanoEmpresarial").val(dadosGlobaisPlanosEmpresariais[attPlanosEmpresariais].descricao_plano_empresarial);
    $("#alterarTipoVelocidadePlanoEmpresarial").val(dadosGlobaisPlanosEmpresariais[attPlanosEmpresariais].tipoVelocidade_plano_empresarial);
    $("#alterarVelocidadePlanoEmpresarial").val(dadosGlobaisPlanosEmpresariais[attPlanosEmpresariais].velocidade_plano_empresarial);
    $("#alterarPrecoPlanoEmpresarial").val(dadosGlobaisPlanosEmpresariais[attPlanosEmpresariais].preco_plano_empresarial);
    $("#alterarNomePlanoEmpresarial").val(dadosGlobaisPlanosEmpresariais[attPlanosEmpresariais].nome_plano_empresarial);
    $("#alterarTituloNomePlanoEmp").html(dadosGlobaisPlanosEmpresariais[attPlanosEmpresariais].nome_plano_empresarial);

}

$('#formAlterarPlanoEmpresarial').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = atualizarPlanoEmpresarial(self);
});

function atualizarPlanoEmpresarial(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/atualizarPlanoEmpresarial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroEditarPlanoEmpresarial').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#formAlterarPlanoEmpresarial').each(function() {
                    this.reset();
                });

                $('#modalEditarPlanosEmpresariais').modal('hide');

                listarPlanosEmpresariais();

            }
        }
    });
}

function showModalExcluirPlanoEmpresarial(excluirPlanoEmpresarial) {

    $('#modalExcluirPlanoEmpresarial').modal('show');

    $("#idExluirPlanoEmpresarial").val(dadosGlobaisPlanosEmpresariais[excluirPlanoEmpresarial].id_plano_empresarial);
    $("#tituloExcluirPlanoEmpresarial").html(dadosGlobaisPlanosEmpresariais[excluirPlanoEmpresarial].nome_plano_empresarial);
    $("#excluirNomePlanoEmpresarial").html(dadosGlobaisPlanosEmpresariais[excluirPlanoEmpresarial].nome_plano_empresarial);

}

$('#formExcluirPlanoEmpresarial').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = excluirPlanoEmpresarial(self);
});

function excluirPlanoEmpresarial(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/site_admin/ourilandia/excluirPlanoEmpresarial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgERROExcluirPlanoEmpresarial').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#modalExcluirPlanoEmpresarial').modal('hide');

                listarPlanosEmpresariais();

            }
        }
    });
}