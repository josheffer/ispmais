var site = 'ispmais';

listarContratacoesEmpresariaisPendentes();

function listarContratacoesEmpresariaisPendentes() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarContratacoesEmpresariais_pendentes",

        ajax: 'data.json',

        dataType: 'json',

        success: function(data) {

            var dados = data;

            dadosGlobais = dados;
            idUsuario = dados.dadosUsuario;

            $('#tabelaListarContratacoesEmpresariais').html('');

            $('#atualizaDiv').html('<a type="button" onclick="listarContratacoesEmpresariaisPendentes();" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>');

            if (parseInt(dados.empresariais.length) > 0) {

                for (var i = 0; i < dados.empresariais.length; i++) {

                    var dataEmpresiarial = moment(dados.empresariais[i].data_solicitacoes_planoEmpresarial).format('DD/MM/YYYY');
                    var horaEmpresiarial = dados.empresariais[i].hora_solicitacoes_planoEmpresarial;

                    var dataHoraEmpresariais = '<span class="badge badge-light">' + dataEmpresiarial + ' às ' + horaEmpresiarial + '</span>';

                    if (dados.empresariais[i].status_solicitacoes_planoEmpresarial === '0') {

                        var btnVerInfos = '<button type="button" class="disabled btn btn-info waves-effect waves-light"><i class="mdi mdi-eye-off-outline"></i></button>';

                        var btnRecepcionar = '<button type="button" onclick="abrirModalRecepcionar(' + i + ')" class="btn btn-block btn-xs btn-primary waves-effect waves-light">Recepcionar</button>';


                    } else {

                        if (dados.empresariais[i].operador_solicitacoes_planoEmpresarial === idUsuario) {

                            var btnVerInfos = '<button type="button" onclick="abrirModalVerInfos(' + i + ')" class="btn btn-purple waves-effect waves-light"><i class="mdi mdi-eye-outline"></i></button>';

                            var btnRecepcionar = '<button type="button" onclick="abrirModalRecepcionar(' + i + ')" class="btn btn-block btn-xs btn-warning waves-effect waves-light">Em atendimento..</button>';

                        } else {

                            var btnVerInfos = '<button type="button" class="disabled btn btn-info waves-effect waves-light"><i class="mdi mdi-eye-off-outline"></i></button>';

                            var btnRecepcionar = '<button type="button" class="disabled btn btn-block btn-xs btn-warning waves-effect waves-light">Em atendimento..</button>';

                        }
                    }


                    $('#tabelaListarContratacoesEmpresariais').append(

                        '<tr>' +

                        '<td class="text-center"> ' + dados.empresariais[i].id_solicitacoes_planoEmpresarial + ' </td>' +

                        '<td class="text-center"> ' + dados.empresariais[i].nome_solicitacoes_planoEmpresarial + ' </td>' +

                        '<td class="text-center"> <h5> ' + dataHoraEmpresariais + ' </h5></td>' +

                        '<td class="text-center">' + btnVerInfos + '</td>' +

                        '<td class="text-center">' +
                        '<div class="button-list">' + btnRecepcionar + '</div>' +
                        '</td>' +

                        '</tr> '

                    );
                }

            } else {

                $('#tabelaListarContratacoesEmpresariais').append(
                    '<td colspan="5">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhuma ligação pendente!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }

        }

    });
}

function abrirModalVerInfos(abrirModalVerInfos) {

    $('#abrirModalVerInfos').modal('show');
    $('#tituloVerInfos').html('Solicitação de contratação de plano de: <strong class="text-warning"><i><u>' + dadosGlobais.empresariais[abrirModalVerInfos].nome_solicitacoes_planoEmpresarial + '</u></i></strong> - Plano: <strong class="text-warning"><i><u>' + dadosGlobais.empresariais[abrirModalVerInfos].nomePlano_solicitacoes_planoEmpresarial + '</u></i></strong> no valor de: <strong class="text-warning"><i><u>' + dadosGlobais.empresariais[abrirModalVerInfos].valorPlano_solicitacoes_planoEmpresarial + '</u></i></strong> com velocidade de: <strong class="text-warning"><i><u>' + dadosGlobais.empresariais[abrirModalVerInfos].velocidadePlano_solicitacoes_planoEmpresarial + '</u></i></strong>');
    $('#nomeVerInfos').val(dadosGlobais.empresariais[abrirModalVerInfos].nome_solicitacoes_planoEmpresarial);
    $('#telefoneVerInfos').val(dadosGlobais.empresariais[abrirModalVerInfos].telefone_solicitacoes_planoEmpresarial);
    $('#telefoneFixoVerInfos').val(dadosGlobais.empresariais[abrirModalVerInfos].telefoneFixo_solicitacoes_planoEmpresarial);
    $('#emailVerInfos').val(dadosGlobais.empresariais[abrirModalVerInfos].email_solicitacoes_planoEmpresarial);
    $('#cnpjVerInfos').val(dadosGlobais.empresariais[abrirModalVerInfos].cnpj_solicitacoes_planoEmpresarial);
    $('#nomeFantasiaVerInfos').val(dadosGlobais.empresariais[abrirModalVerInfos].nomeFantasia_planoEmpresarial);
    $('#cepVerInfos').val(dadosGlobais.empresariais[abrirModalVerInfos].cep_solicitacoes_planoEmpresarial);
    $('#ruaVerInfos').val(dadosGlobais.empresariais[abrirModalVerInfos].rua_solicitacoes_planoEmpresarial);
    $('#complementoVerInfos').val(dadosGlobais.empresariais[abrirModalVerInfos].complemento_solicitacoes_planoEmpresarial);
    $('#numeroVerInfos').val(dadosGlobais.empresariais[abrirModalVerInfos].numero_solicitacoes_planoEmpresarial);
    $('#bairroVerInfos').val(dadosGlobais.empresariais[abrirModalVerInfos].bairro_solicitacoes_planoEmpresarial);
    $('#cidadeVerInfos').val(dadosGlobais.empresariais[abrirModalVerInfos].cidade_solicitacoes_planoEmpresarial);
    $('#estadoVerInfos').val(dadosGlobais.empresariais[abrirModalVerInfos].estado_solicitacoes_planoEmpresarial);
    $('#observacaoVerInfos').val(dadosGlobais.empresariais[abrirModalVerInfos].observacao_solicitacoes_planoEmpresarial);

}

function abrirModalRecepcionar(recepcionar) {

    if (dadosGlobais.empresariais[recepcionar].status_solicitacoes_planoEmpresarial === '1') {

        $('#dadosSolicitacao').html('');

        $('#abrirModalRecepcionar').modal('show');

        $('#tituloRecepcionar').html('Você está recepcionando a solicitação de contratação de: <strong><u><i><span class="text-warning">' + dadosGlobais.empresariais[recepcionar].nome_solicitacoes_planoEmpresarial + '</span></i></u></strong>');

        $('#mensagemRecepcionar').html(
            '<form>' +

            '<div class="form-row">' +

            '<div class="form-group col-md-12">' +
            '<label for="nomeVerInfos" class="col-form-label">Nome</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[recepcionar].nome_solicitacoes_planoEmpresarial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-4">' +
            '<label for="telefoneVerInfos" class="col-form-label">Telefone</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[recepcionar].telefone_solicitacoes_planoEmpresarial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-4">' +
            '<label for="emailVerInfos" class="col-form-label">Telefone fixo</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[recepcionar].telefoneFixo_solicitacoes_planoEmpresarial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-4">' +
            '<label for="emailVerInfos" class="col-form-label">E-mail</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[recepcionar].email_solicitacoes_planoEmpresarial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-2">' +
            '<label for="cpfVerInfos" class="col-form-label">Valor</label>' +
            '<input type="text" class="form-control" value="R$ ' + dadosGlobais.empresariais[recepcionar].valorPlano_solicitacoes_planoEmpresarial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-5">' +
            '<label for="cpfVerInfos" class="col-form-label">C.N.P.J</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[recepcionar].cnpj_solicitacoes_planoEmpresarial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-5">' +
            '<label for="cpfVerInfos" class="col-form-label">Nome fantasia</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[recepcionar].nomeFantasia_planoEmpresarial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-2">' +
            '<label for="cepVerInfos" class="col-form-label">CEP</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[recepcionar].cep_solicitacoes_planoEmpresarial + '"" disabled>' +
            '</div>' +

            '<div class="form-group col-md-10">' +
            '<label for="ruaVerInfos" class="col-form-label">Rua</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[recepcionar].rua_solicitacoes_planoEmpresarial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-10">' +
            '<label for="complementoVerInfos" class="col-form-label">Complemento</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[recepcionar].complemento_solicitacoes_planoEmpresarial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-2">' +
            '<label for="numeroVerInfos" class="col-form-label">Número</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[recepcionar].numero_solicitacoes_planoEmpresarial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-4">' +
            '<label for="bairroVerInfos" class="col-form-label">Bairro</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[recepcionar].bairro_solicitacoes_planoEmpresarial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-4">' +
            '<label for="cidadeVerInfos" class="col-form-label">Cidade</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[recepcionar].cidade_solicitacoes_planoEmpresarial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-4">' +
            '<label for="estadoVerInfos" class="col-form-label">Estado</label>' +
            '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[recepcionar].estado_solicitacoes_planoEmpresarial + '" disabled>' +
            '</div>' +

            '<div class="form-group col-md-12">' +
            '<label for="observacaoVerInfos" class="col-form-label">Observação</label>' +
            '<textarea class="form-control" rows="4" value="' + dadosGlobais.empresariais[recepcionar].observacao_solicitacoes_planoEmpresarial + '" disabled></textarea>' +
            '</div>' +

            '</div>' +

            '</form>' +

            '<div class="row mb-3 mt-3 text-center">' +

            '<div class="col-md-3">' +
            '<button type="submit" onclick="javascript:devolverSolicitacaoContratacaoEpresarial(' + recepcionar + ');" class="btn btn-block btn-purple waves-effect" >Devolver para pendentes</button>' +
            '</div>' +

            '<div class="col-md-3">' +
            '<button type="submit" onclick="javascript:naoAtendidaContratacaoEpresarial(' + recepcionar + ');" class="btn btn-block btn-danger waves-effect" >Não atendida</button>' +
            '</div>' +

            '<div class="col-md-3">' +
            '<button type="submit" onclick="javascript:vendaRealizadaContratacaoEpresarial(' + recepcionar + ');" class="btn btn-block btn-success waves-effect" >Venda realizada</button>' +
            '</div>' +

            '<div class="col-md-3">' +
            '<button type="submit" onclick="javascript:vendaNaoRealizadaContratacaoEpresarial(' + recepcionar + ');" class="btn btn-block btn-warning waves-effect" >Venda não realizada</button>' +
            '</div>' +

            '</div>'
        );
    }

    if (dadosGlobais.empresariais[recepcionar].status_solicitacoes_planoEmpresarial === '0') {

        $('#abrirModalRecepcionar').modal('show');
        $('#dadosSolicitacao').html('');

        $('#tituloRecepcionar').html('Recepcionar solicitação de contratação de: <strong><u><i><span class="text-warning">' + dadosGlobais.empresariais[recepcionar].nome_solicitacoes_planoEmpresarial + '</span></i></u></strong>');

        $('#mensagemRecepcionar').html(
            '<h4 class="text-center">Deseja recepcionar a solicitação de contratação de <strong><u><i><span class="text-warning">' + dadosGlobais.empresariais[recepcionar].nome_solicitacoes_planoEmpresarial + '</span></i></u></strong> ?</h4>' +
            '<div class="col-md-12 text-center">' +
            '<br>' +
            '<button type="submit" onclick="javascript:recepcionarContratacaoEmpresarial(' + recepcionar + ');" class="btn btn-primary waves-effect">Sim, quero!</button>' +
            '</div>'
        );

    }


}

function recepcionarContratacaoEmpresarial(attRecepContrEmpresarial) {

    $.post({

        type: "POST",

        data: {
            idContratarPlanoEmpresarial: dadosGlobais.empresariais[attRecepContrEmpresarial].id_solicitacoes_planoEmpresarial,
            idOperador: idUsuario,
            statusContratarPlanoEmpresarial: '1'
        },

        url: "/dashboard/site_admin/ourilandia/recepcionarContratacaoEmpresarial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#mensagemRecepcionar').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                listarContratacoesEmpresariaisPendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarContratacoesEmpresariaisPendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#mensagemRecepcionar').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#tituloRecepcionar').html('Você está recepcionando a solicitação de contratação de: <strong><u><i><span class="text-warning">' + dadosGlobais.empresariais[attRecepContrEmpresarial].nome_solicitacoes_planoEmpresarial + '</span></i></u></strong>');

                $('#dadosSolicitacao').append(

                    '<form>' +

                    '<div class="form-row">' +

                    '<div class="form-group col-md-12">' +
                    '<label for="nomeVerInfos" class="col-form-label">Nome</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[attRecepContrEmpresarial].nome_solicitacoes_planoEmpresarial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-4">' +
                    '<label for="telefoneVerInfos" class="col-form-label">Telefone</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[attRecepContrEmpresarial].telefone_solicitacoes_planoEmpresarial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-4">' +
                    '<label for="telefoneVerInfos" class="col-form-label">Telefone fixo</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[attRecepContrEmpresarial].telefoneFixo_solicitacoes_planoEmpresarial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-4">' +
                    '<label for="emailVerInfos" class="col-form-label">E-mail</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[attRecepContrEmpresarial].email_solicitacoes_planoEmpresarial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-2">' +
                    '<label for="cpfVerInfos" class="col-form-label">Valor</label>' +
                    '<input type="text" class="form-control" value="R$ ' + dadosGlobais.empresariais[attRecepContrEmpresarial].valorPlano_solicitacoes_planoEmpresarial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-5">' +
                    '<label for="cpfVerInfos" class="col-form-label">C.N.P.J</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[attRecepContrEmpresarial].cnpj_solicitacoes_planoEmpresarial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-5">' +
                    '<label for="cpfVerInfos" class="col-form-label">Nome fantasia</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[attRecepContrEmpresarial].nomeFantasia_planoEmpresarial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-2">' +
                    '<label for="cepVerInfos" class="col-form-label">CEP</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[attRecepContrEmpresarial].cep_solicitacoes_planoEmpresarial + '"" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-10">' +
                    '<label for="ruaVerInfos" class="col-form-label">Rua</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[attRecepContrEmpresarial].rua_solicitacoes_planoEmpresarial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-10">' +
                    '<label for="complementoVerInfos" class="col-form-label">Complemento</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[attRecepContrEmpresarial].complemento_solicitacoes_planoEmpresarial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-2">' +
                    '<label for="numeroVerInfos" class="col-form-label">Número</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[attRecepContrEmpresarial].numero_solicitacoes_planoEmpresarial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-4">' +
                    '<label for="bairroVerInfos" class="col-form-label">Bairro</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[attRecepContrEmpresarial].bairro_solicitacoes_planoEmpresarial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-4">' +
                    '<label for="cidadeVerInfos" class="col-form-label">Cidade</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[attRecepContrEmpresarial].cidade_solicitacoes_planoEmpresarial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-4">' +
                    '<label for="estadoVerInfos" class="col-form-label">Estado</label>' +
                    '<input type="text" class="form-control" value="' + dadosGlobais.empresariais[attRecepContrEmpresarial].estado_solicitacoes_planoEmpresarial + '" disabled>' +
                    '</div>' +

                    '<div class="form-group col-md-12">' +
                    '<label for="observacaoVerInfos" class="col-form-label">Observação</label>' +
                    '<textarea class="form-control" rows="4" value="' + dadosGlobais.empresariais[attRecepContrEmpresarial].observacao_solicitacoes_planoEmpresarial + '" disabled></textarea>' +
                    '</div>' +

                    '</div>' +

                    '</form>' +

                    '<div class="row mb-3 mt-3 text-center">' +

                    '<div class="col-md-3">' +
                    '<button type="submit" onclick="javascript:devolverSolicitacaoContratacaoEpresarial(' + attRecepContrEmpresarial + ');" class="btn btn-block btn-purple waves-effect" >Devolver para pendentes</button>' +
                    '</div>' +

                    '<div class="col-md-3">' +
                    '<button type="submit" onclick="javascript:naoAtendidaContratacaoEpresarial(' + attRecepContrEmpresarial + ');" class="btn btn-block btn-danger waves-effect" >Não atendida</button>' +
                    '</div>' +

                    '<div class="col-md-3">' +
                    '<button type="submit" onclick="javascript:vendaRealizadaContratacaoEpresarial(' + attRecepContrEmpresarial + ');" class="btn btn-block btn-success waves-effect" >Venda realizada</button>' +
                    '</div>' +

                    '<div class="col-md-3">' +
                    '<button type="submit" onclick="javascript:vendaNaoRealizadaContratacaoEpresarial(' + attRecepContrEmpresarial + ');" class="btn btn-block btn-warning waves-effect" >Venda não realizada</button>' +
                    '</div>' +

                    '</div>'

                );


            }
        }


    });

}

function devolverSolicitacaoContratacaoEpresarial(devolSolicit) {

    $.post({

        type: "POST",

        data: {
            idContratarPlanoEmpresarial: dadosGlobais.empresariais[devolSolicit].id_solicitacoes_planoEmpresarial,
            idOperador: "",
            statusContratarPlanoEmpresarial: '0'
        },

        url: "/dashboard/site_admin/ourilandia/recepcionarContratacaoEmpresarial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#mensagemRecepcionar').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarContratacoesEmpresariaisPendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#abrirModalRecepcionar').modal('hide');


            }
        }


    });

}

function naoAtendidaContratacaoEpresarial(naoAtendidaContratacaoResi) {

    $.post({

        type: "POST",

        data: {
            idContratarPlanoEmpresarial: dadosGlobais.empresariais[naoAtendidaContratacaoResi].id_solicitacoes_planoEmpresarial,
            idOperador: idUsuario,
            statusContratarPlanoEmpresarial: '2'
        },

        url: "/dashboard/site_admin/ourilandia/recepcionarContratacaoEmpresarial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#mensagemRecepcionar').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarContratacoesEmpresariaisPendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#abrirModalRecepcionar').modal('hide');

            }
        }


    });

}

function vendaRealizadaContratacaoEpresarial(vendaRealizadaLigacao) {

    $.post({

        type: "POST",

        data: {

            idContratarPlanoEmpresarial: dadosGlobais.empresariais[vendaRealizadaLigacao].id_solicitacoes_planoEmpresarial,
            idOperador: idUsuario,
            statusContratarPlanoEmpresarial: '3'
        },

        url: "/dashboard/site_admin/ourilandia/recepcionarContratacaoEmpresarial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#mensagemRecepcionar').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarContratacoesEmpresariaisPendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#abrirModalRecepcionar').modal('hide');


            }
        }


    });

}

function vendaNaoRealizadaContratacaoEpresarial(vendaNaoRealizadaLigacao) {

    $.post({

        type: "POST",

        data: {
            idContratarPlanoEmpresarial: dadosGlobais.empresariais[vendaNaoRealizadaLigacao].id_solicitacoes_planoEmpresarial,
            idOperador: idUsuario,
            statusContratarPlanoEmpresarial: '4'
        },

        url: "/dashboard/site_admin/ourilandia/recepcionarContratacaoEmpresarial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#mensagemRecepcionar').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                listarContratacoesEmpresariaisPendentes();

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#abrirModalRecepcionar').modal('hide');


            }
        }


    });

}