listarRegioes();

$('#formCadastrarRegiao').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = cadastrarNovaRegiao_site(self);

});

function cadastrarNovaRegiao_site(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/site/cadastrarNovaRegiao_site",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#formCadastrarRegiao').each(function() {
                    this.reset();
                });

                listarRegioes();

            }
        }
    });
}

function listarRegioes() {

    $.ajax({

        url: "/dashboard/listagens/listarRegioes",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            dadosGlobaisRegiao = dados;

            $('#tabelaRegioes').html('');

            if (dados.length > 0) {

                for (var i = 0; i < dados.length; i++) {

                    if (dados[i].status_regioes === '1') {

                        var btnStatus = '<button type="submit" onclick="javascript:liberarBloquearStatus(' + i + ');"  class="btn btn-block btn-xs btn-success waves-effect waves-light">Ativado</button>';

                    } else {

                        var btnStatus = '<button type="submit" onclick="javascript:liberarBloquearStatus(' + i + ');"  class="btn btn-block btn-xs btn-danger waves-effect waves-light">Desativado</button>';

                    }

                    if (dados[i].ativoSite_regioes === '1') {

                        var btnAtivoSite = '<button type="submit" onclick="javascript:liberarBloquearRegiaoSite(' + i + ');"  class="btn btn-block btn-xs btn-success waves-effect waves-light">Ativado</button>';

                    } else {

                        var btnAtivoSite = '<button type="submit" onclick="javascript:liberarBloquearRegiaoSite(' + i + ');"  class="btn btn-block btn-xs btn-danger waves-effect waves-light">Desativado</button>';

                    }

                    $('#tabelaRegioes').append(

                        '<tr>' +
                        '<td>' + dados[i].id_regioes + '</td>' +
                        '<td>' + dados[i].nome_regioes + '</td>' +
                        '<td>' + btnStatus + '</td>' +
                        '<td>' + btnAtivoSite + '</td>' +
                        '<td>' +
                        '<div class="button-list">' +
                        '<button type="button" onclick="javascript:editarDadosRegiao(' + i + ');" class="btn btn-xs btn-info waves-effect waves-light"><i class=" mdi mdi-lead-pencil"></i></button>' +
                        '<button type="button" onclick="javascript:ExcluirRegiao(' + i + ');" class="btn btn-xs btn-danger waves-effect waves-light"><i class="mdi mdi-delete"></i></button>' +
                        '</div>' +
                        '</td>' +
                        '</tr> '

                    );
                }

            } else {

                $('#tabelaRegioes').append(
                    '<td colspan="4">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhuma região cadastrada!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }
        }
    });
}

function liberarBloquearStatus(status) {

    $.post({
        type: "POST",

        data: {
            id_regioes: dadosGlobaisRegiao[status].id_regioes
        },

        url: "/dashboard/atualizar/liberarBloquearStatus",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarRegioes();

            }
        }
    });

}

function liberarBloquearRegiaoSite(statusSite) {

    $.post({
        type: "POST",

        data: {
            id_regioes: dadosGlobaisRegiao[statusSite].id_regioes
        },

        url: "/dashboard/atualizar/liberarBloquearStatusSite",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                listarRegioes();

            }
        }
    });

}

function editarDadosRegiao(attRegiao) {

    $('#formAlterarRegiao').each(function() {
        this.reset();
    });

    showModalEditar(attRegiao);
}

function showModalEditar(attRegiao) {

    $('#formAlterarRegiao').each(function() {
        this.reset();
    });

    $('#alterarInformacoesRegiao').modal('show');

    $("#idRegiao").val(dadosGlobaisRegiao[attRegiao].id_regioes);
    $("#alterarNomeRegiao").val(dadosGlobaisRegiao[attRegiao].nome_regioes);
    $("#alterarLinkPaginaRegiao").val(dadosGlobaisRegiao[attRegiao].link_regioes);
    $("#alterarTituloNomeRegiao").html(dadosGlobaisRegiao[attRegiao].nome_regioes);

    $("#alterarenderecoRegiao").val(dadosGlobaisRegiao[attRegiao].endereco_regioes);
    $("#alteraremailRegiao").val(dadosGlobaisRegiao[attRegiao].email_regioes);
    $("#alterarfoneFixoRegiao").val(dadosGlobaisRegiao[attRegiao].foneFixoTexto_regioes);
    $("#alterarwhatsappRegiao").val(dadosGlobaisRegiao[attRegiao].foneWhatsappTexto_regioes);
    $("#alterarlinkWhatsappRegiao").val(dadosGlobaisRegiao[attRegiao].linkWhatsapp_regioes);

}

$('#formAlterarRegiao').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = atualizarInfosRegiao(self);
});

function atualizarInfosRegiao(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/atualizar/atualizarInfosRegiao",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroEditarRegiao').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#formAlterarRegiao').each(function() {
                    this.reset();
                });

                $('#alterarInformacoesRegiao').modal('hide');

                listarRegioes();

            }
        }
    });
}

function ExcluirRegiao(deletarRegiao) {
    showModalExcluirRegiao(deletarRegiao);
}

function showModalExcluirRegiao(deletarRegiao) {

    $('#ModalExcluiRegiao').modal('show');

    $("#idRegiaoExcluir").val(dadosGlobaisRegiao[deletarRegiao].id_regioes);
    $("#excluirNomeRegiao").html(dadosGlobaisRegiao[deletarRegiao].nome_regioes);
    $("#tituloExcluirRegiao").html(dadosGlobaisRegiao[deletarRegiao].nome_regioes);

}

$('#formExcluirRegiao').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    // alert(self.serialize());
    var retorno = excluirRegiaoModal(self);
});

function excluirRegiaoModal(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/excluir/excluirRegiao",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#msgERROExcluirRegiao').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('#ModalExcluiRegiao').modal('hide');

                listarRegioes();

            }
        }
    });
}