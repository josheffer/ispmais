$('#formLogin').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = autenticarLogin(self);

});

function autenticarLogin(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "autenticarlogin",
        dataType: 'json',

        beforeSend: function() {

            $("#usuario").prop("disabled", true);
            $("#senha").prop("disabled", true);
            $("#botaoAcessarLogin").prop("disabled", true).html('Aguarde...');

        },

        success: function(retorno) {

            if (retorno.ret === false) {

                $("#usuario").prop("disabled", false);
                $("#senha").prop("disabled", false);
                $("#botaoAcessarLogin").prop("disabled", false).html('Tentar login novamente..');
                
                $('#msgErro').html(
                   
                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                        
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                            '<span aria-hidden="true">×</span>' +
                        '</button>' +

                        '<center>' +
                            '<h5 class="text-white">Erro!</h5>' + retorno.msg + 
                        '</center>' +

                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $("#usuario").prop("disabled", false);
                $("#senha").prop("disabled", false);
                $("#botaoAcessarLogin").prop("disabled", false).html('Acessando..');

                window.location = "/dashboard";

                $('#formLogin').each(function() {
                    this.reset();
                });

            }
        }
    });
}