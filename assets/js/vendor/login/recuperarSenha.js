$('#formRecuperarSenha').submit(function(e){
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = recuperarSenha(self);
  
  });

  function recuperarSenha(dados){
    
    $.ajax(
      {
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/recuperarSenha",
        dataType: 'json',
        
        beforeSend: function() { 
            
          $("#usuario_recuperar").prop("disabled", true);
          $("#email_recuperar").prop("disabled", true);
          $("#botaoRecuperarSenha").prop("disabled", true).html('Aguarde...');
  
        },
  
        success: function(retorno){
          
          if(retorno.ret === false){
  
            $("#usuario_recuperar").prop("disabled", false);
            $("#email_recuperar").prop("disabled", false);
            $("#botaoRecuperarSenha").prop("disabled", false).html('Tentar novamente..');
            
            $('#erroRecuperarSenha').html(
                   
                '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">×</span>' +
                    '</button>' +

                    '<h5 class="text-white">Erro! </h5>' + retorno.msg + 

                '</div> </br>'
            );

            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
          } else {

            $('#msgErro').html(
                   
                '<div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">' +
                    
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">×</span>' +
                    '</button>' +

                    '<h5 class="text-body">Tudo certo! </h5><span class="text-body"> ' + retorno.msg + '</span>' +

                '</div> </br>'
            );
  
            $("#usuario_recuperar").prop("disabled", false);
            $("#email_recuperar").prop("disabled", false);
            $("#botaoRecuperarSenha").prop("disabled", false);

            $('#modalRecuperarSenha').modal('hide');
            
            $('#formRecuperarSenha').each (function(){
            this.reset();
            });
            
          }
        }
      }
    );
  }
  
  $('#formAlterarSennha').submit(function(e){
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = alterarSenha(self);
  
  });

  function alterarSenha(dados){
    
    $.ajax(
      {
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/alterarSenhaUsuario",
        dataType: 'json',
        
        // beforeSend: function() { 
            
        //   // $("#usuario_recuperar").prop("disabled", true);
        //   // $("#email_recuperar").prop("disabled", true);
        //   // $("#botaoRecuperarSenha").prop("disabled", true).html('Aguarde...');
  
        // },
  
        success: function(retorno){
          
          if(retorno.ret === false){
  
            // $("#usuario_recuperar").prop("disabled", false);
            // $("#email_recuperar").prop("disabled", false);
            // $("#botaoRecuperarSenha").prop("disabled", false).html('Tentar novamente..');
            
            $('#msgErro').html(
                   
                '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">×</span>' +
                    '</button>' +

                    '<h5 class="text-white">Erro! </h5>' + retorno.msg + 

                '</div> </br>'
            );

            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
          } else {

            $('#msgErro').html(
                   
                '<div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">' +
                    
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">×</span>' +
                    '</button>' +

                    '<h5 class="text-white">Tudo certo! </h5><span class="text-white"> ' + retorno.msg + '</span>' +

                '</div>'
            );
              
            $('#msgGenerica').html('');
            $('#formAlterarSennha').html('<center>Sua senha já foi alterada, faça o login <br> <br> <a href="/dashboard/login"><button class="btn btn-success waves-effect waves-light" type="button">Acessar</button></a></center>');

            $('#formAlterarSennha').each (function(){
            this.reset();
            });
            
          }
        }
      }
    );
  }

    