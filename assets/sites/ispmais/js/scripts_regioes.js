listarRegioes();

function listarRegioes(){
    
    $.ajax(
      {
          
        url: "/site/listagens/listarRegioes",
        
        ajax: 'data.json',
        
        success: function(data){
            
          var dados = JSON.parse(data);

          console.log(dados);
          
          dadosGlobaisRegiao = dados;
          
          if(dados.length>0){
              
            for(var i = 0; i < dados.length; i++) {

                if(dados[i].status_regioes === '1')
                {
                    var boxRegiao = '<div class="col-auto mt-5">' +
								
                                        '<div class="bg-white rounded p-10">' +
                                            
                                            '<div class="card card-custom card-fit card-border">' +
                                                
                                                '<div class="card-header justify-content-center">' +
                                                    
                                                    '<div class="card-title">' +
                                                        
                                                        '<span class="card-icon mt-5">' +
                                                            '<i class="flaticon2-pin text-orange"></i>' +
                                                        '</span>' +

                                                        '<h3 class="card-label mt-5 text-orange"> '+ dados[i].nome_regioes +' </h3>' +
                                                        
                                                    '</div>' +

                                                    '<div class="col-md-12"></div>' +

                                                    '<div class="row">'+
                                                        '<br>' +
                                                        '<div class="card-toolbar col-md-12 justify-content-center mt-5 mb-5">' +
                                                            '<a href="'+ dados[i].link_regioes +'" class="btn btn-sm bg-orange btn-warning font-weight-bold"> Acessar </a>' +
                                                        '</div>' +
                                                    '</div>'

                                                '</>' +

                                            '</div>' +
                                            
                                        '</div>' +
                                        
                                    '</div>'

                } else {

                    var boxRegiao = '';
                }
                
                $('#boxRegiao').append(boxRegiao);
            }
  
          } else {
              
              $('#boxRegiao').append(
                '<td colspan="4">' +
                  '<center class="mt-4 text-center">' +
                      '<div class="col-md-12 text-center">' +
                          '<div class="alert alert-danger text-center">' +
                              '<i class="fas fa-exclamation-circle"></i> Nenhuma região cadastrada!!' +
                          '</div>' +
                      '</div>' +
                  '</center>' +
                '</td>'
              );
  
          }
        }
      }
    );
  }