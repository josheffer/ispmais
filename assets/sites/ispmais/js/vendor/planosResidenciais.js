$(document).ready(function() {

    function limpa_formulario_cep() {

        $("#rua_planoResidencial").val("");
        $("#bairro_planoResidencial").val("");
        $("#cidade_planoResidencial").val("");
        $("#uf_planoResidencial").val("");
        $("#ibge_planoResidencial").val("");
    }

    $("#cep_planoResidencial").blur(function() {

        var cep = $(this).val().replace(/\D/g, '');

        if (cep != "") {
            var validacep = /^[0-9]{8}$/;

            if (validacep.test(cep)) {

                $("#rua_planoResidencial").val("...");
                $("#bairro_planoResidencial").val("...");
                $("#cidade_planoResidencial").val("...");
                $("#uf_planoResidencial").val("...");
                $("#ibge_planoResidencial").val("...");

                $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {

                        $("#rua_planoResidencial").val(dados.logradouro);
                        $("#bairro_planoResidencial").val(dados.bairro);
                        $("#cidade_planoResidencial").val(dados.localidade);
                        $("#uf_planoResidencial").val(dados.uf);
                        $("#ibge_planoResidencial").val(dados.ibge);
                    } else {

                        limpa_formulario_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } else {

                limpa_formulario_cep();
                alert("Formato de CEP inválido.");
            }
        } else {

            limpa_formulario_cep();
        }
    });
});

// ============================================================================================
var telefone_planoResidencial = document.getElementById('telefone_planoResidencial');
var maskOptionsTel_planoResidencial = {
    mask: '(00) 0 0000-0000'
};
var maskTel_planoResidencial = IMask(telefone_planoResidencial, maskOptionsTel_planoResidencial);
// ============================================================================================

// ============================================================================================
var cep_planoResidencial = document.getElementById('cep_planoResidencial');
var maskOptionsCep_planoResidencial = {
    mask: '00.000-000'
};
var maskCep_planoResidencial = IMask(cep_planoResidencial, maskOptionsCep_planoResidencial);
// ============================================================================================

// ============================================================================================
var cpf_planoResidencial = document.getElementById('cpf_planoResidencial');
var maskOptionsCPF_planoResidencial = {
    mask: '000.000.000-00'
};
var maskCPF_planoResidencial = IMask(cpf_planoResidencial, maskOptionsCPF_planoResidencial);
// ============================================================================================


listarPlanosResidenciais();

function listarPlanosResidenciais() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarPlanosResidenciaisSite",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            dadosGlobaisResidenciais = dados;

            $('#tabelaListarPlanosResidenciais').html('');

            if (dados.length > 0) {

                for (var i = 0; i < dados.length; i++) {

                    if (dados[i].destaque_plano_residencial === '1') 
                    { 
                        var bgDestaque = 'orange-bg'; 
                        var btnDestaque = 'btn-orange'; 
                        var corBGDestaque = 'style="background-color:#0d1323;"';

                    } else { 

                        var bgDestaque = ''; 
                        var btnDestaque = 'btn-outline'; 
                        var corBGDestaque = '';
                    }

                    $('#tabelaListarPlanosResidenciais').append(
                        '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 margin-bottom50" style="margin-top: 20px;">' +

                        '<div class="price ' + bgDestaque + '">R$ ' + dados[i].preco_plano_residencial + '/Mês</div>' +

                        '<div class="table mobile-margin-bottom" '+corBGDestaque+'>' +

                        '<h4 style="font-size: 7em;">' + dados[i].velocidade_plano_residencial + '</h4>' +

                        '<h4 style="font-size: 3em;margin-top: -40px;">' + dados[i].tipoVelocidade_plano_residencial + '</h4>' +

                        '<div class="table-content">' +

                        '<h4 style="text-align: center;">' + dados[i].nome_plano_residencial + '</h4>' +

                        '<p style="text-align: center;">' + dados[i].descricao_plano_residencial + '</p>' +

                        '<div class="buttons" style="margin-top: 3em;">' +
                        '<a href="javascript:void(0);" onclick="javascript:modalContratarPlanoResidencial(' + i + ');" class="btn ' + btnDestaque + ' btn-medium"> Contratar</a>' +
                        '</div>' +

                        '</div>' +

                        '</div>' +

                        '</div>');
                }

            } else {

                $('#tabelaListarPlanosResidenciais').append('');

            }

        }

    });
}

function modalContratarPlanoResidencial(modalPlanoResidencial) {

    $('#modalContratarPlanoResidencial').modal('show');

    $("#nomePlanoResidencialModalContratar").html(dadosGlobaisResidenciais[modalPlanoResidencial].nome_plano_residencial);
    $("#nomePlanoResidencialContratar").html(dadosGlobaisResidenciais[modalPlanoResidencial].nome_plano_residencial);
    $("#id_planoResidencial").val(dadosGlobaisResidenciais[modalPlanoResidencial].id_plano_residencial);


}

$('#contratarPlanoresidencial').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = contratarPlanoResidencial(self);
});

function contratarPlanoResidencial(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/site/ourilandia_do_norte/contratarPlanoResidencial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroPlanoResidencial').html(

                    '<div class="bs-example bs-example-standalone" data-example-id="dismissible-alert-js"> ' +
                    '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="background-color: #ff211a;">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true" style="color: white;">×</span>' +
                    '</button>' +

                    '<h4 style="color: #ffffff;">Algo deu errado!</h4> ' +
                    '<p style="color: #ffffff;">' + retorno.msg + '</p> ' +

                    '</div> ' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgSucessoPlanoResidencial').html(

                    '<div class="bs-example bs-example-standalone" data-example-id="dismissible-alert-js"> ' +
                    '<div class="alert alert-success alert-dismissible fade in" role="alert" style="background-color: #5CB85C;">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true" style="color: white;">×</span>' +
                    '</button>' +

                    '<h4 style="color: #ffffff;">Tudo certo!</h4> ' +
                    '<p style="color: #ffffff;">' + retorno.msg + '</p> ' +

                    '</div></br>' +
                    '</div>'
                );

                $('#modalContratarPlanoResidencial').modal('hide');

                // $(".alert").delay(15000).slideUp(1000, function() { $(this).alert('close'); });

                $('html, body').animate({ scrollTop: $('#subirPlanoresidencial').position().top }, 'slow');

                $('#contratarPlanoresidencial').each(function() {
                    this.reset();
                });

            }
        }
    });
}