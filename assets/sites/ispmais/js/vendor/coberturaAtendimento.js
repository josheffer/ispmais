$(document).ready(function() {

    function limpa_formulario_cob_aten_cep() {

        $("#rua").val("");
        $("#bairro").val("");
        $("#cidade").val("");
        $("#uf").val("");
        $("#ibge").val("");
    }

    $("#cep").blur(function() {

        var cep = $(this).val().replace(/\D/g, '');

        if (cep != "") {
            var validacep = /^[0-9]{8}$/;

            if (validacep.test(cep)) {

                $("#rua").val("...");
                $("#bairro").val("...");
                $("#cidade").val("...");
                $("#uf").val("...");
                $("#ibge").val("...");

                $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {

                        $("#rua").val(dados.logradouro);
                        $("#bairro").val(dados.bairro);
                        $("#cidade").val(dados.localidade);
                        $("#uf").val(dados.uf);
                        $("#ibge").val(dados.ibge);
                    } else {

                        limpa_formulario_cob_aten_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } else {

                limpa_formulario_cob_aten_cep();
                alert("Formato de CEP inválido.");
            }
        } else {

            limpa_formulario_cob_aten_cep();
        }
    });
});

var elementTel = document.getElementById('telefoneLigar');

var maskOptionsTel = {
    mask: '(00) 0 0000-0000'
};

var maskTel = IMask(elementTel, maskOptionsTel);

var elementCep = document.getElementById('cep');

var maskOptionsCep = {
    mask: '00.000-000'
};

var maskCep = IMask(elementCep, maskOptionsCep);

listarCoberturaAtendimentoSite();
listarTodosPlanosSite();

function listarCoberturaAtendimentoSite() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarCoberturaAtendimentoSite",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            if (dados.ativo_cobertura === '1') {

                if (dados.ativoBotao1_cobertura === '1') {

                    var botao1 = '<span class="text-orange bold-text">Fale com um atendente</span><a href="' + dados.linkBotao1_cobertura + '" target="_blank" class="btn btn-block btn-primary" style="margin-top:10px;"><i class="fab fa-whatsapp"></i> ' + dados.textoBotao1_cobertura + '</a>';

                } else {

                    var botao1 = '';

                }

                if (dados.ativoBotao2_cobertura === '1') {

                    var botao2 = '<span class="text-orange bold-text">Preencha o formulário</span><a data-toggle="modal" data-target="#modalLigar" class="btn btn-block btn-orange" style="margin-top:10px;">' + dados.textoBotao2_cobertura + '</a>';

                } else {

                    var botao2 = '';
                }

                if (dados.ativoBotao1_cobertura === '1' && dados.ativoBotao2_cobertura === '1') {

                    var coluna = '6';

                } else {

                    var coluna = '12';
                }

                $('#listarCoberturaAtendimento').html(
                    '<div class="search-domain">' +

                    '<div class="custom-width">' +

                    '<div class="row">' +

                    '<div id="msgSucessoLigacoes"></div>' +

                    '<div class="col-sm-5">' +

                    '<div class="text">' +

                    '<h4>' + dados.titulo_cobertura + '</h4>' +

                    '<p>' + dados.descricao_cobertura + '</p>' +

                    '</div>' +

                    '</div>' +

                    '<div class="col-sm-7">' +

                    '<div class="domain-input">' +

                    '<div class="col-md-' + coluna + ' text-center">' + botao1 + '</div>' +

                    '<div class="col-md-' + coluna + ' text-center">' + botao2 + '</div>' +

                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>'

                );
            }

        }

    });
}

function listarTodosPlanosSite() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarTodosPlanosSite",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#modalLigarPlanos').html('');

            if (dados.length > 0) {

                $('#modalLigarPlanos').append('<option value="">Selecione o plano...</option>');

                for (var i = 0; i < dados.length; i++) {

                    if (dados[i].ativo === '1') {
                        $('#modalLigarPlanos').append('<option value="' + dados[i].nomePlano + '">' + dados[i].nomePlano + '</option>');
                    }

                }

            } else {

                $('#modalLigarPlanos').append('<option value="">Nenhum plano disponível</option>');

            }
        }

    });
}

$('#formSolicitarLigacao').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = cadastrarSoliciataoLigacao(self);
});

function cadastrarSoliciataoLigacao(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/site/ourilandia_do_norte/solicitar_ligacao",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroSolicLigacao').html(

                    '<div class="bs-example bs-example-standalone" data-example-id="dismissible-alert-js"> ' +
                    '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="background-color: #ff211a;">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true" style="color: white;">×</span>' +
                    '</button>' +

                    '<h4 style="color: #ffffff;">Algo deu errado!</h4> ' +
                    '<p style="color: #ffffff;">' + retorno.msg + '</p> ' +

                    '</div> ' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgSucessoLigacoes').html(

                    '<div class="bs-example bs-example-standalone" data-example-id="dismissible-alert-js"> ' +
                    '<div class="alert alert-success alert-dismissible fade in" role="alert" style="background-color: #5CB85C;">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true" style="color: white;">×</span>' +
                    '</button>' +

                    '<h4 style="color: #ffffff;">Tudo certo!</h4> ' +
                    '<p style="color: #ffffff;">' + retorno.msg + '</p> ' +

                    '</div>' +
                    '</div>'
                );

                $('#modalLigar').modal('hide');

                $(".alert").delay(15000).slideUp(1000, function() { $(this).alert('close'); });

                $('#formSolicitarLigacao').each(function() {
                    this.reset();
                });

            }
        }
    });
}