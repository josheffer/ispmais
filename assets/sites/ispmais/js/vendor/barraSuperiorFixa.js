listarInfosBarraSuperiorFixa();

function listarInfosBarraSuperiorFixa() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarInfosBarraSuperiorFixa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            $('#mensagem_barraSuperiorFixa').html(dados.mensagem_barraSuperiorFixa);
            $('#email_barraSuperiorFixa').html(dados.email_barraSuperiorFixa);
            $('#telefone_barraSuperiorFixa').html(dados.telefone_barraSuperiorFixa);

            

            if(dados.ativo_facebook_barraSuperiorFixa === '1')
            {
                $('#facebook_barraSuperiorFixa').html('<a class="topbar-link" href="'+ dados.facebook_barraSuperiorFixa +'" target="_blank"><i class="fab fa-facebook-f"></i></a>');
                $('#facebook_footer').html('<a class="topbar-link" href="'+ dados.facebook_barraSuperiorFixa +'" target="_blank"><i class="fab fa-facebook-f"></i></a>');
            }
            
            if(dados.ativo_instagram_barraSuperiorFixa === '1')
            {
                $('#instagram_barraSuperiorFixa').html('<a class="topbar-link" href="'+ dados.instagram_barraSuperiorFixa +'" target="_blank"><i class="fab fa-instagram"></i></a>');
                $('#instagram_footer').html('<a class="topbar-link" href="'+ dados.instagram_barraSuperiorFixa +'" target="_blank"><i class="fab fa-instagram"></i></a>');
            }
            
            if(dados.ativo_minhaConta_barraSuperiorFixa === '1')
            {
                $('#minhaConta_barraSuperiorFixa').html('<a class="topbar-link" href="javascript:void(0);"><i class="fa fa-user"></i> Minha conta</a>');
            }
            
        }

    });
}