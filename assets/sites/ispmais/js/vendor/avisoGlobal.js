listarAvisoGlobal();

function listarAvisoGlobal() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarAvisoGlobal",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            if(dados.ativo_aviso === '1')
            {
                $('#mensagemGlobal').html(
                    
                    '<div class="top-bar margin-for-home" style="text-align: center; background-color: #000000;">' +
                        '<div class="custom-width">' +
                            '<div class="text-white">' +
                                '<p class="text-white" style="color: #ffffff;">AVISO IMPORTANTE!!!</p>' +
                                '<p class="text-white" style="color: #ffffff;">'+ dados.texto_aviso +'</p>' +
                            '</div>' +
                        '</div>' +
                    '</div>'

                );
            }
            
        }

    });
}