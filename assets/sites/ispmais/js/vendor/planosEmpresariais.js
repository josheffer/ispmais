$(document).ready(function() {

    function limpa_formulario_empresarial_cep() {

        $("#rua_planoEmpresarial").val("");
        $("#bairro_planoEmpresarial").val("");
        $("#cidade_planoEmpresarial").val("");
        $("#uf_planoEmpresarial").val("");
        $("#ibge_planoResidencial").val("");
    }

    $("#cep_planoEmpresarial").blur(function() {

        var cep = $(this).val().replace(/\D/g, '');

        if (cep != "") {
            var validacep = /^[0-9]{8}$/;

            if (validacep.test(cep)) {

                $("#rua_planoEmpresarial").val("...");
                $("#bairro_planoEmpresarial").val("...");
                $("#cidade_planoEmpresarial").val("...");
                $("#uf_planoEmpresarial").val("...");
                $("#ibge_planoEmpresarial").val("...");

                $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {

                        $("#rua_planoEmpresarial").val(dados.logradouro);
                        $("#bairro_planoEmpresarial").val(dados.bairro);
                        $("#cidade_planoEmpresarial").val(dados.localidade);
                        $("#uf_planoEmpresarial").val(dados.uf);
                        $("#ibge_planoEmpresarial").val(dados.ibge);
                    } else {

                        limpa_formulario_empresarial_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } else {

                limpa_formulario_empresarial_cep();
                alert("Formato de CEP inválido.");
            }
        } else {

            limpa_formulario_empresarial_cep();
        }
    });
});

// ============================================================================================
var telefone_planoEmpresarial = document.getElementById('telefone_planoEmpresarial');
var maskOptionsTel_planoEmpresarial = {
    mask: '(00) 0 0000-0000'
};
var maskTel_planoEmpresarial = IMask(telefone_planoEmpresarial, maskOptionsTel_planoEmpresarial);
// ============================================================================================

// ============================================================================================
var telefoneFixo_planoEmpresarial = document.getElementById('telefoneFixo_planoEmpresarial');
var maskOptionsTelefoneFixo_planoEmpresarial = {
    mask: '(00) 0000-0000'
};
var maskTelFixo_planoEmpresarial = IMask(telefoneFixo_planoEmpresarial, maskOptionsTelefoneFixo_planoEmpresarial);
// ============================================================================================

// ============================================================================================
var cnpj_planoEmpresarial = document.getElementById('cnpj_planoEmpresarial');
var maskOptionsCNPJ_planoEmpresarial = {
    mask: '00.000.000/0000-00'
};
var maskCNPJ_planoEmpresarial = IMask(cnpj_planoEmpresarial, maskOptionsCNPJ_planoEmpresarial);
// ============================================================================================

listarPlanosEmpresariais();

function listarPlanosEmpresariais() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarPlanosEmpresariaisSite",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            dadosGlobaisEmpresariais = dados;

            $('#tabelaListarPlanosEmpresariais').html('');

            if (dados.length > 0) {

                for (var i = 0; i < dados.length; i++) {

                    if (dados[i].destaque_plano_empresarial === '1') {
                        var bgDestaque = '#0d1323';
                        var btnDestaque = 'btn-orange';
                        var corDestaque = '#be7b2e';
                        var textoDestaque = 'text-orange';
                        var planoTop = '<div class="table-circle aos-init aos-animate" data-aos="zoom-in" data-aos-delay="100"> <i class="fas fa-crown"></i> </div>';
                        var styleDestaque = 'style="margin-top: -50px;"';

                    } else {

                        var bgDestaque = '#151e35';
                        var btnDestaque = 'btn-outline';
                        var corDestaque = '#ffffff';
                        var textoDestaque = '';
                        var planoTop = '';
                        var styleDestaque = '';

                    }

                    $('#tabelaListarPlanosEmpresariais').append(

                        '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 margin-bottom50">' +

                        '<div class="table-content" style="width: auto; text-align: center; height: auto; margin: 0 auto; border-radius: 0 0 20px 20px; background: ' + bgDestaque + ' !important; border-top: 7px solid ' + corDestaque + '; padding: 0 15px 35px 15px;">' +

                        planoTop +

                        '<div class="text-container" ' + styleDestaque + '>' +

                        '</br>' +

                        '<h3 class="' + textoDestaque + '">' + dados[i].nome_plano_empresarial + '</h3>' +

                        '</br>' +

                        '<h4 class="' + textoDestaque + '" style="font-size: 7em; text-align: center;">' + dados[i].velocidade_plano_empresarial + '</h4>' +

                        '<h4 class="' + textoDestaque + '" style="font-size: 3em;margin-top: -20px; text-align: center;">' + dados[i].tipoVelocidade_plano_empresarial + '</h4>' +

                        '<p class="' + textoDestaque + '" style="text-align: center;">' + dados[i].descricao_plano_empresarial + '</p>' +

                        '<div><hr></div>' +

                        '<div class="price">' +
                        '<h3 class="' + textoDestaque + '">R$ ' + dados[i].preco_plano_empresarial + '/mês.' + ' </h3>' +
                        '</div>' +

                        '<a href="javascript:void(0);" onclick="javascript:modalContratarPlanoEmpresarial(' + i + ');" class="btn btn-large ' + btnDestaque + '">Contratar</a>' +
                        '</div>' +
                        '</div>' +
                        '</div>'

                    );
                }

            } else {

                $('#tabelaListarPlanosEmpresariais').append('');

            }

        }

    });
}

function modalContratarPlanoEmpresarial(modalPlanoEmpresarial) {

    $('#modalContratarPlanoEmpresarial').modal('show');

    $("#nomePlanoEmpresarialModalContratar").html(dadosGlobaisEmpresariais[modalPlanoEmpresarial].nome_plano_empresarial);
    $("#nomePlanoEmpresarialContratar").html(dadosGlobaisEmpresariais[modalPlanoEmpresarial].nome_plano_empresarial);

    $("#id_planoEmpresarial").val(dadosGlobaisEmpresariais[modalPlanoEmpresarial].id_plano_empresarial);

}

$('#contratarPlanoEmpresarial').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = contratarPlanoEmpresarial(self);
});

function contratarPlanoEmpresarial(dados) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/site/ourilandia_do_norte/contratarPlanoEmpresarial",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroPlanoEmpresarial').html(

                    '<div class="bs-example bs-example-standalone" data-example-id="dismissible-alert-js"> ' +
                    '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="background-color: #ff211a;">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true" style="color: white;">×</span>' +
                    '</button>' +

                    '<h4 style="color: #ffffff;">Algo deu errado!</h4> ' +
                    '<p style="color: #ffffff;">' + retorno.msg + '</p> ' +

                    '</div> ' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#msgSucessoPlanoEmpresarial').html(

                    '<div class="bs-example bs-example-standalone" data-example-id="dismissible-alert-js"> ' +
                    '<div class="alert alert-success alert-dismissible fade in" role="alert" style="background-color: #5CB85C;">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true" style="color: white;">×</span>' +
                    '</button>' +

                    '<h4 style="color: #ffffff;">Tudo certo!</h4> ' +
                    '<p style="color: #ffffff;">' + retorno.msg + '</p> ' +

                    '</div></br>' +
                    '</div>'
                );

                $('#modalContratarPlanoEmpresarial').modal('hide');

                $('html, body').animate({ scrollTop: $('#subirPlanoEmpresarial').position().top }, 'slow');

                $('#contratarPlanoEmpresarial').each(function() {
                    this.reset();
                });

            }
        }
    });
}