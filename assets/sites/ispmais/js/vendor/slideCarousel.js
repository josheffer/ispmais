listarSlidesCarousel() ;

function listarSlidesCarousel() 
{
    
    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarSlidesCarouselSite",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            dadosGlobaisSlideCarousel = dados;

            $('#listarSlides').html('');
            $('#indicadoresSlides').html('');

            if (dados.length > 0) {

                for (var i = 0; i < dados.length; i++) {
                    
                    if(i == 0)
                    {
                        var bannerActive = 'active';

                    } else {
                        
                        var bannerActive = '';

                    }
                    
                    $('#listarSlides').append('<div class="item '+bannerActive+'"><img alt="'+dados[i].descricao_carousel+'" title="'+dados[i].descricao_carousel+'" src="/assets/sites/ispmais/img/banners/'+dados[i].imagem_carousel+'"></div>');
                }

                for (var i = 0; i < dados.length; i++) {
                    
                    if(i == 0)
                    {
                        var bannerActiveIndicadores = 'active';

                    
                    } else {
                        
                        var bannerActiveIndicadores = '';

                    }
                    
                    $('#indicadoresSlides').append('<li data-target="#fade-quote-carousel" data-slide-to="'+ i +'" class="'+bannerActiveIndicadores+'"></li>');
                }
                
            } else {

                $('#listarSlides').append('<div class="item active"><img src="/assets/sites/ispmais/img/banners/banner-default.png"></div>');
                
                $('#indicadoresSlides').append('<li data-target="#fade-quote-carousel" data-slide-to="0" class="active"></li>');

            }

        }

    });
}