listarFaleConoscoRegioes();

function listarFaleConoscoRegioes() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarFaleConoscoRegioes",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            dadosGlobais = dados;

            $('#listarFaleConosco').html('');

            if (dados.length > 0) {

                for (var i = 0; i < dados.length; i++) {



                    $('#listarFaleConosco').append(
                        '<div class="col-sm-4">' +
                        '<div class="box-container">' +
                        '<div class="box-title">' +
                        '<h4 class="text-center">' + dados[i].nome_regioes + '</h4>' +
                        '</div>' +

                        '<p><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp; ' + dados[i].endereco_regioes + '</p>' +
                        '<p><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp; ' + dados[i].email_regioes + '</p>' +
                        '<p><i class="fa fa-phone" aria-hidden="true"></i>&nbsp; ' + dados[i].foneFixoTexto_regioes + '</p>' +
                        '<p><a href="' + dados[i].linkWhatsapp_regioes + '" target="_blank"><i class="fab fa-whatsapp"></i>&nbsp; ' + dados[i].foneWhatsappTexto_regioes + '</a></p>' +

                        '</div>' +
                        '</div>'
                    );
                }

            } else {

                $('#listarFaleConosco').append('');

            }

        }

    });
}
