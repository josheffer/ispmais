listarLinksBarraSuperiorFixa();

function listarLinksBarraSuperiorFixa() {

    $.ajax({

        url: "/dashboard/site_admin/ourilandia/listarLinksBarraSuperiorFixaSite",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            $('#linksBarraSuperiorFixa').html('');
            
            if (dados.length > 0) {
                
                for (var i = 0; i < dados.length; i++) {

                    if(dados[i].target_links_barraSuperiorFixa === '1')
                    {
                        var targetLink = 'target="_blank"';

                    } else {
                        
                        var targetLink = '';
                    }

                    if(dados[i].status_links_barraSuperiorFixa === '1')
                    {
                        var link = '<li><a '+targetLink+' href="'+dados[i].link_links_barraSuperiorFixa+'" >'+dados[i].nome_links_barraSuperiorFixa+'</a></li>'
                    } else 
                    {
                        var link = '';
                    }
                    
                    $('#linksBarraSuperiorFixa').append(link);
                }

            } else {

                $('#linksBarraSuperiorFixa').append(
                    // '<td colspan="4">' +
                    // '<center class="mt-4 text-center">' +
                    // '<div class="col-md-12 text-center">' +
                    // '<div class="alert alert-danger text-center">' +
                    // '<i class="fas fa-exclamation-circle"></i> Nenhum link cadastrado!!' +
                    // '</div>' +
                    // '</div>' +
                    // '</center>' +
                    // '</td>'
                );

            }
            
        }

    });
}