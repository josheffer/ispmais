// ============================================================================================
var telefone_Newsletter = document.getElementById('telefone_newsletter');
var maskOptionsTel_newsletter = {
    mask: '(00) 0 0000-0000'
};
var maskTel_newsletter = IMask(telefone_Newsletter, maskOptionsTel_newsletter);
// ============================================================================================


$('#formNewsletter').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = assinarNewsletter(self);
});

function assinarNewsletter(dados) {
    
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/site/ourilandia_do_norte/assinarNewsletter",
        dataType: 'json',
        
        success: function(retorno) {
            
            if (retorno.ret == false) {

                $('#msgSucessoNewsletter').html(

                    '<div class="bs-example bs-example-standalone" data-example-id="dismissible-alert-js"> ' +
                        '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="background-color: #ff211a;">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                '<span aria-hidden="true" style="color: white;">×</span>' +
                            '</button>' +
                            
                            '<h4 style="color: #ffffff;">Algo deu errado!</h4> ' +
                            '<p style="color: #ffffff;">'+ retorno.msg +'</p> ' +
                                
                        '</div> ' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html, body').animate({scrollTop:$('#subirNewsletter').position().top}, 'slow');
                
            } else {

                $('#msgSucessoNewsletter').html(

                    '<div class="bs-example bs-example-standalone" data-example-id="dismissible-alert-js"> ' +
                        '<div class="alert alert-success alert-dismissible fade in" role="alert" style="background-color: #5CB85C;">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                '<span aria-hidden="true" style="color: white;">×</span>' +
                            '</button>' +
                            
                            '<h4 style="color: #ffffff;">Tudo certo!</h4> ' +
                            '<p style="color: #ffffff;">'+ retorno.msg +'</p> ' +
                            
                        '</div></br>' +
                    '</div>'
                );
                
                $('html, body').animate({scrollTop:$('#subirNewsletter').position().top}, 'slow');

                $('#formNewsletter').each(function() {
                    this.reset();
                });
                
            }
        }
    });
}