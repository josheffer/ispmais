<div class="container-fluid">
			
	<div class="row mt-2">
		
		<div class="col-12">

			<div class="page-title-box">
				<ol class="breadcrumb m-0">
					<li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
					<li class="breadcrumb-item"><a href="javascript:void(0);">Site</a></li>
					<li class="breadcrumb-item active text-warning">Intro</li>
				</ol>
			</div>
			
			<?php
				$this->load->view('botoesSite');
			?>

			<div class="row mt-3">
				
				<div class="col-lg-12">
					
					<div class="card">

						<div class="card-body">
						
							<div class="row">

								<div class="col-12">
								
									<div class="row">

										<div class="col-xl-12">

											<div id="accordion" class="mb-3">
												
												<div class="card mb-1">
													
													<div class="card-header" id="headingOne">
														<h5 class="m-0">
															<a class="text-dark collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false">
																<i class=" fas fa-plus-circle mr-1 text-mute"></i> 
																Cadastrar nova região
															</a>
														</h5>
													</div>
										
													<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
														
														<div class="card-body">
															
															<form id="formCadastrarRegiao">
																
																<div class="form-row">
																	
																	<div class="form-group col-md-6">
																		<label class="col-form-label">Nome da região</label>
																		<input type="text" class="form-control" id="nomeRegiao" name="nomeRegiao" autocomplete="off">
																	</div>

																	<div class="form-group col-md-6">
																		<label class="col-form-label">Link da página</label>
																		<input type="text" class="form-control" id="linkPaginaRegiao" name="linkPaginaRegiao" autocomplete="off">
																	</div>

																	<div class="form-group col-md-6">
																		<label class="col-form-label"><i class="fas fa-map-marker-alt"></i> Endereço</label>
																		<input type="text" class="form-control" id="enderecoRegiao" name="enderecoRegiao" autocomplete="off">
																	</div>

																	<div class="form-group col-md-6">
																		<label class="col-form-label"><i class="fas fa-envelope"></i> E-mail</label>
																		<input type="text" class="form-control" id="emailRegiao" name="emailRegiao" autocomplete="off">
																	</div>

																	<div class="form-group col-md-6">
																		<label class="col-form-label"><i class="fas fa-phone-volume"></i> Telefone fixo</label>
																		<input type="text" class="form-control" id="foneFixoRegiao" name="foneFixoRegiao" autocomplete="off">
																	</div>

																	<div class="form-group col-md-3">
																		<label class="col-form-label"><i class="fab fa-whatsapp"></i> WhatsApp</label>
																		<input type="text" class="form-control" id="whatsappRegiao" name="whatsappRegiao" autocomplete="off">
																	</div>

																	<div class="form-group col-md-3">
																		<label class="col-form-label"><i class="mdi mdi-link"></i> Link do WhatsApp <small class="text-warning"><a href="https://www.convertte.com.br/gerador-link-whatsapp/" target="_blank" rel="noopener noreferrer">(Gerar Link)</a></small></label>
																		<input type="text" class="form-control" id="linkWhatsappRegiao" name="linkWhatsappRegiao" autocomplete="off">
																	</div>
																	
																	<div class="form-group mt-3 col-md-12">
																		<button type="submit" id="botaoCadastrarRegiao" class="btn btn-block btn-sm btn-primary waves-effect waves-light">Cadastrar</button>
																	</div>
															
																</div>
														
															</form>
													
														</div>

													</div>

												</div>
												
												<div class="card mb-1">
													
													<div class="card-header" id="headingTwo">
														<h5 class="m-0">
															<a class="text-dark" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">
																<i class=" fas fa-map-marker-alt mr-1 text-mute"></i> 
																Regiões cadastradas
															</a>
														</h5>
													</div>

													<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
														
														<div class="card-body">
															
															<div class="table-responsive">
																<table class="table table-bordered table-hover mb-0">
																	
																	<thead>
																		<tr>
																			<th>#</th>
																			<th>Nome da região</th>
																			<th>Status <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Ativa ou desativa a opção na intro do site" data-original-title="Ativa ou desativa a opção na intro do site"></i></th>
																			<th>Ativo no site <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Ative ou desativa a visualização da região na sessão de fale conosco na página do site" data-original-title="Ative ou desativa a visualização da região na sessão de fale conosco na página do site"></i></th>
																			<th>Ações</th>
																		</tr>
																	</thead>

																	<tbody id="tabelaRegioes"> </tbody>
																	
																</table>
															</div>
															
														</div>

													</div>

												</div>
										
											</div>

										</div>

									</div>
							
								</div>

							</div>
						
						</div>

					</div>

				</div>

			</div>

			<div class="row mt-3">
				
				<div class="col-lg-12">
					
					<div class="card">

						<div class="card-body">
						
							<div class="row">

								<div class="col-12">
								
									<div class="row">

										<div class="col-xl-12">

											<div id="accordion" class="mb-3">
												
												<div class="card mb-1">
													
													<div class="card-header" id="headingOne">
														<h5 class="m-0">
															<a class="text-dark collapsed" data-toggle="collapse" href="#configsSEO" aria-expanded="false">
																<i class=" fas fa-plus-circle mr-1 text-mute"></i> 
																Configurações de SEO <small class="text-warning">(Buscas no Google)</small>
															</a>
														</h5>
													</div>
										
													<div id="configsSEO" class="collapse" aria-labelledby="accordSEO" data-parent="#accordion" style="">
														<div class="card-body">
														
															<form id="formSEOIntro">

																<div class="form-row">
																	
																	<div class="form-group col-md-4">
																		<label>CSS personalizado <small class="text-warning">(Exclusivo para aplicações externas)</small> </label>
																		<textarea id="css_seo_intro" name="css_seo_intro" class="form-control" rows="3"></textarea>
																	</div>

																	<div class="form-group col-md-4">
																		<label data-toggle="tooltip" data-placement="top" title="Cole o código COM a tag <script></script>" data-original-title="Cole o código COM a tag <script></script>">JS personalizado <small class="text-warning">(<strong>TAG HEAD</strong>, pixel, google..)</small> </label>
																		<textarea id="js_seo_intro" name="js_seo_intro" class="form-control" rows="3"></textarea>
																	</div>

																	<div class="form-group col-md-4">
																		<label data-toggle="tooltip" data-placement="top" title="Cole o código COM a tag <script></script>" data-original-title="Cole o código COM a tag <script></script>">JS personalizado <small class="text-warning">(<strong>TAG BODY</strong>, pixel, google..)</small> </label>
																		<textarea id="jsBody_seo_intro" name="jsBody_seo_intro" class="form-control" rows="3"></textarea>
																	</div>

																	<div class="form-group col-md-12">
																		<label data-toggle="tooltip" data-placement="top" title="Palavras chaves para ajudar nas buscas" data-original-title="Palavras chaves para ajudar nas buscas"> KeyWords "Palavras chaves"</label>
																		<textarea id="keywords_seo_intro" name="keywords_seo_intro" class="form-control" rows="2"></textarea>
																	</div>

																	<div class="form-group col-md-4">
																		<label data-toggle="tooltip" data-placement="top" title="Descrição 1, fale sobre tópicos da empresa" data-original-title="Descrição 1, fale sobre tópicos da empresa">Description 1 </label>
																		<textarea id="description1_seo_intro" name="description1_seo_intro" class="form-control" rows="3"></textarea>
																	</div>

																	<div class="form-group col-md-4">
																		<label data-toggle="tooltip" data-placement="top" title="Descrição 2, fale sobre tópicos da empresa" data-original-title="Descrição 2, fale sobre tópicos da empresa">Description 2 </label>
																		<textarea id="description2_seo_intro" name="description2_seo_intro" class="form-control" rows="3"></textarea>
																	</div>

																	<div class="form-group col-md-4">
																		<label data-toggle="tooltip" data-placement="top" title="Descrição 3, fale sobre tópicos da empresa" data-original-title="Descrição 3, fale sobre tópicos da empresa">Description 2 </label>
																		<textarea id="description3_seo_intro" name="description3_seo_intro" class="form-control" rows="3"></textarea>
																	</div>

																	<div class="form-group col-md-4">
																		<label data-toggle="tooltip" data-placement="top" title="Informe as coordenadas da sua localização no Google Maps" data-original-title="Informe as coordenadas da sua localização no Google Maps">Coordenadas Google Maps</label>
																		<input type="text" class="form-control" id="coordenadas_seo_intro" name="coordenadas_seo_intro" autocomplete="off">
																	</div>

																	<div class="form-group col-md-4">
																		<label data-toggle="tooltip" data-placement="top" title="Informe a cidade e estado da localização" data-original-title="Informe a cidade e estado da localização">Cidade e estado</label>
																		<input type="text" class="form-control" id="cidadeEstado_seo_intro" name="cidadeEstado_seo_intro" autocomplete="off">
																	</div>

																	<div class="form-group col-md-4">
																		<label data-toggle="tooltip" data-placement="top" title="Informe o estado e país da localização" data-original-title="Informe o estado e país da localização">Estado e País</label>
																		<input type="text" class="form-control" id="estadoPais_seo_intro" name="estadoPais_seo_intro" autocomplete="off">
																	</div>

																	<div class="form-group col-md-6">
																		<label data-toggle="tooltip" data-placement="top" title="Informe título do site" data-original-title="Informe título do site">Título do site (Aba do site)</label>
																		<input type="text" class="form-control" id="titulo_seo_intro" name="titulo_seo_intro" autocomplete="off">
																	</div>

																	<div class="form-group col-md-6">
																		<label data-toggle="tooltip" data-placement="top" title="Informe nome do site" data-original-title="Informe nome do site">Nome do site</label>
																		<input type="text" class="form-control" id="nome_seo_intro" name="nome_seo_intro" autocomplete="off">
																	</div>

																	<div class="form-group col-md-6">
																		<label data-toggle="tooltip" data-placement="top" title="Informe o código de verificação do Google" data-original-title="Informe o código de verificação do Google">Código de verificação do Google</label>
																		<input type="text" class="form-control" id="codigoGoogle_seo_intro" name="codigoGoogle_seo_intro" autocomplete="off">
																	</div>

																	<div class="form-group col-md-6">
																		<label data-toggle="tooltip" data-placement="top" title="Informe a cor do tema do site (Visualização mobile)" data-original-title="Informe a cor do tema do site (Visualização mobile)">Cor do tema <small class="text-warning">(Visualização mobile)</small></label>
																		<input type="color" class="form-control" id="corTema_seo_intro" name="corTema_seo_intro" autocomplete="off">
																	</div>

																	<div class="form-group col-md-6">
																		<label data-toggle="tooltip" data-placement="top" title="Escolha a imagem para aparecer ao compartilhar o link do site" data-original-title="Escolha a imagem para aparecer ao compartilhar o link do site">Logo do site</label>
																		<input type="file" accept="image/*" class="form-control" id="imagemLogo_seo_intro" name="imagemLogo_seo_intro" autocomplete="off">
																	</div>

																	<div class="form-group col-md-6">
																		<label>Imagem</label>
																		<button type="button" class="btn btn-block btn--md btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modalVerImagemSEOIntro">Ver</button>
																	</div>

																	<div class="form-group mt-3 col-md-12">
																		<button type="submit" class="btn btn-block btn-sm btn-primary waves-effect waves-light">Atualizar</button>
																	</div>
																	
																</div>

															</form>
																
														</div>
													</div>

												</div>
												
										
											</div>

										</div>

									</div>
							
								</div>

							</div>
						
						</div>

					</div>

				</div>

			</div>
			
		</div>
		
	</div> 

</div>

<div id="alterarInformacoesRegiao" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Alterar informações da região: <u><i><span id="alterarTituloNomeRegiao"></span></i></u></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>
            
            <div class="modal-body">

            <div id="msgErroEditarRegiao"></div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            
                        <form id="formAlterarRegiao">

                            <input type="hidden" name="idRegiao" id="idRegiao">

                            <div class="form-row align-items-center">
                                
                                <div class="col-md-6">
                                    <h6 class="font-13">Nome da Regiao</h6>
                                    <input type="text" class="form-control mb-2" id="alterarNomeRegiao" name="alterarNomeRegiao" autocomplete="off" autofocus>
                                </div>

								<div class="col-md-6">
                                    <h6 class="font-13">Link da Página</h6>
                                    <input type="text" class="form-control mb-2" id="alterarLinkPaginaRegiao" name="alterarLinkPaginaRegiao" autocomplete="off" autofocus>
                                </div>

								<div class="form-group col-md-6">
									<label class="col-form-label"><i class="fas fa-map-marker-alt"></i> Endereço</label>
									<input type="text" class="form-control" id="alterarenderecoRegiao" name="alterarenderecoRegiao" autocomplete="off">
								</div>

								<div class="form-group col-md-6">
									<label class="col-form-label"><i class="fas fa-envelope"></i> E-mail</label>
									<input type="text" class="form-control" id="alteraremailRegiao" name="alteraremailRegiao" autocomplete="off">
								</div>

								<div class="form-group col-md-6">
									<label class="col-form-label"><i class="fas fa-phone-volume"></i> Telefone fixo</label>
									<input type="text" class="form-control" id="alterarfoneFixoRegiao" name="alterarfoneFixoRegiao" autocomplete="off">
								</div>

								<div class="form-group col-md-3">
									<label class="col-form-label"><i class="fab fa-whatsapp"></i> WhatsApp</label>
									<input type="text" class="form-control" id="alterarwhatsappRegiao" name="alterarwhatsappRegiao" autocomplete="off">
								</div>

								<div class="form-group col-md-3">
									<label class="col-form-label"><i class="mdi mdi-link"></i> Link do WhatsApp <small class="text-warning"><a href="https://www.convertte.com.br/gerador-link-whatsapp/" target="_blank" rel="noopener noreferrer">(Gerar Link)</a></small></label>
									<input type="text" class="form-control" id="alterarlinkWhatsappRegiao" name="alterarlinkWhatsappRegiao" autocomplete="off">
								</div>
								
                                <div class="col-xl-4"></div>
                                
                                <div class="col-xl-12 mt-3">
                                    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                        <button type="submit" id="botaoAtualizarRegiao" class="btn btn-success btn-block waves-effect waves-light">Atualizar</button>
                                    </div>
                                </div>

                                <div class="col-xl-4"></div> 

                            </div>

                        </form>
                            
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>

<div id="ModalExcluiRegiao" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Excluir região: <u><i><strong><span class="text-warning" id="tituloExcluirRegiao"></span></strong></i></u></h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>

			<div id="msgERROExcluirRegiao" class="col-md-12 mt-2"></div>
			
			<form id="formExcluirRegiao">

				<div class="modal-body">
					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="hidden" class="form-control" id="idRegiaoExcluir" name="idRegiaoExcluir">
								<p>Deseja realmente excluir a região: 
									<u><i><strong><span class="text-warning" id="excluirNomeRegiao"></span></strong></i></u>?
									Após a exclusão não será possível reverter essa ação! Tem certeza?
								</p>
							</div>
						</div>
					</div>
					
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-danger waves-effect waves-light">Excluir</button>
				</div>
			</form>

		</div>
	</div>
</div>

<div id="modalVerImagemSEOIntro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Imagem de SEO</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-md-12">

                    <img id="verImagemDoSEOIntro" width="100%" >
                        
                    </div>
                    
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
