<div class="container-fluid">
	
	<div class="row mt-2">
        
		<div class="col-12">

            <div class="page-title-box">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
					<li class="breadcrumb-item"><a href="/dashboard/site">Site</a></li>
					<li class="breadcrumb-item"><a href="/dashboard/site_admin/ourilandia_configs">Ourilândia do Norte</a></li>
					<li class="breadcrumb-item active text-warning">Sobre a empresa</li>
                </ol>
            </div>
			
			<?php
				$this->load->view('botoesSiteConfigs');
			?>

			<div class="row mt-3">

				<div class="col-lg-12">
					
					<div class="card">

						<div class="card-body">
						
								<div class="row">
                                    
                                    <div class="card mb-1">
                                        
                                        <form id="formPaginaSObre" enctype="multipart/form-data">
                                        
                                        <div class="card-body">

                                                <h4> <i class="mdi mdi-arrow-right-bold-circle"></i> <u> Sobre a empresa</u> </h4>  
                                                
                                                <div class="form-row mt-4">

                                                    <div class="form-group col-md-6">
                                                        <label data-toggle="tooltip" data-placement="top" title="Informe o título que irá aparecer ao abrir a página" data-original-title="Informe o título que irá aparecer ao abrir a página">Título</label>
                                                        <input type="text" class="form-control" id="titulo_paginaSobre" name="titulo_paginaSobre" autocomplete="off">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label data-toggle="tooltip" data-placement="top" title="Informe a descrição que irá aparecer ao abrir a página" data-original-title="Informe a descrição que irá aparecer ao abrir a página">Descrição</label>
                                                        <input type="text" class="form-control" id="descricao_paginaSobre" name="descricao_paginaSobre" autocomplete="off">
                                                    </div>


                                                    <div class="form-group col-md-6">
                                                        <label data-toggle="tooltip" data-placement="top" title="Escolha a imagem" data-original-title="Escolha a imagem">Imagem da empresa</label>
                                                        <input type="file" accept="image/*" class="form-control" id="imagem_paginaSobre" name="imagem_paginaSobre" autocomplete="off">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label>Imagem</label>
                                                        <button type="button" class="btn btn-block btn--md btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modalVerImagemSobre">Ver</button>
                                                    </div>
                                                    
                                                    <div class="form-group col-md-12">
                                                        <label>Nome da empresa </label>
                                                        <textarea id="nomeEmpresa_paginaSobre" name="nomeEmpresa_paginaSobre" class="form-control" rows="3"></textarea>
                                                    </div>
                                                    
                                                    <div class="form-group col-md-12">
                                                        <label>Texto sobre a empresa <small class="text-warning">(Fale sobre a empresa)</small> </label>
                                                        <textarea id="textoSobre_paginaSobre" name="textoSobre_paginaSobre" class="form-control" rows="3"></textarea>
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label>Missão</label>
                                                        <textarea id="textoMissao_paginaSobre" name="textoMissao_paginaSobre" class="form-control" rows="3"></textarea>
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label>Visão</label>
                                                        <textarea id="textoVisao_paginaSobre" name="textoVisao_paginaSobre" class="form-control" rows="3"></textarea>
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label>Valores</label>
                                                        <textarea id="textoValores_paginaSobre" name="textoValores_paginaSobre" class="form-control" rows="3"></textarea>
                                                    </div>
                                                    
                                                    <div class="form-group mt-3 col-md-12">
                                                        <button type="submit" id="botaoAttPaginaSobre" class="btn btn-block btn-sm btn-primary waves-effect waves-light">Atualizar</button>
                                                    </div>
                                                    
                                                </div>
                                                
                                        </div>

                                        </form>

                                    </div>
                                    
								</div>
								
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>

<div id="modalVerImagemSobre" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Imagem de SEO</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-md-12">

                    <img id="verImagemSobre" width="100%" >
                        
                    </div>
                    
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>