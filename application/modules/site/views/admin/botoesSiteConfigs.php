<div class="row">

    <div class="col-xl-12">

        <div id="accordion" class="mb-3">


            <div class="card mb-1">
                
                <a class="text-dark collapsed text-center" data-toggle="collapse" href="#menuNavegacaoPainel" aria-expanded="false">
                    <div class="card-header" id="headingOne">
                        <h5 class="m-0 text-center">
                            Menu de navegação
                        </h5>
                    </div>
                </a>

                <div id="menuNavegacaoPainel" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="">
                    <div class="card-body d-flex justify-content-center">

                        <a href="/dashboard/site" data-toggle="tooltip" data-placement="top" data-original-title="Configurações da intro do site">
                            <button type="button" class="btn mr-1 btn-warning text-white">Intro</button>
                        </a>

                        <a href="/dashboard/site/bio" data-toggle="tooltip" data-placement="top" data-original-title="Configurações da BIO do site">
                            <button type="button" class="btn mr-1 btn-danger text-white">BIO</button>
                        </a>

                        <i class="mdi mdi-chevron-right mt-1 ml-1 mr-1"></i>

                        <div class="btn-group mb-2 mr-1">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Páginas internas <i class="mdi mdi-chevron-down"></i></button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="/dashboard/site_admin/ourilandia_pagina_sobre" data-toggle="tooltip" data-placement="top" data-original-title="Configurações da página SOBRE A EMPRESA">Sobre</a>
                                <!-- <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Separated link</a> -->
                            </div>
                        </div>

                        <a href="/dashboard/site_admin/ourilandia_configs" data-toggle="tooltip" data-placement="top" data-original-title="Configurações da página de Ourilândia do Norte">
                            <button type="button" class="btn mr-1 bg-primary text-white">Configurações</button>
                        </a>
                        
                        <a href="/dashboard/site_admin/ourilandia_ligacoes_pendentes" data-toggle="tooltip" data-placement="top" data-original-title="Solicitações de ligações">
                            <button type="button" class="btn mr-1 bg-purple text-white">Ligações</button>
                        </a>

                        <a href="/dashboard/site_admin/ourilandia_solicitacoesResidencias_pendentes" data-toggle="tooltip" data-placement="top" data-original-title="Solicitações residenciais">
                            <button type="button" class="btn mr-1 bg-danger text-white">Residenciais</button>
                        </a>

                        <a href="/dashboard/site_admin/ourilandia_solicitacoesEmpresariais_pendentes" data-toggle="tooltip" data-placement="top" data-original-title="Solicitações empresariais">
                            <button type="button" class="btn mr-1 bg-dark text-white">Empresariais</button>
                        </a>

                        <a href="/dashboard/site_admin/ourilandia_newsletter" data-toggle="tooltip" data-placement="top" data-original-title="Cadastros de newsletter">
                            <button type="button" class="btn mr-1 bg-info text-white">Newsletter</button>
                        </a>

                    </div>

                </div>

            </div>

        </div>

    </div>
    
</div>