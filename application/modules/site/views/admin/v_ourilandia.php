<div class="container-fluid">
	
	<div class="row mt-2">
		
		<div class="col-12">

			<div class="page-title-box">
				<ol class="breadcrumb m-0">
					<li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
					<li class="breadcrumb-item"><a href="/dashboard/site">Site</a></li>
					<li class="breadcrumb-item active text-warning">Ourilândia do Norte</li>
				</ol>
			</div>
			
			<?php
				$this->load->view('botoesSiteConfigs');
			?>

			<div class="row mt-3">
				<div class="col-lg-12">
					
					<div class="card">
						<div class="card-body">
						
								<div class="row">
									<div class="col-12">
									
										<div class="row">
											<div class="col-xl-12">
												<div id="accordion" class="mb-3">

													<div class="card mb-1">
														
														<form id="formAttAConfigSEO" enctype="multipart/form-data">

															<div class="card-header" id="accordSEO">
																
																<h5 class="m-0">
																	
																	<a class="text-dark collapsed" data-toggle="collapse" href="#accordSEO_ourilandia" aria-expanded="false">
																		<i class=" mdi mdi-search-web mr-1 text-mute"></i> 
																		SEO <small class="text-warning">(Otimização para buscas)</small>
																	</a>
																	
																</h5>
																
															</div>
											
															<div id="accordSEO_ourilandia" class="collapse" aria-labelledby="accordSEO" data-parent="#accordion" style="">
																<div class="card-body">
																
																		<div class="form-row">
																			
																			<div class="form-group col-md-4">
																				<label>CSS personalizado <small class="text-warning">(Campo exclusivo para css de outras aplicações)</small> </label>
																				<textarea id="css_seo_ourilandia" name="css_seo_ourilandia" class="form-control" rows="3"></textarea>
																			</div>

																			<div class="form-group col-md-4">
																				<label data-toggle="tooltip" data-placement="top" title="Cole o código COM a tag <script></script>" data-original-title="Cole o código COM a tag <script></script>">JS personalizado <small class="text-warning">(<strong>TAG HEAD</strong>, pixel, google..)</small> </label>
																				<textarea id="js_seo_ourilandia" name="js_seo_ourilandia" class="form-control" rows="3"></textarea>
																			</div>

																			<div class="form-group col-md-4">
																				<label data-toggle="tooltip" data-placement="top" title="Cole o código COM a tag <script></script>" data-original-title="Cole o código COM a tag <script></script>">JS personalizado <small class="text-warning">(<strong>TAG BODY</strong>, pixel, google..)</small> </label>
																				<textarea id="jsBody_seo_ourilandia" name="jsBody_seo_ourilandia" class="form-control" rows="3"></textarea>
																			</div>

																			<div class="form-group col-md-12">
																				<label data-toggle="tooltip" data-placement="top" title="Palavras chaves para ajudar nas buscas" data-original-title="Palavras chaves para ajudar nas buscas"> KeyWords "Palavras chaves"</label>
																				<textarea id="keywords_seo_ourilandia" name="keywords_seo_ourilandia" class="form-control" rows="2"></textarea>
																			</div>

																			<div class="form-group col-md-4">
																				<label data-toggle="tooltip" data-placement="top" title="Descrição 1, fale sobre tópicos da empresa" data-original-title="Descrição 1, fale sobre tópicos da empresa">Description 1 </label>
																				<textarea id="description1_seo_ourilandia" name="description1_seo_ourilandia" class="form-control" rows="3"></textarea>
																			</div>

																			<div class="form-group col-md-4">
																				<label data-toggle="tooltip" data-placement="top" title="Descrição 2, fale sobre tópicos da empresa" data-original-title="Descrição 2, fale sobre tópicos da empresa">Description 2 </label>
																				<textarea id="description2_seo_ourilandia" name="description2_seo_ourilandia" class="form-control" rows="3"></textarea>
																			</div>

																			<div class="form-group col-md-4">
																				<label data-toggle="tooltip" data-placement="top" title="Descrição 3, fale sobre tópicos da empresa" data-original-title="Descrição 3, fale sobre tópicos da empresa">Description 2 </label>
																				<textarea id="description3_seo_ourilandia" name="description3_seo_ourilandia" class="form-control" rows="3"></textarea>
																			</div>

																			<div class="form-group col-md-4">
																				<label data-toggle="tooltip" data-placement="top" title="Informe as coordenadas da sua localização no Google Maps" data-original-title="Informe as coordenadas da sua localização no Google Maps">Coordenadas Google Maps</label>
																				<input type="text" class="form-control" id="coordenadas_seo_ourilandia" name="coordenadas_seo_ourilandia" autocomplete="off">
																			</div>

																			<div class="form-group col-md-4">
																				<label data-toggle="tooltip" data-placement="top" title="Informe a cidade e estado da localização" data-original-title="Informe a cidade e estado da localização">Cidade e estado</label>
																				<input type="text" class="form-control" id="cidadeEstado_seo_ourilandia" name="cidadeEstado_seo_ourilandia" autocomplete="off">
																			</div>

																			<div class="form-group col-md-4">
																				<label data-toggle="tooltip" data-placement="top" title="Informe o estado e país da localização" data-original-title="Informe o estado e país da localização">Estado e País</label>
																				<input type="text" class="form-control" id="estadoPais_seo_ourilandia" name="estadoPais_seo_ourilandia" autocomplete="off">
																			</div>

																			<div class="form-group col-md-6">
																				<label data-toggle="tooltip" data-placement="top" title="Informe título do site" data-original-title="Informe título do site">Título do site (Aba do site)</label>
																				<input type="text" class="form-control" id="titulo_seo_ourilandia" name="titulo_seo_ourilandia" autocomplete="off">
																			</div>

																			<div class="form-group col-md-6">
																				<label data-toggle="tooltip" data-placement="top" title="Informe nome do site" data-original-title="Informe nome do site">Nome do site</label>
																				<input type="text" class="form-control" id="nome_seo_ourilandia" name="nome_seo_ourilandia" autocomplete="off">
																			</div>

																			<div class="form-group col-md-6">
																				<label data-toggle="tooltip" data-placement="top" title="Informe o código de verificação do Google" data-original-title="Informe o código de verificação do Google">Código de verificação do Google</label>
																				<input type="text" class="form-control" id="codigoGoogle_seo_ourilandia" name="codigoGoogle_seo_ourilandia" autocomplete="off">
																			</div>

																			<div class="form-group col-md-6">
																				<label data-toggle="tooltip" data-placement="top" title="Informe a cor do tema do site (Visualização mobile)" data-original-title="Informe a cor do tema do site (Visualização mobile)">Cor do tema <small class="text-warning">(Visualização mobile)</small></label>
																				<input type="color" class="form-control" id="corTema_seo_ourilandia" name="corTema_seo_ourilandia" autocomplete="off">
																			</div>

																			<div class="form-group col-md-6">
																				<label data-toggle="tooltip" data-placement="top" title="Escolha a imagem para aparecer ao compartilhar o link do site" data-original-title="Escolha a imagem para aparecer ao compartilhar o link do site">Logo do site</label>
																				<input type="file" accept="image/*" class="form-control" id="imagemLogo_seo_ourilandia" name="imagemLogo_seo_ourilandia" autocomplete="off">
																			</div>

																			<div class="form-group col-md-6">
																				<label>Imagem</label>
																				<button type="button" class="btn btn-block btn--md btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modalVerImagemSEO">Ver</button>
																			</div>

																			<div class="form-group mt-3 col-md-12">
																				<button type="submit" id="botaoAttAvisoGlobal" class="btn btn-block btn-sm btn-primary waves-effect waves-light">Atualizar</button>
																			</div>
																			
																		</div>
																		
																</div>
															</div>

														</form>

													</div>

													<div class="card mb-1">
													
														<form id="formAttAvisoGlobal">

															<div class="card-header" id="acoordAvisoGlobal">
																<h5 class="m-0">
																	<a class="text-dark collapsed" data-toggle="collapse" href="#accordionAvisoGlobal" aria-expanded="false">
																		<i class=" mdi mdi-alert mr-1 text-mute"></i> 
																		Aviso global <small class="text-warning">(Topo do site)</small>
																	</a>

																	<a class="text-dark pull-right">
																		<input type="checkbox" class="switch_1" id="statusAvisoGlobalOurilandia" name="statusAvisoGlobalOurilandia">
																	</a>
																</h5>
															</div>
											
															<div id="accordionAvisoGlobal" class="collapse" aria-labelledby="acoordAvisoGlobal" data-parent="#accordion" style="">
																<div class="card-body">
																
																		<div class="form-row">
																			
																			<div class="form-group col-md-12">
																				<label class="col-form-label">Texto</label>
																				<input type="text" class="form-control" id="textoAvisoGlobal" name="textoAvisoGlobal" autocomplete="off">
																			</div>

																			<div class="form-group mt-3 col-md-12">
																				<button type="submit" id="botaoAttAvisoGlobal" class="btn btn-block btn-sm btn-primary waves-effect waves-light">Atualizar</button>
																			</div>
																			
																		</div>
																		
																</div>
															</div>

														</form>
													</div>
													
													<div class="card mb-1">
														<div class="card-header" id="barraSuperior">
															<h5 class="m-0">
																<a class="text-dark" data-toggle="collapse" href="#barraFixaSuperior" aria-expanded="false">
																	<i class="mdi mdi-playlist-star mr-1 text-mute"></i> 
																	Barra superior fixa
																</a>
															</h5>
														</div>
														<div id="barraFixaSuperior" class="collapse" aria-labelledby="barraSuperior" data-parent="#accordion">
															<div class="card-body">

																<form id="formAttBarraSupFixa">
																	
																	<div class="form-row">
																		
																		<div class="form-group col-md-6">
																			<label class="col-form-label">Mensagem boas vindas</label>
																			<input type="text" class="form-control" id="mensagem_BarraSupFixa" name="mensagem_BarraSupFixa" autocomplete="off">
																		</div>

																		<div class="form-group col-md-6">
																			<label class="col-form-label">E-mail</label>
																			<input type="text" class="form-control" id="email_BarraSupFixa" name="email_BarraSupFixa" autocomplete="off">
																		</div>

																		<div class="form-group col-md-10">
																			<label class="col-form-label">Telefone</label>
																			<input type="text" class="form-control" id="telefone_BarraSupFixa" name="telefone_BarraSupFixa" data-toggle="input-mask" data-mask-format="(00) 0 0000-0000" data-reverse="false" autocomplete="off">
																		</div>

																		<div class="form-group col-md-2">
																			<label class="col-form-label">Minha conta</label><br>
																			<a class="text-dark pull-left">
																				<input type="checkbox" class="switch_1" id="ativoMinhaConta_BarraSupFixa" name="ativoMinhaConta_BarraSupFixa">
																			</a>
																		</div>

																		<div class="form-group col-md-6">
																			<label class="col-form-label">Facebook</label>
																			<a class="text-dark pull-right">
																				<input type="checkbox" class="switch_1" id="ativoFacebook_BarraSupFixa" name="ativoFacebook_BarraSupFixa">
																			</a>
																			<input type="text" class="form-control" id="facebook_BarraSupFixa" name="facebook_BarraSupFixa" autocomplete="off">
																		</div>

																		<div class="form-group col-md-6">
																			<label class="col-form-label">Instagram</label>
																			<a class="text-dark pull-right">
																				<input type="checkbox" class="switch_1" id="ativoInstagram_BarraSupFixa" name="ativoInstagram_BarraSupFixa">
																			</a>
																			<input type="text" class="form-control" id="instagram_BarraSupFixa" name="instagram_BarraSupFixa" autocomplete="off">
																		</div>

																		<div class="form-group mt-3 col-md-12">
																			<button type="submit" class="btn btn-block btn-sm btn-primary waves-effect waves-light">Atualizar</button>
																		</div>
																		
																	</div>
																	
																</form>

															</div>
														</div>
													</div>

													<div class="card mb-1">
														<div class="card-header" id="linksBarraSuperior">
															<h5 class="m-0">
																<a class="text-dark" data-toggle="collapse" href="#linksBarraFixaSuperior" aria-expanded="false">
																	<i class="mdi mdi-link-variant mr-1 text-mute"></i> 
																	Links barra superior fixa
																</a>
															</h5>
														</div>
														<div id="linksBarraFixaSuperior" class="collapse" aria-labelledby="linksBarraSuperior" data-parent="#accordion">
															
															<div class="card-body">

																<form id="formNovoLinkBarraSuperior">
																	
																	<div class="form-row">
																		
																		<div class="form-group col-md-6">
																			<label class="col-form-label">Nome</label>
																			<input type="text" class="form-control" id="nome_linksBarraSupFixa" name="nome_linksBarraSupFixa" autocomplete="off">
																		</div>

																		<div class="form-group col-md-6">
																			<label class="col-form-label">Link</label>
																			<input type="text" class="form-control" id="link_linksBarraSupFixa" name="link_linksBarraSupFixa" autocomplete="off">
																		</div>

																		<div class="form-group mt-3 col-md-12">
																			<button type="submit" class="btn btn-block btn-sm btn-primary waves-effect waves-light">Cadastrar</button>
																		</div>
																		
																	</div>
																	
																</form>

															</div>

															<div class="card-body">

																<div class="table-responsive">
															
																	<table class="table table-bordered table-hover table-centered  table-nowrap m-0">

																		<thead class="thead-light">
																			<tr>
																				<th>#</th>
																				<th>Nome</th>
																				<th>Abrir em nova aba</th>
																				<th>Status</th>
																				<th>Ações</th>
																			</tr>
																		</thead>
																		
																		<tbody id="tabelaLinksBarraSupFixa"></tbody>

																	</table>

																</div> 
																
															</div>

														</div>
													</div>

													<div class="card mb-1">
														<div class="card-header" id="linksBarraDireita">
															<h5 class="m-0">
																
																<a class="text-dark" data-toggle="collapse" href="#linksBarraFixaDireita" aria-expanded="false">
																	<i class="mdi mdi-view-carousel mr-1 text-mute"></i> 
																	Slide - Carousel
																</a>
																
															</h5>
														</div>
														<div id="linksBarraFixaDireita" class="collapse" aria-labelledby="linksBarraDireita" data-parent="#accordion">
															
															<div class="card-body">

																<form id="formCadastrarNovoSlide" enctype="multipart/form-data">
																	
																	<div class="form-row">
																		
																		<div class="form-group col-md-6">
																			<label class="col-form-label">Imagem</label>
																			<input type="file" class="form-control" id="imagemSlide" name="imagemSlide">
																		</div>

																		<div class="form-group col-md-6">
																			<label class="col-form-label">Descrição da imagem</label>
																			<input type="text" class="form-control" id="descricaoImagemSlide" name="descricaoImagemSlide" autocomplete="off">
																		</div>

																		<div class="form-group mt-3 col-md-12">
																			<button type="submit" class="btn btn-block btn-sm btn-primary waves-effect waves-light">Cadastrar</button>
																		</div>
																		
																	</div>
																	
																</form>

															</div>

															<div class="card-body">

																<div class="table-responsive">
															
																	<table class="table table-bordered table-hover table-centered  table-nowrap m-0">

																		<thead class="thead-light">
																			<tr>
																				<th>#</th>
																				<th>Imagem</th>
																				<th>Status</th>
																				<th>Ações</th>
																			</tr>
																		</thead>
																		
																		<tbody id="tabelaListarSlideCarousel"> </tbody>

																	</table>

																</div> 
																
															</div>
															
														</div>
													</div>

													<div class="card mb-1">
														<div class="card-header" id="sessaoCobertura">
															<h5 class="m-0">

																<a class="text-dark" data-toggle="collapse" href="#coberturaContato" aria-expanded="false">
																	<i class="mdi mdi-contacts mr-1 text-mute"></i> 
																	Cobertura - Atendimento
																</a>

																<a class="text-dark pull-right" id="botaoStatusCoberturaAtendimento"></a>
																
															</h5>
														</div>
														<div id="coberturaContato" class="collapse" aria-labelledby="sessaoCobertura" data-parent="#accordion">
															<div class="card-body">

																<form id="formAtualizarCoberturaAtendimento">
																	
																	<div class="form-row">
																		
																		<div class="form-group col-md-6">
																			<label class="col-form-label">Título</label>
																			<input type="text" class="form-control" id="tituloCoberturaAtendimento" name="tituloCoberturaAtendimento" autocomplete="off">
																		</div>

																		<div class="form-group col-md-6">
																			<label class="col-form-label">Descrição</label>
																			<input type="text" class="form-control" id="descricaoCoberturaAtendimento" name="descricaoCoberturaAtendimento" autocomplete="off">
																		</div>

																		<div class="col-md-12">
																			<hr>
																		</div>
																		
																		<div class="col-md-12">
																			<label class="col-form-label"><h4>Botões</h4></label>
																		</div>
																		
																		<div class="form-group col-md-5">
																			<label class="col-form-label">Botão 1 <small class="text-warning">(Texto)</small></label>
																			<input type="text" class="form-control" id="textoBotao1CoberturaAtendimento" name="textoBotao1CoberturaAtendimento" data-toggle="input-mask" data-mask-format="(00) 0 0000-0000" data-reverse="false" autocomplete="off">
																		</div>

																		<div class="form-group col-md-5">
																			<label class="col-form-label">Link Botão 1 <small class="text-warning"><a href="https://www.convertte.com.br/gerador-link-whatsapp/" target="_blank" rel="noopener noreferrer">(Gerar Link)</a></small></label>
																			<input type="text" class="form-control" id="LinkBotao1CoberturaAtendimento" name="LinkBotao1CoberturaAtendimento" autocomplete="off">
																		</div>

																		<div class="form-group col-md-2">
																			<label class="col-form-label">Ativo?</label><br>
																			<a class="text-dark pull-left">
																				<input type="checkbox" class="switch_1" id="ativoBotao1CoberturaAtendimento" name="ativoBotao1CoberturaAtendimento">
																			</a>
																		</div>

																		<div class="form-group col-md-10">
																			<label class="col-form-label">Botão 2 <small class="text-warning">(Texto)</small></label>
																			<input type="text" class="form-control" id="textoBotao2CoberturaAtendimento" name="textoBotao2CoberturaAtendimento" autocomplete="off">
																		</div>

																		<div class="form-group col-md-2">
																			<label class="col-form-label">Ativo?</label><br>
																			<a class="text-dark pull-left">
																				<input type="checkbox" class="switch_1" id="ativoBotao2CoberturaAtendimento" name="ativoBotao2CoberturaAtendimento">
																			</a>
																		</div>

																		<div class="form-group mt-3 col-md-12">
																			<button type="submit" class="btn btn-block btn-sm btn-primary waves-effect waves-light">Atualizar</button>
																		</div>
																		
																	</div>
																	
																</form>

															</div>
														</div>
													</div>

													<div class="card mb-1">
														<div class="card-header" id="precosResidencial">
															<h5 class="m-0">
																
																<a class="text-dark" data-toggle="collapse" href="#boxPrecosResidencial" aria-expanded="false">
																	<i class="mdi mdi-home-group mr-1 text-mute"></i> 
																	Planos residenciais
																</a>
																
															</h5>
														</div>
														<div id="boxPrecosResidencial" class="collapse" aria-labelledby="precosResidencial" data-parent="#accordion">
															
															<div class="card-body">

																<form id="formPlanosResidenciais">
																	
																	<div class="form-row">
																		
																		<div class="form-group col-md-3">
																			<label class="col-form-label">Nome do plano</label>
																			<input type="text" class="form-control" id="nomePlanoCadastro" name="nomePlanoCadastro" autocomplete="off">
																		</div>

																		<div class="form-group col-md-3">
																			<label class="col-form-label">Descrição</label>
																			<input type="text" class="form-control" id="descricaoPlanoCadastro" name="descricaoPlanoCadastro" autocomplete="off">
																		</div>

																		<div class="form-group col-md-2">
																			<label class="col-form-label">Preço</label>
																			<input type="text" class="form-control" id="precoPlanoCadastro" name="precoPlanoCadastro" data-toggle="input-mask" data-mask-format="000,00" data-reverse="true" autocomplete="off">
																		</div>

																		<div class="form-group col-md-2">
																			<label class="col-form-label">Velocidade</label>
																			<input type="text" class="form-control" id="velocidadePlanoCadastro" name="velocidadePlanoCadastro" autocomplete="off">
																		</div>

																		<div class="form-group col-md-2">
																			<label class="col-form-label">Tipo <small class="text-warning">(KB, MB, GB, TB)</small></label>
																			<input type="text" class="form-control" id="tipoVelocidadePlanoCadastro" name="tipoVelocidadePlanoCadastro" autocomplete="off">
																		</div>
																		
																		<div class="form-group mt-3 col-md-12">
																			<button type="submit" class="btn btn-block btn-sm btn-primary waves-effect waves-light">Cadastrar</button>
																		</div>
																		
																	</div>
																	
																</form>

															</div>

															<div class="card-body">

																<div class="table-responsive">
															
																	<table class="table table-bordered table-hover table-centered  table-nowrap m-0">

																		<thead class="thead-light">
																			<tr>
																				<th>#</th>
																				<th>Nome</th>
																				<th>Valor</th>
																				<th>Velocidade</th>
																				<th>Destaque</th>
																				<th>Status</th>
																				<th>Ações</th>
																			</tr>
																		</thead>
																		
																		<tbody id="tabelaPlanosResidenciais"></tbody>

																	</table>

																</div> 
																
															</div>
															
														</div>
													</div>

													<div class="card mb-1">

														<div class="card-header" id="precosEmpresariais">
															<h5 class="m-0">
																
																<a class="text-dark" data-toggle="collapse" href="#boxPrecosEmpresariais" aria-expanded="false">
																	<i class="fas fa-building mr-1 text-mute"></i> 
																	Planos empresariais
																</a>
																
															</h5>
														</div>
														
														<div id="boxPrecosEmpresariais" class="collapse" aria-labelledby="precosEmpresariais" data-parent="#accordion">
															
															<div class="card-body">

																<form id="formPlanosEmpresariais">
																	
																	<div class="form-row">
																		
																		<div class="form-group col-md-3">
																			<label class="col-form-label">Nome do plano</label>
																			<input type="text" class="form-control" id="nomePlanoCadastro" name="nomePlanoCadastroEmpresarial" autocomplete="off">
																		</div>

																		<div class="form-group col-md-3">
																			<label class="col-form-label">Descrição</label>
																			<input type="text" class="form-control" id="descricaoPlanoCadastroEmpresarial" name="descricaoPlanoCadastroEmpresarial" autocomplete="off">
																		</div>

																		<div class="form-group col-md-2">
																			<label class="col-form-label">Preço</label>
																			<input type="text" class="form-control" id="precoPlanoCadastroEmpresarial" name="precoPlanoCadastroEmpresarial" data-toggle="input-mask" data-mask-format="000,00" data-reverse="true" autocomplete="off">
																		</div>

																		<div class="form-group col-md-2">
																			<label class="col-form-label">Velocidade</label>
																			<input type="text" class="form-control" id="velocidadePlanoCadastroEmpresarial" name="velocidadePlanoCadastroEmpresarial" autocomplete="off">
																		</div>

																		<div class="form-group col-md-2">
																			<label class="col-form-label">Tipo <small class="text-warning">(KB, MB, GB, TB)</small></label>
																			<input type="text" class="form-control" id="tipoVelocidadePlanoCadastroEmpresarial" name="tipoVelocidadePlanoCadastroEmpresarial" autocomplete="off">
																		</div>
																		
																		<div class="form-group mt-3 col-md-12">
																			<button type="submit" class="btn btn-block btn-sm btn-primary waves-effect waves-light">Cadastrar</button>
																		</div>
																		
																	</div>
																	
																</form>

															</div>

															<div class="card-body">

																<div class="table-responsive">
															
																	<table class="table table-bordered table-hover table-centered  table-nowrap m-0">

																		<thead class="thead-light">
																			<tr>
																				<th>#</th>
																				<th>Nome</th>
																				<th>Valor</th>
																				<th>Velocidade</th>
																				<th>Destaque</th>
																				<th>Status</th>
																				<th>Ações</th>
																			</tr>
																		</thead>
																		
																		<tbody id="tabelaPlanosEmpresariais"></tbody>

																	</table>

																</div> 
																
															</div>
															
														</div>
													
													</div>
													
												</div>
											</div>
										</div>
										
									</div>
								</div>
								
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>

<div id="alterarInformacoesRegiao" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Alterar informações da região: <u><i><span id="alterarTituloNomeRegiao"></span></i></u></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>
            
            <div class="modal-body">

            <div id="msgErroEditarRegiao"></div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            
                        <form id="formAlterarRegiao">

                            <input type="hidden" name="idRegiao" id="idRegiao">

                            <div class="form-row align-items-center">
                                
                                <div class="col-md-6">
                                    <h6 class="font-13">Nome da Regiao</h6>
                                    <input type="text" class="form-control mb-2" id="alterarNomeRegiao" name="alterarNomeRegiao" autocomplete="off" autofocus>
                                </div>

								<div class="col-md-6">
                                    <h6 class="font-13">Link da Página</h6>
                                    <input type="text" class="form-control mb-2" id="alterarLinkPaginaRegiao" name="alterarLinkPaginaRegiao" autocomplete="off" autofocus>
                                </div>

                                <div class="col-xl-4"></div>
                                
                                <div class="col-xl-12 mt-3">
                                    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                        <button type="submit" id="botaoAtualizarRegiao" class="btn btn-success btn-block waves-effect waves-light">Atualizar</button>
                                    </div>
                                </div>

                                <div class="col-xl-4"></div> 

                            </div>

                        </form>
                            
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>

<div id="ModalExcluiRegiao" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Excluir região: <u><i><strong><span class="text-warning" id="tituloExcluirRegiao"></span></strong></i></u></h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>

			<div id="msgERROExcluirRegiao" class="col-md-12 mt-2"></div>
			
			<form id="formExcluirRegiao">

				<div class="modal-body">
					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="hidden" class="form-control" id="idRegiaoExcluir" name="idRegiaoExcluir">
								<p>Deseja realmente excluir a região: 
									<u><i><strong><span class="text-warning" id="excluirNomeRegiao"></span></strong></i></u>?
									Após a exclusão não será possível reverter essa ação! Tem certeza?
								</p>
							</div>
						</div>
					</div>
					
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-danger waves-effect waves-light">Excluir</button>
				</div>
			</form>

		</div>
	</div>
</div>

<div id="alterarInfosLinksBarraSupFixa" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Alterar informações do link: <u><i><span id="alterarTituloNomeLink"></span></i></u></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>
            
            <div class="modal-body">

            <div id="msgErroEditarLinksBarraSupFixa"></div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            
                        <form id="formAlterarLinksBarraSupFixa">

                            <input type="hidden" name="idLinksBarraSupFixa" id="idLinksBarraSupFixa">

                            <div class="form-row align-items-center">
                                
                                <div class="col-md-6">
                                    <h6 class="font-13">Nome do Link</h6>
                                    <input type="text" class="form-control mb-2" id="alterarNomeLinksBarraSupFixa" name="alterarNomeLinksBarraSupFixa" autocomplete="off" autofocus>
                                </div>

								<div class="col-md-6">
                                    <h6 class="font-13">Link</h6>
                                    <input type="text" class="form-control mb-2" id="alterarLinksBarraSupFixa" name="alterarLinksBarraSupFixa" autocomplete="off" autofocus>
                                </div>

                                <div class="col-xl-4"></div>
                                
                                <div class="col-xl-12 mt-3">
                                    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                        <button type="submit" id="botaoAtualizarLinksBarraSupFixa" class="btn btn-success btn-block waves-effect waves-light">Atualizar</button>
                                    </div>
                                </div>

                                <div class="col-xl-4"></div> 

                            </div>

                        </form>
                            
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>

<div id="ModalExcluirLinksBarraSuperiorFixa" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Excluir o link: <u><i><strong><span class="text-warning" id="tituloExcluirLinkBarraSuperiorFixa"></span></strong></i></u></h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>

			<div id="msgERROExcluirLinksBarraSuperiorFixa" class="col-md-12 mt-2"></div>
			
			<form id="formExcluirBarraSuperiorFixa">

				<div class="modal-body">
					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="hidden" class="form-control" id="idExcluirBarraSuperiorFixa" name="idExcluirBarraSuperiorFixa">
								<p>Deseja realmente excluir o link: 
									<u><i><strong><span class="text-warning" id="excluirNomeBarraSuperiorFixa"></span></strong></i></u>?
									Após a exclusão não será possível reverter essa ação! Tem certeza?
								</p>
							</div>
						</div>
					</div>
					
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-danger waves-effect waves-light">Excluir</button>
				</div>
			</form>

		</div>
	</div>
</div>

<div id="modalVerImagemSlideCarousel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Você está visualizando a imagem do slide: <u><i><span id="idItemSlideCarousel"></span></i></u></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>
            
            <div class="modal-body">
			
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
							<img class="img-fluid" id="imagemSlideModal">
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
            	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>

<div id="modalVEditarItemCarousel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Você está editando a imagem do slide: <u><i><span id="tituloIdItemSlideCarousel"></span></i></u></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>
            
            <div class="modal-body">

			<div id="msgErroEditarItemSlide"></div>
			
			<div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            
                        <form id="formEditarItemSlide">

                            <input type="hidden" name="editarIdItemSlide" id="editarIdItemSlide">

                            <div class="form-row align-items-center">
                                
                                <div class="col-md-6">
                                    <h6 class="font-13">Nova Imagem <small class="text-warning">(Opcional)</small></h6>
                                    <input type="file" class="form-control mb-2" id="novaImagemItemCarouselSlide" name="novaImagemItemCarouselSlide">
                                </div>

								<div class="col-md-6">
                                    <h6 class="font-13">Descrição</h6>
                                    <input type="text" class="form-control mb-2" id="descricaoEditarItemCarousel" name="descricaoEditarItemCarousel" autocomplete="off" autofocus>
                                </div>

								<div class="col-md-12">
                                    <h6 class="font-13">Imagem Atual</h6>
									<img class="img-fluid" id="imagemSlideModalEditar">
                                </div>

                                <div class="col-xl-4"></div>
                                
                                <div class="col-xl-12 mt-3">
                                    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                        <button type="submit" id="botaoAtualizarItemSlide" class="btn btn-success btn-block waves-effect waves-light">Atualizar</button>
                                    </div>
                                </div>

                                <div class="col-xl-4"></div> 

                            </div>

                        </form>
                            
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
            	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>

<div id="modalExcluirItemSlide" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Excluir a imagem: <u><i><strong><span class="text-warning" id="tituloIdExcluirImagem"></span></strong></i></u></h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>

			<div id="msgERROExcluirImagemSlide" class="col-md-12 mt-2"></div>
			
			<form id="formExcluirItemSlide">

				<div class="modal-body">
					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								
								<input type="hidden" class="form-control" id="idExluirImagemSlide" name="idExcluirBarraSuperiorFixa">

								<img class="img-fluid" id="imagemSlideModalExcluir">
								
								<p class="mt-3 text-center">Deseja realmente excluir a imagem: 
									<u><i><strong><span class="text-warning" id="excluirNomeSlideCarousel"></span></strong></i></u> do slide?
									Após a exclusão não será possível reverter essa ação! Tem certeza?
								</p>
								
							</div>
						</div>
					</div>
					
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-danger waves-effect waves-light">Sim! Quero excluir</button>
				</div>
			</form>

		</div>
	</div>
</div>

<div id="modalEditarPlanosResidenciais" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Alterar informações do plano: <u><i><span class="text-warning" id="alterarTituloNomePlano"></span></i></u></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>
            
            <div class="modal-body">

            <div id="msgErroEditarPlanoResidencial"></div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            
                        <form id="formAlterarPlanoResidencial">

                            <input type="hidden" name="idPlanoResidencial" id="idPlanoResidencial">

                            <div class="form-row align-items-center">
                                
                                <div class="col-md-6">
                                    <h6 class="font-13">Nome do Plano</h6>
                                    <input type="text" class="form-control mb-2" id="alterarNomePlanoResidencial" name="alterarNomePlanoResidencial" autocomplete="off" autofocus>
                                </div>

								<div class="col-md-6">
                                    <h6 class="font-13">Descrição do plano</h6>
                                    <input type="text" class="form-control mb-2" id="alterarDescricaoPlanoResidencial" name="alterarDescricaoPlanoResidencial" autocomplete="off">
								</div>
								
								<div class="col-md-4">
                                    <h6 class="font-13">Preço</h6>
                                    <input type="text" class="form-control mb-2" id="alterarPrecoPlanoResidencial" name="alterarPrecoPlanoResidencial" data-toggle="input-mask" data-mask-format="000,00" data-reverse="true" autocomplete="off">
								</div>
								
								<div class="col-md-4">
                                    <h6 class="font-13">Velocidade</h6>
                                    <input type="text" class="form-control mb-2" id="alterarVelocidadePlanoResidencial" name="alterarVelocidadePlanoResidencial" autocomplete="off">
								</div>
								
								<div class="col-md-4">
                                    <h6 class="font-13">Tipo <small class="text-warning">(KB, MB, GB, TB)</small></h6>
                                    <input type="text" class="form-control mb-2" id="alterarTipoVelocidadePlanoResidencial" name="alterarTipoVelocidadePlanoResidencial" autocomplete="off">
                                </div>

                                <div class="col-xl-4"></div>
                                
                                <div class="col-xl-12 mt-3">
                                    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                        <button type="submit" id="botaoAtualizarPlanoResidencial" class="btn btn-success btn-block waves-effect waves-light">Atualizar plano</button>
                                    </div>
                                </div>

                                <div class="col-xl-4"></div> 

                            </div>

                        </form>
                            
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>

<div id="modalExcluirPlanoResidencial" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Excluir o plano: <u><i><strong><span class="text-warning" id="tituloExcluirPlanoResidencial"></span></strong></i></u></h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>

			<div id="msgERROExcluirPlanoResidencial" class="col-md-12 mt-2"></div>
			
			<form id="formExcluirPlanoResidencial">

				<div class="modal-body">
					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								
								<input type="hidden" class="form-control" id="idExluirPlanoResidencial" name="idExluirPlanoResidencial">
								
								<p class="mt-3 text-center">Deseja realmente excluir o plano: 
									<u><i><strong><span class="text-warning" id="excluirNomePlanoResidencial"></span></strong></i></u> ?
									Após a exclusão não será possível reverter essa ação! Tem certeza?
								</p>
								
							</div>
						</div>
					</div>
					
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-danger waves-effect waves-light">Sim! Quero excluir</button>
				</div>
			</form>

		</div>
	</div>
</div>

<div id="modalEditarPlanosEmpresariais" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Alterar informações do plano: <u><i><span class="text-warning" id="alterarTituloNomePlanoEmp"></span></i></u></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>
            
            <div class="modal-body">

            <div id="msgErroEditarPlanoEmpresarial"></div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            
                        <form id="formAlterarPlanoEmpresarial">

                            <input type="hidden" name="idPlanoEmpresarial" id="idPlanoEmpresarial">

                            <div class="form-row align-items-center">
                                
                                <div class="col-md-6">
                                    <h6 class="font-13">Nome do Plano</h6>
                                    <input type="text" class="form-control mb-2" id="alterarNomePlanoEmpresarial" name="alterarNomePlanoEmpresarial" autocomplete="off" autofocus>
                                </div>

								<div class="col-md-6">
                                    <h6 class="font-13">Descrição do plano</h6>
                                    <input type="text" class="form-control mb-2" id="alterarDescricaoPlanoEmpresarial" name="alterarDescricaoPlanoEmpresarial" autocomplete="off">
								</div>
								
								<div class="col-md-4">
                                    <h6 class="font-13">Preço</h6>
                                    <input type="text" class="form-control mb-2" id="alterarPrecoPlanoEmpresarial" name="alterarPrecoPlanoEmpresarial" data-toggle="input-mask" data-mask-format="000,00" data-reverse="true" autocomplete="off">
								</div>
								
								<div class="col-md-4">
                                    <h6 class="font-13">Velocidade</h6>
                                    <input type="text" class="form-control mb-2" id="alterarVelocidadePlanoEmpresarial" name="alterarVelocidadePlanoEmpresarial" autocomplete="off">
								</div>
								
								<div class="col-md-4">
                                    <h6 class="font-13">Tipo <small class="text-warning">(KB, MB, GB, TB)</small></h6>
                                    <input type="text" class="form-control mb-2" id="alterarTipoVelocidadePlanoEmpresarial" name="alterarTipoVelocidadePlanoEmpresarial" autocomplete="off">
                                </div>

                                <div class="col-xl-4"></div>
                                
                                <div class="col-xl-12 mt-3">
                                    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                        <button type="submit" id="botaoAtualizarPlanoEmpresarial" class="btn btn-success btn-block waves-effect waves-light">Atualizar plano</button>
                                    </div>
                                </div>

                                <div class="col-xl-4"></div> 

                            </div>

                        </form>
                            
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>

<div id="modalExcluirPlanoEmpresarial" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Excluir o plano: <u><i><strong><span class="text-warning" id="tituloExcluirPlanoEmpresarial"></span></strong></i></u></h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>

			<div id="msgERROExcluirPlanoEmpresarial" class="col-md-12 mt-2"></div>
			
			<form id="formExcluirPlanoEmpresarial">

				<div class="modal-body">
					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								
								<input type="hidden" class="form-control" id="idExluirPlanoEmpresarial" name="idExluirPlanoEmpresarial">
								
								<p class="mt-3 text-center">Deseja realmente excluir o plano: 
									<u><i><strong><span class="text-warning" id="excluirNomePlanoEmpresarial"></span></strong></i></u> ?
									Após a exclusão não será possível reverter essa ação! Tem certeza?
								</p>
								
							</div>
						</div>
					</div>
					
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-danger waves-effect waves-light">Sim! Quero excluir</button>
				</div>
			</form>

		</div>
	</div>
</div>

<div id="modalVerImagemSEO" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Imagem de SEO</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-md-12">

                    <img id="verImagemDoSEO" width="100%" >
                        
                    </div>
                    
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

