<div class="container-fluid">
        
    <div class="row mt-2">
        
        <div class="col-12">
            
            <div class="page-title-box">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
					<li class="breadcrumb-item"><a href="/dashboard/site">Site</a></li>
                    <li class="breadcrumb-item"><a href="/dashboard/site_admin/ourilandia_configs">Ourilândia do Norte</a></li>
                    <li class="breadcrumb-item active text-warning">Newsletter</li>
                </ol>
            </div>

            <?php
                $this->load->view('botoesSiteConfigs');
            ?>
            
            <div class="row mt-3">
                <div class="col-lg-12">
                    
                    <div class="card">
                        <div class="card-body">

                            <div class="card-widgets" id="atualizaDivNewsletter"></div>
                            
                            <h5 class="card-title mb-3">Todos leads realizados na página <span class="text-warning text-uppercase">Ourilândia do Norte</span> </h5>
                            
                            <div class="table-responsive">
                                
                                <table class="table table-bordered table-hover table-responsive-xl mb-0">
                                    
                                    <thead class="thead-light">
                                        <tr>
                                            <th>#</th>
                                            <th>Nome</th>
                                            <th>E-mail</th>
                                            <th>Dia/Hora</th>
                                            <th>WhatsApp</th>
                                        </tr>
                                    </thead>

                                    <tbody id="tabelaListarNewsletter"></tbody>

                                </table>

                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
