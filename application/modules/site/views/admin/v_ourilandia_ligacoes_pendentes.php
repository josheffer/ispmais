<div class="container-fluid">
    
    <div class="row mt-2">
        
        <div class="col-12">

            <div class="page-title-box">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
					<li class="breadcrumb-item"><a href="/dashboard/site">Site</a></li>
					<li class="breadcrumb-item"><a href="/dashboard/site_admin/ourilandia_configs">Ourilândia do Norte</a></li>
                    <li class="breadcrumb-item active text-warning">Ligações pendentes</li>
                </ol>
            </div>
            
            <?php
                $this->load->view('botoesSiteConfigs');
            ?>

            <div class="row mt-3 mb-2">
                                                
                <div class="col-xl-6">
                    <a href="/dashboard/site_admin/ourilandia_ligacoes_pendentes">
                        <button type="button" disabled class="btn btn-block btn-sm btn-danger waves-effect waves-light">
                            <span class="btn "><i class="mdi mdi-alarm"></i></span>Pendentes
                        </button>
                    </a> 
                </div>

                <div class="col-xl-6">
                    <a href="/dashboard/site_admin/ourilandia_ligacoes_efetuadas">
                        <button type="button" class="btn btn-block btn-sm btn-success waves-effect waves-light">
                            <span class="btn "><i class=" mdi mdi-clipboard-check-outline"></i></span>Efetuadas
                        </button>
                    </a> 
                </div>
                                
            </div>

            <div class="row mt-3">

                <div class="col-md-12">
                    <!-- Portlet card -->
                    <div class="card">
                        <div class="card-body">

                            <div class="card-widgets" id="atualizaDiv"></div>
                            
                            <h5 class="card-title mb-3">Todas as solicitações pendentes de <span class="text-warning text-uppercase">ligações</span></h5>
                            
                            <div class="table-responsive mt-3">
                                <table class="table table-bordered table-hover table-responsive-xl mb-0">
                                    
                                    <thead class="thead-light">
                                        <tr>
                                            <th>#</th>
                                            <th>Nome</th>
                                            <th>Dia/Hora</th>
                                            <th>WhatsApp</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>

                                    <tbody id="tabelaListarSolicitacoesLigacoes"></tbody>

                                </table>
                            </div>
                            
                        </div>
                    </div> 
                </div>
                
            </div>
        </div>
    </div> 
</div>

<div id="abrirModalRecepcionar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel"><span id="tituloRecepcionar"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">
                
                <div id="mensagemRecepcionar"></div>

                <div id="dadosSolicitacao"></div>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>