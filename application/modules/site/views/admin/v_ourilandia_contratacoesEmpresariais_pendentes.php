<div class="container-fluid">
    
    <div class="row mt-2">
        
        <div class="col-12">
            
            <div class="page-title-box">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
					<li class="breadcrumb-item"><a href="/dashboard/site">Site</a></li>
                    <li class="breadcrumb-item"><a href="/dashboard/site_admin/ourilandia_configs">Ourilândia do Norte</a></li>
                    <li class="breadcrumb-item active text-warning">Empresariais pendentes</li>
                </ol>
            </div>

            <?php
                $this->load->view('botoesSiteConfigs');
            ?>

            <div class="row mt-3 mb-2">
                                                
                <div class="col-xl-6">
                    <a href="/dashboard/site_admin/ourilandia_solicitacoesEmpresariais_pendentes">
                        <button type="button" disabled class="btn btn-block btn-sm btn-danger waves-effect waves-light">
                            <span class="btn "><i class="mdi mdi-alarm"></i></span>Pendentes
                        </button>
                    </a> 
                </div>

                <div class="col-xl-6">
                    <a href="/dashboard/site_admin/ourilandia_solicitacoesEmpresariais_efetuadas">
                        <button type="button" class="btn btn-block btn-sm btn-success waves-effect waves-light">
                            <span class="btn "><i class=" mdi mdi-clipboard-check-outline"></i></span>Efetuadas
                        </button>
                    </a> 
                </div>
                                
            </div>

            <div class="row mt-3">

                <div class="col-md-12">
                
                    <div class="card">
                        <div class="card-body">

                            <div class="card-widgets" id="atualizaDiv"></div>

                            <h5 class="card-title mb-3">Todas as solicitações pendentes de <span class="text-info text-uppercase">contratações de planos empresariais</span></h5>
                            
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-responsive-xl mb-0">
                                    
                                    <thead class="thead-light">
                                        <tr>
                                            <th>#</th>
                                            <th>Nome</th>
                                            <th>Dia/Hora</th>
                                            <th>Informações</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>

                                    <tbody id="tabelaListarContratacoesEmpresariais"></tbody>

                                </table>
                            </div>
                            
                        </div>
                    </div> 
                </div>
                
            </div>

        </div>
    </div> 
</div>
        
<div id="abrirModalRecepcionar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel"><span id="tituloRecepcionar"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">
                
                <div id="mensagemRecepcionar"></div>

                <div id="dadosSolicitacao"></div>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="abrirModalVerInfos" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel"><span id="tituloVerInfos"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">
                
                <form>

                    <div class="form-row">
                        
                        <div class="form-group col-md-12">
                            <label for="nomeVerInfos" class="col-form-label">Nome</label>
                            <input type="text" class="form-control" id="nomeVerInfos" disabled>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="telefoneVerInfos" class="col-form-label">Telefone</label>
                            <input type="text" class="form-control" id="telefoneVerInfos" disabled>
                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="telefoneFixoVerInfos" class="col-form-label">Telefone fixo</label>
                            <input type="text" class="form-control" id="telefoneFixoVerInfos" disabled>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="emailVerInfos" class="col-form-label">E-mail</label>
                            <input type="text" class="form-control" id="emailVerInfos" disabled>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="cnpjVerInfos" class="col-form-label">C.N.P.J</label>
                            <input type="text" class="form-control" id="cnpjVerInfos" disabled>
                        </div>
                        
                        <div class="form-group col-md-6">
                            <label for="nomeFantasiaVerInfos" class="col-form-label">Nome fantasia</label>
                            <input type="text" class="form-control" id="nomeFantasiaVerInfos" disabled>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="cepVerInfos" class="col-form-label">CEP</label>
                            <input type="text" class="form-control" id="cepVerInfos" disabled>
                        </div>

                        <div class="form-group col-md-10">
                            <label for="ruaVerInfos" class="col-form-label">Rua</label>
                            <input type="text" class="form-control" id="ruaVerInfos" disabled>
                        </div>

                        <div class="form-group col-md-10">
                            <label for="complementoVerInfos" class="col-form-label">Complemento</label>
                            <input type="text" class="form-control" id="complementoVerInfos" disabled>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="numeroVerInfos" class="col-form-label">Número</label>
                            <input type="text" class="form-control" id="numeroVerInfos" disabled>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="bairroVerInfos" class="col-form-label">Bairro</label>
                            <input type="text" class="form-control" id="bairroVerInfos" disabled>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="cidadeVerInfos" class="col-form-label">Cidade</label>
                            <input type="text" class="form-control" id="cidadeVerInfos" disabled>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="estadoVerInfos" class="col-form-label">Estado</label>
                            <input type="text" class="form-control" id="estadoVerInfos" disabled>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="observacaoVerInfos" class="col-form-label">Observação</label>
                            <textarea class="form-control" rows="4" id="observacaoVerInfos" disabled></textarea>
                        </div>

                    </div>
                    
                </form>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
