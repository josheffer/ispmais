<div class="row">

    <div class="col-xl-12">

        <div id="accordion" class="mb-3">
            
            <div class="card mb-1">
                
                <a class="text-dark collapsed text-center" data-toggle="collapse" href="#menuNavegacaoPainel" aria-expanded="false">
                    <div class="card-header" id="headingOne">
                        <h5 class="m-0 text-center">
                            Menu de navegação
                        </h5>
                    </div>
                </a>

                <div id="menuNavegacaoPainel" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="">
                    
                    <div class="card-body d-flex justify-content-center">

                        <a href="/dashboard/site" data-toggle="tooltip" data-placement="top" data-original-title="Configurações da intro do site">
                            <button type="button" class="btn mr-1 btn-warning text-white">Intro</button>
                        </a>
                        
                        <a href="/dashboard/site/bio" data-toggle="tooltip" data-placement="top" data-original-title="Configurações da BIO do site">
                            <button type="button" class="btn mr-1 btn-danger text-white">BIO</button>
                        </a>
                        
                        <a href="/dashboard/site_admin/ourilandia_configs" data-toggle="tooltip" data-placement="top" data-original-title="Gerencie a página Ourilândia do Norte">
                            <button type="button" class="btn mr-1 bg-info text-white">Site > Ourilândia do Norte</button>
                        </a>
                        
                        <!-- <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Gerencie a página Tucumã">
                            <button type="button" class="btn mr-1 bg-success text-white">Site > Tucumã</button>
                        </a>

                        <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Gerencie a página São Felix do Xingú">
                            <button type="button" class="btn mr-1 bg-primary text-white">Site > São Felix do Xingú</button>
                        </a> -->

                    </div>

                </div>

            </div>

        </div>

    </div>
    
</div>
