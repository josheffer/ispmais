<div class="container-fluid">
			
	<div class="row mt-2">
		
		<div class="col-12">
			
			<div class="page-title-box">
				<ol class="breadcrumb m-0">
					<li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
					<li class="breadcrumb-item"><a href="/dashboard/site">Site</a></li>
					<li class="breadcrumb-item active text-warning">BIO</li>
				</ol>
			</div>

			<?php
				$this->load->view('botoesSite');
			?>

			<div class="row mt-3">
				
				<div class="col-lg-12">
					
					<div class="card">

						<div class="card-body">
						
							<div class="row">

								<div class="col-12">
								
									<div class="row">

										<div class="col-xl-12">
                                            
											<div id="accordion" class="mb-3">
													
												<div class="card mb-1">
													<a class="text-dark collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false">
														<div class="card-header" id="headingOne">
															<h5 class="m-0"> <i class="fas fa-edit mr-1 text-mute"></i>  Foto e informações </h5>
														</div>
													</a>
										
													<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
														
														<div class="card-body">
															
															<form id="formEditarInfosBio" enctype="multipart/form-data">
																
																<div class="form-row">
																	
																	<div class="form-group col-md-4">
																		<label class="col-form-label">Nome</label>
																		<input type="text" class="form-control" id="nome_bio" name="nome_bio" autocomplete="off">
																	</div>
																	
																	<div class="form-group col-md-4">
																		<label class="col-form-label">Ano da empresa <small>(Rodapé)</small></label>
																		<input type="text" class="form-control" id="anoEmpresa_bio" name="anoEmpresa_bio" autocomplete="off">
																	</div>

																	<div class="form-group col-md-4">
																		<label class="col-form-label">Texto copyright <small>(Rodapé)</small></label>
																		<input type="text" class="form-control" id="TextoEmpresa_bio" name="TextoEmpresa_bio" autocomplete="off">
																	</div>

																	<div class="form-group col-md-12">
																		<label class="col-form-label">Descrição</label>
																		<input type="text" class="form-control" id="descricao_bio" name="descricao_bio" autocomplete="off">
																	</div>

																	<div class="form-group col-md-6">
																		<label>Nova imagem</label>
																		<input type="file" accept="image/*" class="form-control" id="imagem_bio" name="imagem_bio" autocomplete="off">
																	</div>

																	<div class="form-group col-md-6">
																		<label>Imagem</label>
																		<button type="button" class="btn btn-block btn--md btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modalImagemBio">Ver</button>
																	</div>
																	
																	<div class="form-group mt-3 col-md-12">
																		<button type="submit" id="botaoAtualizarInfosBio" class="btn btn-block btn-sm btn-success waves-effect waves-light">Atualizar</button>
																	</div>
															
																</div>
														
															</form>
													
														</div>

													</div>

												</div>
												
												<div class="card mb-1">
													
													<div class="card-header" id="headingTwo">
														<a class="text-dark" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">
															<h5 class="m-0">
																<i class="mdi mdi-web mr-1 text-mute"></i> 
																Sites
															</h5>
														</a>
													</div>

													<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
														
														<div class="card-body">
														
															<form id="formCadastrarSite">
																
																<div class="form-row">
																
																	<div class="form-group col-md-5">
																		<label class="col-form-label">Nome do site</label>
																		<input type="text" class="form-control" id="nomeSite_bio" name="nomeSite_bio" autocomplete="off">
																	</div>

																	<div class="form-group col-md-5">
																		<label class="col-form-label">URL do site</label>
																		<input type="text" class="form-control" id="urlSite_bio" name="urlSite_bio" autocomplete="off">
																	</div>
																	
																	<div class="form-group mt-4 col-md-2">
																		<button type="submit" id="" class="btn btn-block btn-primary waves-effect waves-light">Cadastrar</button>
																	</div>

																</div>
																
															</form>

															<div class="col-md-12">
																<hr>
															</div>

															<div class="table-responsive">
																<table class="table table-bordered table-hover mb-0">
																	
																	<thead>
																		<tr>
																			<th>#</th>
																			<th>Nome do site</th>
																			<th>Status</th>
																			<th>Ações</th>
																		</tr>
																	</thead>

																	<tbody id="tabelaSitesBio"></tbody>
																	
																</table>
															</div>
															
														</div>

													</div>

												</div>

												<div class="card mb-1">
													
													<div class="card-header" id="headingTree">
														<a class="text-dark" data-toggle="collapse" href="#collapseTree" aria-expanded="false">
															<h5 class="m-0">
																<i class=" fas fa-map-marker-alt mr-1 text-mute"></i> 
																Canais de atendimento
															</h5>
														</a>
													</div>

													<div id="collapseTree" class="collapse" aria-labelledby="headingTree" data-parent="#accordion">
														
														<div class="card-body">
																
															<form id="formCadastrarcanalAtendimento">
																	
																<div class="form-row">
																		
																	<div class="form-group col-md-4">
																		<label class="col-form-label">Nome do canal de atendimento</label>
																		<input type="text" class="form-control" id="nome_canalAtendimento" name="nome_canalAtendimento" autocomplete="off">
																	</div>

																	<div class="form-group col-md-4">
																		<label class="col-form-label">URL</label>
																		<input type="text" class="form-control" id="url_canalAtendimento" name="url_canalAtendimento" autocomplete="off">
																	</div>
																		
																	<div class="form-group col-md-4 pl-3">

																		<label class="col-form-label">Icone</label> <br>
																		
																		<div class="mt-1">

																			<div class="form-check-inline">
																				
																				<div class="radio">
																					<input type="radio" name="icone_canalAtendimento" id="radio1" value="1" checked>
																					<label for="radio1"> <i class="mdi mdi-whatsapp"></i> </label>
																				</div>

																				<div class="radio ml-2">
																					<input type="radio" name="icone_canalAtendimento" id="radio2" value="2">
																					<label for="radio2"> <i class="mdi mdi-instagram"></i> </label>
																				</div>
																				
																				<div class="radio ml-2">
																					<input type="radio" name="icone_canalAtendimento" id="radio3" value="3">
																					<label for="radio3"> <i class="mdi mdi-facebook-messenger"></i> </label>
																				</div>
																				
																				<div class="radio ml-2">
																					<input type="radio" name="icone_canalAtendimento" id="radio4" value="4">
																					<label for="radio4"> <i class="mdi mdi-telegram"></i> </label>
																				</div>

																				<div class="radio ml-2">
																					<input type="radio" name="icone_canalAtendimento" id="radio5" value="5">
																					<label for="radio5"> <i class="mdi mdi-email"></i> </label>
																				</div>
																				
																				<div class="radio ml-2">
																					<input type="radio" name="icone_canalAtendimento" id="radio6" value="6">
																					<label for="radio6"> <i class="mdi mdi-phone-classic"></i> </label>
																				</div>

																			</div>
																			
																		</div>

																	</div>
																		
																	<div class="form-group col-md-12">
																		<button type="submit" id="" class="btn btn-block btn-primary waves-effect waves-light">Cadastrar</button>
																	</div>
																	
																</div>

															</form>

															<div class="table-responsive mt-3">
																<table class="table table-bordered table-hover mb-0">
																	
																	<thead>
																		<tr>
																			<th>#</th>
																			<th>Nome do canal de atendimento</th>
																			<th>Status</th>
																			<th>Ações</th>
																			<th>Testar</th>
																		</tr>
																	</thead>

																	<tbody id="tabelaListarCanaisAtendimento"></tbody>
																	
																</table>
															</div>
															
														</div>

													</div>

												</div>

												<div class="card mb-1">
													
													<div class="card-header" id="headingFour">
														<a class="text-dark" data-toggle="collapse" href="#collapseFour" aria-expanded="false">
															<h5 class="m-0">
																<i class=" fas fa-map-marker-alt mr-1 text-mute"></i> 
																Redes sociais
															</h5>
														</a>
													</div>

													<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
														
														<div class="card-body">
															
															<form id="formCadastrarRedeSocial">
																	
																<div class="form-row">
																	
																	<div class="form-group col-md-5">
																		<label class="col-form-label">Nome da rede social</label>
																		<input type="text" class="form-control" id="cadastrarNomeRedeSocial" name="cadastrarNomeRedeSocial" autocomplete="off">
																	</div>

																	<div class="form-group col-md-4">
																		<label class="col-form-label">URL da rede social</label>
																		<input type="text" class="form-control" id="cadastrarUrlRedeSocial" name="cadastrarUrlRedeSocial" autocomplete="off">
																	</div>

																	<div class="form-group col-md-3 pl-2">
																		<label class="col-form-label">Icone</label> <br>
																		
																		<div class="mt-1">

																			<div class="form-check-inline">
																				
																				<div class="radio">
																					<input type="radio" name="cadastrarIconeRedeSocial" id="radio22" value="1" checked>
																					<label for="radio22"> <i class="mdi mdi-instagram"></i> </label>
																				</div>

																				<div class="radio ml-2">
																					<input type="radio" name="cadastrarIconeRedeSocial" id="radio23" value="2">
																					<label for="radio23"> <i class="mdi mdi-facebook-box"></i> </label>
																				</div>
																				
																				<div class="radio ml-2">
																					<input type="radio" name="cadastrarIconeRedeSocial" id="radio24" value="3">
																					<label for="radio24"> <i class="mdi mdi-youtube"></i> </label>
																				</div>
																				
																				<div class="radio ml-2">
																					<input type="radio" name="cadastrarIconeRedeSocial" id="radio25" value="4">
																					<label for="radio25"> <i class="mdi mdi-twitter"></i> </label>
																				</div>
																				
																				<div class="radio ml-2">
																					<input type="radio" name="cadastrarIconeRedeSocial" id="radio26" value="5">
																					<label for="radio26"> <i class="mdi mdi-pinterest"></i> </label>
																				</div>

																			</div>
																			
																		</div>

																	</div>
																	
																	<div class="form-group mt-1 col-md-12">
																		<button type="submit" id="" class="btn btn-block btn-primary waves-effect waves-light">Cadastrar</button>
																	</div>

																	<div class="col-md-12">
																		<hr>
																	</div>

																</div>
													
															</form>

															<div class="table-responsive">
																<table class="table table-bordered table-hover mb-0">
																	
																	<thead>
																		<tr>
																			<th>#</th>
																			<th>Nome da rede social</th>
																			<th>Status</th>
																			<th>Ações</th>
																			<th>Testar</th>
																		</tr>
																	</thead>

																	<tbody id="tabelaListarRedesSociais"></tbody>
																	
																</table>
															</div>
															
														</div>

													</div>

												</div>

												<div class="card mb-1">
													
													<div class="card-header" id="headingFive">
														<a class="text-dark" data-toggle="collapse" href="#collapseFive" aria-expanded="false">
															<h5 class="m-0">
																<i class="  mdi mdi-format-color-fill mr-1 text-mute"></i> 
																Estilo e cores
															</h5>
														</a>
													</div>

													<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
														
														<div class="card-body">
															
															<form id="formAtualizarCoresEstiloBio">
																	
																<div class="form-row">
																	
																	<div class="form-group col-md-3">
																		<label class="col-form-label">Cor do fundo</label>
																		<input type="color" class="form-control" id="corBgBio" name="corBgBio">
																	</div>

																	<div class="form-group col-md-3">
																		<label class="col-form-label">Cor box meio</label>
																		<input type="color" class="form-control" id="corBoxMeioBio" name="corBoxMeioBio">
																	</div>

																	<div class="form-group col-md-3">
																		<label class="col-form-label">Cor faixas horizontais box meio</label>
																		<input type="color" class="form-control" id="corFaixasBoxMeioBio" name="corFaixasBoxMeioBio">
																	</div>	
																	
																	<div class="form-group col-md-3">
																		<label class="col-form-label">Cor dos textos faixa</label>
																		<input type="color" class="form-control" id="corTextosFaixaBio" name="corTextosFaixaBio">
																	</div>	
																	
																	<div class="form-group col-md-3">
																		<label class="col-form-label">Cor dos textos</label>
																		<input type="color" class="form-control" id="corTextosBio" name="corTextosBio">
																	</div>	

																	<div class="form-group col-md-3">
																		<label class="col-form-label">Cor dos textos dos botões</label>
																		<input type="color" class="form-control" id="corTextosBotoesBio" name="corTextosBotoesBio">
																	</div>

																	<div class="form-group col-md-3">
																		<label class="col-form-label">Cor dos textos dos botões ao passar mouse</label>
																		<input type="color" class="form-control" id="corTextosBotoesHoverBio" name="corTextosBotoesHoverBio">
																	</div>

																	<div class="form-group col-md-3">
																		<label class="col-form-label">Cor dos botões</label>
																		<input type="color" class="form-control" id="corBotoesBio" name="corBotoesBio">
																	</div>

																	<div class="form-group col-md-6">
																		<label class="col-form-label">Cor dos botões ao passar mouse</label>
																		<input type="color" class="form-control" id="corBotoeHoversBio" name="corBotoeHoversBio">
																	</div>

																	<div class="form-group col-md-6">
																		<label class="col-form-label">Cor texto rodapé</label>
																		<input type="color" class="form-control" id="corTextoRodapeBio" name="corTextoRodapeBio">
																	</div>
																	
																	<div class="form-group mt-3 col-md-12">
																		<button type="submit" id="" class="btn btn-block btn-success waves-effect waves-light">Atualizar</button>
																	</div>

																</div>
													
															</form>
															
														</div>

													</div>

												</div>
											
											</div>

										</div>

									</div>
							
								</div>

							</div>
						
						</div>

					</div>

				</div>

            </div>
            
		</div>
		
	</div> 

</div>

<div id="modalImagemBio" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel"><span id="tituloRecepcionar"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">
                
				<img id="verImagemDaBio" width="100%" >	
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="alterarInfosSIteBio" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Alterar informações do site: <u><i><span id="alterarTituloNomeSite"></span></i></u></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>
            
            <div class="modal-body">

            <div id="msgErroEditarSite"></div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            
                        <form id="formEditarSiteBio">

                            <input type="hidden" name="idEditarSiteBio" id="idEditarSiteBio">

                            <div class="form-row align-items-center">
                                
                                <div class="col-md-6">
                                    <h6 class="font-13">Nome do site</h6>
                                    <input type="text" class="form-control mb-2" id="alterarNomeSiteBio" name="alterarNomeSiteBio" autocomplete="off" autofocus>
                                </div>

								<div class="col-md-6">
                                    <h6 class="font-13">URL do site</h6>
                                    <input type="text" class="form-control mb-2" id="alterarLinkSiteBio" name="alterarLinkSiteBio" autocomplete="off" autofocus>
                                </div>

                                <div class="col-xl-4"></div>
                                
                                <div class="col-xl-12 mt-3">
                                    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                        <button type="submit" id="botaoAtualizarSiteBio" class="btn btn-success btn-block waves-effect waves-light">Atualizar</button>
                                    </div>
                                </div>

                                <div class="col-xl-4"></div> 

                            </div>

                        </form>
                            
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>

<div id="ModalExcluiSiteBio" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Excluir site: <u><i><strong><span class="text-warning" id="tituloExcluirSiteBio"></span></strong></i></u> da bio?</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>

			<div id="msgERROExcluirSiteBio" class="col-md-12 mt-2"></div>
			
			<form id="formExcluirSiteBio">

				<div class="modal-body">
					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="hidden" class="form-control" id="idExcluirSiteBio" name="idExcluirSiteBio">
								<p>Deseja realmente excluir o site: 
									<u><i><strong><span class="text-warning" id="excluirNomeSiteBio"></span></strong></i></u> ?
									Após a exclusão não será possível reverter essa ação! Tem certeza?
								</p>
							</div>
						</div>
					</div>
					
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-danger waves-effect waves-light">Excluir</button>
				</div>
			</form>

		</div>
	</div>
</div>

<div id="alterarInfosCanalAtendimento" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Alterar informações do canal de atendimento: <u><i><span id="alterarTituloNomeCanalAtendimento"></span></i></u></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>
            
            <div class="modal-body">

            <div id="msgErroEditarInfosCanaisAtendimento"></div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            
                        <form id="formEditarCanaisDeAtendimento">

                            <input type="hidden" name="idEditarCanalAtendimento" id="idEditarCanalAtendimento">

                            <div class="form-row align-items-center">
                                
                                <div class="col-md-4">
                                    <h6 class="font-13">Nome do site</h6>
                                    <input type="text" class="form-control mb-2" id="alterarNomeCanalAtendimento" name="alterarNomeCanalAtendimento" autocomplete="off" autofocus>
                                </div>

								<div class="col-md-4">
                                    <h6 class="font-13">URL do site</h6>
                                    <input type="text" class="form-control mb-2" id="alterarLinkCanalAtendimento" name="alterarLinkCanalAtendimento" autocomplete="off" autofocus>
                                </div>

								<div class="form-group col-md-4 pl-3">

									<label class="col-form-label">Icone <small class="text-warning">(Opcional)</small></label> <br>
									
									<div class="mt-1">

										<div class="form-check-inline">
											
											<div class="radio">
												<input type="radio" name="editarIcone_canalAtendimento" id="radio100" value="0" checked>
												<label for="radio100"> Atual </label>
											</div>

											<div class="radio ml-1">
												<input type="radio" name="editarIcone_canalAtendimento" id="radio10" value="1">
												<label for="radio10"> <i class="mdi mdi-whatsapp"></i> </label>
											</div>

											<div class="radio ml-2">
												<input type="radio" name="editarIcone_canalAtendimento" id="radio20" value="2">
												<label for="radio20"> <i class="mdi mdi-instagram"></i> </label>
											</div>
											
											<div class="radio ml-2">
												<input type="radio" name="editarIcone_canalAtendimento" id="radio30" value="3">
												<label for="radio30"> <i class="mdi mdi-facebook-messenger"></i> </label>
											</div>
											
											<div class="radio ml-2">
												<input type="radio" name="editarIcone_canalAtendimento" id="radio40" value="4">
												<label for="radio40"> <i class="mdi mdi-telegram"></i> </label>
											</div>

											<div class="radio ml-2">
												<input type="radio" name="editarIcone_canalAtendimento" id="radio50" value="5">
												<label for="radio50"> <i class="mdi mdi-email"></i> </label>
											</div>
											
											<div class="radio ml-2">
												<input type="radio" name="editarIcone_canalAtendimento" id="radio60" value="6">
												<label for="radio60"> <i class="mdi mdi-phone-classic"></i> </label>
											</div>

										</div>
										
									</div>

								</div>

                                <div class="col-xl-4"></div>
                                
                                <div class="col-xl-12 mt-3">
                                    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                        <button type="submit" id="botaoAtualizarSiteBio" class="btn btn-success btn-block waves-effect waves-light">Atualizar</button>
                                    </div>
                                </div>

                                <div class="col-xl-4"></div> 

                            </div>

                        </form>
                            
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>

<div id="ModalExcluiCanalAtendimento" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Excluir site: <u><i><strong><span class="text-warning" id="tituloExcluirCanalAtendimento"></span></strong></i></u> da bio?</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>

			<div id="msgERROExcluirCanalAtendimento" class="col-md-12 mt-2"></div>
			
			<form id="formExcluirCanalAtendimento">

				<div class="modal-body">
					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="hidden" class="form-control" id="idExcluirCanalAtendimento" name="idExcluirCanalAtendimento">
								<p>Deseja realmente excluir o site: 
									<u><i><strong><span class="text-warning" id="excluirNomeCanalAtendimento"></span></strong></i></u> ?
									Após a exclusão não será possível reverter essa ação! Tem certeza?
								</p>
							</div>
						</div>
					</div>
					
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-danger waves-effect waves-light">Sim! Quero excluir.</button>
				</div>
			</form>

		</div>
	</div>
</div>

<div id="alterarInfosRedesSociais" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Alterar informações da Rede Social: <u><i><span id="alterarTituloNomeRedeSocial"></span></i></u></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>
            
            <div class="modal-body">

            <div id="msgErroEditarRedesSociais"></div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            
                        <form id="formEditarRedesSociaisBio">

                            <input type="hidden" name="idEditarRedesSociaisBio" id="idEditarRedesSociaisBio">

                            <div class="form-row align-items-center">
                                
                                <div class="col-md-4">
                                    <h6 class="font-13">Nome da rede social</h6>
                                    <input type="text" class="form-control mb-2" id="alterarNomeRedesSociaisBio" name="alterarNomeRedesSociaisBio" autocomplete="off" autofocus>
                                </div>

								<div class="col-md-4">
                                    <h6 class="font-13">URL da rede social</h6>
                                    <input type="text" class="form-control mb-2" id="alterarLinkRedesSociaisBio" name="alterarLinkRedesSociaisBio" autocomplete="off" autofocus>
								</div>
								
								<div class="form-group col-md-4 pl-3">

									<label class="col-form-label">Icone <small class="text-warning">(Opcional)</small></label> <br>
									
									<div class="mt-1">

										<div class="form-check-inline">
											
											<div class="radio">
												<input type="radio" name="editarIcone_redesSociais" id="radio200" value="0" checked>
												<label for="radio200"> Atual </label>
											</div>

											<div class="radio">
												<input type="radio" name="editarIcone_redesSociais" id="radio210" value="1">
												<label for="radio210"> <i class="mdi mdi-instagram"></i> </label>
											</div>

											<div class="radio ml-2">
												<input type="radio" name="editarIcone_redesSociais" id="radio220" value="2">
												<label for="radio220"> <i class="mdi mdi-facebook-box"></i> </label>
											</div>
											
											<div class="radio ml-2">
												<input type="radio" name="editarIcone_redesSociais" id="radio230" value="3">
												<label for="radio230"> <i class="mdi mdi-youtube"></i> </label>
											</div>
											
											<div class="radio ml-2">
												<input type="radio" name="editarIcone_redesSociais" id="radio240" value="4">
												<label for="radio240"> <i class="mdi mdi-twitter"></i> </label>
											</div>
											
											<div class="radio ml-2">
												<input type="radio" name="editarIcone_redesSociais" id="radio250" value="5">
												<label for="radio250"> <i class="mdi mdi-pinterest"></i> </label>
											</div>

										</div>
										
									</div>

								</div>

                                <div class="col-xl-4"></div>
                                
                                <div class="col-xl-12 mt-3">
                                    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                        <button type="submit" class="btn btn-success btn-block waves-effect waves-light">Atualizar</button>
                                    </div>
                                </div>

                                <div class="col-xl-4"></div> 

                            </div>

                        </form>
                            
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>

<div id="ModalExcluiRedeSocial" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Excluir site: <u><i><strong><span class="text-warning" id="tituloExcluirRedeSocial"></span></strong></i></u> da bio?</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>

			<div id="msgERROExcluirRedeSocial" class="col-md-12 mt-2"></div>
			
			<form id="formExcluirRedeSocial">

				<div class="modal-body">
					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="hidden" class="form-control" id="idExcluirRedeSocial" name="idExcluirRedeSocial">
								<p>Deseja realmente excluir o site: 
									<u><i><strong><span class="text-warning" id="excluirNomeRedeSocial"></span></strong></i></u> ?
									Após a exclusão não será possível reverter essa ação! Tem certeza?
								</p>
							</div>
						</div>
					</div>
					
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-danger waves-effect waves-light">Sim! Quero excluir.</button>
				</div>
			</form>

		</div>
	</div>
</div>