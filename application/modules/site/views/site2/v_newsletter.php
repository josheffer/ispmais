<section class="col-md-12" id="subirNewsletter"></section>

<section>

    <div class="boxes-one" style="background-color: #ff722d; padding-top:30px;padding-bottom:30px;">
        
        <div class="container">
            
            <div class="row">
                
                <div class="col-md-12">
                    <div class="text-center">
                        <h3>Receba nossas novidades</h3>
                        <p style="color: white; margin-bottom:10px;">Assine nossa newsletter e receba ofertas, promoções, novidades, e muito mais..</p>
                    </div>
                </div>

                <div class="col-md-12" id="msgSucessoNewsletter"></div>

                <div class="col-md-12">
                
                    <form class="form-horizontal" id="formNewsletter">
                    
                            <div class="col-md-3 col-xs-12"> 
                                <input type="text" id="nome_newsletter" name="nome_newsletter" class="form-control input-form-ligar" placeholder="Nome *" autocomplete="off" style="background: #af5022; color: white;"> 
                            </div>

                            <div class="col-md-3 col-xs-12"> 
                                <input type="text" id="telefone_newsletter" name="telefone_newsletter" class="form-control input-form-ligar" placeholder="Telefone (WhatsApp) *" autocomplete="off" style="background: #af5022; color: white;"> 
                            </div>

                            <div class="col-md-3 col-xs-12"> 
                                <input type="email" id="email_newsletter" name="email_newsletter" class="form-control input-form-ligar" placeholder="E-mail *" autocomplete="off" style="background: #af5022; color: white;"> 
                            </div>

                            <div class="col-md-3 col-xs-12"> 
                                <button type="submit" class="btn btn-orangeplus btn-medium btn-block" style="margin-top: 10px;"> Assinar </button>
                            </div>
                            
                        <div class="form-group col-md-12 col-xs-12" class="text-center">
                            <p style="color: white; text-align:center;">Fique atento (a) a sua caixa de spam/lixo eletrônico.</p>
                        </div>

                    </form>
                        
                </div>
            </div>
        </div>
    </div>

</section>