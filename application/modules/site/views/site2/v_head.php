<!DOCTYPE html>

<html class="no-js" lang="pt-br" style="overflow-x: hidden;">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="description" content="Soluções Completas em Internet para sua Casa e Empresa. Alta Velocidade e Disponibilidade. Qualidade e Economia. Atendimento Diferenciado.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="SolidSystem.net.br">
    <meta name="keywords" content="internet, plano de internet, internet rápida, internet veloz, internet fibra, internet alta velocidade, ultra veloz, fibra ótica, fibra">
	
	<link rel="icon" type="image/png" sizes="32x32" href="<?=base_url('assets/sites/ispmais/site_base/images/favicon.png')?>">

    <title><?=$titulo;?></title>
    
    <link rel="canonical" href="<?=base_url()?>"/>
    <meta name="description" content="<?=$seo->seo_description1;?>">
    <meta name="keywords" content="<?=$seo->seo_keywords;?>">
    <meta name="Abstract" content="<?=$seo->seo_description1;?>" />
    <meta name="robots" content="index, follow">
    <meta name="revisit-after" content="1 day">
    <meta name="language" content="Portuguese">
    <meta name="generator" content="N/A">
    <meta name="country" content="BRA" />
    <meta name="email" content="contato@ispmais.com">
    <meta name="currency" content="R$" />   
    <base href="<?=base_url();?>">
    <link href="" rel="publisher" />
    <meta name="google-site-verification" content="<?=$seo->seo_googleVerificacao;?>">
    <meta property="og:locale" content="pt_BR"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="<?=$seo->seo_nomeSite;?>"/>
    <meta property="og:description" content="<?=$seo->seo_description1;?>"/>
    <meta property="og:url" content="<?=base_url()?>"/>
    <meta property="og:site_name" content="<?=$seo->seo_nomeSite;?>"/>
    <meta property="og:image" content="<?=base_url('/assets/sites/ispmais/img/seo/').$seo->seo_imagem;?>"/>
    <meta property="og:image:secure_url" content="<?=base_url('/assets/sites/ispmais/img/seo/').$seo->seo_imagem?>"/>
    <meta property="og:image:alt" content="<?=$seo->seo_nomeSite?>"/>
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="850">
    <meta property="og:image:height" content="400">
    <meta itemprop="name" content="<?=$seo->seo_nomeSite?>" />
    <meta itemprop="url" content="<?=base_url()?>" />
    <meta name="author" content="Josheffer Robert">
    <meta name="application-name" content="<?=$seo->seo_nomeSite?>">
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:description" content="<?=$seo->seo_description2?>"/>
    <meta name="twitter:title" content="<?=$seo->seo_nomeSite?>"/>
    <meta name="twitter:image" content="<?=base_url('/assets/sites/ispmais/img/seo/').$seo->seo_imagem?>"/>
    <meta content="<?=$seo->seo_corTema?>" name=theme-color>
    <meta name="geo.position" content="<?=$seo->seo_coordenadasMaps?>" />
    <meta name="geo.placename" content="<?=$seo->seo_cidade?>" />
    <meta name="geo.region" content="<?=$seo->seo_estado?>" />
    <meta name="ICBM" content="<?=$seo->seo_coordenadasMaps?>" />
    <meta name="author" content="Josheffer Robert" />
    <meta name="copyright" content="Josheffer Robert" />
    <meta name="DC.title" content="<?=$seo->seo_nomeSite?>" />
    <meta name="distribution" content="'Global" />
    <meta name="rating" content="'General" />
    <meta name="doc-class" content="'Completed" />
    
    <link rel="icon" href="<?=base_url('assets/images/')?>favicon.png" type="image/png">
    
    <link rel="stylesheet" href="<?=base_url('assets/sites/ispmais/site_base/css/fontawesome-icons/css/all.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/sites/ispmais/site_base/css/bootsnav.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/sites/ispmais/site_base/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/sites/ispmais/site_base/style/other/aos.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/sites/ispmais/site_base/style/other/animate.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/sites/ispmais/site_base/style/style.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/sites/ispmais/site_base/style/responsive.css')?>">
    
    <style>

        .centered {
        text-align: center;
        font-size: 0;
        }

        .centered > div {
        float: none;
        display: inline-block;
        text-align: left;
        font-size: 13px;
        }
    
        <?=$seo->seo_css;?>

    </style>
    

    <?=$seo->seo_js?>
    
</head>