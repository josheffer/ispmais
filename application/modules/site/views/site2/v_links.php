<nav class="navbar navbar-default dark navbar-sticky bootsnav" style="height: 80px;">
    
    <div class="custom-width">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <i class="fa fa-bars"></i>
            </button>

            <?php
                $url_atual = $_SERVER['REQUEST_URI'];
                $url = explode('/', $url_atual);

                $url_site = base_url().$url[1]."/".$url[2];
            ?>
            
            <a class="navbar-brand" href="<?=$url_site?>">
                <img src="<?=base_url('assets/sites/ispmais/img/logo.png')?>" class="logo" alt="">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="navbar-menu">
            <ul class="nav navbar-nav navbar-right" style="background-color: #19233f;" data-in="fadeIn" data-out="fadeOut" id="linksBarraSuperiorFixa"> </ul>
        </div>

    </div>
    
</nav>