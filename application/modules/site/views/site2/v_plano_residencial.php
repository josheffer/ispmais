<section id="subirPlanoresidencial"></section>

<div class="pricing-tables padding-top50 padding-bottom50">
    <div class="custom-width">

        <div class="centered row">
        
            <div class="padding-bottom50 text-center">
                <h2><strong class="text-orange"><i>Banda larga</i></strong> com ultra velocidade para a sua <strong class="text-orange"><i>casa</i></strong></h2>
            </div>

            <div id="msgSucessoPlanoResidencial"></div>

            <center>
                <div id="tabelaListarPlanosResidenciais" class="centered"></div>
            </center>
            
        </div>
    </div>
</div>