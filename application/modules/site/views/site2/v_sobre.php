<div class="boxes-two search-domain padding-bottom50 padding-top50">
    <div class="container">
        <div class="row">
            
            <div class="main-title text-center">
                <h2><?=$dados_sobre->titulo_sobre;?></h2>
                <p><?=$dados_sobre->descricao_sobre;?></p>
            </div>

            <section style="justify-content: center; display: flex;">
                <img class="img-fluid" width="220px" src="<?=base_url('assets/sites/ispmais/img/sobre/').$dados_sobre->imagem_sobre;?>" title="" alt="">
            </section>
            
            <section class="margin-top50">
                <center>
                    <h4><?=$dados_sobre->nomeEmpresa_sobre;?></h4>
                    <p><?=$dados_sobre->texto_sobre;?></p>
                </center>
            </section>

            <hr class="margin-top50">

            <div class="col-md-12">

                <div class="col-md-4">
                    <section class="margin-top50">

                        <center>
                            <h4>Missão</h4>
                            <p><?=$dados_sobre->missao_sobre;?></p>
                        </center>

                    </section>
                </div>

                <div class="col-md-4">
                    <section class="margin-top50">

                        <center>
                            <h4>Visão</h4>
                            <p><?=$dados_sobre->visao_sobre;?></p>
                        </center>

                    </section>
                </div>

                <div class="col-md-4">
                    <section class="margin-top50">

                        <center>
                            <h4>Valores</h4>
                            <p><?=$dados_sobre->valores_sobre;?></p>
                        </center>

                    </section>
                </div>

            </div>
            
        </div>
    </div>
</div>