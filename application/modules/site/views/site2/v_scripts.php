
    <script src="<?=base_url('assets/sites/ispmais/site_base/js/jquery.js')?>"></script>
    <script src="https://unpkg.com/imask"></script>
	<script src="<?=base_url('assets/sites/ispmais/site_base/js/fakeLoader.min.js')?>"></script>
    <script src="<?=base_url('assets/sites/ispmais/site_base/js/validator.js')?>"></script>
	<script src="<?=base_url('assets/sites/ispmais/site_base/js/contact.js')?>"></script>
    <script src="<?=base_url('assets/sites/ispmais/site_base/js/bootsnav.js')?>"></script>
	<script src="<?=base_url('assets/sites/ispmais/site_base/js/bootstrap.min.js')?>"></script>
	<script src="<?=base_url('assets/sites/ispmais/site_base/js/aos.js')?>"></script>
	<script src="<?=base_url('assets/sites/ispmais/site_base/js/custom.js')?>"></script>

	<script src="<?=base_url('assets/sites/ispmais/js/vendor/avisoGlobal.js')?>"></script>
	<script src="<?=base_url('assets/sites/ispmais/js/vendor/barraSuperiorFixa.js')?>"></script>
	<script src="<?=base_url('assets/sites/ispmais/js/vendor/linksBarraSuperiorFixa.js')?>"></script>
	<script src="<?=base_url('assets/sites/ispmais/js/vendor/slideCarousel.js')?>"></script>
	<script src="<?=base_url('assets/sites/ispmais/js/vendor/coberturaAtendimento.js')?>"></script>
	<script src="<?=base_url('assets/sites/ispmais/js/vendor/planosResidenciais.js')?>"></script>
	<script src="<?=base_url('assets/sites/ispmais/js/vendor/planosEmpresariais.js')?>"></script>
	<script src="<?=base_url('assets/sites/ispmais/js/vendor/fale_conosco_regioes.js')?>"></script>
	<script src="<?=base_url('assets/sites/ispmais/js/vendor/assinarNewsletter.js')?>"></script>
	
</body>

</html>