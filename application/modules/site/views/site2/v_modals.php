<div id="modalLigar" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal">&times;</button> 
                <h4 class="modal-title">Ligaremos para você!</h4> 
            </div> 

            <div class="modal-body"> 

                <form id="formSolicitarLigacao"> 
                
                    <div class="form-group col-md-6"> 
                        <input type="text" id="nomeLigar" name="nomeLigar" class="form-control input-form-ligar" placeholder="Nome completo *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-6"> 
                        <input type="text" name="telefoneLigar" id="telefoneLigar" class="form-control input-form-ligar" placeholder="Telefone (WhatsApp) *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-6"> 
                        <input type="text" id="emailLigar" name="emailLigar" class="form-control input-form-ligar" placeholder="E-mail" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-6"> 
                        <select class="form-control input-form-ligar" name="PlanoLigar" id="modalLigarPlanos"></select>
                    </div> 
                    
                    <div class="form-group col-md-3"> 
                        <input type="text" id="cep" name="cepLigar" value="" size="11" maxlength="10" class="form-control input-form-ligar" placeholder="CEP *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-9"> 
                        <input type="text" name="ruaLigar" id="rua" size="60" class="form-control input-form-ligar" placeholder="Rua *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-9"> 
                        <input type="text" name="complementoLigar" id="complementoLigar" class="form-control input-form-ligar" placeholder="Complemento *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-3"> 
                        <input type="text" name="numeroEnderecoLigar" id="numeroEnderecoLigar" class="form-control input-form-ligar" placeholder="Número ou S/N *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-4"> 
                        <input type="text" name="bairroLigar" id="bairro" size="40" class="form-control input-form-ligar" placeholder="Bairro *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-4"> 
                        <input  type="text" name="cidadeLigar" id="cidade" class="form-control input-form-ligar" placeholder="Cidade *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-4"> 
                        <input type="text" name="ufLigar" id="uf" size="2" class="form-control input-form-ligar" placeholder="Estado *" autocomplete="off"> 
                    </div> 
                    
                    <div class="form-group col-md-12"> 
                        <textarea id="observacaoLigar" name="observacaoLigar" class="form-control input-form-ligar" placeholder="Observações" rows="4"></textarea>
                    </div> 

                    <div class="form-group col-md-12"> 
                        <div id="msgErroSolicLigacao"></div>
                    </div> 
                    
                    <div class="form-group col-md-12"> 
                        <div clas="col-md-12">
                            <button class="btn btn-blue btn-block btn-small"> Solicitar ligação </button>
                        </div>
                    </div> 

                    <div class="col-md-12">
                        <h6>&nbsp;&nbsp;&nbsp;</h6>
                        </hr>
                    </div> 

                </form> 

            </div> 

            <div class="modal-footer"> 
                <div class="col-md-10"></div> 
                <div class="col-md-2"><button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Fechar</button></div> 
            </div> 

        </div> 
    </div>
</div>
    
<div id="modalContratarPlanoResidencial" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal">&times;</button> 
                <h4 class="modal-title">Contratar plano: <strong class="text-orange"><i><u><span id="nomePlanoResidencialModalContratar"></span></u></i></strong> !</h4> 
            </div> 

            <div class="modal-body"> 

                <form id="contratarPlanoresidencial"> 

                <input type="hidden" name="id_planoResidencial" id="id_planoResidencial" > 
                
                    <div class="form-group col-md-6"> 
                        <input type="text" name="nome_planoResidencial" id="nome_planoResidencial" class="form-control input-form-ligar" placeholder="Nome completo *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-6"> 
                        <input type="text"  name="telefone_planoResidencial" id="telefone_planoResidencial" class="form-control input-form-ligar" placeholder="Telefone (WhatsApp) *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-6"> 
                        <input type="text" name="email_planoResidencial" id="email_planoResidencial" class="form-control input-form-ligar" placeholder="E-mail" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-6"> 
                        <input type="text" name="cpf_planoResidencial" id="cpf_planoResidencial" class="form-control input-form-ligar" placeholder="C.P.F *" autocomplete="off"> 
                    </div> 
                    
                    <div class="form-group col-md-3"> 
                        <input type="text" name="cep_planoResidencial" id="cep_planoResidencial" value="" size="11" maxlength="10" class="form-control input-form-ligar" placeholder="CEP *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-9"> 
                        <input  type="text" name="rua_planoResidencial" id="rua_planoResidencial" size="60" class="form-control input-form-ligar" placeholder="Rua *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-9"> 
                        <input type="text" name="complemento_planoResidencial" id="complemento_planoResidencial" class="form-control input-form-ligar" placeholder="Complemento *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-3"> 
                        <input type="text" name="numeroResidencial_planoResidencial" id="numeroResidencial_planoResidencial" class="form-control input-form-ligar" placeholder="Número ou S/N *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-4"> 
                        <input  type="text" name="bairro_planoResidencial" id="bairro_planoResidencial" size="40" class="form-control input-form-ligar" placeholder="Bairro *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-4"> 
                        <input  type="text" name="cidade_planoResidencial" id="cidade_planoResidencial" class="form-control input-form-ligar" placeholder="Cidade *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-4"> 
                        <input  type="text" name="uf_planoResidencial" id="uf_planoResidencial" size="2" class="form-control input-form-ligar" placeholder="Estado *" autocomplete="off"> 
                    </div>                             
                    
                    <div class="form-group col-md-12"> 
                        <textarea name="observacao_planoResidencial" id="observacao_planoResidencial" class="form-control input-form-ligar" placeholder="Observações" rows="4"></textarea> 
                    </div> 

                    <div class="form-group col-md-12"> 
                        <div id="msgErroPlanoResidencial"></div>
                    </div> 
                    
                    <div class="form-group col-md-12"> 
                        <div clas="col-md-12">
                            <button class="btn btn-blue btn-block btn-small"> Contratar plano: <span id="nomePlanoResidencialContratar"></span></button>
                        </div>
                    </div> 

                    <div col-md-12><h6>&nbsp;&nbsp;&nbsp;</h6></hr></div> 

                </form> 

            </div> 

            <div class="modal-footer"> 
                <div class="col-md-10"></div> 
                <div class="col-md-2"><button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Fechar</button></div> 
            </div> 

        </div> 
    </div>
</div>

<div id="modalContratarPlanoEmpresarial" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal">&times;</button> 
                <h4 class="modal-title">Contratar plano: <strong class="text-orange"><i><u><span id="nomePlanoEmpresarialModalContratar"></span></u></i></strong> !</h4> 
            </div> 

            <div class="modal-body"> 

                <form id="contratarPlanoEmpresarial"> 
                    
                    <input type="hidden" name="id_planoEmpresarial" id="id_planoEmpresarial">

                    <div class="form-group col-md-12"> 
                        <input type="text" name="nome_planoEmpresarial" id="nome_planoEmpresarial" class="form-control input-form-ligar" placeholder="Nome completo *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-6"> 
                        <input type="text" name="telefone_planoEmpresarial" id="telefone_planoEmpresarial" class="form-control input-form-ligar" placeholder="Telefone (WhatsApp) *" autocomplete="off"> 
                    </div> 
                    
                    <div class="form-group col-md-6"> 
                        <input type="text" name="telefoneFixo_planoEmpresarial" id="telefoneFixo_planoEmpresarial" class="form-control input-form-ligar" placeholder="Telefone Fixo *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-4"> 
                        <input type="text" name="email_planoEmpresarial" id="email_planoEmpresarial" class="form-control input-form-ligar" placeholder="E-mail" autocomplete="off"> 
                    </div> 
                    
                    <div class="form-group col-md-4"> 
                        <input type="text" name="cnpj_planoEmpresarial" id="cnpj_planoEmpresarial" class="form-control input-form-ligar" placeholder="CNPJ *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-4"> 
                        <input type="text" name="nomeFantasia_planoEmpresarial" id="nomeFantasia_planoEmpresarial" class="form-control input-form-ligar" placeholder="Nome Fantasia *" autocomplete="off"> 
                    </div> 
                    
                    <div class="form-group col-md-3"> 
                        <input type="text" id="cep_planoEmpresarial" name="cep_planoEmpresarial" size="11" maxlength="10" class="form-control input-form-ligar" placeholder="CEP *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-9"> 
                        <input type="text" name="rua_planoEmpresarial" id="rua_planoEmpresarial" size="60" class="form-control input-form-ligar" placeholder="Rua *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-9"> 
                        <input type="text" name="complemento_planoEmpresarial" id="complemento_planoEmpresarial" class="form-control input-form-ligar" placeholder="Complemento *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-3"> 
                        <input type="text" name="numero_planoEmpresarial" id="numero_planoEmpresarial" class="form-control input-form-ligar" placeholder="Número ou S/N *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-4"> 
                        <input  type="text" name="bairro_planoEmpresarial" id="bairro_planoEmpresarial" size="40" class="form-control input-form-ligar" placeholder="Bairro *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-4"> 
                        <input  type="text" name="cidade_planoEmpresarial" id="cidade_planoEmpresarial" class="form-control input-form-ligar" placeholder="Cidade *" autocomplete="off"> 
                    </div> 

                    <div class="form-group col-md-4"> 
                        <input  type="text" name="uf_planoEmpresarial" id="uf_planoEmpresarial" size="2" class="form-control input-form-ligar" placeholder="Estado *" autocomplete="off"> 
                    </div> 
                    
                    <div class="form-group col-md-12"> 
                        <textarea name="observacao_planoEmpresarial" id="observacao_planoEmpresarial" class="form-control input-form-ligar" placeholder="Observações" rows="4"></textarea> 
                    </div> 

                    <div class="form-group col-md-12"> 
                        <div id="msgErroPlanoEmpresarial"></div>
                    </div> 
                    
                    <div class="form-group col-md-12"> 
                        <div clas="col-md-12">
                            <button class="btn btn-blue btn-block btn-small"> Contratar plano: <span id="nomePlanoEmpresarialContratar"></span></button>
                        </div>
                    </div> 

                    <div col-md-12><h6>&nbsp;&nbsp;&nbsp;</h6></hr></div> 

                </form> 

            </div> 

            <div class="modal-footer"> 
                <div class="col-md-10"></div> 
                <div class="col-md-2"><button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Fechar</button></div> 
            </div> 

        </div> 
    </div>
</div>