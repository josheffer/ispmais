<div class="under-footer">
    <div class="container">
        <div class="row">
            
            <div class="col-md-6">
                <p>Todos os direitos reservados ® 2011 - <?=date('Y');?>. www.ispmais.com</p>
            </div>

            <div class="col-md-3">
                <div class="social-media">
                    <a>Siga a ISPMAIS</i></a>
                    
                    <a id="facebook_footer"></a>
                    <!-- <a id="facebook_footer" href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a> -->
                    <!-- <a id="instagram_footer" href="javascript:void(0);"> <i class="fab fa-instagram"></i></a> -->
                    <a id="instagram_footer"></a>
                </div>
            </div>

            <div class="col-md-3">
                <p class="pull-right">Dev. 
                    <a href="https://solidsystem.net.br" target="_blank">
                        <img src="<?=base_url('assets/sites/ispmais/img/SolidSystem.png')?>" alt="SolidSystem - Sistemas web" width="100px">
                    </a>
                </p>
            </div>

        </div>
    </div>
</div>