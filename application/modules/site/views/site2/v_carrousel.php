<section class="lighter-bg" id="carousel">
    <div class="custom-width" style="padding: 0; overflow-x: hidden;">
        <div class="row">

            <div class="col-md-12">
                
                <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="4000" style="padding-bottom: 0;">
                    
                    <ol class="carousel-indicators" style="z-index: 1;" id="indicadoresSlides"></ol>
                    
                    <div class="carousel-inner" id="listarSlides"></div>

                </div>
            </div>
        </div>
    </div>
</section>