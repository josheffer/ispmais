
<!DOCTYPE html>
<html lang="pt-br">
    <head>

        <meta charset="utf-8" />

        <title>Bio Instagram ISPMAIS Telecom</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <link rel="icon" type="image/png" sizes="32x32" href="http://ispmais.net.br/assets/sites/ispmais/img/favicon.png">
        
        <link href="/assets/painel/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/painel/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/painel/css/app.min.css" rel="stylesheet" type="text/css" />
        
        <style>
            body.authentication-bg {
                background-color: <?=$cores_estilo->corBg_cores_estilos;?> !important;
            }

            .card {
                background-color: <?=$cores_estilo->corBoxMeio_cores_estilos;?> !important;
                box-shadow: 14px 12px 14px #0000004d !important;
            }

            .auth-title {
                background-color: <?=$cores_estilo->CorFaixas_cores_estilos;?> !important;
                border-top: 1px solid <?=$cores_estilo->CorFaixas_cores_estilos;?> !important;
                border-bottom: 1px solid <?=$cores_estilo->CorFaixas_cores_estilos;?> !important;
                color: <?=$cores_estilo->corTextoFaixa_cores_estilos;?> !important;
            }

            .img-thumbnail {
                background-color: <?=$cores_estilo->corBoxMeio_cores_estilos;?> !important;
                border: 1px solid <?=$cores_estilo->CorFaixas_cores_estilos;?> !important;
            }

            .corTextoFooter {
                color: <?=$cores_estilo->corTextoRodape_cores_estilos;?> !important;
            }

            .botaoCorEstilo {
                color: <?=$cores_estilo->corTextosBotoes_cores_estilos;?> !important;
                background-color: <?=$cores_estilo->corBgBotoes_cores_estilos;?> !important;
            }

            .botaoCorEstilo:hover {
                color: <?=$cores_estilo->corTextosBotoesHover_cores_estilos;?> !important;
                background-color: <?=$cores_estilo->corBgBotoesHover_cores_estilos;?> !important;
            }
        </style>

    </head>
    
    <body class="authentication-bg">

        <div class="account-pages mt-5 mb-5">

            <div class="container">

                <div class="row justify-content-center">

                    <div class="col-md-8 col-lg-6 col-xl-5">

                        <div class="card">

                            <div class="card-body p-4">
                                
                                <div class="text-center m-auto">
                                    <img src="/assets/sites/ispmais/img/bio/<?=$infos->imagem_infos_bio;?>" width="150" alt="Logo ISPMAIS" class="rounded-circle img-thumbnail">
                                    <h4 class="text-dark-50 text-center mt-3"> <?=$infos->nome_infos_bio;?> <i class="text-primary mdi mdi-check-circle" data-toggle="tooltip" data-placement="top" data-original-title="Verificado"></i></h4>
                                    <p class="text-muted mb-4"><?=$infos->descricao_infos_bio;?></p>
                                </div>

                                <h5 class="auth-title">Sites</h5>

                                <?php
                                    foreach ($sites as $site) { 
                                        
                                        if($site->status_sites_bio === '1'){ ?>
                                        
                                            <div class="form-group mb-2 text-center">
                                                <a href="<?=$site->url_sites_bio;?>" target="_blank" rel="noopener noreferrer">
                                                    <button class="btn botaoCorEstilo btn-block" type="button"> <i class="mdi mdi-web"></i> <?=$site->nome_sites_bio;?> </button>
                                                </a>
                                            </div>

                                        <?php } 

                                    }
                                ?>
                                
                                <h5 class="mt-3 auth-title">Nossos canais de atendimento</h5>

                                <?php foreach ($canais_atendimento as $canal) { 
                                        
                                    if($canal->ativo_canal_atendimento === '1') 
                                    { 
                                        switch ($canal->tipo_canal_atendimento) {
                                            case '1':
                                                $linkCanal = $canal->url_canal_atendimento;
                                                break;
                                            case '2':
                                                $linkCanal = $canal->url_canal_atendimento;
                                                break;
                                            case '3':
                                                $linkCanal = $canal->url_canal_atendimento;
                                                break;
                                            case '4':
                                                $linkCanal = $canal->url_canal_atendimento;
                                                break;
                                            case '5':
                                                $linkCanal = "mailto:".$canal->url_canal_atendimento;
                                                break;
                                            case '6':
                                                $linkCanal = "tel:".$canal->url_canal_atendimento;
                                                break;
                                        } ?>

                                        <div class="form-group mb-2 text-center">
                                            <a href="<?=$linkCanal;?>" target="_blank" rel="noopener noreferrer">
                                                <button class="btn botaoCorEstilo btn-block" type="button"> <i class="<?=$canal->icone_canal_atendimento;?>"></i> <?=$canal->nome_canal_atendimento;?> </button>
                                            </a>
                                        </div>
                                        
                                    <?php } 

                                }  ?>
                                
                                <h5 class="mt-3 auth-title">Redes sociais</h5>

                                <?php
                                    foreach ($redes_sociais as $rede) { 
                                        
                                        if($rede->ativo_redes_sociais === '1'){ ?>
                                        
                                            <div class="form-group mb-2 text-center">
                                                <a href="<?=$rede->url_redes_sociais;?>" target="_blank" rel="noopener noreferrer">
                                                    <button class="btn botaoCorEstilo btn-block" type="button"> <i class="<?=$rede->icone_redes_sociais;?>"></i> <?=$rede->nome_redes_sociais;?> </button>
                                                </a>
                                            </div>

                                        <?php } 

                                    }
                                ?>
                                
                            </div> 
                            
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
        <!-- <footer class="footer footer-alt corTextoFooter"> 2011 - 2020 © ISPMAIS Telecom </footer> -->
        
        <footer class="footer footer-alt corTextoFooter mt-5"> <?=$infos->anoEmpresa_infos_bio;?> - <?=date('Y');?> © <?=$infos->textoEmpresa_infos_bio;?> </footer>
        
        <script src="/assets/painel/js/vendor.min.js"></script>
        <script src="/assets/painel/js/app.min.js"></script>
        
    </body>

</html>