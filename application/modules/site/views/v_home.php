<!DOCTYPE html>

<html lang="pt-br">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="author" content="Josheffer Robert, SolidSystem.net.br">

	<link rel="icon" type="image/png" sizes="32x32" href="<?=base_url('assets/sites/ispmais/site/images/favicon.png')?>">

	<title><?=$titulo;?></title>
	
	<link rel="canonical" href="<?=base_url()?>"/>
	<meta name="description" content="<?=$seo->intro_seo_description1;?>">
	<meta name="keywords" content="<?=$seo->intro_seo_keywords;?>">
	<meta name="Abstract" content="<?=$seo->intro_seo_description1;?>" />
	<meta name="robots" content="index, follow">
	<meta name="revisit-after" content="1 day">
	<meta name="language" content="Portuguese">
	<meta name="generator" content="N/A">
	<meta name="country" content="BRA" />
	<meta name="email" content="contato@ispmais.com">
	<meta name="currency" content="R$" />   
	<base href="<?=base_url();?>">
	<link href="" rel="publisher" />
	<meta name="google-site-verification" content="<?=$seo->intro_seo_googleVerificacao;?>">
	<meta property="og:locale" content="pt_BR"/>
	<meta property="og:type" content="website"/>
	<meta property="og:title" content="<?=$seo->intro_seo_nomeSite;?>"/>
	<meta property="og:description" content="<?=$seo->intro_seo_description1;?>"/>
	<meta property="og:url" content="<?=base_url()?>"/>
	<meta property="og:site_name" content="<?=$seo->intro_seo_nomeSite;?>"/>
	<meta property="og:image" content="<?=base_url('/assets/sites/ispmais/img/seo/').$seo->intro_seo_imagem;?>"/>
	<meta property="og:image:secure_url" content="<?=base_url('/assets/sites/ispmais/img/seo/').$seo->intro_seo_imagem?>"/>
	<meta property="og:image:alt" content="<?=$seo->intro_seo_nomeSite?>"/>
	<meta property="og:image:type" content="image/jpeg">
	<meta property="og:image:width" content="850">
	<meta property="og:image:height" content="400">
	<meta itemprop="name" content="<?=$seo->intro_seo_nomeSite?>" />
	<meta itemprop="url" content="<?=base_url()?>" />
	<meta name="application-name" content="<?=$seo->intro_seo_nomeSite?>">
	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:description" content="<?=$seo->intro_seo_description2?>"/>
	<meta name="twitter:title" content="<?=$seo->intro_seo_nomeSite?>"/>
	<meta name="twitter:image" content="<?=base_url('/assets/sites/ispmais/img/seo/').$seo->intro_seo_imagem?>"/>
	<meta content="<?=$seo->intro_seo_corTema?>" name=theme-color>
	<meta name="geo.position" content="<?=$seo->intro_seo_coordenadasMaps?>" />
	<meta name="geo.placename" content="<?=$seo->intro_seo_cidade?>" />
	<meta name="geo.region" content="<?=$seo->intro_seo_estado?>" />
	<meta name="ICBM" content="<?=$seo->intro_seo_coordenadasMaps?>" />
	<meta name="copyright" content="Josheffer Robert, SolidSystem.net.br" />
	<meta name="DC.title" content="<?=$seo->intro_seo_nomeSite?>" />
	<meta name="distribution" content="'Global" />
	<meta name="rating" content="'General" />
	<meta name="doc-class" content="'Completed" />
    
	<?=$scriptsCSS?>
	
	<style>

        <?=$seo->intro_seo_css;?>

    </style>

	<?=$seo->intro_seo_js;?>
	
	
</head>

<body class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading" style="background-image: url('<?=base_url("/assets/sites/ispmais/img/bg-2.jpg");?>');">

	<div class="d-flex flex-column flex-root">

		<div class="login-5 login-signin-on d-flex flex-row-fluid" id="kt_login">
			
			<div class="d-flex flex-center bgi-size-cover bgi-no-repeat flex-row-fluid">
				
				<div class="login-form text-center text-white p-7 position-relative overflow-hidden">
					
					<div class="d-flex flex-center mb-5">
						
						<a href="javascript: void(0);">
							<img src="<?=base_url('/assets/sites/ispmais/img/logo.png');?>" class="max-h-125px" alt="ISPMAIS - Soluções Completas em Internet para sua Casa e Empresa. Alta Velocidade e Disponibilidade. Qualidade e Economia. Atendimento Diferenciado." />
						</a>

					</div>

					<h1 class="font-weight-normal mb-5 text-orange">Escolha a sua região</h1>

						<div class="row" id="boxRegiao" ></div>

					</div>
					
				</div>
				
			</div>

			<div class="footer mt-5 bg-orange py-4 d-flex flex-lg-column " id="kt_footer">
		
				<div class=" container-fluid  d-flex flex-column flex-md-row align-items-center justify-content-between">
					
					<div class="text-dark order-2 order-md-1">
						<span class="text-white font-weight-bold mr-1">2020 © </span>
						<span class="text-white">Desenvolvimento:</span> <a href="https://solidsystem.net.br" target="_blank" class="text-white">
							<img class="ml-2" src="<?=base_url('/assets/sites/ispmais/img/SolidSystem.png');?>" alt="SolidSystem - Sistemas web" width="100px">
						</a> 
					</div>
					
					<div class="nav nav-dark">
						<span class="text-white">Direitos reservados a <span class="text-white font-weight-bold mr-2">ISPMAIS Telecom</span> - www.ispmais.com</span>
					</div>
		
				</div>
				
			</div>
			
		</div>
		
	</div>

	<?=$scriptsJS?>
	
</body>

</html>