<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends MX_Controller  {

	function __construct()
    {
        parent::__construct();

		$this->load->model("Site_model", "site");
		$this->load->model("Site/Site_admin_model", "site_admin");

		$this->load->library('contadorvisitas');
		
	}
	
	public function index()
	{
		$dados = array(
            'colunas'       => "*",
            'tabela'        => "site_intro_seo",
            'coluna_where'  => "id_intro_seo",
            'valor_where'   => (int)1
        );
    
		$SEO = $this->site_admin->m_buscar_1_Item($dados);
		
		$scripts = array(
			
			"titulo" => $SEO->intro_seo_tituloSite,

			"seo" => $SEO,

			"scriptsJS" =>'<script src="'.base_url("/assets/sites/ispmais/js/jquery-3.5.1.min.js").'"></script>
							<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
							<script src="'.base_url("/assets/sites/ispmais/js/scripts_regioes.js").'"></script>
							', 
								
			"scriptsCSS" => '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
							<link rel="shortcut icon" href="'.base_url("/assets/sites/ispmais/img/favicon.png").'"/>
							<link href="'.base_url("/assets/sites/ispmais/css/plugins.bundle.css").'" rel="stylesheet" type="text/css"/>
							<link href="'.base_url("/assets/sites/ispmais/css/style.bundle.css").'" rel="stylesheet" type="text/css"/>',
			
			"scriptsCSSacima" => "",
			
			"scriptsJSacima" => ''
		);
		
		$this->load->view('v_home', $scripts);

		// INICIO CONTADOR DE VISITAS
		$tabela = "site_visitas_intro";

		$this->contadorvisitas->contarVisitas($tabela);
		// FIM CONTADOR DE VISITAS
	}

	public function v_bio()
	{
		$dados1 = array(
            'colunas'       => "*",
            'tabela'        => "site_bio_infos",
            'coluna_where'  => "id_infos_bio",
            'valor_where'   => (int)1
        );
    
        $resultado1 = $this->site_admin->m_buscar_1_Item($dados1);
        
        $dados2 = array(
            'colunas'   => "*",
            'tabela'    => "site_bio_sites",
            'coluna_where' => "id_sites_bio",
            'orderBy'   => "DESC"
        );
    
        $resultado2 = $this->site_admin->m_buscar_tudo_orderBy($dados2);

        $dados3 = array(
            'colunas'   => "*",
            'tabela'    => "site_bio_canais_atendimento",
            'coluna_where' => "id_canal_atendimento",
            'orderBy'   => "DESC"
        );
    
        $resultado3 = $this->site_admin->m_buscar_tudo_orderBy($dados3);

        $dados4 = array(
            'colunas'   => "*",
            'tabela'    => "site_bio_redes_sociais",
            'coluna_where' => "id_redes_sociais",
            'orderBy'   => "DESC"
        );
    
		$resultado4 = $this->site_admin->m_buscar_tudo_orderBy($dados4);

		$dados5 = array(
            'colunas'       => "*",
            'tabela'        => "site_bio_cores_estilos",
            'coluna_where'  => "id_cores_estilos",
            'valor_where'   => (int)1
        );
    
        $resultado5 = $this->site_admin->m_buscar_1_Item($dados5);
		
		$scripts = array(

			"infos" 				=> $resultado1,
			"sites" 				=> $resultado2,
			"canais_atendimento" 	=> $resultado3,
			"redes_sociais" 		=> $resultado4,
			"cores_estilo" 			=> $resultado5,
			
			"scriptsJS" =>'<script src="'.base_url("/assets/sites/ispmais/js/jquery-3.5.1.min.js").'"></script>
							<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
							<script src="'.base_url("/assets/sites/ispmais/js/scripts_regioes.js").'"></script>
							', 
								
			"scriptsCSS" => '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
							<link rel="shortcut icon" href="'.base_url("/assets/sites/ispmais/img/favicon.png").'"/>
							<link href="'.base_url("/assets/sites/ispmais/css/plugins.bundle.css").'" rel="stylesheet" type="text/css"/>
							<link href="'.base_url("/assets/sites/ispmais/css/style.bundle.css").'" rel="stylesheet" type="text/css"/>',
			
			"scriptsCSSacima" => "",
			
			"scriptsJSacima" => ''
		);
		
		$this->load->view('v_bio', $scripts);

		// INICIO CONTADOR DE VISITAS
		$tabela = "site_visitas_intro";

		$this->contadorvisitas->contarVisitas($tabela);
		// FIM CONTADOR DE VISITAS
	}

	public function v_site1()
	{
		$scripts = array(
			
			"titulo" => "Internet Banda Larga | ISP Mais Telecom",

			"scriptsJS" =>'<script src="/assets/sites/ispmais/js/jquery-3.5.1.min.js"></script>
							<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
							<script src="/assets/sites/ispmais/js/scripts_regioes.js"></script>
							', 
								
			"scriptsCSS" => '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
							<link rel="shortcut icon" href="/assets/sites/ispmais/img/favicon.png" />
							<link href="/assets/sites/ispmais/css/plugins.bundle.css" rel="stylesheet" type="text/css"/>
							<link href="/assets/sites/ispmais/css/style.bundle.css" rel="stylesheet" type="text/css"/>',
			
			"scriptsCSSacima" => "",
			
			"scriptsJSacima" => ''
		);
		
		$this->load->view('v_home', $scripts);
	}

	public function v_site2()
	{	
		$dados = array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_seo",
            'coluna_where'  => "id_seo",
            'valor_where'   => (int)1
        );
    
		$SEO = $this->site_admin->m_buscar_1_Item($dados);
		
		$scripts = array(
			
			"titulo" => $SEO->seo_tituloSite,

			"seo" => $SEO,

			"scriptsJS" =>'<script src="/assets/sites/ispmais/js/jquery-3.5.1.min.js"></script>
							<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
							<script src="/assets/sites/ispmais/js/scripts_regioes.js"></script>
							', 
								
			"scriptsCSS" => '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
							<link rel="shortcut icon" href="/assets/sites/ispmais/img/favicon.png" />
							<link href="/assets/sites/ispmais/css/plugins.bundle.css" rel="stylesheet" type="text/css"/>
							<link href="/assets/sites/ispmais/css/style.bundle.css" rel="stylesheet" type="text/css"/>',
			
			"scriptsCSSacima" => "",
			
			"scriptsJSacima" => ''
		);
		
		$this->load->view('site2/v_head', $scripts);
		$this->load->view('site2/v_msg_global', $scripts);
		$this->load->view('site2/v_barra_superior', $scripts);
		$this->load->view('site2/v_links', $scripts);
		$this->load->view('site2/v_carrousel', $scripts);
		$this->load->view('site2/v_cobertura', $scripts);
		$this->load->view('site2/v_plano_residencial', $scripts);
		$this->load->view('site2/v_plano_empresarial', $scripts);
		$this->load->view('site2/v_fale_conosco', $scripts);
		$this->load->view('site2/v_newsletter', $scripts);
		$this->load->view('site2/v_footer', $scripts);
		$this->load->view('site2/v_modals', $scripts);
		$this->load->view('site2/v_scripts', $scripts);

		// INICIO CONTADOR DE VISITAS
		$tabela = "site_visitas_ourilandia";

		$this->contadorvisitas->contarVisitas($tabela);
		// FIM CONTADOR DE VISITAS
	}

	public function v_site2_sobre()
	{
		$dados = array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_seo",
            'coluna_where'  => "id_seo",
            'valor_where'   => (int)1
        );
    
		$SEO = $this->site_admin->m_buscar_1_Item($dados);

		$infosDados = array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_pagina_sobre",
            'coluna_where'  => "id_sobre",
            'valor_where'   => (int)1
		);
		
		$scripts = array(
			
			"titulo" => $SEO->seo_tituloSite,

			"seo" => $SEO,

			"scriptsJS" =>'<script src="/assets/sites/ispmais/js/jquery-3.5.1.min.js"></script>
							<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
							<script src="/assets/sites/ispmais/js/scripts_regioes.js"></script>
							', 
								
			"scriptsCSS" => '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
							<link rel="shortcut icon" href="/assets/sites/ispmais/img/favicon.png" />
							<link href="/assets/sites/ispmais/css/plugins.bundle.css" rel="stylesheet" type="text/css"/>
							<link href="/assets/sites/ispmais/css/style.bundle.css" rel="stylesheet" type="text/css"/>',
			
			"scriptsCSSacima" => "",
			
			"scriptsJSacima" => '',

			"dados_sobre" => $this->site_admin->m_buscar_1_Item($infosDados)
		);
		
		$this->load->view('site2/v_head', $scripts);
		$this->load->view('site2/v_msg_global', $scripts);
		$this->load->view('site2/v_barra_superior', $scripts);
		$this->load->view('site2/v_links', $scripts);
		$this->load->view('site2/v_sobre', $scripts);
		$this->load->view('site2/v_newsletter', $scripts);
		$this->load->view('site2/v_footer', $scripts);
		$this->load->view('site2/v_modals', $scripts);
		$this->load->view('site2/v_scripts', $scripts);

		// INICIO CONTADOR DE VISITAS
		$tabela = "site_visitas_ourilandia";

		$this->contadorvisitas->contarVisitas($tabela);
		// FIM CONTADOR DE VISITAS	
	}

	public function v_site3()
	{
		$scripts = array(
			
			"titulo" => "Internet Banda Larga | ISP Mais Telecom",

			"scriptsJS" =>'<script src="/assets/sites/ispmais/js/jquery-3.5.1.min.js"></script>
							<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
							<script src="/assets/sites/ispmais/js/scripts_regioes.js"></script>
							', 
								
			"scriptsCSS" => '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
							<link rel="shortcut icon" href="/assets/sites/ispmais/img/favicon.png" />
							<link href="/assets/sites/ispmais/css/plugins.bundle.css" rel="stylesheet" type="text/css"/>
							<link href="/assets/sites/ispmais/css/style.bundle.css" rel="stylesheet" type="text/css"/>',
			
			"scriptsCSSacima" => "",
			
			"scriptsJSacima" => ''
		);
		
		$this->load->view('v_home', $scripts);
	}
	
}
