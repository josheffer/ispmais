<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site_admin_ourilandia extends MX_Controller  {

    function __construct()
    {
        parent::__construct();

        $this->load->model("landingpage/Landingpage_model", "landingpage");
        $this->load->model("landingpage/Templatelandingpage_model", "template");
        $this->load->model("landingpage/Modelos_model", "modelos");
        $this->load->model("permissoes/Permissoes_model", "permissoes");
        $this->load->model("Site/Site_model", "site");
        $this->load->model("Site/Site_admin_model", "site_admin");
        $this->load->model("Site/Site_admin_model_ourilandia", "m_ourilandia");
        $this->load->library('permitiracesso');
        $this->load->library('conversores');
        $this->load->library('uploadarquivos');
        $this->load->library('datas');

        $this->load->model("empresa/Empresa_model", "empresa");

        $this->site = $this->load->database('site', TRUE);
        
    }
    
    //  =================
    // MÉTODOS VIEWS     |
    // ==================

    public function v_ourilandia()
    {
        $this->permitiracesso->verifica_sessao();
        
        $view = "admin/v_ourilandia";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);
        
        $scripts = array(

            "titulo" => "Configurações Ourilândia do Norte | Solid System",

            "scriptsJS" => "<script src='/assets/libs/jquery-mask-plugin/jquery.mask.min.js'></script>
                            <script src='/assets/libs/form-masks.init.js'></script>
                            <script src='/assets/painel/js/vendor/site_admin_site_ourilandia.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view("admin/v_ourilandia", $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }
    
    public function v_ourilandia_ligacoes_pendentes()
    {

        $this->permitiracesso->verifica_sessao();
        
        $view = "admin/v_ourilandia_ligacoes_pendentes";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);
        
        $scripts = array(

            "titulo" => "Solicitações de ligações Ourilândia do Norte | Solid System",

            "scriptsJS" => "<script src='/assets/libs/jquery-mask-plugin/jquery.mask.min.js'></script>
                            <script src='/assets/libs/form-masks.init.js'></script>
                            <script src='/assets/libs/moment.js'></script>
                            
                            <script src='/assets/painel/js/vendor/site_admin_site_ourilandia_ligacoes_pendentes.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>

                            <link href='/assets/libs/datatables/dataTables.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/responsive.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/buttons.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/select.bootstrap4.css' rel='stylesheet' type='text/css' />
                            
                            ",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view($view, $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }

    public function v_ourilandia_ligacoes_efetuadas()
    {
        $this->permitiracesso->verifica_sessao();
        
        $view = "admin/v_ourilandia_ligacoes_efetuadas";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);
        
        $scripts = array(

            "titulo" => "Solicitações Efetuadas Ourilândia do Norte | Solid System",

            "scriptsJS" => "<script src='/assets/libs/jquery-mask-plugin/jquery.mask.min.js'></script>
                            <script src='/assets/libs/form-masks.init.js'></script>
                            <script src='/assets/libs/moment.js'></script>
                            
                            <script src='/assets/painel/js/vendor/site_admin_site_ourilandia_ligacoes_efetuadas.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>

                            <link href='/assets/libs/datatables/dataTables.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/responsive.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/buttons.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/select.bootstrap4.css' rel='stylesheet' type='text/css' />
                            
                            ",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view("admin/v_ourilandia_ligacoes_efetuadas", $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }

    public function v_ourilandia_solicitacoesResidencias_pendentes()
    {
        $this->permitiracesso->verifica_sessao();
        
        $view = "admin/v_ourilandia_contratacoesResidenciais_pendentes";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);
        
        $scripts = array(

            "titulo" => "Solicitações Pendentes de contratações Residênciais em Ourilândia do Norte | Solid System",

            "scriptsJS" => "<script src='/assets/libs/jquery-mask-plugin/jquery.mask.min.js'></script>
                            <script src='/assets/libs/form-masks.init.js'></script>
                            <script src='/assets/libs/moment.js'></script>
                            
                            <script src='/assets/painel/js/vendor/site_admin_site_ourilandia_contratacoesResidenciais_pendentes.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>

                            <link href='/assets/libs/datatables/dataTables.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/responsive.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/buttons.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/select.bootstrap4.css' rel='stylesheet' type='text/css' />
                            
                            ",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view("admin/v_ourilandia_contratacoesResidenciais_pendentes", $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
        
    }

    public function v_ourilandia_solicitacoesResidencias_efetuadas()
    {
        $this->permitiracesso->verifica_sessao();
        
        $view = "admin/v_ourilandia_contratacoesResidenciais_efetuadas";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);
        
        $scripts = array(
            
            "titulo" => "Solicitações Efetuadas de contratações Residênciais em Ourilândia do Norte | Solid System",

            "scriptsJS" => "<script src='/assets/libs/jquery-mask-plugin/jquery.mask.min.js'></script>
                            <script src='/assets/libs/form-masks.init.js'></script>
                            <script src='/assets/libs/moment.js'></script>
                            
                            <script src='/assets/painel/js/vendor/site_admin_site_ourilandia_contratacoesResidenciais_efetuadas.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>

                            <link href='/assets/libs/datatables/dataTables.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/responsive.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/buttons.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/select.bootstrap4.css' rel='stylesheet' type='text/css' />
                            
                            ",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view("admin/v_ourilandia_contratacoesResidenciais_efetuadas", $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
        
    }
    
    public function v_ourilandia_solicitacoesEmpresariais_pendentes()
    {
        $this->permitiracesso->verifica_sessao();
        
        $view = "admin/v_ourilandia_contratacoesEmpresariais_pendentes";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);
        
        $scripts = array(

            "titulo" => "Solicitações Pendentes de contratações Empresariais em Ourilândia do Norte | Solid System",

            "scriptsJS" => "<script src='/assets/libs/jquery-mask-plugin/jquery.mask.min.js'></script>
                            <script src='/assets/libs/form-masks.init.js'></script>
                            <script src='/assets/libs/moment.js'></script>
                            
                            <script src='/assets/painel/js/vendor/site_admin_site_ourilandia_contratacoesEmpresariais_pendentes.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>

                            <link href='/assets/libs/datatables/dataTables.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/responsive.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/buttons.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/select.bootstrap4.css' rel='stylesheet' type='text/css' />
                            
                            ",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view("admin/v_ourilandia_contratacoesEmpresariais_pendentes", $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
        
    }
    
    public function v_ourilandia_solicitacoesEmpresariais_efetuadas()
    {
        $this->permitiracesso->verifica_sessao();
        
        $view = "admin/v_ourilandia_contratacoesEmpresariais_efetuadas";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);
        
        $scripts = array(
            
            "titulo" => "Solicitações Efetuadas de contratações Empresariais em Ourilândia do Norte | Solid System",

            "scriptsJS" => "<script src='/assets/libs/jquery-mask-plugin/jquery.mask.min.js'></script>
                            <script src='/assets/libs/form-masks.init.js'></script>
                            <script src='/assets/libs/moment.js'></script>
                            
                            <script src='/assets/painel/js/vendor/site_admin_site_ourilandia_contratacoesEmpresariais_efetuadas.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>

                            <link href='/assets/libs/datatables/dataTables.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/responsive.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/buttons.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/select.bootstrap4.css' rel='stylesheet' type='text/css' />
                            
                            ",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view("admin/v_ourilandia_contratacoesEmpresariais_efetuadas", $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
        
    }
    
    public function v_ourilandia_newsletter()
    {
        $this->permitiracesso->verifica_sessao();
        
        $view = "admin/v_ourilandia_newsletter";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);
        
        $scripts = array(
            
            "titulo" => "Solicitações Efetuadas de contratações Empresariais em Ourilândia do Norte | Solid System",

            "scriptsJS" => "<script src='/assets/libs/moment.js'></script>
            <script src='/assets/js/pagination.js'></script>
                            <script src='/assets/painel/js/vendor/site_admin_site_ourilandia_newsletter.js'></script>
                            ",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>

                            <link href='/assets/libs/datatables/dataTables.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/responsive.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/buttons.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/select.bootstrap4.css' rel='stylesheet' type='text/css' />
                            
                            ",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view("admin/v_ourilandia_newsletter", $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
        
    }
    
    public function v_ourilandia_pagina_sobre()
    {

        $this->permitiracesso->verifica_sessao();
        
        $view = "admin/v_ourilandia_pagina_sobre";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);
        
        $scripts = array(
            
            "titulo" => "Página SOBRE em Ourilândia do Norte | Solid System",

            "scriptsJS" => "<script src='/assets/libs/moment.js'></script>
                            <script src='/assets/painel/js/vendor/site_admin_site_ourilandia_sobre.js'></script>
                            ",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>

                            <link href='/assets/libs/datatables/dataTables.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/responsive.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/buttons.bootstrap4.css' rel='stylesheet' type='text/css' />
                            <link href='/assets/libs/datatables/select.bootstrap4.css' rel='stylesheet' type='text/css' />
                            
                            ",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );

        $dados = array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_pagina_sobre",
            'coluna_where'  => "id_sobre",
            'valor_where'   => (int)1
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view("admin/v_ourilandia_pagina_sobre", $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
        
    }
    
    //  =================
    // MÉTODOS LISTAR    |
    // ==================

    public function c_listarDadosPaginaSobre()
    { 
        $dados = array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_pagina_sobre",
            'coluna_where'  => "id_sobre",
            'valor_where'   => (int)1
        );
    
        $resultado = $this->m_ourilandia->m_buscar_1_Item($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarConfigsSeo()
    { 
        $dados = array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_seo",
            'coluna_where'  => "id_seo",
            'valor_where'   => (int)1
        );
    
        $resultado = $this->m_ourilandia->m_buscar_1_Item($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarAvisoGlobal()
    { 
        $dados = array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_avisoglobal",
            'coluna_where'  => "id_aviso",
            'valor_where'   => (int)1
        );
    
        $resultado = $this->m_ourilandia->m_buscar_1_Item($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarCoberturaAtendimento()
    { 
        $dados = array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_cobertura_tendimento",
            'coluna_where'  => "id_cobertura",
            'valor_where'   => (int)1
        );
    
        $resultado = $this->m_ourilandia->m_buscar_1_Item($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarInfosBarraSuperiorFixa()
    {
        $dados = array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_barra_superior_fixa",
            'coluna_where'  => "id_barraSuperiorFixa",
            'valor_where'   => (int)1
        );
        
        $resultado = $this->m_ourilandia->m_buscar_1_Item($dados);
    
        echo json_encode($resultado);
    }

    public function c_listarLinksBarraSuperiorFixa()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_ourilandia_links_barra_superior_fixa",
            'coluna_where' => "id_links_barraSuperiorFixa",
            'orderBy'   => "DESC"
        );
    
        $resultado = $this->m_ourilandia->m_buscar_tudo_orderBy($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarLinksBarraSuperiorFixaSite()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_ourilandia_links_barra_superior_fixa",
            'coluna_where' => "id_links_barraSuperiorFixa",
            'orderBy'   => "ASC"
        );

        $resultado = $this->m_ourilandia->m_buscar_tudo_orderBy($dados);

        echo json_encode($resultado);
    }
    
    public function c_listarPlanosResidenciaisSite()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_ourilandia_planos_residenciais",
            'coluna_where' => "id_plano_residencial",
            'coluna_where2' => "ativo_plano_residencial",
            'valor_where' => "1",
            'orderBy'   => "ASC"
        );
    
        $resultado = $this->m_ourilandia->m_buscar_tudo_where_orderBy($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarPlanosEmpresariaisSite()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_ourilandia_planos_empresariais",
            'coluna_where' => "id_plano_empresarial",
            'coluna_where2' => "ativo_plano_empresarial",
            'valor_where' => "1",
            'orderBy'   => "ASC"
        );
    
        $resultado = $this->m_ourilandia->m_buscar_tudo_where_orderBy($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarFaleConoscoRegioes()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_regioes",
            'coluna_where' => "id_regioes",
            'coluna_where2' => "ativoSite_regioes",
            'valor_where' => "1",
            'orderBy'   => "ASC"
        );
    
        $resultado = $this->m_ourilandia->m_buscar_tudo_where_orderBy($dados);
    
        echo json_encode($resultado);
    }

    public function c_listarSlidesCarousel()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_ourilandia_carousel",
            'coluna_where' => "id_carousel",
            'orderBy'   => "DESC"
        );
    
        $resultado = $this->m_ourilandia->m_buscar_tudo_orderBy($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarSlidesCarouselSite()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_ourilandia_carousel",
            'coluna_where' => "id_carousel",
            'coluna_where2' => "ativo_carousel",
            'valor_where' => "1",
            'orderBy'   => "ASC"
        );
    
        $resultado = $this->m_ourilandia->m_buscar_tudo_where_orderBy($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarPlanosResidenciais()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_ourilandia_planos_residenciais",
            'coluna_where' => "id_plano_residencial",
            'orderBy'   => "DESC"
        );
    
        $resultado = $this->m_ourilandia->m_buscar_tudo_orderBy($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarPlanosEmpresariais()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_ourilandia_planos_empresariais",
            'coluna_where' => "id_plano_empresarial",
            'orderBy'   => "DESC"
        );
    
        $resultado = $this->m_ourilandia->m_buscar_tudo_orderBy($dados);
    
        echo json_encode($resultado);
    } 
    
    public function c_listarTodosPlanosSite()
    {
        $empresa = array(
            'colunas'   => "id_plano_empresarial as idPlano, nome_plano_empresarial as nomePlano, ativo_plano_empresarial as ativo",
            'tabela'    => "site_ourilandia_planos_empresariais",
            'coluna_where' => "id_plano_empresarial",
            'orderBy'   => "ASC"
        );
    
        $empresariais = $this->m_ourilandia->m_buscar_tudo_orderBy($empresa);

        $residencia = array(
            'colunas'   => "id_plano_residencial as idPlano, nome_plano_residencial as nomePlano, ativo_plano_residencial as ativo",
            'tabela'    => "site_ourilandia_planos_residenciais",
            'coluna_where' => "id_plano_residencial",
            'orderBy'   => "ASC"
        );
    
        $residenciais = $this->m_ourilandia->m_buscar_tudo_orderBy($residencia);
        
        $resultado = array_merge(
            (array)$residenciais, 
            (array)$empresariais
        );
    
        echo json_encode($resultado);
        
    }
    
    public function c_listarligacoes_pendentes()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_ourilandia_solicitacoes_ligacoes",
            'coluna_where' => "id_solicitacoes_ligacoes",
            'coluna_where2' => "status_solicitacoes_ligacoes",
            'valor_where' => array('0', '1'),
            'orderBy'   => "ASC"
        );
    
        $resultado['ligacoes']      = $this->m_ourilandia->m_buscar_tudo_where_in_orderBy($dados);
        $resultado['dadosUsuario']  = $this->session->userdata['id'];
    
        echo json_encode($resultado);
        
    }

    public function c_listarligacoes_efetuadas()
    {
        $dados = array(
            'id'                    => $this->session->userdata['id'],
            'colunas'               => "*",
            'nomeDB1'               => $this->db->database,
            'tabela1'               => "site_ourilandia_solicitacoes_ligacoes",
            'apelido_tabela1'       => "a",
            'nomeDB2'               => $this->site->database,
            'tabela2'               => "usuarios",
            'apelido_tabela2'       => "b",
            'coluna_where1'         => "id_usuario",
            'coluna_where2'         => "operador_solicitacoes_ligacoes",
            'coluna_where3'         => "status_solicitacoes_ligacoes",
            'valor_coluna_where3'   => array('2', '3', '4'),
            'coluna_orderBy'        => "id_solicitacoes_ligacoes",
            'orderBy'               => "DESC"
        );
        
        $resultado['ligacoes']      = $this->m_ourilandia->m_buscar_tudo_inner_join_where_in_orderBy($dados);
        $resultado['dadosUsuario']  = $this->session->userdata['id'];
    
        echo json_encode($resultado);
        
    }
    
    public function c_listarContratacoes_pendentes()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_ourilandia_solicitacoes_planoResidencial",
            'coluna_where' => "id_solicitacoes_planoResidencial",
            'coluna_where2' => "status_solicitacoes_planoResidencial",
            'valor_where' => array('0', '1'),
            'orderBy'   => "ASC"
        );
        
        $resultado['residenciais']      = $this->m_ourilandia->m_buscar_tudo_where_in_orderBy($dados);
        $resultado['dadosUsuario']  = $this->session->userdata['id'];
    
        echo json_encode($resultado);
        
    }
    
    public function c_listarContratacoes_efetuadas()
    {
        $dados = array(
            'id'                    => $this->session->userdata['id'],
            'colunas'               => "*",
            'nomeDB1'               => $this->db->database,
            'tabela1'               => "site_ourilandia_solicitacoes_planoResidencial",
            'apelido_tabela1'       => "a",
            'nomeDB2'               => $this->site->database,
            'tabela2'               => "usuarios",
            'apelido_tabela2'       => "b",
            'coluna_where1'         => "id_usuario",
            'coluna_where2'         => "operador_solicitacoes_planoResidencial",
            'coluna_where3'         => "status_solicitacoes_planoResidencial",
            'valor_coluna_where3'   => array('2', '3', '4'),
            'coluna_orderBy'        => "id_solicitacoes_planoResidencial",
            'orderBy'               => "DESC"
        );
    
        $resultado['residenciais']      = $this->m_ourilandia->m_buscar_tudo_inner_join_where_in_orderBy($dados);
        $resultado['dadosUsuario']  = $this->session->userdata['id'];
    
        echo json_encode($resultado);
        
    }
    
    public function c_listarContratacoesEmpresariais_pendentes()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_ourilandia_solicitacoes_planoEmpresarial",
            'coluna_where' => "id_solicitacoes_planoEmpresarial",
            'coluna_where2' => "status_solicitacoes_planoEmpresarial",
            'valor_where' => array('0', '1'),
            'orderBy'   => "ASC"
        );
    
        $resultado['empresariais']      = $this->m_ourilandia->m_buscar_tudo_where_in_orderBy($dados);
        $resultado['dadosUsuario']  = $this->session->userdata['id'];
    
        echo json_encode($resultado);
        
    }

    public function c_listarContratacoesEmpresariais_efetuadas()
    {
        $dados = array(
            'id'            => $this->session->userdata['id'],
            'colunas'               => "*",
            'nomeDB1'               => $this->db->database,
            'tabela1'               => "site_ourilandia_solicitacoes_planoEmpresarial",
            'apelido_tabela1'       => "a",
            'nomeDB2'               => $this->site->database,
            'tabela2'               => "usuarios",
            'apelido_tabela2'       => "b",
            'coluna_where1'         => "id_usuario",
            'coluna_where2'         => "operador_solicitacoes_planoEmpresarial",
            'coluna_where3'         => "status_solicitacoes_planoEmpresarial",
            'valor_coluna_where3'   => array('2', '3', '4'),
            'coluna_orderBy'        => "id_solicitacoes_planoEmpresarial",
            'orderBy'               => "DESC"
        );
        
        $resultado['empresariais']      = $this->m_ourilandia->m_buscar_tudo_inner_join_where_in_orderBy($dados);
        $resultado['dadosUsuario']  = $this->session->userdata['id'];
    
        echo json_encode($resultado);
        
    }
    
    public function c_listarInfosLigacoes()
    {
        
        $dados = array(
            'id'            => $this->session->userdata['id'],
            'tabela'        => 'site_ourilandia_solicitacoes_ligacoes',
            'where1'        => 'operador_solicitacoes_ligacoes',
            'where2'        => 'status_solicitacoes_ligacoes',
            'whereIn1'      => '0',
            'whereIn2'      => '1',
            'whereIn3'      => '"2", "3", "4"',
            'groupBy'       => 'id_solicitacoes_ligacoes',
        );
        
        $resultado['ligacoes']      = $this->m_ourilandia->m_listarInfosLigacoes($dados);
        
    
        echo json_encode($resultado);
        
    }
    
    public function c_listarInfosResidenciais()
    {
        
        $dados = array(
            'id'            => $this->session->userdata['id'],
            'tabela'        => 'site_ourilandia_solicitacoes_planoResidencial',
            'where1'        => 'operador_solicitacoes_planoResidencial',
            'where2'        => 'status_solicitacoes_planoResidencial',
            'whereIn1'      => '0',
            'whereIn2'      => '1',
            'whereIn3'      => '"2", "3", "4"',
            'groupBy'       => 'id_solicitacoes_planoResidencial',
        );
        
        $resultado['ligacoes']      = $this->m_ourilandia->m_listarInfosLigacoes($dados);
        
    
        echo json_encode($resultado);
        
    }
    
    public function c_listarInfosEmpresariais()
    {
        
        $dados = array(
            'id'            => $this->session->userdata['id'],
            'tabela'        => 'site_ourilandia_solicitacoes_planoEmpresarial',
            'where1'        => 'operador_solicitacoes_planoEmpresarial',
            'where2'        => 'status_solicitacoes_planoEmpresarial',
            'whereIn1'      => '0',
            'whereIn2'      => '1',
            'whereIn3'      => '"2", "3", "4"',
            'groupBy'       => 'id_solicitacoes_planoEmpresarial',
        );
        
        $resultado['ligacoes']      = $this->m_ourilandia->m_listarInfosLigacoes($dados);
        
    
        echo json_encode($resultado);
        
    }
    
    public function c_listarRelatorioChatjsGeral()
    {   
        $dataAnual = date('Y');
        $datas = $this->datas->dataAnual($dataAnual);
        
        $ligacoes = array(
            'tabela'        => 'site_ourilandia_solicitacoes_ligacoes',
            'where1'        => 'operador_solicitacoes_ligacoes',
            'where2'        => 'status_solicitacoes_ligacoes',
            'colunaData'    => 'data_solicitacoes_ligacoes',
            'datas'         => $datas
        );

        $residenciais = array(
            'tabela'        => 'site_ourilandia_solicitacoes_planoResidencial',
            'where1'        => 'operador_solicitacoes_planoResidencial',
            'where2'        => 'status_solicitacoes_planoResidencial',
            'colunaData'    => 'data_solicitacoes_planoResidencial',
            'datas'         => $datas
        );

        $empresariais = array(
            'tabela'        => 'site_ourilandia_solicitacoes_planoEmpresarial',
            'where1'        => 'operador_solicitacoes_planoEmpresarial',
            'where2'        => 'status_solicitacoes_planoEmpresarial',
            'colunaData'    => 'data_solicitacoes_planoEmpresarial',
            'datas'         => $datas
        );
        
        $resultado['ligacoes'] = $this->m_ourilandia->m_listarInfosRelatoriosGeral($ligacoes);
        $resultado['residenciais'] = $this->m_ourilandia->m_listarInfosRelatoriosGeral($residenciais);
        $resultado['empresariais'] = $this->m_ourilandia->m_listarInfosRelatoriosGeral($empresariais);
        
        echo json_encode($resultado);
    }

    public function c_listarRelatorioChatjs()
    {   
        $dataAnual = date('Y');
        $datas = $this->datas->dataAnual($dataAnual);
        
        $dados = array(
            'id'            => $this->session->userdata['id'],
            'tabela'        => 'site_ourilandia_solicitacoes_ligacoes',
            'where1'        => 'operador_solicitacoes_ligacoes',
            'where2'        => 'status_solicitacoes_ligacoes',
            'colunaData'    => 'data_solicitacoes_ligacoes',
            'datas'         => $datas
        );

        $dados1 = array(
            'id'            => $this->session->userdata['id'],
            'tabela'        => 'site_ourilandia_solicitacoes_planoResidencial',
            'where1'        => 'operador_solicitacoes_planoResidencial',
            'where2'        => 'status_solicitacoes_planoResidencial',
            'colunaData'    => 'data_solicitacoes_planoResidencial',
            'datas'         => $datas
        );

        $dados2 = array(
            'id'            => $this->session->userdata['id'],
            'tabela'        => 'site_ourilandia_solicitacoes_planoEmpresarial',
            'where1'        => 'operador_solicitacoes_planoEmpresarial',
            'where2'        => 'status_solicitacoes_planoEmpresarial',
            'colunaData'    => 'data_solicitacoes_planoEmpresarial',
            'datas'         => $datas
        );
        
        $resultado['ligacoes'] = $this->m_ourilandia->m_listarInfosRelatorios($dados);
        $resultado['residenciais'] = $this->m_ourilandia->m_listarInfosRelatorios($dados1);
        $resultado['empresariais'] = $this->m_ourilandia->m_listarInfosRelatorios($dados2);
        
        echo json_encode($resultado);
    }
    
    public function c_listarValores()
    {
        $dadosLigacoesPerdidas = array(
            'id'            => $this->session->userdata['id'],
            'tabela'        => 'site_ourilandia_solicitacoes_ligacoes',
            'colunaSum'     => 'valorPlano_solicitacoes_ligacoes',
            'apelido'       => 'totalLigacoesPerdidas',
            'colunaWhere'   => 'status_solicitacoes_ligacoes',
            'colunaAnd'     => 'operador_solicitacoes_ligacoes',
            'valorWhere'    => '"2", "4"'
        );

        $dadosLigacoesVendas = array(
            'id'            => $this->session->userdata['id'],
            'tabela'        => 'site_ourilandia_solicitacoes_ligacoes',
            'colunaSum'     => 'valorPlano_solicitacoes_ligacoes',
            'apelido'       => 'totalLigacoesVendas',
            'colunaWhere'   => 'status_solicitacoes_ligacoes',
            'colunaAnd'     => 'operador_solicitacoes_ligacoes',
            'valorWhere'    => '"3"'
        );
        
        $dadosResidenciaisPerdidas = array(
            'id'            => $this->session->userdata['id'],
            'tabela'        => 'site_ourilandia_solicitacoes_planoResidencial',
            'colunaSum'     => 'valorPlano_solicitacoes_planoResidencial',
            'apelido'       => 'totalResidenciaisPerdidas',
            'colunaWhere'   => 'status_solicitacoes_planoResidencial',
            'colunaAnd'     => 'operador_solicitacoes_planoResidencial',
            'valorWhere'    => '"2", "4"'
        );

        $dadosResidenciaisVendas = array(
            'id'            => $this->session->userdata['id'],
            'tabela'        => 'site_ourilandia_solicitacoes_planoResidencial',
            'colunaSum'     => 'valorPlano_solicitacoes_planoResidencial',
            'apelido'       => 'totalResidenciaisVendas',
            'colunaWhere'   => 'status_solicitacoes_planoResidencial',
            'colunaAnd'     => 'operador_solicitacoes_planoResidencial',
            'valorWhere'    => '"3"'
        );

        $dadosEmpresariaisPerdidas = array(
            'id'            => $this->session->userdata['id'],
            'tabela'        => 'site_ourilandia_solicitacoes_planoEmpresarial',
            'colunaSum'     => 'valorPlano_solicitacoes_planoEmpresarial',
            'apelido'       => 'totalEmpresariaisPerdidas',
            'colunaWhere'   => 'status_solicitacoes_planoEmpresarial',
            'colunaAnd'     => 'operador_solicitacoes_planoEmpresarial',
            'valorWhere'    => '"2", "4"'
        );

        $dadosEmpresariaisVendas = array(
            'id'            => $this->session->userdata['id'],
            'tabela'        => 'site_ourilandia_solicitacoes_planoEmpresarial',
            'colunaSum'     => 'valorPlano_solicitacoes_planoEmpresarial',
            'apelido'       => 'totalEmpresariaisVendas',
            'colunaWhere'   => 'status_solicitacoes_planoEmpresarial',
            'colunaAnd'     => 'operador_solicitacoes_planoEmpresarial',
            'valorWhere'    => '"3"'
        );

        $resultado = $this->m_ourilandia->m_listarValores($dadosLigacoesPerdidas, $dadosLigacoesVendas, $dadosResidenciaisPerdidas, $dadosResidenciaisVendas, $dadosEmpresariaisPerdidas, $dadosEmpresariaisVendas);

        echo json_encode($resultado);
    }
    
    public function c_listarNewsletter()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_ourilandia_newsletter",
            'coluna_where' => "id_newsletter",
            'orderBy'   => "DESC"
        );
    
        $resultado = $this->m_ourilandia->m_buscar_tudo_orderBy($dados);
    
        echo json_encode($resultado);
    }
    
    //  =================
    // MÉTODOS CADASTRAR |
    // ==================

    public function c_cadastrarNovoLinkBarraSupFixa()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $gerarCodigo = (object)array(
            'coluna'       => "id_links_barraSuperiorFixa",
            'tabela'        => "site_ourilandia_links_barra_superior_fixa"
        );
        
        $dados['id_links_barraSuperiorFixa']        = $this->m_ourilandia->m_gerarCodigo($gerarCodigo);
        $dados['nome_links_barraSuperiorFixa']      = $this->input->post('nome_linksBarraSupFixa');
        $dados['link_links_barraSuperiorFixa']      = $this->input->post('link_linksBarraSupFixa');
        $dados['status_links_barraSuperiorFixa']    = '1';

        $id = $dados['id_links_barraSuperiorFixa'];
        
        if(empty($dados['nome_links_barraSuperiorFixa']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        } 

        if(empty($dados['link_links_barraSuperiorFixa']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>LINK</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        $nome = $dados['nome_links_barraSuperiorFixa'];
        $link = $dados['link_links_barraSuperiorFixa'];
        
        $pesquisarNome = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_links_barra_superior_fixa",
            'coluna_where'  => "nome_links_barraSuperiorFixa",
            'valor_where'   => $nome
        );

        $pesquisarLink = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_links_barra_superior_fixa",
            'coluna_where'  => "link_links_barraSuperiorFixa",
            'valor_where'   => $link
        );
        
        $resultNomeLinkExiste   = $this->m_ourilandia->m_buscar_1_Item($pesquisarNome);
        $resultLinkExiste       = $this->m_ourilandia->m_buscar_1_Item($pesquisarLink);
        
        if($resultNomeLinkExiste)
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>NOME</strong> já <strong>EXISTE</strong>!<br>';
            $sinal = true;
        } 
        
        if($resultLinkExiste)
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>LINK</strong> já <strong>EXISTE</strong>!<br>';
            $sinal = true;
        } 
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $tabela = 'site_ourilandia_links_barra_superior_fixa';
        
        $resultado = $this->m_ourilandia->m_criar($dados, $tabela);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Link <strong>'. $dados['nome_links_barraSuperiorFixa'].'</strong> criado com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível criar o link '. $dados['nome_links_barraSuperiorFixa'].', tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }

    public function c_cadastrarNovoPlanoResidencial()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $gerarCodigo = (object)array(
            'coluna'       => "id_plano_residencial",
            'tabela'        => "site_ourilandia_planos_residenciais"
        );

        $valorPlano = str_replace(',', '.', $this->input->post('precoPlanoCadastro'));
        
        $dados['id_plano_residencial']               = $this->m_ourilandia->m_gerarCodigo($gerarCodigo);
        $dados['nome_plano_residencial']             = $this->input->post('nomePlanoCadastro');
        $dados['descricao_plano_residencial']        = $this->input->post('descricaoPlanoCadastro');
        $dados['velocidade_plano_residencial']       = $this->input->post('velocidadePlanoCadastro');
        $dados['tipoVelocidade_plano_residencial']   = $this->input->post('tipoVelocidadePlanoCadastro');
        $dados['preco_plano_residencial']            = $valorPlano;
        $dados['destaque_plano_residencial']         = '0';
        $dados['ativo_plano_residencial']            = '0';
        
        $id = $dados['id_plano_residencial'];
        
        if(empty($dados['nome_plano_residencial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        } 

        if(empty($dados['descricao_plano_residencial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong>DESCRIÇÃO</strong> não pode ser vazia!<br>';
            $sinal = true;
            
        }

        if(empty($dados['preco_plano_residencial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>PREÇO</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['velocidade_plano_residencial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong>VELOCIDADE</strong> não pode ser vazia!<br>';
            $sinal = true;
            
        }
        
        if(empty($dados['tipoVelocidade_plano_residencial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>TIPO</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        $nome = $dados['nome_plano_residencial'];
        
        $pesquisarNome = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_planos_residenciais",
            'coluna_where'  => "nome_plano_residencial",
            'valor_where'   => $nome
        );
        
        $resultNomePlano   = $this->m_ourilandia->m_buscar_1_Item($pesquisarNome);
        
        if($resultNomePlano)
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>NOME</strong> para o plano já <strong>EXISTE</strong>!<br>';
            $sinal = true;
        } 
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        
        $tabela = 'site_ourilandia_planos_residenciais';
        
        $resultado = $this->m_ourilandia->m_criar($dados, $tabela);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Plano residencial <strong>'. $dados['nome_plano_residencial'].'</strong> criado com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível criar o plano residencial'. $dados['nome_plano_residencial'].', tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    public function c_cadastrarNovoPlanoEmpresarial()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $gerarCodigo = (object)array(
            'coluna'       => "id_plano_empresarial",
            'tabela'        => "site_ourilandia_planos_empresariais"
        );
        
        $valorPlano = str_replace(',', '.', $this->input->post('precoPlanoCadastroEmpresarial'));
        
        $dados['id_plano_empresarial']               = $this->m_ourilandia->m_gerarCodigo($gerarCodigo);
        $dados['nome_plano_empresarial']             = $this->input->post('nomePlanoCadastroEmpresarial');
        $dados['descricao_plano_empresarial']        = $this->input->post('descricaoPlanoCadastroEmpresarial');
        $dados['velocidade_plano_empresarial']       = $this->input->post('velocidadePlanoCadastroEmpresarial');
        $dados['tipoVelocidade_plano_empresarial']   = $this->input->post('tipoVelocidadePlanoCadastroEmpresarial');
        $dados['preco_plano_empresarial']            = $valorPlano;
        $dados['destaque_plano_empresarial']         = '0';
        $dados['ativo_plano_empresarial']            = '0';
        
        $id = $dados['id_plano_empresarial'];
        
        if(empty($dados['nome_plano_empresarial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        } 

        if(empty($dados['descricao_plano_empresarial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong>DESCRIÇÃO</strong> não pode ser vazia!<br>';
            $sinal = true;
            
        }

        if(empty($dados['preco_plano_empresarial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>PREÇO</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['velocidade_plano_empresarial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong>VELOCIDADE</strong> não pode ser vazia!<br>';
            $sinal = true;
            
        }
        
        if(empty($dados['tipoVelocidade_plano_empresarial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>TIPO</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        $nome = $dados['nome_plano_empresarial'];
        
        $pesquisarNome = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_planos_empresariais",
            'coluna_where'  => "nome_plano_empresarial",
            'valor_where'   => $nome
        );
        
        $resultNomePlano   = $this->m_ourilandia->m_buscar_1_Item($pesquisarNome);
        
        if($resultNomePlano)
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>NOME</strong> para o plano já <strong>EXISTE</strong>!<br>';
            $sinal = true;
        } 
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        
        $tabela = 'site_ourilandia_planos_empresariais';
        
        $resultado = $this->m_ourilandia->m_criar($dados, $tabela);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Plano empresarial <strong>'. $dados['nome_plano_empresarial'].'</strong> criado com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível criar o plano empresarial'. $dados['nome_plano_empresarial'].', tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    public function c_cadastrarNovoSlide()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $gerarCodigo = (object)array(
            'coluna'    => "id_carousel",
            'tabela'    => "site_ourilandia_carousel"
        );
        
        $dados['id_carousel']           = $this->m_ourilandia->m_gerarCodigo($gerarCodigo);
        $dados['descricao_carousel']    = $this->input->post('descricaoImagemSlide');
        
        if(!empty($_FILES['imagemSlide']['size']))
        {   
            $site = 'ispmais';

            $config['upload_path'] = './assets/sites/'.$site.'/img/banners';
            $config['file_name'] = "img_slide_".rand().".jpg";
            $config['allowed_types'] = '*' ;

            $dados['imagem_carousel'] = $config['file_name'];

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
        }
        
        $dados['ativo_carousel']    = '1';
        
        if(empty($dados['descricao_carousel']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong>DESCRIÇÃO</strong> não pode ser vazia!<br>';
            $sinal = true;
            
        } 

        if(empty($_FILES['imagemSlide']['size']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong>IMAGEM</strong> não pode ser vazia!<br>';
            $sinal = true;
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $tabela = 'site_ourilandia_carousel';
        
        $resultado = $this->m_ourilandia->m_criar($dados, $tabela);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Banner para o slide criado com sucesso!';
            echo json_encode($retorno);

            $this->upload->do_upload('imagemSlide');

        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível criar o banner para o slide, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    // ==================
    // MÉTODOS ATUALIZAR |
    // ==================

    public function c_liberarStatusItemCarouselSlide()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('id_itemCarouselSlide');
        
        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_carousel",
            'coluna_where'  => "id_carousel",
            'valor_where'   => $id
        );

        $resultadoDadoExiste = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if(is_null($resultadoDadoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Item não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_carousel'] = $id;

        }
        
        if($resultadoDadoExiste->ativo_carousel === '1')
        {
           $dados['ativo_carousel'] = '0';

        } else {

            $dados['ativo_carousel'] = '1';
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $dados = (object) $dados;
        
        $infosDB = (object)array(
            'coluna_where'  =>  'id_carousel',
            'valor_where'   =>  $id,
            'tabela'        =>  'site_ourilandia_carousel'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            if($resultadoDadoExiste->ativo_carousel === '1')
            {
                 $retorno['ret'] = true;
                $retorno['msg'] = 'Slide <strong class="text-light text-uppercase"> #'.$resultadoDadoExiste->id_carousel.' oculto</strong> no carousel';
                echo json_encode($retorno);
    
            } else {
                
                $retorno['ret'] = true;
                $retorno['msg'] = 'Slide <strong class="text-light text-uppercase"> #'.$resultadoDadoExiste->id_carousel.' visivel</strong> no carousel';
                echo json_encode($retorno);
                
            }
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Não foi possível atualizar o status do slide';
            echo json_encode($retorno);
        }
    }
    
    public function c_liberarStatusCoberturaAtendimento()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('id_coberturaAtendimento');
        
        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_cobertura_tendimento",
            'coluna_where'  => "id_cobertura",
            'valor_where'   => $id
        );

        $resultadoDadoExiste = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if(is_null($resultadoDadoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Item não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_cobertura'] = $id;

        }
        
        if($resultadoDadoExiste->ativo_cobertura === '1')
        {
           $dados['ativo_cobertura'] = '0';

        } else {

            $dados['ativo_cobertura'] = '1';
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $dados = (object) $dados;
        
        $infosDB = (object)array(
            'coluna_where'  =>  'id_cobertura',
            'valor_where'   =>  $id,
            'tabela'        =>  'site_ourilandia_cobertura_tendimento'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            if($resultadoDadoExiste->ativo_cobertura === '1')
            {
                $retorno['ret'] = true;
                $retorno['msg'] = 'Sessão <strong class="text-uppercase text-light">oculta</strong> no site';
                echo json_encode($retorno);
    
            } else {
                
                $retorno['ret'] = true;
                $retorno['msg'] = 'Sessão <strong class="text-uppercase text-light">visível</strong> no site';
                echo json_encode($retorno);
                
            }
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Não foi possível atualizar o status.';
            echo json_encode($retorno);
        }
    }
    
    public function c_editarItemSlideCarousel()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['id_carousel']           = $this->input->post('editarIdItemSlide');
        $dados['descricao_carousel']    = $this->input->post('descricaoEditarItemCarousel');
        
        $id = $dados['id_carousel'];
        
        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_carousel",
            'coluna_where'  => "id_carousel",
            'valor_where'   => $id
        );

        $resultadoDadoExiste = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if(is_null($resultadoDadoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= '  <strong class="text-uppercase text-light">Item não encontrado</strong> , tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_carousel'] = $id;

            if(empty($dados['descricao_carousel']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'A <strong class="text-uppercase text-light">descrição</strong> não pode ser vazia!<br>';
                $sinal = true;
            }   

            if($_FILES['novaImagemItemCarouselSlide']['error'] == 1)
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Imagem <strong class="text-uppercase text-light">inválida</strong>, procure outra!<br>';
                $sinal = true;
            
            } else {

                if($_FILES['novaImagemItemCarouselSlide']['size'] == 0 )
                {
                    $dados['imagem_carousel'] = $resultadoDadoExiste->imagem_carousel;
                }

                if($_FILES['novaImagemItemCarouselSlide']['size'] != 0)
                {
                    $site = 'ispmais';
                    
                    $imagemPesquisaExiste = 'assets/sites/'.$site.'/img/banners/'.$resultadoDadoExiste->imagem_carousel;
            
                    if (file_exists($imagemPesquisaExiste)) {
                    
                        $config['upload_path']      = './assets/sites/'.$site.'/img/banners';
                        $config['file_name']        = "img_slide_".rand().".jpg";
                        $config['allowed_types']    = '*' ;
                        $config['max_width']        = 1921;
                        $config['max_height']       = 551;

                        $dados['imagem_carousel']   = $config['file_name'];

                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('novaImagemItemCarouselSlide'))
                        {
                            $retorno['ret'] = false;
                            $retorno['msg'].= 'Imagem muito grande, escolha uma imagem até: <strong class="text-light text-uppercase">1920px x 550px</strong> <br>';
                            $sinal = true;

                        } else {

                            unlink('assets/sites/'.$site.'/img/banners/'.$resultadoDadoExiste->imagem_carousel);
                        }

                    } else {

                        $config['upload_path']      = './assets/sites/'.$site.'/img/banners';
                        $config['file_name']        = "img_slide_".rand().".jpg";
                        $config['allowed_types']    = '*' ;
                        $config['max_width']        = 1921;
                        $config['max_height']       = 551;

                        $dados['imagem_carousel']   = $config['file_name'];

                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        
                        if (!$this->upload->do_upload('novaImagemItemCarouselSlide'))
                        {
                            $retorno['ret'] = false;
                            $retorno['msg'].= 'Imagem muito grande, escolha uma imagem até: <strong class="text-light text-uppercase">1920px x 550px</strong> <br>';
                            $sinal = true;
                            
                        } 
                    }
                    
                }

            }

            if($sinal)
            {
                echo json_encode($retorno);
                exit;
            }
            
            $dados = (object) $dados;

            $infosDB = (object)array(
                'coluna_where'  => 'id_carousel',
                'valor_where'   => $id,
                'tabela'        => 'site_ourilandia_carousel'
            );
            
            $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
            
            if($resultado)
            {
                $retorno['ret'] = true;
                $retorno['msg'] = ' Dados da <strong class="text-light text-uppercase">imagem do slide</strong> atualizados com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível atualizar, tente novamente mais tarde!';
                echo json_encode($retorno);
            }

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
    }
    
    public function c_atualizarCoberturaAtendimento()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['titulo_cobertura']          = $this->input->post('tituloCoberturaAtendimento');
        $dados['descricao_cobertura']       = $this->input->post('descricaoCoberturaAtendimento');
        $dados['textoBotao1_cobertura']     = $this->input->post('textoBotao1CoberturaAtendimento');
        $dados['textoBotao2_cobertura']     = $this->input->post('textoBotao2CoberturaAtendimento');
        $dados['linkBotao1_cobertura']      = $this->input->post('LinkBotao1CoberturaAtendimento');

        if(empty($this->input->post('ativoBotao1CoberturaAtendimento'))){$dados['ativoBotao1_cobertura']="0";}else{$dados['ativoBotao1_cobertura']="1";}
        if(empty($this->input->post('ativoBotao2CoberturaAtendimento'))){$dados['ativoBotao2_cobertura']="0";}else{$dados['ativoBotao2_cobertura']="1";}
        
        if(empty($dados['titulo_cobertura']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-light text-uppercase">título</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['descricao_cobertura']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong class="text-light text-uppercase">descrição</strong> não pode ser vazia!<br>';
            $sinal = true;
            
        }

        if(empty($dados['textoBotao1_cobertura']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O texto do <strong class="text-light text-uppercase">botão 1</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['linkBotao1_cobertura']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O link do <strong class="text-light text-uppercase">botão 1</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }
        
        if(empty($dados['textoBotao2_cobertura']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O texto do <strong class="text-light text-uppercase">botão 2</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $dados = (object) $dados;

        $infosDB = (object)array(
            'coluna_where'  => 'id_cobertura',
            'valor_where'   => (int)1,
            'tabela'        => 'site_ourilandia_cobertura_tendimento'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' As informações da sessão <strong class="text-light text-uppercase">cobertura/atendimento</strong> foram atualizadas com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }

    public function c_atualizarAvisoGlobal_ourilandia()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['texto_aviso'] = $this->input->post('textoAvisoGlobal');
        if(empty($this->input->post('statusAvisoGlobalOurilandia'))){$dados['ativo_aviso']="0";}else{$dados['ativo_aviso']="1";}
        
        if(empty($dados['texto_aviso']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">nome</strong> não pode ser vazio';
            $sinal = true;
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $dados = (object) $dados;

        $infosDB = (object)array(
            'coluna_where'  => 'id_aviso',
            'valor_where'   => 1,
            'tabela'        => 'site_ourilandia_avisoglobal'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Mensagem de <strong>AVISO GLOBAL</strong> atualizada com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar a mensagem de <strong>AVISO GLOBAL</strong>, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    public function c_atualizarSEO_ourilandia()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
       
        $id = 1;

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_seo",
            'coluna_where'  => "id_seo",
            'valor_where'   => $id
        );

        $resultadoDadoExiste = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        $dados['id_seo']                    = 1;
        $dados['seo_tituloSite']            = $this->input->post('titulo_seo_ourilandia');
        $dados['seo_nomeSite']              = $this->input->post('nome_seo_ourilandia');
        $dados['seo_corTema']               = $this->input->post('corTema_seo_ourilandia');
        $dados['seo_css']                   = $this->input->post('css_seo_ourilandia');
        $dados['seo_js']                    = $this->input->post('js_seo_ourilandia');
        $dados['seo_js_body']               = $this->input->post('jsBody_seo_ourilandia');
        $dados['seo_keywords']              = $this->input->post('keywords_seo_ourilandia');
        $dados['seo_description1']          = $this->input->post('description1_seo_ourilandia');
        $dados['seo_description2']          = $this->input->post('description2_seo_ourilandia');
        $dados['seo_description3']          = $this->input->post('description3_seo_ourilandia');
        $dados['seo_coordenadasMaps']       = $this->input->post('coordenadas_seo_ourilandia');
        $dados['seo_cidade']                = $this->input->post('cidadeEstado_seo_ourilandia');
        $dados['seo_estado']                = $this->input->post('estadoPais_seo_ourilandia');
        $dados['seo_googleVerificacao']     = $this->input->post('codigoGoogle_seo_ourilandia');
        
        if($_FILES['imagemLogo_seo_ourilandia']['error'] == 1)
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Imagem <strong class="text-uppercase text-light">inválida</strong>, procure outra!<br>';
            $sinal = true;
        
        } else {

            if($_FILES['imagemLogo_seo_ourilandia']['size'] == 0 )
            {
                $dados['seo_imagem'] = $resultadoDadoExiste->seo_imagem;
            }

            if($_FILES['imagemLogo_seo_ourilandia']['size'] != 0)
            {
                $site = 'ispmais';
                
                $imagemPesquisaExiste = 'assets/sites/'.$site.'/img/seo/'.$resultadoDadoExiste->seo_imagem;
        
                if (file_exists($imagemPesquisaExiste)) {
                
                    $config['upload_path']      = './assets/sites/'.$site.'/img/seo';
                    $config['file_name']        = "img_seo_".rand().".png";
                    $config['allowed_types']    = '*' ;
                    $config['max_width']        = 1921;
                    $config['max_height']       = 551;

                    $dados['seo_imagem']   = $config['file_name'];

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('imagemLogo_seo_ourilandia'))
                    {
                        $retorno['ret'] = false;
                        $retorno['msg'].= 'Imagem muito grande, escolha uma imagem até: <strong class="text-light text-uppercase">1920px x 550px</strong> <br>';
                        $sinal = true;

                    } else {

                        unlink('assets/sites/'.$site.'/img/seo/'.$resultadoDadoExiste->seo_imagem);
                    }

                } else {

                    $config['upload_path']      = './assets/sites/'.$site.'/img/seo';
                    $config['file_name']        = "img_seo_".rand().".png";
                    $config['allowed_types']    = '*' ;
                    $config['max_width']        = 1921;
                    $config['max_height']       = 551;

                    $dados['seo_imagem']   = $config['file_name'];

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('imagemLogo_seo_ourilandia'))
                    {
                        $retorno['ret'] = false;
                        $retorno['msg'].= 'Imagem muito grande, escolha uma imagem até: <strong class="text-light text-uppercase">1920px x 550px</strong> <br>';
                        $sinal = true;
                        
                    } 
                }
                
            }

        }

        if(empty($dados['seo_tituloSite']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">título do site</strong> não pode ser vazio';
            $sinal = true;
            
        }
        
        if(empty($dados['seo_nomeSite']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">nome do site</strong> não pode ser vazio';
            $sinal = true;
            
        }

        if(empty($dados['seo_corTema']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong class="text-uppercase">cor do tema</strong> não pode ser vazio';
            $sinal = true;
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $dados = (object) $dados;

        $infosDB = (object)array(
            'coluna_where'  => 'id_seo',
            'valor_where'   => $id,
            'tabela'        => 'site_ourilandia_seo'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados de <strong>SEO</strong> atualizados com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar os dados de <strong>SEO</strong>, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }

    public function c_atualizarBarraSupFixa_ourilandia()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['mensagem_barraSuperiorFixa']    = $this->input->post('mensagem_BarraSupFixa');
        $dados['email_barraSuperiorFixa']       = $this->input->post('email_BarraSupFixa');
        $dados['telefone_barraSuperiorFixa']    = $this->input->post('telefone_BarraSupFixa');
        $dados['facebook_barraSuperiorFixa']    = $this->input->post('facebook_BarraSupFixa');
        $dados['instagram_barraSuperiorFixa']   = $this->input->post('instagram_BarraSupFixa');

        if(empty($this->input->post('ativoFacebook_BarraSupFixa'))){$dados['ativo_facebook_barraSuperiorFixa']="0";}else{$dados['ativo_facebook_barraSuperiorFixa']="1";}
        if(empty($this->input->post('ativoInstagram_BarraSupFixa'))){$dados['ativo_instagram_barraSuperiorFixa']="0";}else{$dados['ativo_instagram_barraSuperiorFixa']="1";}
        if(empty($this->input->post('ativoMinhaConta_BarraSupFixa'))){$dados['ativo_minhaConta_barraSuperiorFixa']="0";}else{$dados['ativo_minhaConta_barraSuperiorFixa']="1";}

        if(empty($dados['mensagem_barraSuperiorFixa']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong class="text-uppercase">mensagem</strong> não pode ser vazia!<br>';
            $sinal = true;
            
        }

        if(empty($dados['email_barraSuperiorFixa']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">e-mail</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['telefone_barraSuperiorFixa']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">telefone</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['facebook_barraSuperiorFixa']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">facebook</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['instagram_barraSuperiorFixa']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">instagram</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $dados = (object) $dados;

        $infosDB = (object)array(
            'coluna_where'  => 'id_barraSuperiorFixa',
            'valor_where'   => 1,
            'tabela'        => 'site_ourilandia_barra_superior_fixa'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Informações da <strong>BARRA SUPERIOR FIXA</strong> atualizadas com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }

    public function c_atualizarLinksBarraSuperiorFixa()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['nome_links_barraSuperiorFixa']      = $this->input->post('alterarNomeLinksBarraSupFixa');
        $dados['link_links_barraSuperiorFixa']      = $this->input->post('alterarLinksBarraSupFixa');
        
        if(empty($dados['nome_links_barraSuperiorFixa']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase text-light">nome</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['link_links_barraSuperiorFixa']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase text-light">link</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $dados = (object) $dados;

        $id = $this->input->post('idLinksBarraSupFixa');

        $infosDB = (object)array(
            'coluna_where'  =>  'id_links_barraSuperiorFixa',
            'valor_where'   =>  $id,
            'tabela'        =>  'site_ourilandia_links_barra_superior_fixa'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Informações do link: <strong class="text-uppercase text-light">'.$dados->nome_links_barraSuperiorFixa.'</strong> atualizadas com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    public function c_liberarBloquearStatusLinksBarraSupFixa()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $id = $this->input->post('id_link');
        
        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_links_barra_superior_fixa",
            'coluna_where'  => "id_links_barraSuperiorFixa",
            'valor_where'   => $id
        );

        $resultLinkExiste   = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if(empty($resultLinkExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Link não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_links_barraSuperiorFixa'] = $id;

        }
        
        if($resultLinkExiste->status_links_barraSuperiorFixa === '1')
        {
           $dados['status_links_barraSuperiorFixa'] = '0';

        } else {

            $dados['status_links_barraSuperiorFixa'] = '1';
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $dados = (object) $dados;
        
        $infosDB = (object)array(
            'coluna_where'  =>  'id_links_barraSuperiorFixa',
            'valor_where'   =>  $id,
            'tabela'        =>  'site_ourilandia_links_barra_superior_fixa'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Status do link: <strong class="text-light">'.$resultLinkExiste->nome_links_barraSuperiorFixa.'</strong> atualizadas com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar o status do link <strong class="text-light">'.$resultLinkExiste->nome_links_barraSuperiorFixa.'</strong>, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    public function c_abrirLinkAba_ourilandia()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('id_link');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_links_barra_superior_fixa",
            'coluna_where'  => "id_links_barraSuperiorFixa",
            'valor_where'   => $id
        );

        $resultLinkExiste   = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if(empty($resultLinkExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Link não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_links_barraSuperiorFixa'] = $id;

        }
        
        if($resultLinkExiste->target_links_barraSuperiorFixa === '1')
        {
           $dados['target_links_barraSuperiorFixa'] = '0';

        } else {

            $dados['target_links_barraSuperiorFixa'] = '1';
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $dados = (object) $dados;
        
        $infosDB = (object)array(
            'coluna_where'  =>  'id_links_barraSuperiorFixa',
            'valor_where'   =>  $id,
            'tabela'        =>  'site_ourilandia_links_barra_superior_fixa'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Link: <strong class="text-light">'.$resultLinkExiste->nome_links_barraSuperiorFixa.'</strong> atualizadas com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar o  link <strong class="text-light">'.$resultLinkExiste->nome_links_barraSuperiorFixa.'</strong>, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    public function c_atualizarPlanoDestaqueResidencial()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('id_plano_residencial');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_planos_residenciais",
            'coluna_where'  => "id_plano_residencial",
            'valor_where'   => $id
        );

        $planoExiste   = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if(empty($planoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Plano não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_plano_residencial'] = $id;

        }
        
        if($planoExiste->destaque_plano_residencial === '1')
        {
           $dados['destaque_plano_residencial'] = '0';

        } else {

            $dados['destaque_plano_residencial'] = '1';
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $dados = (object) $dados;
        
        $infosDB = (object)array(
            'coluna_where'  =>  'id_plano_residencial',
            'valor_where'   =>  $id,
            'valor_zerar'   =>  '0',
            'coluna'        =>  'destaque_plano_residencial',
            'tabela'        =>  'site_ourilandia_planos_residenciais'
        );
        
        $resultado = $this->m_ourilandia->m_atualizarTudo($infosDB);
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Plano: <strong class="text-light">'.$planoExiste->nome_plano_residencial.'</strong> em destaque!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível colocar o plano: <strong class="text-light">'.$planoExiste->nome_plano_residencial.'</strong> em destaque!';
            echo json_encode($retorno);
        }
    }

    public function c_atualizarPlanoStatusResidencial()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('id_plano_residencial');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_planos_residenciais",
            'coluna_where'  => "id_plano_residencial",
            'valor_where'   => $id
        );

        $planoExiste   = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if(empty($planoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Plano não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_plano_residencial'] = $id;

        }
        
        if($planoExiste->ativo_plano_residencial === '1')
        {
           $dados['ativo_plano_residencial'] = '0';

        } else {

            $dados['ativo_plano_residencial'] = '1';
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $dados = (object) $dados;
        
        $infosDB = (object)array(
            'coluna_where'  =>  'id_plano_residencial',
            'valor_where'   =>  $id,
            'tabela'        =>  'site_ourilandia_planos_residenciais'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            if($planoExiste->ativo_plano_residencial === '1')
            {
                $retorno['ret'] = true;
                $retorno['msg'] = 'Plano:  '.$planoExiste->nome_plano_residencial.' <strong class="text-uppercase text-light">oculto</strong> no site';
                echo json_encode($retorno);
    
            } else {
                
                $retorno['ret'] = true;
                $retorno['msg'] = 'Plano: '.$planoExiste->nome_plano_residencial.' <strong class="text-uppercase text-light">visível</strong> no site';
                echo json_encode($retorno);
                
            }
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Não foi possível atualizar o status.';
            echo json_encode($retorno);
        }
    }
    
    public function c_atualizarPlanoResidencial()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $valorPlano = str_replace(',', '.', $this->input->post('alterarPrecoPlanoResidencial'));
        
        $id                                         = $this->input->post('idPlanoResidencial');
        $dados['id_plano_residencial']              = $this->input->post('idPlanoResidencial');
        $dados['nome_plano_residencial']            = $this->input->post('alterarNomePlanoResidencial');
        $dados['descricao_plano_residencial']       = $this->input->post('alterarDescricaoPlanoResidencial');
        $dados['preco_plano_residencial']           = $valorPlano;
        $dados['velocidade_plano_residencial']      = $this->input->post('alterarVelocidadePlanoResidencial');
        $dados['tipoVelocidade_plano_residencial']  = $this->input->post('alterarTipoVelocidadePlanoResidencial');

        if(empty($dados['nome_plano_residencial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">nome</strong> do plano não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['descricao_plano_residencial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong class="text-uppercase">descrição</strong> do plano não pode ser vazia!<br>';
            $sinal = true;
            
        }

        if(empty($dados['preco_plano_residencial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">preço</strong> do plano não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['velocidade_plano_residencial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong class="text-uppercase">velocidade</strong> do plano não pode ser vazia!<br>';
            $sinal = true;
            
        }

        if(empty($dados['tipoVelocidade_plano_residencial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">tipo de velocidade</strong> do plano não pode ser vazia!<br>';
            $sinal = true;
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $dados = (object) $dados;

        $infosDB = (object)array(
            'coluna_where'  => 'id_plano_residencial',
            'valor_where'   => $id,
            'tabela'        => 'site_ourilandia_planos_residenciais'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Informações do <strong class="text-uppercase text-light">'.$dados->nome_plano_residencial.'</strong> atualizadas com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    public function c_atualizarPlanoDestaqueEmpresarial()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('id_plano_Empresarial');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_planos_empresariais",
            'coluna_where'  => "id_plano_empresarial",
            'valor_where'   => $id
        );

        $planoExiste   = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if(empty($planoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Plano não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_plano_empresarial'] = $id;

        }
        
        if($planoExiste->destaque_plano_empresarial === '1')
        {
           $dados['destaque_plano_empresarial'] = '0';

        } else {

            $dados['destaque_plano_empresarial'] = '1';
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $dados = (object) $dados;
        
        $infosDB = (object)array(
            'coluna_where'  =>  'id_plano_empresarial',
            'valor_where'   =>  $id,
            'valor_zerar'   =>  '0',
            'coluna'        =>  'destaque_plano_empresarial',
            'tabela'        =>  'site_ourilandia_planos_empresariais'
        );
        
        $resultado = $this->m_ourilandia->m_atualizarTudo($infosDB);
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Plano: <strong class="text-light">'.$planoExiste->nome_plano_empresarial.'</strong> em destaque!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível colocar o plano: <strong class="text-light">'.$planoExiste->nome_plano_empresarial.'</strong> em destaque!';
            echo json_encode($retorno);
        }
    }
    
    public function c_atualizarStatusPlanoEmpresarial()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('id_plano_empresarial');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_planos_empresariais",
            'coluna_where'  => "id_plano_empresarial",
            'valor_where'   => $id
        );

        $planoExiste   = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if(empty($planoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Plano não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_plano_empresarial'] = $id;

        }
        
        if($planoExiste->ativo_plano_empresarial === '1')
        {
           $dados['ativo_plano_empresarial'] = '0';

        } else {

            $dados['ativo_plano_empresarial'] = '1';
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $dados = (object) $dados;
        
        $infosDB = (object)array(
            'coluna_where'  =>  'id_plano_empresarial',
            'valor_where'   =>  $id,
            'tabela'        =>  'site_ourilandia_planos_empresariais'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            if($planoExiste->ativo_plano_empresarial === '1')
            {
                $retorno['ret'] = true;
                $retorno['msg'] = 'Plano:  '.$planoExiste->nome_plano_empresarial.' <strong class="text-uppercase text-light">oculto</strong> no site';
                echo json_encode($retorno);
    
            } else {
                
                $retorno['ret'] = true;
                $retorno['msg'] = 'Plano: '.$planoExiste->nome_plano_empresarial.' <strong class="text-uppercase text-light">visível</strong> no site';
                echo json_encode($retorno);
                
            }
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Não foi possível atualizar o status.';
            echo json_encode($retorno);
        }
    }
    
    public function c_atualizarPlanoEmpresarial()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $valorPlano = str_replace(',', '.', $this->input->post('alterarPrecoPlanoEmpresarial'));
        
        $dados['id_plano_empresarial']              = $this->input->post('idPlanoEmpresarial');
        $dados['nome_plano_empresarial']            = $this->input->post('alterarNomePlanoEmpresarial');
        $dados['descricao_plano_empresarial']       = $this->input->post('alterarDescricaoPlanoEmpresarial');
        $dados['preco_plano_empresarial']           = $valorPlano;
        $dados['velocidade_plano_empresarial']      = $this->input->post('alterarVelocidadePlanoEmpresarial');
        $dados['tipoVelocidade_plano_empresarial']  = $this->input->post('alterarTipoVelocidadePlanoEmpresarial');

        $id = $dados['id_plano_empresarial'];

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_planos_empresariais",
            'coluna_where'  => "id_plano_empresarial",
            'valor_where'   => $id
        );

        $planoExiste   = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if(empty($planoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Plano não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_plano_empresarial'] = $id;

        }
        
        if(empty($dados['nome_plano_empresarial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">nome</strong> do plano não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['descricao_plano_empresarial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong class="text-uppercase">descrição</strong> do plano não pode ser vazia!<br>';
            $sinal = true;
            
        }

        if(empty($dados['preco_plano_empresarial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">preço</strong> do plano não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['velocidade_plano_empresarial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong class="text-uppercase">velocidade</strong> do plano não pode ser vazia!<br>';
            $sinal = true;
            
        }

        if(empty($dados['tipoVelocidade_plano_empresarial']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">tipo de velocidade</strong> do plano não pode ser vazia!<br>';
            $sinal = true;
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $dados = (object) $dados;

        $infosDB = (object)array(
            'coluna_where'  => 'id_plano_empresarial',
            'valor_where'   => $id,
            'tabela'        => 'site_ourilandia_planos_empresariais'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Informações do <strong class="text-uppercase text-light">'.$dados->nome_plano_empresarial.'</strong> atualizadas com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }

    public function c_recepcionarLigacao()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['id_solicitacoes_ligacoes']          = $this->input->post('idLigacao');
        $dados['operador_solicitacoes_ligacoes']    = $this->input->post('idOperador');
        $dados['status_solicitacoes_ligacoes']      = $this->input->post('statusLigacao');;

        $id = $dados['id_solicitacoes_ligacoes'];

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_solicitacoes_ligacoes",
            'coluna_where'  => "id_solicitacoes_ligacoes",
            'valor_where'   => $id
        );

        $dadoExiste   = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if($dados['status_solicitacoes_ligacoes'] === '1')
        {
            if(!empty($dadoExiste->operador_solicitacoes_ligacoes))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Esta solicitação já foi encaminhada para outra pessoa!<br>';
                $sinal = true;
            }
        }
        
        if(empty($dadoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Solicitação não encontrada, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_solicitacoes_ligacoes'] = $id;

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $dados = (object) $dados;

        $infosDB = (object)array(
            'coluna_where'  => 'id_solicitacoes_ligacoes',
            'valor_where'   => $id,
            'tabela'        => 'site_ourilandia_solicitacoes_ligacoes'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Status da  solicitação de ligação de: <strong class="text-uppercase text-light">'.$dadoExiste->nome_solicitacoes_ligacoes.'</strong> atualizado com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar o status, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    public function c_recepcionarContratarPlanoresidencial()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['id_solicitacoes_planoResidencial']          = $this->input->post('idContratarPlanoResidencial');
        $dados['operador_solicitacoes_planoResidencial']    = $this->input->post('idOperador');
        $dados['status_solicitacoes_planoResidencial']      = $this->input->post('statusContratarPlanoResidencial');;

        $id = $dados['id_solicitacoes_planoResidencial'];

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_solicitacoes_planoResidencial",
            'coluna_where'  => "id_solicitacoes_planoResidencial",
            'valor_where'   => $id
        );

        $dadoExiste   = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if($dados['status_solicitacoes_planoResidencial'] === '1')
        {
            if(!empty($dadoExiste->operador_solicitacoes_planoResidencial))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Esta solicitação já foi encaminhada para outra pessoa!<br>';
                $sinal = true;
            }
        }
        
        if(empty($dadoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Solicitação não encontrada, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_solicitacoes_planoResidencial'] = $id;

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $dados = (object) $dados;

        $infosDB = (object)array(
            'coluna_where'  => 'id_solicitacoes_planoResidencial',
            'valor_where'   => $id,
            'tabela'        => 'site_ourilandia_solicitacoes_planoResidencial'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Status da  solicitação de contratação de: <strong class="text-uppercase text-light">'.$dadoExiste->nome_solicitacoes_planoResidencial.'</strong> atualizado com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar o status, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    public function c_recepcionarContratacaoEmpresarial()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['id_solicitacoes_planoEmpresarial']          = $this->input->post('idContratarPlanoEmpresarial');
        $dados['operador_solicitacoes_planoEmpresarial']    = $this->input->post('idOperador');
        $dados['status_solicitacoes_planoEmpresarial']      = $this->input->post('statusContratarPlanoEmpresarial');;

        $id = $dados['id_solicitacoes_planoEmpresarial'];

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_solicitacoes_planoEmpresarial",
            'coluna_where'  => "id_solicitacoes_planoEmpresarial",
            'valor_where'   => $id
        );

        $dadoExiste   = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if($dados['status_solicitacoes_planoEmpresarial'] === '1')
        {
            if(!empty($dadoExiste->operador_solicitacoes_planoResidencial))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Esta solicitação já foi encaminhada para outra pessoa!<br>';
                $sinal = true;
            }
        }
        
        if(empty($dadoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Solicitação não encontrada, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_solicitacoes_planoEmpresarial'] = $id;

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $dados = (object) $dados;

        $infosDB = (object)array(
            'coluna_where'  => 'id_solicitacoes_planoEmpresarial',
            'valor_where'   => $id,
            'tabela'        => 'site_ourilandia_solicitacoes_planoEmpresarial'
        );
        
        $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Status da  solicitação de contratação de: <strong class="text-uppercase text-light">'.$dadoExiste->nomeFantasia_planoEmpresarial.'</strong> atualizado com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar o status, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    public function c_atualizar_dadosSobre()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = 1;

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_pagina_sobre",
            'coluna_where'  => "id_sobre",
            'valor_where'   => $id
        );

        $resultadoDadoExiste = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if(!empty($resultadoDadoExiste))
        {
            $dados['id_sobre']              = $resultadoDadoExiste->id_sobre;
            $dados['titulo_sobre']          = $this->input->post('titulo_paginaSobre');
            $dados['descricao_sobre']       = $this->input->post('descricao_paginaSobre');
            
            if($_FILES['imagem_paginaSobre']['size'] === 0 )
            {
                $dados['imagem_sobre'] = $resultadoDadoExiste->imagem_sobre;
            }

            if(!empty($_FILES['imagem_paginaSobre']['size']))
            {

                if($resultadoDadoExiste->imagem_sobre === 'logo_default.png')
                {
                    $config['upload_path']      = './assets/sites/ispmais/img/sobre';
                    $config['file_name']        = "img_sobre_".rand().".jpg";
                    $config['allowed_types']    = '*' ;

                    $dados['imagem_sobre'] = $config['file_name'];

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('imagem_paginaSobre'))
                    {
                        $retorno['ret'] = false;
                        $retorno['msg'].= 'Erro ao salvar imagem<br>';
                        $sinal = true;

                    } 

                } else {

                    $imagemExiste = 'assets/sites/ispmais/img/sobre/'.$resultadoDadoExiste->imagem_sobre;
                
                    if (file_exists($imagemExiste)) {
                        
                        $config['upload_path']      = './assets/sites/ispmais/img/sobre';
                        $config['file_name']        = "img_sobre_".rand().".jpg";
                        $config['allowed_types']    = '*' ;

                        $dados['imagem_sobre'] = $config['file_name'];

                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                    if (!$this->upload->do_upload('imagem_paginaSobre'))
                        {
                            $retorno['ret'] = false;
                            $retorno['msg'].= 'Erro ao salvar imagem<br>';
                            $sinal = true;

                        } else {
                            
                            unlink('assets/sites/ispmais/img/sobre/'.$resultadoDadoExiste->imagem_sobre);
                        }

                    } else {

                        $config['upload_path']      = './assets/sites/ispmais/img/sobre';
                        $config['file_name']        = "img_sobre_".rand().".jpg";
                        $config['allowed_types']    = '*' ;

                        $dados['imagem_sobre'] = $config['file_name'];

                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        
                        if (!$this->upload->do_upload('imagem_paginaSobre'))
                        {
                            $retorno['ret'] = false;
                            $retorno['msg'].= 'Erro ao salvar imagem<br>';
                            $sinal = true;

                        } 

                    }

                }
                
            }
            
            $dados['texto_sobre']           = $this->input->post('textoSobre_paginaSobre');
            $dados['missao_sobre']          = $this->input->post('textoMissao_paginaSobre');
            $dados['visao_sobre']           = $this->input->post('textoVisao_paginaSobre');
            $dados['valores_sobre']         = $this->input->post('textoValores_paginaSobre');
            $dados['nomeEmpresa_sobre']     = $this->input->post('nomeEmpresa_paginaSobre');
                                   
            if($sinal)
            {
                echo json_encode($retorno);
                exit;
            }
            
            $dados = (object) $dados;

            $infosDB = (object)array(
                'coluna_where'  => 'id_sobre',
                'valor_where'   => $id,
                'tabela'        => 'site_ourilandia_pagina_sobre'
            );
            
            $resultado = $this->m_ourilandia->m_atualizar($dados, $infosDB);
            
            if($resultado)
            {
                $retorno['ret'] = true;
                $retorno['msg'] = ' Dados da página <strong>SOBRE</strong> atualizados com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível atualizar os dados da página <strong>SOBRE</strong>, tente novamente mais tarde!';
                echo json_encode($retorno);
            }

        } else {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Não foi possível encontrar os dados para atualização!!';
            echo json_encode($retorno);
        }
    }

    // =================
    // MÉTODOS EXCLUIR | 
    // =================

    public function c_excluirItemSlideCarousel()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('idExcluirBarraSuperiorFixa');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_carousel",
            'coluna_where'  => "id_carousel",
            'valor_where'   => $id
        );

        $resultPesquisa   = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if(empty($resultPesquisa))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Slide não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else { 

            $infosDB = (object)array(
                'tabela'        => "site_ourilandia_carousel",
                'coluna_where'  => "id_carousel",
                'valor_where'   => $id
            );
            
            $resultado = $this->m_ourilandia->m_excluir($infosDB);
        
            if($resultado)
            {   
                $site = "ispmais";
                
                unlink('assets/sites/'.$site.'/img/banners/'.$resultPesquisa->imagem_carousel);

                $retorno['ret'] = true;
                $retorno['msg'] = ' Imagem: <strong>#'.$resultPesquisa->id_carousel.'</strong> excluída com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível excluir a imagem '.$resultPesquisa->id_carousel.', tente novamente mais tarde!';
                echo json_encode($retorno);
            }

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    
    }
    
    public function c_excluirLinkBarraSuperiorFixa()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('idExcluirBarraSuperiorFixa');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_links_barra_superior_fixa",
            'coluna_where'  => "id_links_barraSuperiorFixa",
            'valor_where'   => $id
        );

        $resultLinkExiste   = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if(empty($resultLinkExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Link não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else { 

            $infosDB = (object)array(
                'tabela'        => "site_ourilandia_links_barra_superior_fixa",
                'coluna_where'  => "id_links_barraSuperiorFixa",
                'valor_where'   => $id
            );
            
            $resultado = $this->m_ourilandia->m_excluir($infosDB);
        
            if($resultado)
            {
                $retorno['ret'] = true;
                $retorno['msg'] = ' Link: <strong>'.$resultLinkExiste->nome_links_barraSuperiorFixa.'</strong> excluída com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível excluir o link '.$resultLinkExiste->nome_links_barraSuperiorFixa.', tente novamente mais tarde!';
                echo json_encode($retorno);
            }

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }
    
    public function c_excluirPlanoResidencial()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('idExluirPlanoResidencial');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_planos_residenciais",
            'coluna_where'  => "id_plano_residencial",
            'valor_where'   => $id
        );

        $resultPesquisa   = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if(empty($resultPesquisa))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Plano não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else { 

            $infosDB = (object)array(
                'tabela'        => "site_ourilandia_planos_residenciais",
                'coluna_where'  => "id_plano_residencial",
                'valor_where'   => $id
            );
            
            $resultado = $this->m_ourilandia->m_excluir($infosDB);
        
            if($resultado)
            {
                $retorno['ret'] = true;
                $retorno['msg'] = ' Plano: <strong>'.$resultPesquisa->nome_plano_residencial.'</strong> excluído com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível excluir o plano '.$resultPesquisa->nome_plano_residencial.', tente novamente mais tarde!';
                echo json_encode($retorno);
            }

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }
    
    public function c_excluirPlanoEmpresarial()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('idExluirPlanoEmpresarial');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_planos_empresariais",
            'coluna_where'  => "id_plano_empresarial",
            'valor_where'   => $id
        );
        
        $resultPesquisa   = $this->m_ourilandia->m_buscar_1_Item($pesquisarItem);
        
        if(empty($resultPesquisa))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Plano não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else { 

            $infosDB = (object)array(
                'tabela'        => "site_ourilandia_planos_empresariais",
                'coluna_where'  => "id_plano_empresarial",
                'valor_where'   => $id
            );
            
            $resultado = $this->m_ourilandia->m_excluir($infosDB);
        
            if($resultado)
            {
                $retorno['ret'] = true;
                $retorno['msg'] = ' Plano: <strong>'.$resultPesquisa->nome_plano_empresarial.'</strong> excluído com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível excluir o plano '.$resultPesquisa->nome_plano_empresarial.', tente novamente mais tarde!';
                echo json_encode($retorno);
            }

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    
}