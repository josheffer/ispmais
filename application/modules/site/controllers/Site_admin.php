<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site_admin extends MX_Controller  {

	function __construct()
    {
        parent::__construct();

        $this->load->model("landingpage/Landingpage_model", "landingpage");
        $this->load->model("landingpage/Templatelandingpage_model", "template");
        $this->load->model("landingpage/Modelos_model", "modelos");
        $this->load->model("permissoes/Permissoes_model", "permissoes");
        $this->load->model("Site/Site_model", "site");
        $this->load->model("Site/Site_admin_model", "site_admin");
        $this->load->library('permitiracesso');
        $this->load->library('conversores');
        $this->load->library('uploadarquivos');

        $this->load->model("empresa/Empresa_model", "empresa");
        
    }
	
	public function index()
    {
        $this->permitiracesso->verifica_sessao();
        
        $view = "admin/v_home_admin";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);

        $nivelAcesso_usuario = $permitirAcessoPagina['dados']['dados']['nivelAcesso_usuario'];

        $permitirAcessoPagina['dados']['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $scripts = array(

            "titulo" => "Gerenciar regiões | Solid System",

            "scriptsJS" => "<script src='/assets/libs/jquery-mask-plugin/jquery.mask.min.js'></script>
                            <script src='/assets/libs/form-masks.init.js'></script>
                            <script src='/assets/painel/js/vendor/site_admin_regioes.js'></script>
                            <script src='/assets/painel/js/vendor/site_admin_SEO_intro.js'></script>
                            ",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view("admin/v_home_admin", $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
    }
    
    public function v_bio()
    {
        $this->permitiracesso->verifica_sessao();
        
        $view = "admin/v_bio_admin";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);

        $nivelAcesso_usuario = $permitirAcessoPagina['dados']['dados']['nivelAcesso_usuario'];

        $permitirAcessoPagina['dados']['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
                
        $scripts = array(

            "titulo" => "Gerenciar BIO | Solid System",

            "scriptsJS" => "<script src='/assets/painel/js/vendor/site_admin_bio.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view("admin/v_bio_admin", $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
    }

    public function c_listarBioInfos()
    {
        $dados = array(
            'colunas'       => "*",
            'tabela'        => "site_bio_infos",
            'coluna_where'  => "id_infos_bio",
            'valor_where'   => (int)1
        );
    
		$resultado = $this->site_admin->m_buscar_1_Item($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarCoresEstilo()
    {
        $dados = array(
            'colunas'       => "*",
            'tabela'        => "site_bio_cores_estilos",
            'coluna_where'  => "id_cores_estilos",
            'valor_where'   => (int)1
        );
    
		$resultado = $this->site_admin->m_buscar_1_Item($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarSites()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_bio_sites",
            'coluna_where' => "id_sites_bio",
            'orderBy'   => "DESC"
        );
    
        $resultado = $this->site_admin->m_buscar_tudo_orderBy($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarCanaisAtendimento()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_bio_canais_atendimento",
            'coluna_where' => "id_canal_atendimento",
            'orderBy'   => "DESC"
        );
    
        $resultado = $this->site_admin->m_buscar_tudo_orderBy($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarRedesSociaisBio()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_bio_redes_sociais",
            'coluna_where' => "id_redes_sociais",
            'orderBy'   => "DESC"
        );
    
        $resultado = $this->site_admin->m_buscar_tudo_orderBy($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarRegioes()
    {
        $dados = array(
            'colunas'   => "*",
            'tabela'    => "site_regioes",
            'coluna_where' => "id_regioes",
            'orderBy'   => "DESC"
        );
    
        $resultado = $this->site_admin->m_buscar_tudo_orderBy($dados);
    
        echo json_encode($resultado);
    }
    
    public function c_listarConfigsSeo_intro()
    {
        $dados = array(
            'colunas'       => "*",
            'tabela'        => "site_intro_seo",
            'coluna_where'  => "id_intro_seo",
            'valor_where'   => (int)1
        );
    
		$resultado = $this->site_admin->m_buscar_1_Item($dados);
    
        echo json_encode($resultado);
    }

    public function c_cadastrarSiteBio()
    {
        
        $retorno['msg'] = "";
        $sinal = false;

        $gerarCodigo = (object)array(
            'coluna'       => "id_sites_bio",
            'tabela'        => "site_bio_sites"
        );
        
        $dados['id_sites_bio']      = $this->site_admin->m_gerarCodigo($gerarCodigo);
        $dados['nome_sites_bio']    = $this->input->post('nomeSite_bio');
        $dados['url_sites_bio']     = $this->input->post('urlSite_bio');
        $dados['status_sites_bio']  = '0';
        
        
        if(empty($dados['nome_sites_bio']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        } 

        if(empty($dados['url_sites_bio']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong>URL</strong> não pode ser vazia!<br>';
            $sinal = true;
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $tabela = 'site_bio_sites';
        
        $resultado = $this->site_admin->m_criar($dados, $tabela);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Site <strong>'. $dados['nome_sites_bio'].'</strong> criado com sucesso! <br> <span class="text-black-50">Obs. Mude o status do botão na lista abaixo para mostrar o botão na página da bio.</span>';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível criar o botão do site, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    public function c_cadastrarCanalAtendimentoBio()
    {
        
        $retorno['msg'] = "";
        $sinal = false;
        
        $gerarCodigo = (object)array(
            'coluna'       => "id_canal_atendimento",
            'tabela'        => "site_bio_canais_atendimento"
        );
        
        $dados['id_canal_atendimento']      = $this->site_admin->m_gerarCodigo($gerarCodigo);
        $dados['nome_canal_atendimento']    = $this->input->post('nome_canalAtendimento');
        $dados['url_canal_atendimento']     = $this->input->post('url_canalAtendimento');
        
        switch ($this->input->post('icone_canalAtendimento')) {
            case 1:
                $dados['icone_canal_atendimento'] = 'mdi mdi-whatsapp';
                $dados['tipo_canal_atendimento'] = '1';
                break;
            case 2:
                $dados['icone_canal_atendimento'] = 'mdi mdi-instagram';
                $dados['tipo_canal_atendimento'] = '2';
                break;
            case 3:
                $dados['icone_canal_atendimento'] = 'mdi mdi-facebook-messenger';
                $dados['tipo_canal_atendimento'] = '3';
                break;

            case 4:
                $dados['icone_canal_atendimento'] = 'mdi mdi-telegram';
                $dados['tipo_canal_atendimento'] = '4';
                break;

            case 5:
                $dados['icone_canal_atendimento'] = 'mdi mdi-email';
                $dados['tipo_canal_atendimento'] = '5';
                break;

            case 6:
                $dados['icone_canal_atendimento'] = 'mdi mdi-phone-classic';
                $dados['tipo_canal_atendimento'] = '6';
                break;    

            default:
                $dados['icone_canal_atendimento'] = '';
                $dados['tipo_canal_atendimento'] = '';            
        } 

        $dados['ativo_canal_atendimento']   = '0';
        
        
        if(empty($dados['nome_canal_atendimento']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        } 

        if(empty($dados['url_canal_atendimento']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong>URL</strong> não pode ser vazia!<br>';
            $sinal = true;
            
        }

        if(empty($dados['icone_canal_atendimento']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Escolha o  <strong>ICONE</strong> para o canal de atendimento!<br>';
            $sinal = true;
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $tabela = 'site_bio_canais_atendimento';
        
        $resultado = $this->site_admin->m_criar($dados, $tabela);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Canal de atendimento <strong>'. $dados['nome_canal_atendimento'].'</strong> criado com sucesso! <br> <span class="text-black-50">Obs. Mude o status do botão na lista abaixo para mostrar o botão na página da bio.</span>';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível criar o botão do site, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    public function c_cadastrarRedeSocialBio()
    {
        
        $retorno['msg'] = "";
        $sinal = false;
        
        $gerarCodigo = (object)array(
            'coluna'       => "id_redes_sociais",
            'tabela'        => "site_bio_redes_sociais"
        );
        
        $dados['id_redes_sociais']      = $this->site_admin->m_gerarCodigo($gerarCodigo);
        $dados['nome_redes_sociais']    = $this->input->post('cadastrarNomeRedeSocial');
        $dados['url_redes_sociais']     = $this->input->post('cadastrarUrlRedeSocial');
        
        if(empty($this->input->post('cadastrarIconeRedeSocial')))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' Escolha o ícone para a rede social!<br>';
            $sinal = true;
            
        } else {
            
            switch ($this->input->post('cadastrarIconeRedeSocial')) {
                case 1:
                    $dados['icone_redes_sociais'] = 'mdi mdi-instagram';
                    break;
                case 2:
                    $dados['icone_redes_sociais'] = 'mdi mdi-facebook-box';
                    break;
                case 3:
                    $dados['icone_redes_sociais'] = 'mdi mdi-youtube';
                    break;

                case 4:
                    $dados['icone_redes_sociais'] = 'mdi mdi-twitter';
                    break;

                case 5:
                    $dados['icone_redes_sociais'] = 'mdi mdi-pinterest';
                    break;           
            } 

        }

        $dados['ativo_redes_sociais']   = '0';
        
        if(empty($dados['nome_redes_sociais']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        } 

        if(empty($dados['url_redes_sociais']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong>URL</strong> não pode ser vazia!<br>';
            $sinal = true;
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $tabela = 'site_bio_redes_sociais';
        
        $resultado = $this->site_admin->m_criar($dados, $tabela);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Rede social <strong>'. $dados['nome_redes_sociais'].'</strong> criada com sucesso! <br> <span class="text-black-50">Obs. Mude o status do botão na lista abaixo para mostrar o botão na página da bio.</span>';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível criar o botão do site, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    public function c_cadastrarNovaRegiao_site()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $gerarCodigo = (object)array(
            'coluna'       => "id_regioes",
            'tabela'        => "site_regioes"
        );
        
        $dados['id_regioes']                    = $this->site_admin->m_gerarCodigo($gerarCodigo);
        $dados['nome_regioes']                  = $this->input->post('nomeRegiao');
        $dados['link_regioes']                  = $this->input->post('linkPaginaRegiao');
        $dados['status_regioes'] = '1';
        $dados['endereco_regioes']              = $this->input->post('enderecoRegiao');
        $dados['email_regioes']                 = $this->input->post('emailRegiao');
        $dados['foneFixoTexto_regioes']         = $this->input->post('foneFixoRegiao');
        $dados['foneWhatsappTexto_regioes']     = $this->input->post('whatsappRegiao');
        $dados['linkWhatsapp_regioes']          = $this->input->post('linkWhatsappRegiao');
        
        if(empty($dados['nome_regioes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>NOME DA REGIÃO</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        } 

        if(empty($dados['link_regioes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>LINK DA REGIÃO</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['endereco_regioes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>ENDEREÇO REGIÃO</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['email_regioes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>E-MAIL REGIÃO</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['foneFixoTexto_regioes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>TELEFONE FIXO</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['foneWhatsappTexto_regioes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>TELEFONE WHATSAPP</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['linkWhatsapp_regioes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>LINK DO WHATSAPP</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        $nome = $dados['nome_regioes'];
        $pesquisarNome = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_regioes",
            'coluna_where'  => "nome_regioes",
            'valor_where'   => $nome
        );

        $linkRegiao = $dados['link_regioes'];
        $pesquisarLinkRegiao = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_regioes",
            'coluna_where'  => "link_regioes",
            'valor_where'   => $linkRegiao
        );
        
        $resultPesquisa1   = $this->site_admin->m_buscar_1_Item($pesquisarNome);
        $resultPesquisa2       = $this->site_admin->m_buscar_1_Item($pesquisarLinkRegiao);
        
        if($resultPesquisa1)
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>NOME DA REGIÃO</strong> já <strong>EXISTE</strong>!<br>';
            $sinal = true;
        } 
        
        if($resultPesquisa2)
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>LINK DA REGIÃO</strong> já <strong>EXISTE</strong>!<br>';
            $sinal = true;
        } 
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $tabela = 'site_regioes';
        
        $resultado = $this->site_admin->m_criar($dados, $tabela);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Região <strong>'. $dados['nome_regioes'].'</strong> criada com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível criar a região '. $dados['nome_regioes'].', tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    public function c_statusBioSite()
    {

        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('id_site_bio');
        
        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_bio_sites",
            'coluna_where'  => "id_sites_bio",
            'valor_where'   => $id
        );
        
        $resultadoDadoExiste = $this->site_admin->m_buscar_1_Item($pesquisarItem);
        
        if(is_null($resultadoDadoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Site não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_sites_bio'] = $id;

        }
        
        if($resultadoDadoExiste->status_sites_bio === '1')
        {
           $dados['status_sites_bio'] = '0';

        } else {

            $dados['status_sites_bio'] = '1';
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $dados = (object) $dados;
        
        $infosDB = (object)array(
            'coluna_where'  =>  'id_sites_bio',
            'valor_where'   =>  $id,
            'tabela'        =>  'site_bio_sites'
        );
        
        $resultado = $this->site_admin->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            if($resultadoDadoExiste->id_sites_bio === '1')
            {
                 $retorno['ret'] = true;
                $retorno['msg'] = 'Site <strong class="text-light text-uppercase">'.$resultadoDadoExiste->nome_sites_bio.' </strong> oculto na BIO';
                echo json_encode($retorno);
    
            } else {
                
                $retorno['ret'] = true;
                $retorno['msg'] = 'Site <strong class="text-light text-uppercase"> '.$resultadoDadoExiste->nome_sites_bio.' </strong> visivel na BIO';
                echo json_encode($retorno);
                
            }
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Não foi possível atualizar o status do site da BIO: '.$resultadoDadoExiste->nome_sites_bio;
            echo json_encode($retorno);
        }
    }
    
    public function c_statusBioRedesSociais()
    {

        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('id_site_bio_redesSociais');
        
        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_bio_redes_sociais",
            'coluna_where'  => "id_redes_sociais",
            'valor_where'   => $id
        );
        
        $resultadoDadoExiste = $this->site_admin->m_buscar_1_Item($pesquisarItem);
        
        if(is_null($resultadoDadoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Rede social não encontrada, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_redes_sociais'] = $id;

        }
        
        if($resultadoDadoExiste->ativo_redes_sociais === '1')
        {
           $dados['ativo_redes_sociais'] = '0';

        } else {

            $dados['ativo_redes_sociais'] = '1';
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $dados = (object) $dados;
        
        $infosDB = (object)array(
            'coluna_where'  =>  'id_redes_sociais',
            'valor_where'   =>  $id,
            'tabela'        =>  'site_bio_redes_sociais'
        );
        
        $resultado = $this->site_admin->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            if($resultadoDadoExiste->id_redes_sociais === '1')
            {
                 $retorno['ret'] = true;
                $retorno['msg'] = 'Rede Social <strong class="text-light text-uppercase">'.$resultadoDadoExiste->nome_redes_sociais.' </strong> oculta na BIO';
                echo json_encode($retorno);
    
            } else {
                
                $retorno['ret'] = true;
                $retorno['msg'] = 'Rede Social <strong class="text-light text-uppercase"> '.$resultadoDadoExiste->nome_redes_sociais.' </strong> visivel na BIO';
                echo json_encode($retorno);
                
            }
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Não foi possível atualizar o status do rede social da BIO: '.$resultadoDadoExiste->nome_redes_sociais;
            echo json_encode($retorno);
        }
    }
    
    public function c_statusBioCanalAtendimento()
    {

        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('id_site_bio_canaisAtendimento');
        
        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_bio_canais_atendimento",
            'coluna_where'  => "id_canal_atendimento",
            'valor_where'   => $id
        );
        
        $resultadoDadoExiste = $this->site_admin->m_buscar_1_Item($pesquisarItem);
        
        if(is_null($resultadoDadoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Canal de atendimento não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_canal_atendimento'] = $id;

        }
        
        if($resultadoDadoExiste->ativo_canal_atendimento === '1')
        {
           $dados['ativo_canal_atendimento'] = '0';

        } else {

            $dados['ativo_canal_atendimento'] = '1';
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $dados = (object) $dados;
        
        $infosDB = (object)array(
            'coluna_where'  =>  'id_canal_atendimento',
            'valor_where'   =>  $id,
            'tabela'        =>  'site_bio_canais_atendimento'
        );
        
        $resultado = $this->site_admin->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            if($resultadoDadoExiste->id_canal_atendimento === '1')
            {
                 $retorno['ret'] = true;
                $retorno['msg'] = 'Canal de atendimento <strong class="text-light text-uppercase">'.$resultadoDadoExiste->nome_canal_atendimento.' </strong> oculto na BIO';
                echo json_encode($retorno);
    
            } else {
                
                $retorno['ret'] = true;
                $retorno['msg'] = 'Canal de atendimento <strong class="text-light text-uppercase"> '.$resultadoDadoExiste->nome_canal_atendimento.' </strong> visivel na BIO';
                echo json_encode($retorno);
                
            }
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Não foi possível atualizar o status do site da BIO: '.$resultadoDadoExiste->nome_sites_bio;
            echo json_encode($retorno);
        }
    }
    
    public function c_liberarBloquearStatus()
    {

        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('id_regioes');
        
        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_regioes",
            'coluna_where'  => "id_regioes",
            'valor_where'   => $id
        );

        $resultadoDadoExiste = $this->site_admin->m_buscar_1_Item($pesquisarItem);
        
        if(is_null($resultadoDadoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Região não encontrada, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_regioes'] = $id;

        }
        
        if($resultadoDadoExiste->status_regioes === '1')
        {
           $dados['status_regioes'] = '0';

        } else {

            $dados['status_regioes'] = '1';
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $dados = (object) $dados;
        
        $infosDB = (object)array(
            'coluna_where'  =>  'id_regioes',
            'valor_where'   =>  $id,
            'tabela'        =>  'site_regioes'
        );
        
        $resultado = $this->site_admin->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            if($resultadoDadoExiste->status_regioes === '1')
            {
                 $retorno['ret'] = true;
                $retorno['msg'] = 'Região <strong class="text-light text-uppercase">'.$resultadoDadoExiste->nome_regioes.' oculto</strong> na intro do site';
                echo json_encode($retorno);
    
            } else {
                
                $retorno['ret'] = true;
                $retorno['msg'] = 'Região <strong class="text-light text-uppercase"> '.$resultadoDadoExiste->nome_regioes.' visivel</strong> na intro do site';
                echo json_encode($retorno);
                
            }
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Não foi possível atualizar o status da região: '.$resultadoDadoExiste->nome_regioes;
            echo json_encode($retorno);
        }
    }
    
    public function c_liberarBloquearStatusSite()
    {

        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('id_regioes');
        
        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_regioes",
            'coluna_where'  => "id_regioes",
            'valor_where'   => $id
        );

        $resultadoDadoExiste = $this->site_admin->m_buscar_1_Item($pesquisarItem);
        
        if(is_null($resultadoDadoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Região não encontrada, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_regioes'] = $id;

        }
        
        if($resultadoDadoExiste->ativoSite_regioes === '1')
        {
           $dados['ativoSite_regioes'] = '0';

        } else {

            $dados['ativoSite_regioes'] = '1';
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $dados = (object) $dados;
        
        $infosDB = (object)array(
            'coluna_where'  =>  'id_regioes',
            'valor_where'   =>  $id,
            'tabela'        =>  'site_regioes'
        );
        
        $resultado = $this->site_admin->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            if($resultadoDadoExiste->status_regioes === '1')
            {
                 $retorno['ret'] = true;
                $retorno['msg'] = 'Região <strong class="text-light text-uppercase">'.$resultadoDadoExiste->nome_regioes.' oculto</strong> na página home do site';
                echo json_encode($retorno);
    
            } else {
                
                $retorno['ret'] = true;
                $retorno['msg'] = 'Região <strong class="text-light text-uppercase"> '.$resultadoDadoExiste->nome_regioes.' visivel</strong> na página home do site';
                echo json_encode($retorno);
                
            }
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Não foi possível atualizar o status da região: '.$resultadoDadoExiste->nome_regioes;
            echo json_encode($retorno);
        }
    }

    public function c_editarInfosBio()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = 1;

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_bio_infos",
            'coluna_where'  => "id_infos_bio",
            'valor_where'   => $id
        );

        $pesquisa   = $this->site_admin->m_buscar_1_Item($pesquisarItem);
        
        if(empty($pesquisa))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' Erro ao consultar dados!<br>';
            $sinal = true;
            
        } else {

            $dados['id_infos_bio'] = $id;

        }
        
        $dados['nome_infos_bio']         = $this->input->post('nome_bio');
        
        if(empty($dados['nome_infos_bio']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">nome</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }
        
        $dados['descricao_infos_bio']    = $this->input->post('descricao_bio');
        
        if($_FILES['imagem_bio']['error'] == 1)
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Imagem <strong class="text-uppercase text-light">inválida</strong>, procure outra!<br>';
            $sinal = true;
        
        } else {

            if($_FILES['imagem_bio']['size'] == 0 )
            {
                $dados['imagem_infos_bio'] = $pesquisa->imagem_infos_bio;
            }

            if($_FILES['imagem_bio']['size'] != 0)
            {
                $site = 'ispmais';
                
                $imagemPesquisaExiste = 'assets/sites/'.$site.'/img/bio/'.$pesquisa->imagem_infos_bio;
        
                if (file_exists($imagemPesquisaExiste)) {
                    
                    $config['upload_path']      = './assets/sites/'.$site.'/img/bio';
                    $config['file_name']        = "img_bio_".rand().".png";
                    $config['allowed_types']    = '*' ;
                    $config['max_width']        = 1921;
                    $config['max_height']       = 1921;
                    
                    $dados['imagem_infos_bio']   = $config['file_name'];

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('imagem_bio'))
                    {
                        $retorno['ret'] = false;
                        $retorno['msg'].= 'Imagem muito grande, escolha uma imagem até: <strong class="text-light text-uppercase">1920px x 1920px</strong> <br>';
                        $sinal = true;

                    } else {

                        unlink($imagemPesquisaExiste);
                    }

                } else {
                                        
                    $config['upload_path']      = './assets/sites/'.$site.'/img/bio';
                    $config['file_name']        = "img_bio_".rand().".png";
                    $config['allowed_types']    = '*' ;
                    $config['max_width']        = 1921;
                    $config['max_height']       = 1921;
                    
                    $dados['imagem_infos_bio']   = $config['file_name'];

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('imagem_bio'))
                    {
                        $retorno['ret'] = false;
                        $retorno['msg'].= 'Imagem muito grande, escolha uma imagem até: <strong class="text-light text-uppercase">1920px x 1920px</strong> <br>';
                        $sinal = true;
                        
                    } 
                }
                
            }

        }

        $dados['anoEmpresa_infos_bio']      = $this->input->post('anoEmpresa_bio');
        $dados['textoEmpresa_infos_bio']    = $this->input->post('TextoEmpresa_bio');

        if(empty($dados['anoEmpresa_infos_bio']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">ano</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['textoEmpresa_infos_bio']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O texto de <strong class="text-uppercase">copyright</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }
        
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $dados = (object) $dados;

            $infosDB = (object)array(
                'coluna_where'  => 'id_infos_bio',
                'valor_where'   => $id,
                'tabela'        => 'site_bio_infos'
            );
        
        $resultado = $this->site_admin->m_atualizar($dados, $infosDB);
    
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' As <strong>Informações</strong> foram atualizadas com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar as informações, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
    }
    
    public function c_editarSiteBio()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('idEditarSiteBio');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_bio_sites",
            'coluna_where'  => "id_sites_bio",
            'valor_where'   => $id
        );

        $pesquisa   = $this->site_admin->m_buscar_1_Item($pesquisarItem);
                
        if(empty($pesquisa))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' Erro ao consultar dados!<br>';
            $sinal = true;
            
        } else {
            
            $dados['id_sites_bio'] = $id;
            $dados['nome_sites_bio']    = $this->input->post('alterarNomeSiteBio');
            $dados['url_sites_bio']     = $this->input->post('alterarLinkSiteBio');
                    
            if(empty($dados['nome_sites_bio']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'O <strong class="text-uppercase">nome</strong> não pode ser vazio!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['url_sites_bio']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'A <strong class="text-uppercase">url</strong> não pode ser vazio!<br>';
                $sinal = true;
                
            }
            
            if($sinal)
            {
                echo json_encode($retorno);
                exit;
            }

            $dados = (object) $dados;

            $infosDB = (object)array(
                'coluna_where'  => 'id_sites_bio',
                'valor_where'   => $id,
                'tabela'        => 'site_bio_sites'
            );
            
            $resultado = $this->site_admin->m_atualizar($dados, $infosDB);
        
            if($resultado)
            {
                $retorno['ret'] = true;
                $retorno['msg'] = ' As <strong>Informações</strong> foram atualizadas com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível atualizar as informações, tente novamente mais tarde!';
                echo json_encode($retorno);
            }

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
    }
    
    public function c_atualizarCoresEstiloBio()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = 1;
        
        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_bio_cores_estilos",
            'coluna_where'  => "id_cores_estilos",
            'valor_where'   => $id
        );

        $pesquisa   = $this->site_admin->m_buscar_1_Item($pesquisarItem);
                
        if(empty($pesquisa))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' Erro ao consultar dados!<br>';
            $sinal = true;
            
        } else {
            
            $dados['id_cores_estilos']                      = $id;
            $dados['corBg_cores_estilos']                   = $this->input->post('corBgBio');
            $dados['corBoxMeio_cores_estilos']              = $this->input->post('corBoxMeioBio');
            $dados['CorFaixas_cores_estilos']               = $this->input->post('corFaixasBoxMeioBio');
            $dados['corTextoFaixa_cores_estilos']           = $this->input->post('corTextosFaixaBio');
            $dados['CorTextos_cores_estilos']               = $this->input->post('corTextosBio');
            $dados['corTextosBotoes_cores_estilos']         = $this->input->post('corTextosBotoesBio');
            $dados['corTextosBotoesHover_cores_estilos']    = $this->input->post('corTextosBotoesHoverBio');
            $dados['corBgBotoes_cores_estilos']             = $this->input->post('corBotoesBio');
            $dados['corBgBotoesHover_cores_estilos']        = $this->input->post('corBotoeHoversBio');
            $dados['corTextoRodape_cores_estilos']          = $this->input->post('corTextoRodapeBio');
                    
            
            
            if($sinal)
            {
                echo json_encode($retorno);
                exit;
            }

            $dados = (object) $dados;

            $infosDB = (object)array(
                'coluna_where'  => 'id_cores_estilos',
                'valor_where'   => $id,
                'tabela'        => 'site_bio_cores_estilos'
            );
            
            $resultado = $this->site_admin->m_atualizar($dados, $infosDB);
        
            if($resultado)
            {
                $retorno['ret'] = true;
                $retorno['msg'] = ' As <strong>Informações</strong> foram atualizadas com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível atualizar as informações, tente novamente mais tarde!';
                echo json_encode($retorno);
            }

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
    }
        
    public function c_editarInfosCanalAtendimentoModal()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('idEditarCanalAtendimento');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_bio_canais_atendimento",
            'coluna_where'  => "id_canal_atendimento",
            'valor_where'   => $id
        );

        $pesquisa   = $this->site_admin->m_buscar_1_Item($pesquisarItem);
                
        if(empty($pesquisa))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' Erro ao consultar dados!<br>';
            $sinal = true;
            
        } else {
            
            $dados['id_canal_atendimento'] = $id;
            $dados['nome_canal_atendimento']    = $this->input->post('alterarNomeCanalAtendimento');
            $dados['url_canal_atendimento']     = $this->input->post('alterarLinkCanalAtendimento');
            
            if($this->input->post('editarIcone_canalAtendimento') === '0')
            {
                $dados['icone_canal_atendimento']   = $pesquisa->icone_canal_atendimento;
                $dados['tipo_canal_atendimento']    = $pesquisa->tipo_canal_atendimento;
            
            } else {

                switch ($this->input->post('editarIcone_canalAtendimento')) {
                    case 1:
                        $dados['icone_canal_atendimento'] = 'mdi mdi-whatsapp';
                        $dados['tipo_canal_atendimento'] = '1';
                        break;
                    case 2:
                        $dados['icone_canal_atendimento'] = 'mdi mdi-instagram';
                        $dados['tipo_canal_atendimento'] = '2';
                        break;
                    case 3:
                        $dados['icone_canal_atendimento'] = 'mdi mdi-facebook-messenger';
                        $dados['tipo_canal_atendimento'] = '3';
                        break;
        
                    case 4:
                        $dados['icone_canal_atendimento'] = 'mdi mdi-telegram';
                        $dados['tipo_canal_atendimento'] = '4';
                        break;
        
                    case 5:
                        $dados['icone_canal_atendimento'] = 'mdi mdi-email';
                        $dados['tipo_canal_atendimento'] = '5';
                        break;
        
                    case 6:
                        $dados['icone_canal_atendimento'] = 'mdi mdi-phone-classic';
                        $dados['tipo_canal_atendimento'] = '6';
                        break;           
                } 

            }
            
            if(empty($dados['nome_canal_atendimento']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'O <strong class="text-uppercase">nome</strong> não pode ser vazio!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['url_canal_atendimento']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'A <strong class="text-uppercase">url</strong> não pode ser vazio!<br>';
                $sinal = true;
                
            }
            
            if($sinal)
            {
                echo json_encode($retorno);
                exit;
            }
            
            $dados = (object) $dados;

            $infosDB = (object)array(
                'coluna_where'  => 'id_canal_atendimento',
                'valor_where'   => $id,
                'tabela'        => 'site_bio_canais_atendimento'
            );
            
            $resultado = $this->site_admin->m_atualizar($dados, $infosDB);
        
            if($resultado)
            {
                $retorno['ret'] = true;
                $retorno['msg'] = ' As <strong>Informações</strong> foram atualizadas com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível atualizar as informações, tente novamente mais tarde!';
                echo json_encode($retorno);
            }

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
    }
    
    public function c_editarInfosRedesSociaisBio()
    {
        $retorno['msg'] = "";
        $sinal = false;
                
        $id = $this->input->post('idEditarRedesSociaisBio');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_bio_redes_sociais",
            'coluna_where'  => "id_redes_sociais",
            'valor_where'   => $id
        );

        $pesquisa   = $this->site_admin->m_buscar_1_Item($pesquisarItem);
                
        if(empty($pesquisa))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' Erro ao consultar dados!<br>';
            $sinal = true;
            
        } else {
            
            $dados['id_redes_sociais'] = $id;
            $dados['nome_redes_sociais']    = $this->input->post('alterarNomeRedesSociaisBio');
            $dados['url_redes_sociais']     = $this->input->post('alterarLinkRedesSociaisBio');
            
            if($this->input->post('editarIcone_redesSociais') === '0')
            {
                $dados['icone_redes_sociais']   = $pesquisa->icone_redes_sociais;
            
            } else {
                
                switch ($this->input->post('editarIcone_redesSociais')) {
                    case 1:
                        $dados['icone_redes_sociais'] = 'mdi mdi-instagram';
                        break;
                    case 2:
                        $dados['icone_redes_sociais'] = 'mdi mdi-facebook-box';
                        break;
                    case 3:
                        $dados['icone_redes_sociais'] = 'mdi mdi-youtube';
                        break;
        
                    case 4:
                        $dados['icone_redes_sociais'] = 'mdi mdi-twitter';
                        break;
        
                    case 5:
                        $dados['icone_redes_sociais'] = 'mdi mdi-pinterest';
                        break;
                } 

            }
            
            if(empty($dados['nome_redes_sociais']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'O <strong class="text-uppercase">nome</strong> não pode ser vazio!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['url_redes_sociais']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'A <strong class="text-uppercase">url</strong> não pode ser vazio!<br>';
                $sinal = true;
                
            }
            
            if($sinal)
            {
                echo json_encode($retorno);
                exit;
            }
            
            $dados = (object) $dados;

            $infosDB = (object)array(
                'coluna_where'  => 'id_redes_sociais',
                'valor_where'   => $id,
                'tabela'        => 'site_bio_redes_sociais'
            );
            
            $resultado = $this->site_admin->m_atualizar($dados, $infosDB);
        
            if($resultado)
            {
                $retorno['ret'] = true;
                $retorno['msg'] = ' As <strong>Informações</strong> foram atualizadas com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível atualizar as informações, tente novamente mais tarde!';
                echo json_encode($retorno);
            }

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
    }
    
    public function c_atualizarInfosRegiao()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['id_regioes']                    = $this->input->post('idRegiao');
        $dados['nome_regioes']                  = $this->input->post('alterarNomeRegiao');
        $dados['link_regioes']                  = $this->input->post('alterarLinkPaginaRegiao');
        $dados['endereco_regioes']              = $this->input->post('alterarenderecoRegiao');
        $dados['email_regioes']                 = $this->input->post('alteraremailRegiao');
        $dados['foneFixoTexto_regioes']         = $this->input->post('alterarfoneFixoRegiao');
        $dados['foneWhatsappTexto_regioes']     = $this->input->post('alterarwhatsappRegiao');
        $dados['linkWhatsapp_regioes']          = $this->input->post('alterarlinkWhatsappRegiao');

        $id = $this->input->post('idRegiao');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_regioes",
            'coluna_where'  => "id_regioes",
            'valor_where'   => $id
        );

        $pesquisa   = $this->site_admin->m_buscar_1_Item($pesquisarItem);
        
        if(empty($pesquisa))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Região não encontrada, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $dados['id_regioes'] = $id;

        }
        
        if(empty($dados['nome_regioes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">nome</strong> da região não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['link_regioes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">link</strong> da região não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['endereco_regioes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">endereço</strong> da região não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['email_regioes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">E-mail</strong> da região não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['foneFixoTexto_regioes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">telefone fixo</strong> da região não pode ser vazio!<br>';
            $sinal = true;
            
        }
        
        if(empty($dados['foneWhatsappTexto_regioes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">telefone whatsapp</strong> da região não pode ser vazio!<br>';
            $sinal = true;
            
        }
        
        if(empty($dados['linkWhatsapp_regioes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">link do whatsapp</strong> da região não pode ser vaziao!<br>';
            $sinal = true;
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $dados = (object) $dados;

        $infosDB = (object)array(
            'coluna_where'  => 'id_regioes',
            'valor_where'   => $id,
            'tabela'        => 'site_regioes'
        );
        
        $resultado = $this->site_admin->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Informações da região: <strong class="text-uppercase text-light">'.$dados->nome_regioes.'</strong> atualizadas com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar, tente novamente mais tarde!';
            echo json_encode($retorno);
        }

        
        // $retorno['msg'] = "";
        // $sinal = false;

        // $id                     = $this->input->post('idRegiao');
        // $dados['nome_regioes']  = $this->input->post('alterarNomeRegiao');
        // $dados['link_regioes']  = $this->input->post('alterarLinkPaginaRegiao');
        
        // $resultRegiaoExiste = $this->site_admin->m_verificaRegiaoExiste($id);
        
        // if(empty($resultRegiaoExiste))
        // {
        //     $retorno['ret'] = false;
        //     $retorno['msg'].= 'Região não encontrada, tente novamente em alguns minutos!<br>';
        //     $sinal = true;
            
        // } else {

        //     $dados['id_regioes'] = $id;

        //     if(empty($dados['nome_regioes']))
        //     {
        //         $retorno['ret'] = false;
        //         $retorno['msg'].= 'O <strong>NOME DA REGIÃO</strong> não pode ser vazio!<br>';
        //         $sinal = true;
                
        //     }

        //     if(empty($dados['link_regioes']))
        //     {
        //         $retorno['ret'] = false;
        //         $retorno['msg'].= 'O <strong>LINK DA REGIÃO</strong> não pode ser vazio!<br>';
        //         $sinal = true;
                
        //     }

        //     if($sinal)
        //     {
        //         echo json_encode($retorno);
        //         exit;
        //     }

        //     $resultado = $this->site_admin->m_atualizarInfosRegiao($dados);
        
        //     if($resultado)
        //     {
        //         $retorno['ret'] = true;
        //         $retorno['msg'] = ' Região: <strong>'.$resultRegiaoExiste->nome_regioes.'</strong> atualizada com sucesso!';
        //         echo json_encode($retorno);
                
        //     } else {

        //         $retorno['ret'] = false;
        //         $retorno['msg'] = ' Não foi possível atualizar as informações da região '.$resultRegiaoExiste->nome_regioes.', tente novamente mais tarde!';
        //         echo json_encode($retorno);
        //     }
            
        // }
        
        // if($sinal)
        // {
        //     echo json_encode($retorno);
        //     exit;
        // }
    }

    public function c_excluirSiteBio()
    {

        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('idExcluirSiteBio');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_bio_sites",
            'coluna_where'  => "id_sites_bio",
            'valor_where'   => $id
        );

        $resultPesquisa   = $this->site_admin->m_buscar_1_Item($pesquisarItem);
        
        if(empty($resultPesquisa))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Site não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else { 

            $infosDB = (object)array(
                'tabela'        => "site_bio_sites",
                'coluna_where'  => "id_sites_bio",
                'valor_where'   => $id
            );
            
            $resultado = $this->site_admin->m_excluir($infosDB);
        
            if($resultado)
            { 
                $retorno['ret'] = true;
                $retorno['msg'] = ' Site: <strong>'.$resultPesquisa->nome_sites_bio.'</strong> excluído com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível excluir o site '.$resultPesquisa->nome_sites_bio.', tente novamente mais tarde!';
                echo json_encode($retorno);
            }

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
    }
    
    public function c_excluirCanalAtendimentoBioModal()
    {

        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('idExcluirCanalAtendimento');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_bio_canais_atendimento",
            'coluna_where'  => "id_canal_atendimento",
            'valor_where'   => $id
        );

        $resultPesquisa   = $this->site_admin->m_buscar_1_Item($pesquisarItem);
        
        if(empty($resultPesquisa))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Canal de atendimento não encontrado, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else { 

            $infosDB = (object)array(
                'tabela'        => "site_bio_canais_atendimento",
                'coluna_where'  => "id_canal_atendimento",
                'valor_where'   => $id
            );
            
            $resultado = $this->site_admin->m_excluir($infosDB);
        
            if($resultado)
            { 
                $retorno['ret'] = true;
                $retorno['msg'] = ' Canal de atendimento: <strong>'.$resultPesquisa->nome_canal_atendimento.'</strong> excluído com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível excluir o Canal de atendimento '.$resultPesquisa->nome_canal_atendimento.', tente novamente mais tarde!';
                echo json_encode($retorno);
            }

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
    }
    
    public function c_excluirRedesSociaisBio()
    {

        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('idExcluirRedeSocial');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_bio_redes_sociais",
            'coluna_where'  => "id_redes_sociais",
            'valor_where'   => $id
        );

        $resultPesquisa   = $this->site_admin->m_buscar_1_Item($pesquisarItem);
        
        if(empty($resultPesquisa))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Rede Social não encontrada, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else { 

            $infosDB = (object)array(
                'tabela'        => "site_bio_redes_sociais",
                'coluna_where'  => "id_redes_sociais",
                'valor_where'   => $id
            );
            
            $resultado = $this->site_admin->m_excluir($infosDB);
        
            if($resultado)
            { 
                $retorno['ret'] = true;
                $retorno['msg'] = ' Rede Social: <strong>'.$resultPesquisa->nome_redes_sociais.'</strong> excluída com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível excluir o Rede Social '.$resultPesquisa->nome_redes_sociais.', tente novamente mais tarde!';
                echo json_encode($retorno);
            }

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
    }
    
    public function c_excluirRegiao()
    {

        $retorno['msg'] = "";
        $sinal = false;
        
        $id = $this->input->post('idRegiaoExcluir');

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_regioes",
            'coluna_where'  => "id_regioes",
            'valor_where'   => $id
        );

        $resultPesquisa   = $this->site_admin->m_buscar_1_Item($pesquisarItem);
        
        if(empty($resultPesquisa))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'região não encontrada, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else { 

            $infosDB = (object)array(
                'tabela'        => "site_regioes",
                'coluna_where'  => "id_regioes",
                'valor_where'   => $id
            );
            
            $resultado = $this->site_admin->m_excluir($infosDB);
        
            if($resultado)
            { 
                $retorno['ret'] = true;
                $retorno['msg'] = ' Região: <strong>'.$resultPesquisa->nome_regioes.'</strong> excluída com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível excluir a imagem '.$resultPesquisa->id_carousel.', tente novamente mais tarde!';
                echo json_encode($retorno);
            }

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }


        // $retorno['msg'] = "";
        // $sinal = false;
        
        // $id = $this->input->post('idRegiaoExcluir');

        // $resultRegiaoExiste = $this->site_admin->m_verificaRegiaoExiste($id);

        // if(empty($resultRegiaoExiste))
        // {
        //     $retorno['ret'] = false;
        //     $retorno['msg'].= 'Região não encontrada, tente novamente em alguns minutos!<br>';
        //     $sinal = true;
            
        // } else { 

        //     $idExcluir = $resultRegiaoExiste->id_regioes;

        //     $resultado = $this->site_admin->m_excluirRegiao($id);
        
        //     if($resultado)
        //     {
        //         $retorno['ret'] = true;
        //         $retorno['msg'] = ' Região: <strong>'.$resultRegiaoExiste->nome_regioes.'</strong> excluída com sucesso!';
        //         echo json_encode($retorno);
                
        //     } else {

        //         $retorno['ret'] = false;
        //         $retorno['msg'] = ' Não foi possível excluir a região '.$resultRegiaoExiste->nome_regioes.', tente novamente mais tarde!';
        //         echo json_encode($retorno);
        //     }

        // }
        
        // if($sinal)
        // {
        //     echo json_encode($retorno);
        //     exit;
        // }
    }

    public function c_atualizarSEO_site()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
       
        $id = 1;

        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_intro_seo",
            'coluna_where'  => "id_intro_seo",
            'valor_where'   => $id
        );
        
        $resultadoDadoExiste = $this->site_admin->m_buscar_1_Item($pesquisarItem);
        
        $dados['id_intro_seo']                  = 1;
        $dados['intro_seo_tituloSite']          = $this->input->post('titulo_seo_intro');
        $dados['intro_seo_nomeSite']            = $this->input->post('nome_seo_intro');
        $dados['intro_seo_corTema']             = $this->input->post('corTema_seo_intro');
        $dados['intro_seo_css']                 = $this->input->post('css_seo_intro');
        $dados['intro_seo_js']                  = $this->input->post('js_seo_intro');
        $dados['intro_seo_js_body']             = $this->input->post('jsBody_seo_intro');
        $dados['intro_seo_keywords']            = $this->input->post('keywords_seo_intro');
        $dados['intro_seo_description1']        = $this->input->post('description1_seo_intro');
        $dados['intro_seo_description2']        = $this->input->post('description2_seo_intro');
        $dados['intro_seo_description3']        = $this->input->post('description3_seo_intro');
        $dados['intro_seo_coordenadasMaps']     = $this->input->post('coordenadas_seo_intro');
        $dados['intro_seo_cidade']              = $this->input->post('cidadeEstado_seo_intro');
        $dados['intro_seo_estado']              = $this->input->post('estadoPais_seo_intro');
        $dados['intro_seo_googleVerificacao']   = $this->input->post('codigoGoogle_seo_intro');
        
        if($_FILES['imagemLogo_seo_intro']['error'] == 1)
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Imagem <strong class="text-uppercase text-light">inválida</strong>, procure outra!<br>';
            $sinal = true;
        
        } else {

            if($_FILES['imagemLogo_seo_intro']['size'] == 0 )
            {
                $dados['intro_seo_imagem'] = $resultadoDadoExiste->intro_seo_imagem;
            }

            if($_FILES['imagemLogo_seo_intro']['size'] != 0)
            {
                $site = 'ispmais';
                
                $imagemPesquisaExiste = 'assets/sites/'.$site.'/img/seo/'.$resultadoDadoExiste->intro_seo_imagem;
        
                if (file_exists($imagemPesquisaExiste)) {
                
                    $config['upload_path']      = './assets/sites/'.$site.'/img/seo';
                    $config['file_name']        = "img_seo_".rand().".png";
                    $config['allowed_types']    = '*' ;
                    $config['max_width']        = 1921;
                    $config['max_height']       = 551;

                    $dados['intro_seo_imagem']   = $config['file_name'];

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('imagemLogo_seo_intro'))
                    {
                        $retorno['ret'] = false;
                        $retorno['msg'].= 'Imagem muito grande, escolha uma imagem até: <strong class="text-light text-uppercase">1920px x 550px</strong> <br>';
                        $sinal = true;

                    } else {

                        unlink('assets/sites/'.$site.'/img/seo/'.$resultadoDadoExiste->intro_seo_imagem);
                    }

                } else {

                    $config['upload_path']      = './assets/sites/'.$site.'/img/seo';
                    $config['file_name']        = "img_seo_".rand().".png";
                    $config['allowed_types']    = '*' ;
                    $config['max_width']        = 1921;
                    $config['max_height']       = 551;

                    $dados['intro_seo_imagem']   = $config['file_name'];

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('imagemLogo_seo_intro'))
                    {
                        $retorno['ret'] = false;
                        $retorno['msg'].= 'Imagem muito grande, escolha uma imagem até: <strong class="text-light text-uppercase">1920px x 550px</strong> <br>';
                        $sinal = true;
                        
                    } 
                }
                
            }

        }

        if(empty($dados['intro_seo_tituloSite']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">título do site</strong> não pode ser vazio';
            $sinal = true;
            
        }
        
        if(empty($dados['intro_seo_nomeSite']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong class="text-uppercase">nome do site</strong> não pode ser vazio';
            $sinal = true;
            
        }

        if(empty($dados['intro_seo_corTema']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'A <strong class="text-uppercase">cor do tema</strong> não pode ser vazio';
            $sinal = true;
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $dados = (object) $dados;

        $infosDB = (object)array(
            'coluna_where'  => 'id_intro_seo',
            'valor_where'   => $id,
            'tabela'        => 'site_intro_seo'
        );
        
        $resultado = $this->site_admin->m_atualizar($dados, $infosDB);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados de <strong>SEO da INTRO do SITE</strong> atualizados com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar os dados de <strong>SEO da INTRO do SITE</strong>, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
    }
    
    //PÁGINA CRIADA
    public function c_solicitar_ligacao()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $gerarCodigo = (object)array(
            'coluna'       => "id_solicitacoes_ligacoes",
            'tabela'        => "site_ourilandia_solicitacoes_ligacoes"
        );
        
        $dados['id_solicitacoes_ligacoes']                      = $this->site_admin->m_gerarCodigo($gerarCodigo);
        $dados['nome_solicitacoes_ligacoes']                    = $this->input->post('nomeLigar');
        $dados['telefone_solicitacoes_ligacoes']                = $this->input->post('telefoneLigar');
        $dados['email_solicitacoes_ligacoes']                   = $this->input->post('emailLigar');
        $dados['plano_solicitacoes_ligacoes']                   = $this->input->post('PlanoLigar');
        
        if(empty($dados['plano_solicitacoes_ligacoes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe o <span class="text-uppercase">plano</span>!<br>';
            $sinal = true;
            
        } else {

            $buscarPlanoEmp = (object)array(
                'colunas'       => "*",
                'tabela'        => "site_ourilandia_planos_empresariais",
                'coluna_where'  => "nome_plano_empresarial",
                'valor_where'   => $this->input->post('PlanoLigar')
            );
    
            $buscarPlanoRes = (object)array(
                'colunas'       => "*",
                'tabela'        => "site_ourilandia_planos_residenciais",
                'coluna_where'  => "nome_plano_residencial",
                'valor_where'   => $this->input->post('PlanoLigar')
            );
            
            $planoEmp = $this->site_admin->m_buscar_1_Item($buscarPlanoEmp);
            $planoRes = $this->site_admin->m_buscar_1_Item($buscarPlanoRes);
    
            if($planoEmp == ''){
                $dados['valorPlano_solicitacoes_ligacoes'] = $planoRes->preco_plano_residencial;
            } else {
                $dados['valorPlano_solicitacoes_ligacoes'] = $planoEmp->preco_plano_empresarial;
            }
        }        

        $dados['cepEndereco_solicitacoes_ligacoes']             = $this->input->post('cepLigar');
        $dados['ruaEndereco_solicitacoes_ligacoes']             = $this->input->post('ruaLigar');
        $dados['complementoEndereco_solicitacoes_ligacoes']     = $this->input->post('complementoLigar');
        $dados['numeroEndereco_solicitacoes_ligacoes']          = $this->input->post('numeroEnderecoLigar');
        $dados['bairroEndereco_solicitacoes_ligacoes']          = $this->input->post('bairroLigar');
        $dados['cidadeEndereco_solicitacoes_ligacoes']          = $this->input->post('cidadeLigar');
        $dados['estadoEndereco_solicitacoes_ligacoes']          = $this->input->post('ufLigar');
        $dados['observacoes_solicitacoes_ligacoes']             = $this->input->post('observacaoLigar');
        $dados['operador_solicitacoes_ligacoes']                = '';
        $dados['data_solicitacoes_ligacoes']                    = date('Ymd');
        $dados['hora_solicitacoes_ligacoes']                    = date('H:i');
        $dados['status_solicitacoes_ligacoes']                  = '0';
        
        if(empty($dados['nome_solicitacoes_ligacoes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe seu <span class="text-uppercase">nome completo</span>!<br>';
            $sinal = true;
            
        } 

        if(empty($dados['telefone_solicitacoes_ligacoes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe seu <span class="text-uppercase">telefone</span>!<br>';
            $sinal = true;
            
        }
        
        if(empty($dados['cepEndereco_solicitacoes_ligacoes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'informe o <span class="text-uppercase">CEP</span> da sua <span class="text-uppercase">residência/Empresa</span>!<br>';
            $sinal = true;
            
        }

        if(empty($dados['ruaEndereco_solicitacoes_ligacoes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe o nome da <span class="text-uppercase">rua</span>!<br>';
            $sinal = true;
            
        }

        if(empty($dados['complementoEndereco_solicitacoes_ligacoes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe o <span class="text-uppercase">complemento</span>. Ex: Qd, Lt, área..!<br>';
            $sinal = true;
            
        }

        if(empty($dados['numeroEndereco_solicitacoes_ligacoes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe o <span class="text-uppercase">número</span> da residencia/empresa ou preencha com S/N!<br>';
            $sinal = true;
            
        }

        if(empty($dados['bairroEndereco_solicitacoes_ligacoes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe o <span class="text-uppercase">bairro</span>!<br>';
            $sinal = true;
            
        }

        if(empty($dados['cidadeEndereco_solicitacoes_ligacoes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe a <span class="text-uppercase">cidade</span>!<br>';
            $sinal = true;
            
        }

        if(empty($dados['estadoEndereco_solicitacoes_ligacoes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe o <span class="text-uppercase">estado</span>!<br>';
            $sinal = true;
            
        }
        
        if(empty($dados['observacoes_solicitacoes_ligacoes']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe o <span class="text-uppercase">texto de observação</span>!<br>';
            $sinal = true;
            
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $tabela = 'site_ourilandia_solicitacoes_ligacoes';
        
        $resultado = $this->site_admin->m_criar($dados, $tabela);
        
        if(!is_null($resultado))
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' <strong>'. $dados['nome_solicitacoes_ligacoes'].'</strong>, seu pedido foi realizado com sucesso, em breve um de nossos operadores irá entrar em contato, fique atento!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Algum erro técnico aconteceu na hora de solicitar a ligação, tente novamente em alguns minutos!';
            echo json_encode($retorno);
        }
    }
    //PÁGINA CRIADA
    
    //PÁGINA CRIADA
    public function c_contratarPlanoResidencial()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $idPlano = $this->input->post('id_planoResidencial');
        
        $pesquisaPlano = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_planos_residenciais",
            'coluna_where'  => "id_plano_residencial",
            'valor_where'   => $idPlano
        );

        $resultPlanoExiste = $this->site_admin->m_buscar_1_Item($pesquisaPlano);
        
        if(is_null($resultPlanoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Erro ao consultar dados do plano, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $gerarCodigo = (object)array(
                'coluna'       => "id_solicitacoes_planoResidencial",
                'tabela'        => "site_ourilandia_solicitacoes_planoResidencial"
            );
            
            $dados['id_solicitacoes_planoResidencial']                  = $this->site_admin->m_gerarCodigo($gerarCodigo);
            $dados['nome_solicitacoes_planoResidencial']                = $this->input->post('nome_planoResidencial');
            $dados['telefone_solicitacoes_planoResidencial']            = $this->input->post('telefone_planoResidencial');
            $dados['email_solicitacoes_planoResidencial']               = $this->input->post('email_planoResidencial');
            $dados['cpf_solicitacoes_planoResidencial']                 = $this->input->post('cpf_planoResidencial');
            $dados['cep_solicitacoes_planoResidencial']                 = $this->input->post('cep_planoResidencial');
            $dados['rua_solicitacoes_planoResidencial']                 = $this->input->post('rua_planoResidencial');
            $dados['complemento_solicitacoes_planoResidencial']         = $this->input->post('complemento_planoResidencial');
            $dados['numero_solicitacoes_planoResidencial']              = $this->input->post('numeroResidencial_planoResidencial');
            $dados['bairro_solicitacoes_planoResidencial']              = $this->input->post('bairro_planoResidencial');
            $dados['cidade_solicitacoes_planoResidencial']              = $this->input->post('cidade_planoResidencial');
            $dados['estado_solicitacoes_planoResidencial']              = $this->input->post('uf_planoResidencial');
            $dados['observacao_solicitacoes_planoResidencial']          = $this->input->post('observacao_planoResidencial');
            $dados['nomePlano_solicitacoes_planoResidencial']           = $resultPlanoExiste->nome_plano_residencial;
            $dados['valorPlano_solicitacoes_planoResidencial']          = $resultPlanoExiste->preco_plano_residencial;
            $dados['velocidadePlano_solicitacoes_planoResidencial']     = $resultPlanoExiste->velocidade_plano_residencial.$resultPlanoExiste->tipoVelocidade_plano_residencial;
            $dados['operador_solicitacoes_planoResidencial']            = '';
            $dados['status_solicitacoes_planoResidencial']              = '0';
            $dados['data_solicitacoes_planoResidencial']                = date('Ymd');
            $dados['hora_solicitacoes_planoResidencial']                = date('H:i');
            
            if(empty($dados['nome_solicitacoes_planoResidencial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe seu <span class="text-uppercase">nome completo</span>!<br>';
                $sinal = true;
                
            } 
    
            if(empty($dados['telefone_solicitacoes_planoResidencial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe seu <span class="text-uppercase">telefone</span>!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['cpf_solicitacoes_planoResidencial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe seu <span class="text-uppercase">C.P.F</span>!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['cep_solicitacoes_planoResidencial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'informe o <span class="text-uppercase">CEP</span> da sua <span class="text-uppercase">residência/Empresa</span>!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['rua_solicitacoes_planoResidencial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe o nome da <span class="text-uppercase">rua</span>!<br>';
                $sinal = true;
                
            }
    
            if(empty($dados['complemento_solicitacoes_planoResidencial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe o <span class="text-uppercase">complemento</span>. Ex: Qd, Lt, área..!<br>';
                $sinal = true;
                
            }
    
            if(empty($dados['numero_solicitacoes_planoResidencial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe o <span class="text-uppercase">número</span> da residencia/empresa ou preencha com S/N!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['bairro_solicitacoes_planoResidencial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe o <span class="text-uppercase">bairro</span>!<br>';
                $sinal = true;
                
            }
    
            if(empty($dados['cidade_solicitacoes_planoResidencial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe a <span class="text-uppercase">cidade</span>!<br>';
                $sinal = true;
                
            }
    
            if(empty($dados['estado_solicitacoes_planoResidencial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe o <span class="text-uppercase">estado</span>!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['observacao_solicitacoes_planoResidencial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe o <span class="text-uppercase">texto de observação</span>!<br>';
                $sinal = true;
                
            }
            
            if($sinal)
            {
                echo json_encode($retorno);
                exit;
            }
            
            $tabela = 'site_ourilandia_solicitacoes_planoResidencial';
            
            $resultado = $this->site_admin->m_criar($dados, $tabela);
            
            if(!is_null($resultado))
            {
                $retorno['ret'] = true;
                $retorno['msg'] = ' <strong class="text-orangeplus">'. $dados['nome_solicitacoes_planoResidencial'].'</strong>, seu pedido foi realizado com sucesso, em breve um de nossos consultores irá entrar em contato, fique atento!';
                echo json_encode($retorno);
                
            } else {
    
                $retorno['ret'] = false;
                $retorno['msg'] = ' Erro técnico durante a solicitação do pedido, tente novamente em alguns minutos!';
                echo json_encode($retorno);
            }

        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
    }
    //PÁGINA CRIADA

    //PÁGINA CRIADA
    public function c_contratarPlanoEmpresarial()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $idPlano = $this->input->post('id_planoEmpresarial');
        
        $pesquisaPlano = (object)array(
            'colunas'       => "*",
            'tabela'        => "site_ourilandia_planos_empresariais",
            'coluna_where'  => "id_plano_empresarial",
            'valor_where'   => $idPlano
        );

        $resultPlanoExiste = $this->site_admin->m_buscar_1_Item($pesquisaPlano);
        
        if(is_null($resultPlanoExiste))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Erro ao consultar dados do plano, tente novamente em alguns minutos!<br>';
            $sinal = true;
            
        } else {

            $gerarCodigo = (object)array(
                'coluna'       => "id_solicitacoes_planoEmpresarial",
                'tabela'        => "site_ourilandia_solicitacoes_planoEmpresarial"
            );
            
            $dados['id_solicitacoes_planoEmpresarial']                  = $this->site_admin->m_gerarCodigo($gerarCodigo);
            $dados['nome_solicitacoes_planoEmpresarial']                = $this->input->post('nome_planoEmpresarial');
            $dados['telefone_solicitacoes_planoEmpresarial']            = $this->input->post('telefone_planoEmpresarial');
            $dados['telefoneFixo_solicitacoes_planoEmpresarial']        = $this->input->post('telefoneFixo_planoEmpresarial');
            $dados['email_solicitacoes_planoEmpresarial']               = $this->input->post('email_planoEmpresarial');
            $dados['cnpj_solicitacoes_planoEmpresarial']                = $this->input->post('cnpj_planoEmpresarial');
            $dados['nomeFantasia_planoEmpresarial']                     = $this->input->post('nomeFantasia_planoEmpresarial');
            $dados['cep_solicitacoes_planoEmpresarial']                 = $this->input->post('cep_planoEmpresarial');
            $dados['rua_solicitacoes_planoEmpresarial']                 = $this->input->post('rua_planoEmpresarial');
            $dados['complemento_solicitacoes_planoEmpresarial']         = $this->input->post('complemento_planoEmpresarial');
            $dados['numero_solicitacoes_planoEmpresarial']              = $this->input->post('numero_planoEmpresarial');
            $dados['bairro_solicitacoes_planoEmpresarial']              = $this->input->post('bairro_planoEmpresarial');
            $dados['cidade_solicitacoes_planoEmpresarial']              = $this->input->post('cidade_planoEmpresarial');
            $dados['estado_solicitacoes_planoEmpresarial']              = $this->input->post('uf_planoEmpresarial');
            $dados['observacao_solicitacoes_planoEmpresarial']          = $this->input->post('observacao_planoEmpresarial');
            $dados['nomePlano_solicitacoes_planoEmpresarial']           = $resultPlanoExiste->nome_plano_empresarial;
            $dados['valorPlano_solicitacoes_planoEmpresarial']          = $resultPlanoExiste->preco_plano_empresarial;
            $dados['velocidadePlano_solicitacoes_planoEmpresarial']     = $resultPlanoExiste->velocidade_plano_empresarial.$resultPlanoExiste->tipoVelocidade_plano_empresarial;
            $dados['operador_solicitacoes_planoEmpresarial']            = '';
            $dados['status_solicitacoes_planoEmpresarial']              = '0';
            $dados['data_solicitacoes_planoEmpresarial']                = date('Ymd');
            $dados['hora_solicitacoes_planoEmpresarial']                = date('H:i');
            
            if(empty($dados['nome_solicitacoes_planoEmpresarial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe seu <span class="text-uppercase">nome completo</span>!<br>';
                $sinal = true;
                
            } 
    
            if(empty($dados['telefone_solicitacoes_planoEmpresarial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe seu <span class="text-uppercase text-warning">telefone celular</span>!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['telefoneFixo_solicitacoes_planoEmpresarial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe seu <span class="text-uppercase">telefone fixo</span>!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['email_solicitacoes_planoEmpresarial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe seu <span class="text-uppercase">E-mail</span>!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['cnpj_solicitacoes_planoEmpresarial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe seu <span class="text-uppercase">C.N.P.J</span>!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['nomeFantasia_planoEmpresarial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe o <span class="text-uppercase">NOME FANTASIA</span>!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['cep_solicitacoes_planoEmpresarial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'informe o <span class="text-uppercase">CEP</span> da sua <span class="text-uppercase">residência/Empresa</span>!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['rua_solicitacoes_planoEmpresarial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe o nome da <span class="text-uppercase">rua</span>!<br>';
                $sinal = true;
                
            }
    
            if(empty($dados['complemento_solicitacoes_planoEmpresarial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe o <span class="text-uppercase">complemento</span>. Ex: Qd, Lt, área..!<br>';
                $sinal = true;
                
            }
    
            if(empty($dados['numero_solicitacoes_planoEmpresarial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe o <span class="text-uppercase">número</span> da residencia/empresa ou preencha com S/N!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['bairro_solicitacoes_planoEmpresarial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe o <span class="text-uppercase">bairro</span>!<br>';
                $sinal = true;
                
            }
    
            if(empty($dados['cidade_solicitacoes_planoEmpresarial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe a <span class="text-uppercase">cidade</span>!<br>';
                $sinal = true;
                
            }
    
            if(empty($dados['estado_solicitacoes_planoEmpresarial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe o <span class="text-uppercase">estado</span>!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['observacao_solicitacoes_planoEmpresarial']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Informe o <span class="text-uppercase">texto de observação</span>!<br>';
                $sinal = true;
                
            }
            
            if($sinal)
            {
                echo json_encode($retorno);
                exit;
            }
            
            $tabela = 'site_ourilandia_solicitacoes_planoEmpresarial';
            
            $resultado = $this->site_admin->m_criar($dados, $tabela);
            
            if(!is_null($resultado))
            {
                $retorno['ret'] = true;
                $retorno['msg'] = ' <strong class="text-orangeplus">'. $dados['nome_solicitacoes_planoEmpresarial'].'</strong>, seu pedido foi realizado com sucesso, em breve um de nossos consultores irá entrar em contato, fique atento!';
                echo json_encode($retorno);
                
            } else {
    
                $retorno['ret'] = false;
                $retorno['msg'] = ' Erro técnico durante a solicitação do pedido, tente novamente em alguns minutos!';
                echo json_encode($retorno);
            }

        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
    }
    //PÁGINA CRIADA

    //PÁGINA CRIADA
    public function c_assinarNewsletter()
    {   
        $retorno['msg'] = "";
        $sinal = false;
        
        $idPlano = $this->input->post('id_planoEmpresarial');
        
        $gerarCodigo = (object)array(
            'coluna'       => "id_newsletter",
            'tabela'        => "site_ourilandia_newsletter"
        );
            
        $dados['id_newsletter']                 = $this->site_admin->m_gerarCodigo($gerarCodigo);
        $dados['nomeCliente_newsletter']        = $this->input->post('nome_newsletter');
        $dados['telefoneCliente_newsletter']    = $this->input->post('telefone_newsletter');

        $mailOk = trim(mb_strtolower($this->input->post('email_newsletter')));

        $dados['emailCliente_newsletter']       = $mailOk;
        $dados['data_newsletter']               = date('Ymd');
        $dados['hora_newsletter']               = date('H:i');

        if(empty($dados['emailCliente_newsletter']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe seu <span class="text-uppercase">e-mail</span>!<br>';
            $sinal = true;
            
        } else {

            $email = $dados['emailCliente_newsletter'];
        
            $pesquisar = (object)array(
                'colunas'       => "*",
                'tabela'        => "site_ourilandia_newsletter",
                'coluna_where'  => "emailCliente_newsletter",
                'valor_where'   => $email
            );
    
            $result = $this->site_admin->m_buscar_1_Item($pesquisar);
            
            if($result)
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'E-mail já utilizado!<br>';
                $sinal = true;
                
            }

            if($sinal)
            {
                echo json_encode($retorno);
                exit;
            }
        

        }
        
        if(empty($dados['nomeCliente_newsletter']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe seu <span class="text-uppercase">nome</span>!<br>';
            $sinal = true;
            
        } 

        if(empty($dados['telefoneCliente_newsletter']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe seu <span class="text-uppercase">telefone</span>!<br>';
            $sinal = true;
            
        } else {
            
            if(strlen($dados['telefoneCliente_newsletter']) < '16')
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'Insira todos os números para o seu <span class="text-uppercase">telefone</span>!<br>';
                $sinal = true;
            }

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $tabela = 'site_ourilandia_newsletter';
        
        $resultado = $this->site_admin->m_criar($dados, $tabela);
        
        if(!is_null($resultado))
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' <strong class="text-orangeplus">'. $dados['nomeCliente_newsletter'].'</strong>, você assinou nossa newsletter com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Erro técnico durante a solicitação do pedido, tente novamente em alguns minutos!';
            echo json_encode($retorno);
        }
            
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }
    //PÁGINA CRIADA
    
}
