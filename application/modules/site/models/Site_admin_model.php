<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site_admin_model extends CI_Model {

    function __construct()
    {
        parent::__construct();

        $this->site = $this->load->database('site', TRUE);
    }

    public function __destruct() 
    { 
        $this->db->close(); 
    } 
    
    public function m_gerarCodigo($gerarCodigo) 
    {
        
        if ($this->site->table_exists($gerarCodigo->tabela) )
        {
            $result =  $this->site->query("SELECT MAX($gerarCodigo->coluna) as codigo FROM ".$gerarCodigo->tabela);

            foreach ($result->result_array() as $dados)
            {
                
                if(is_null($dados['codigo'])) return 1;

                return intval($dados['codigo'])+1;
            }
        }
        
        
    }

    public function m_buscar_1_Item($dados)
    {
        $objeto = (object)$dados;
        
        if ($this->site->table_exists($objeto->tabela) )
        {
            $this->site->select($objeto->colunas);
            $this->site->where($objeto->coluna_where, $objeto->valor_where);

            $resultado = $this->site->get($objeto->tabela)->result();
            
            if(is_null($resultado))
            {
                return $resultado;

            } else {

                foreach ($resultado as $dados)
                {
                    return $dados;    
                }   

            }
        }
        
        
    }
    
    public function m_buscar_tudo_orderBy($dados)
    {
        $objeto = (object)$dados;

        if ($this->site->table_exists($objeto->tabela) )
        {
            $this->site->select($objeto->colunas);
            $this->site->order_by($objeto->coluna_where, $objeto->orderBy);

            $resultado = $this->site->get($objeto->tabela)->result();

            return $resultado;
        }
        
        
    }
    
    public function m_criar($dados, $tabela)
    {
        
        if ($this->site->table_exists($tabela) )
        {
            return $this->site->insert($tabela, $dados);
        }
        
    }
    
    public function m_atualizar($dados, $infosDB)
    {
        
        if ($this->site->table_exists($infosDB->tabela) )
        {
            $this->site->where($infosDB->coluna_where, $infosDB->valor_where);
        
            $resultado = $this->site->update($infosDB->tabela, $dados);

            return $resultado;
        }
        
    }
    
    public function m_excluir($infosDB)
    {

        if ($this->site->table_exists($infosDB->tabela) )
        {
            $this->site->where($infosDB->coluna_where, $infosDB->valor_where);

            $result = $this->site->delete($infosDB->tabela);
            
            return $result;
        }
        
        
    }
    

}
