<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site_admin_model_ourilandia extends CI_Model {

    function __construct()
    {
        parent::__construct();

        $this->site = $this->load->database('site', TRUE);
    }

    public function m_gerarCodigo($gerarCodigo) 
    {
        
        $result =  $this->site->query("SELECT MAX($gerarCodigo->coluna) as codigo FROM ".$gerarCodigo->tabela);

        foreach ($result->result_array() as $dados)
        {
            
            if(is_null($dados['codigo'])) return 1;

            return intval($dados['codigo'])+1;
        }
        
    }

    public function m_buscar_1_Item($dados)
    {
        $objeto = (object)$dados;
        
        $this->site->select($objeto->colunas);
        $this->site->where($objeto->coluna_where, $objeto->valor_where);

        $resultado = $this->site->get($objeto->tabela)->result();

        if(is_null($resultado))
        {
            return $resultado;

        } else {

            foreach ($resultado as $dados)
            {
                return $dados;    
            }   

        }
        
    }

    public function m_buscar_tudo_orderBy($dados)
    {
        $objeto = (object)$dados;
        
        $this->site->select($objeto->colunas);
        $this->site->order_by($objeto->coluna_where, $objeto->orderBy);

        $resultado = $this->site->get($objeto->tabela)->result();

        return $resultado;
    }
    
    public function m_buscar_tudo_where_orderBy($dados)
    {
        $objeto = (object)$dados;
        
        $this->site->select($objeto->colunas);
        $this->site->where($objeto->coluna_where2, $objeto->valor_where);
        $this->site->order_by($objeto->coluna_where, $objeto->orderBy);

        $resultado = $this->site->get($objeto->tabela)->result();

        return $resultado;
    }

    public function m_buscar_tudo_where_in_orderBy($dados)
    {
        $objeto = (object)$dados;
        
        $this->site->select($objeto->colunas);
        $this->site->where_in($objeto->coluna_where2, $objeto->valor_where);
        $this->site->order_by($objeto->coluna_where, $objeto->orderBy);

        $resultado = $this->site->get($objeto->tabela)->result();

        return $resultado;
    }
    
    public function m_buscar_tudo_inner_join_where_in_orderBy($dados)
    {
        $dados = (object)$dados;
        
        $this->db->select($dados->colunas);
        $this->db->from($dados->nomeDB2.".".$dados->tabela1 .' AS '. $dados->apelido_tabela1);
        $this->db->join($dados->nomeDB1.".".$dados->tabela2 .' AS '. $dados->apelido_tabela2, $dados->apelido_tabela2.".".$dados->coluna_where1 .' = '. $dados->apelido_tabela1.".".$dados->coluna_where2);
        $this->db->where_in($dados->apelido_tabela1.".".$dados->coluna_where3, $dados->valor_coluna_where3);
        $this->db->where($dados->apelido_tabela1.".".$dados->coluna_where2, $dados->id);
        $this->db->order_by($dados->apelido_tabela1.".".$dados->coluna_orderBy, $dados->orderBy);
        
        $resultado = $this->db->get()->result_array();
        
        return $resultado;
    }
    
    public function m_listarInfosLigacoes($dados)
    {

        $objeto = (object)$dados;
        
        $resultado['pendentes']     = $this->site->query("SELECT * FROM $objeto->tabela
                                                                where $objeto->where2 = '".$objeto->whereIn1."'")->num_rows();

        $resultado['andamento']     = $this->site->query("SELECT * FROM $objeto->tabela
                                                                where $objeto->where1 = '".$objeto->id."'
                                                                and $objeto->where2 in ('".$objeto->whereIn2."')
                                                                group by $objeto->groupBy;")->num_rows();

        $resultado['realizadas']    = $this->site->query("SELECT * FROM $objeto->tabela
                                                                where $objeto->where1 = '".$objeto->id."'
                                                                and $objeto->where2 in ($objeto->whereIn3)
                                                                group by $objeto->groupBy;")->num_rows();
        
        return $resultado;
    }
    
    public function m_listarValores($dadosLigacoesPerdidas, $dadosLigacoesVendas, $dadosResidenciaisPerdidas, $dadosResidenciaisVendas, $dadosEmpresariaisPerdidas, $dadosEmpresariaisVendas)
    {

        $ligPerdidas        = (object)$dadosLigacoesPerdidas;
        $ligVendas          = (object)$dadosLigacoesVendas;
        $resiPerdidas       = (object)$dadosResidenciaisPerdidas;
        $resiVendas         = (object)$dadosResidenciaisVendas;
        $emprePerdidas      = (object)$dadosEmpresariaisPerdidas;
        $empreVendas        = (object)$dadosEmpresariaisVendas;
        
        $resultado['ligacoesPerdidas']      = $this->site->query(
                                                "SELECT SUM($ligPerdidas->colunaSum) AS $ligPerdidas->apelido 
                                                FROM $ligPerdidas->tabela
                                                WHERE $ligPerdidas->colunaWhere IN ($ligPerdidas->valorWhere)
                                                AND $ligPerdidas->colunaAnd = $ligPerdidas->id;")->result();
                                                
        $resultado['ligacoesVendas']        = $this->site->query(
                                                "SELECT SUM($ligVendas->colunaSum) AS $ligVendas->apelido 
                                                FROM $ligVendas->tabela
                                                WHERE $ligVendas->colunaWhere IN ($ligVendas->valorWhere)
                                                AND $ligVendas->colunaAnd = $ligVendas->id;")->result();

        $resultado['residenciaisPerdidas']  = $this->site->query(
                                                "SELECT SUM($resiPerdidas->colunaSum) AS $resiPerdidas->apelido 
                                                FROM $resiPerdidas->tabela
                                                WHERE $resiPerdidas->colunaWhere IN ($resiPerdidas->valorWhere)
                                                AND $resiPerdidas->colunaAnd = $resiPerdidas->id;")->result();
                                                
        $resultado['residenciaisVendas']    = $this->site->query(
                                                "SELECT SUM($resiVendas->colunaSum) AS $resiVendas->apelido 
                                                FROM $resiVendas->tabela
                                                WHERE $resiVendas->colunaWhere IN ($resiVendas->valorWhere)
                                                AND $resiVendas->colunaAnd = $resiVendas->id;")->result();
                                                
        $resultado['empresariaisPerdidas']  = $this->site->query(
                                                "SELECT SUM($emprePerdidas->colunaSum) AS $emprePerdidas->apelido 
                                                FROM $emprePerdidas->tabela
                                                WHERE $emprePerdidas->colunaWhere IN ($emprePerdidas->valorWhere)
                                                AND $emprePerdidas->colunaAnd = $emprePerdidas->id;")->result();

        $resultado['empresariaisVendas']    = $this->site->query(
                                                "SELECT SUM($empreVendas->colunaSum) AS $empreVendas->apelido 
                                                FROM $empreVendas->tabela
                                                WHERE $empreVendas->colunaWhere IN ($empreVendas->valorWhere)
                                                AND $empreVendas->colunaAnd = $empreVendas->id;")->result();
                                                
        
        
        return $resultado;
    }

    public function m_listarInfosRelatoriosGeral($dados)
    {
        $objeto = (object)$dados;

        // ========================================
        $resultado[1]['janeiro_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jan_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jan_fim']."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[1]['janeiro_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jan_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jan_fim']."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[1]['janeiro_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jan_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jan_fim']."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[2]['fevereiro_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['fev_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['fev_fim']."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[2]['fevereiro_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['fev_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['fev_fim']."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[2]['fevereiro_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['fev_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['fev_fim']."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[3]['marco_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['mar_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['mar_fim']."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[3]['marco_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['mar_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['mar_fim']."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[3]['marco_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['mar_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['mar_fim']."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[4]['abril_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['abr_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['abr_fim']."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[4]['abril_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['abr_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['abr_fim']."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[4]['abril_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['abr_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['abr_fim']."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[5]['maio_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['mai_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['mai_fim']."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[5]['maio_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['mai_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['mai_fim']."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[5]['maio_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['mai_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['mai_fim']."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[6]['junho_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jun_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jun_fim']."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[6]['junho_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jun_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jun_fim']."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[6]['junho_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jun_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jun_fim']."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================
        
        // ========================================
        $resultado[7]['julho_naoAtendida'] = $this->site->query(
                "SELECT * FROM $objeto->tabela
                    where   $objeto->colunaData >= '".$objeto->datas['jul_ini']."'
                    and     $objeto->colunaData <= '".$objeto->datas['jul_fim']."'
                    and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[7]['julho_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jul_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jul_fim']."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[7]['julho_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jul_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jul_fim']."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[8]['agosto_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['ago_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['ago_fim']."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[8]['agosto_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['ago_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['ago_fim']."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[8]['agosto_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['ago_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['ago_fim']."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[9]['setembro_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['set_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['set_fim']."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[9]['setembro_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['set_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['set_fim']."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[9]['setembro_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['set_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['set_fim']."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[10]['outubro_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['out_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['out_fim']."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[10]['outubro_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['out_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['out_fim']."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[10]['outubro_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['out_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['out_fim']."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[11]['novembro_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['nov_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['nov_fim']."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[11]['novembro_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['nov_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['nov_fim']."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[11]['novembro_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['nov_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['nov_fim']."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[12]['dezembro_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['dez_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['dez_fim']."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[12]['dezembro_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['dez_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['dez_fim']."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[12]['dezembro_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['dez_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['dez_fim']."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        return $resultado;
    }
    
    public function m_listarInfosRelatorios($dados)
    {
        $objeto = (object)$dados;

        // ========================================
        $resultado[1]['janeiro_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jan_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jan_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[1]['janeiro_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jan_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jan_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[1]['janeiro_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jan_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jan_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[2]['fevereiro_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['fev_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['fev_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[2]['fevereiro_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['fev_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['fev_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[2]['fevereiro_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['fev_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['fev_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[3]['marco_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['mar_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['mar_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[3]['marco_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['mar_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['mar_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[3]['marco_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['mar_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['mar_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[4]['abril_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['abr_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['abr_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[4]['abril_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['abr_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['abr_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[4]['abril_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['abr_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['abr_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[5]['maio_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['mai_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['mai_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[5]['maio_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['mai_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['mai_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[5]['maio_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['mai_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['mai_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[6]['junho_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jun_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jun_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[6]['junho_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jun_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jun_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[6]['junho_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jun_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jun_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================
        
        // ========================================
        $resultado[7]['julho_naoAtendida'] = $this->site->query(
                "SELECT * FROM $objeto->tabela
                    where   $objeto->colunaData >= '".$objeto->datas['jul_ini']."'
                    and     $objeto->colunaData <= '".$objeto->datas['jul_fim']."'
                    and     $objeto->where1 = '".$objeto->id."'
                    and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[7]['julho_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jul_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jul_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[7]['julho_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['jul_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['jul_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[8]['agosto_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['ago_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['ago_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[8]['agosto_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['ago_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['ago_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[8]['agosto_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['ago_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['ago_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[9]['setembro_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['set_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['set_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[9]['setembro_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['set_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['set_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[9]['setembro_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['set_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['set_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[10]['outubro_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['out_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['out_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[10]['outubro_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['out_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['out_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[10]['outubro_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['out_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['out_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[11]['novembro_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['nov_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['nov_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[11]['novembro_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['nov_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['nov_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[11]['novembro_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['nov_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['nov_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        // ========================================
        $resultado[12]['dezembro_naoAtendida'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['dez_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['dez_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '2';"
        )->num_rows();

        $resultado[12]['dezembro_vendaRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['dez_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['dez_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '3';"
        )->num_rows();

        $resultado[12]['dezembro_vendaNaoRealizada'] = $this->site->query(
            "SELECT * FROM $objeto->tabela
                where   $objeto->colunaData >= '".$objeto->datas['dez_ini']."'
                and     $objeto->colunaData <= '".$objeto->datas['dez_fim']."'
                and     $objeto->where1 = '".$objeto->id."'
                and     $objeto->where2 = '4';"
        )->num_rows();
        // ========================================

        return $resultado;
    }
    
    public function m_criar($dados, $tabela)
    {
        return $this->site->insert($tabela, $dados);
    }
    
    public function m_atualizar($dados, $infosDB)
    {
        $this->site->where($infosDB->coluna_where, $infosDB->valor_where);
        
        $resultado = $this->site->update($infosDB->tabela, $dados);

        return $resultado;
        
    }
    
    public function m_atualizarTudo($infosDB)
    {
        $resultado = $this->site->query("UPDATE ".$infosDB->tabela." SET ".$infosDB->coluna." = '".$infosDB->valor_zerar."';");
        
        return $resultado;
        
    }
    
    public function m_excluir($infosDB)
    {
        $this->site->where($infosDB->coluna_where, $infosDB->valor_where);

        $result = $this->site->delete($infosDB->tabela);
        
        return $result;
    }
}