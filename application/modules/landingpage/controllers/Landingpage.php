<?php

date_default_timezone_set('America/Sao_Paulo');

defined('BASEPATH') OR exit('No direct script access allowed');
class Landingpage extends MX_Controller {

	function __construct()
    {
        parent::__construct();

        $this->load->model("LandingPage_model", "landingpage");
        $this->load->model("TemplateLandingPage_model", "template");
        $this->load->model("Modelos_model", "modelos");
        $this->load->model("permissoes/Permissoes_model", "permissoes");
        $this->load->model("empresa/Empresa_model", "empresa");
        
        $this->load->library('permitiracesso');
        $this->load->library('emails');
    }
    
	public function index()
	{
        $this->permitiracesso->verifica_sessao();
        
        $view = "v_landingPages";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);

        $nivelAcesso_usuario = $permitirAcessoPagina['dados']['dados']['nivelAcesso_usuario'];

        $permitirAcessoPagina['dados']['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $scripts = array(

            "titulo" => "Landings Page | Solid System",

            "scriptsJS" => "<script src='/assets/libs/custombox/custombox.min.js'></script>
                            <script src='/assets/painel/js/vendor/scriptsLandingPage.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsCSSacima" => "<link href='/assets/libs/custombox/custombox.min.css' rel='stylesheet'>",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view("v_landingPages", $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);

    }
    
    public function v_cadastrarNovaLandingPage()
    {
        $this->permitiracesso->verifica_sessao();
        
        $view = "v_cadastrarNovaLandingPage";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);

        $nivelAcesso_usuario = $permitirAcessoPagina['dados']['dados']['nivelAcesso_usuario'];

        $permitirAcessoPagina['dados']['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $scripts = array(

            "titulo" => "Cadastrar nova Landings Page | Solid System",
            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsLandingPage.js'></script>",
            "scriptsCSS" => "",
            "scriptsJSacima" => "",
            "scriptsCSSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view($permitirAcessoPagina['view'], $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
        
    }
    
    public function v_configuracoes()
    {
        $this->permitiracesso->verifica_sessao();
        
        $view = "v_configuracoes";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);

        $nivelAcesso_usuario = $permitirAcessoPagina['dados']['dados']['nivelAcesso_usuario'];

        $permitirAcessoPagina['dados']['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);

        $resultado = $this->landingpage->buscarConfisgs($id = 1);
        
        $permitirAcessoPagina['dados']['configs'] = $resultado[0];
        
        $scripts = array(

            "titulo" => "Configurações Landings Page | Solid System",
            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsConfiguracoes.js'></script>",
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",
            "scriptsJSacima" => "",
            "scriptsCSSacima" => ""
            
        );
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view("v_configuracoes", $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
        
    }

    //ok
    public function c_cadastrarNovaLandingPage()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "dashboard/cadastrarNovaLandingPage";

        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                $sinal = false;

                $id = $this->landingpage->m_gerarCodigoCriarLandingPage();

                $dados['id_landingPage'] = $id;

                if(empty($this->input->post('nomeLandingPage')))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                } else {

                    $dados['nome_landingPage'] = $this->input->post('nomeLandingPage');
                }

                $tamanhoNome = strlen($this->input->post('nomeLandingPage'));

                if($tamanhoNome > 80)
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>NOME</strong> não pode ser maior que <strong>80</strong> caracteres!<br>';
                    $sinal = true;
                    
                } else {

                    $dados['nome_landingPage'] = $this->input->post('nomeLandingPage');
                }
                
                if(empty($this->input->post('tituloLandingPage')))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>TÍTULO</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                } else {

                    $dados['titulo_landingPage'] = $this->input->post('tituloLandingPage');
                    
                }
                
                if(empty($this->input->post('listaEmailsLandingPage')))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'Por favor, escolha um <strong>GRUPO</strong> para receber os <strong>CADASTROS</strong><br>';
                    $sinal = true;
                    
                } else {

                    $dados['grupoLista_landingPage'] = $this->input->post('listaEmailsLandingPage');
                    
                }
                
                if($sinal)
                {
                    echo json_encode($retorno);
                    exit;
                }
                
                $dados['status_landingPage'] = '1';
                $dados['template_landingPage'] = 1;
                $string = $dados['titulo_landingPage'];
                $nomeLink = str_replace(" ","-", preg_replace("/&([a-z])[a-z]+;/i", "$1", strtolower(htmlentities(trim($string)))));
                
                $dados['link_landingPage'] = base_url()."landingpages/".$id."/".$nomeLink;
                
                $resultado = $this->landingpage->m_CriarLandingPage($dados);

                if($resultado)
                {
                    $retorno['ret'] = true;
                    $retorno['msg'] = ' LandingPage <strong>'. $dados['nome_landingPage'].'</strong> criada com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = ' Não foi possível criar a LandingPage '. $dados['nome_landingPage'].', tente novamente mais tarde!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }
    // ok

    // ok
    public function atualizarConfigsLandingPage()
    {
        
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "landingpages/atualizar/configuracoes";

        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                $sinal = false;
                
                $dados['fone1_landingPage'] = $this->input->post('fone1_configs');
                $dados['fone2_landingPage'] = $this->input->post('fone2_configs');
                $dados['fone1TextoWhats_landingPage'] = $this->input->post('fone1TextoWhats_configs');
                $dados['fone2TextoWhats_landingPage'] = $this->input->post('fone2TextoWhats_configs');

                if(empty($this->input->post('confirmaWhatsapp1'))){$dados['verificaWhatsapp1_landingPage'] = '0';}
                else {$dados['verificaWhatsapp1_landingPage'] = '1';}

                if(empty($this->input->post('confirmaWhatsapp2'))){$dados['verificaWhatsapp2_landingPage'] = '0';}
                else {$dados['verificaWhatsapp2_landingPage'] = '1';}
                
                if(empty($this->input->post('ativoTelefone1'))){$dados['fone1Ativo_landingPage'] = '0';}
                else {$dados['fone1Ativo_landingPage'] = '1';}

                if(empty($this->input->post('ativoTelefone2'))){$dados['fone2Ativo_landingPage'] = '0';}
                else {$dados['fone2Ativo_landingPage'] = '1';}
                
                $dados['linkFacebook_landingPage'] = $this->input->post('facebook_configs');
                $dados['linkInstagram_landingPage'] = $this->input->post('instagram_configs');

                if(empty($this->input->post('facebook_configs_ativo'))){$dados['facebookAtivo_landingPage'] = '0';}
                else {$dados['facebookAtivo_landingPage'] = '1';}

                if(empty($this->input->post('instagram_configs_ativo'))){$dados['instagramAtivo_landingPage'] = '0';}
                else {$dados['instagramAtivo_landingPage'] = '1';}
                
                $dados['linkSite_landingPage'] = $this->input->post('site_configs');
                $dados['linkAlternativo1_landingPage'] = $this->input->post('linkAlternativo1_configs');
                $dados['linkAlternativo2_landingPage'] = $this->input->post('linkAlternativo2_configs');
                $dados['email_landingPage'] = $this->input->post('email_configs');
                $dados['nomeEmail_landingPage'] = $this->input->post('nomeEmail_configs');
                $dados['endereco_landingPage'] = $this->input->post('endereco_configs');
                $dados['googleMaps_landingPage'] = $this->input->post('maps_configs');

                $dados['host_config_landingpage']       = $this->input->post('host_configNotificacaoLeads');
                $dados['usuario_config_landingpage']    = $this->input->post('usuario_configNotificacaoLeads');
                $dados['senha_config_landingpage']      = $this->input->post('senha_configNotificacaoLeads');
                $dados['debug_config_landingpage']      = $this->input->post('debug_configNotificacaoLeads');
                $dados['seguranca_config_landingpage']  = $this->input->post('seguranca_configNotificacaoLeads');
                $dados['auth_config_landingpage']       = $this->input->post('auth_configNotificacaoLeads');
                $dados['porta_config_landingpage']      = $this->input->post('porta_configNotificacaoLeads');
                $dados['encoding_config_landingpage']   = $this->input->post('enconding_configNotificacaoLeads');
                $dados['charset_config_landingpage']    = $this->input->post('charSet_configNotificacaoLeads');
                
                if($sinal)
                {
                    echo json_encode($retorno);
                    exit;
                }

                // SEM IMAGEM SELECIONADA
                // VAI MANTER A IMAGEM ATUAL
                if($_FILES['imagemLogotipo_configs']['size'] === 0 )
                {
                    $id = 1;

                    $resultado = $this->landingpage->buscarConfisgs($id);

                    foreach ($resultado as $key){

                        $dados['logo_landingPage'] = $key->logo_landingPage;
                    }
                }

                if($_FILES['imagemFavicon_configs']['size'] === 0 )
                {
                    $id = 1;

                    $resultado = $this->landingpage->buscarConfisgs($id);

                    foreach ($resultado as $key){

                        $dados['favicon_landingPage'] = $key->favicon_landingPage;
                    }
                }
                
                if(!empty($_FILES['imagemLogotipo_configs']['size']))
                {
                    $id = 1;

                    $result = $this->landingpage->buscarConfisgs($id);

                    foreach ($result as $key) {

                        $logoExiste = 'assets/images/logofavicon/'.$key->logo_landingPage;

                        if (file_exists($logoExiste)) {

                            unlink('assets/images/logofavicon/'.$key->logo_landingPage);

                            $config['upload_path'] = './assets/images/logofavicon';
                            $config['file_name'] = "img_logo_".rand().".jpg";
                            $config['allowed_types'] = '*' ;

                            $dados['logo_landingPage'] = $config['file_name'];

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            $this->upload->do_upload('imagemLogotipo_configs');

                        } else {

                            $config['upload_path'] = './assets/images/logofavicon';
                            $config['file_name'] = "img_logo_".rand().".jpg";
                            $config['allowed_types'] = '*' ;

                            $dados['logo_landingPage'] = $config['file_name'];

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            $this->upload->do_upload('imagemLogotipo_configs');

                        }
                        
                    }

                }

                if(!empty($_FILES['imagemFavicon_configs']['size']))
                {

                    $id = 1;

                    $result2 = $this->landingpage->buscarConfisgs($id);

                    foreach ($result2 as $key2) {

                        $faviconExiste = 'assets/images/logofavicon/'.$key2->favicon_landingPage;

                        if (file_exists($faviconExiste)) {

                            unlink('assets/images/logofavicon/'.$key2->favicon_landingPage);

                            $config2['upload_path'] = './assets/images/logofavicon';
                            $config2['file_name'] = "img_favicon_".rand().".jpg";
                            $config2['allowed_types'] = '*' ;

                            $dados['favicon_landingPage'] = $config2['file_name'];

                            $this->load->library('upload', $config2);
                            $this->upload->initialize($config2);
                            $this->upload->do_upload('imagemFavicon_configs');
                            
                        } else {

                            $config2['upload_path'] = './assets/images/logofavicon';
                            $config2['file_name'] = "img_favicon_".rand().".jpg";
                            $config2['allowed_types'] = '*' ;

                            $dados['favicon_landingPage'] = $config2['file_name'];

                            $this->load->library('upload', $config2);
                            $this->upload->initialize($config2);
                            $this->upload->do_upload('imagemFavicon_configs');
                            
                        }
                        
                    }

                }
                
                $resultado = $this->landingpage->alterarDadosConfigs($dados, $id);

                if ($resultado)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Configurações alteradas com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Falha ao tentar alterar os dados de configurações!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

    }
    // ok

    public function c_listarDadosConfigs()
    {

        $id = 1;

        $resultado = $this->landingpage->buscarConfisgs($id);

        echo json_encode($resultado);

    }

    public function c_listarTodasLandingPages()
    {
        $dadosSessao['dados'] = $this->session->userdata;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $resultado['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 
        $resultado['dados'] = $this->landingpage->m_listarTodasLandingPages();

        echo json_encode($resultado);
    }
    
    public function v_landingPages()
    {
        // CONTADOR DE VIEWS
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[2];
        
        $carregarLandingPage = $this->landingpage->buscarLandingPage($id);
        
        if(!$carregarLandingPage || $carregarLandingPage[0]->status_landingPage === '0')
        {   
            echo $this->load->view('errors/html/404');

        } else {

            $carregarConfig = $this->landingpage->buscarConfisgs($idconf=1);
            $carregarConfigsModeloHead = $this->template->buscarConfigsHead($id);
            $carregarConfigsModeloLogoMenuSlide = $this->template->buscarConfigsLogoMenuSlide($id);
            $carregarConfigsModeloRedesSociais = $this->template->buscarConfigsRedesSociais($id);
            $carregarConfigsModeloServico1 = $this->template->buscarConfigsServico1($id);
            $carregarConfigsModeloServico2 = $this->template->buscarConfigsServico2($id);
            $carregarConfigsModeloServico3 = $this->template->buscarConfigsServico3($id);
            $carregarConfigsModeloServico4 = $this->template->buscarConfigsServico4($id);
            $carregarConfigsModeloServico5 = $this->template->buscarConfigsServico5($id);
            $carregarConfigsModeloServico6 = $this->template->buscarConfigsServico6($id);
            $carregarConfigsModeloServico7 = $this->template->buscarConfigsServico7($id);
            $carregarConfigsModeloServico8 = $this->template->buscarConfigsServico8($id);
            $carregarConfigsModeloServicoPaginaSucesso = $this->template->buscarConfigsPaginaSucesso($id);
            $carregarConfigsModeloMaps = $this->template->buscarConfigsMaps($id);
            $carregarConfigsModeloForms = $this->template->buscarConfigsForms($id);
            $carregarConfigsModeloPersonalizacao = $this->template->buscarConfigsPersonalizacao($id);
            
            $this->load->library('contador');

            $dispositivo = $this->contador->dispositivo();
            
            if($_SERVER['REMOTE_ADDR'] === '192.168.10.1')
            {$ip = "177.30.111.114";}else{$ip = $_SERVER['REMOTE_ADDR'];}
            
            $dados_ip['user_ip'] = $ip;
            
            $contador['cidade_contador']="Goiania";
            $contador['estado_contador']="Goias";
            $contador['pais_contador']="Brazil";
            $contador['ip_contador'] = $dados_ip['user_ip'];            $contador['dispositivo_contador'] = $dispositivo['dispositivo'];
            $contador['navegador_contador'] = $dispositivo['navegador'];
            $contador['SO_contador'] = $dispositivo['sistemaOperacional'];
            $contador['landingPage_contador'] = $carregarLandingPage[0]->id_landingPage;
            
            $contador['data_contador'] = date('Ymd');
            $contador['hora_contador'] = date('Hi');
            $contador['token_contador'] = session_id();
            
            //VERIFICA SE JÁ EXISTE UM TOKEN PARA A PAGINA ATUAL
            $resultToken = $this->landingpage->m_buscarContadorToken($contador['token_contador'], $contador['landingPage_contador']);
            
            if(!$resultToken)
            {
                $this->landingpage->m_contadorDeVisitas($contador);
            }

            $carregarConfigsModelo = (object)array_merge(
                                        (array)$carregarConfigsModeloHead, 
                                        (array)$carregarConfigsModeloLogoMenuSlide,
                                        (array)$carregarConfigsModeloRedesSociais,
                                        (array)$carregarConfigsModeloServico1,
                                        (array)$carregarConfigsModeloServico2,
                                        (array)$carregarConfigsModeloServico3,
                                        (array)$carregarConfigsModeloServico4,
                                        (array)$carregarConfigsModeloServico5,
                                        (array)$carregarConfigsModeloServico6,
                                        (array)$carregarConfigsModeloServico7,
                                        (array)$carregarConfigsModeloServico8,
                                        (array)$carregarConfigsModeloMaps,
                                        (array)$carregarConfigsModeloForms,
                                        (array)$carregarConfigsModeloPersonalizacao,
                                        (array)$carregarConfigsModeloServicoPaginaSucesso
                                    );
                                                        
            foreach ($carregarLandingPage as $dadosLandingPage) {
                
                $configsModelo = array(

                    "titulo" => $dadosLandingPage->titulo_landingPage,
                    "configs" => $carregarConfig[0],
                    "configsModelo" => $carregarConfigsModelo,
                    "url_atual" => $url[0],
                    "dadosLandingPage" => $dadosLandingPage,
                    "tempoOnline" => "<script src='/assets/painel/js/vendor/scriptTempoOnline.js'></script>",
                    "token" =>  $contador['token_contador'],
                    "scriptCSS" => "",
                    "scriptJS" => "<script src='/assets/painel/js/vendor/scriptsLeadsCadastroForms.js'></script>
                                    <script src='/assets/painel/js/vendor/scriptsColetaClicks.js'></script>
                                    <script src='/assets/libs/jquery-mask-plugin/jquery.mask.min.js'></script>
                                    <script src='/assets/libs/form-masks.init.js'></script>
                                    "
                    
                );
                
                $this->load->view('modelos/modelo'
                                    .$dadosLandingPage->template_landingPage.
                                    '/v_modelo'
                                    .$dadosLandingPage->template_landingPage, $configsModelo);

            }
        }
    }

    public function c_tempoOnline()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $token = $this->input->post('token');
        
        $resultado = $this->landingpage->buscarLandingPageToken($token, $id);
        
        $tempo1 = $this->input->post('tempoOnline');
        $tempo2 = $resultado[0]->tempo_contador;

        $tempoTotal = $tempo1+$tempo2;

        $arrayTempo = explode('.', $tempoTotal);
        
        $dados['landingPage_contador'] = $id;
        $dados['token_contador'] = $this->input->post('token');
        $dados['tempo_contador'] = $arrayTempo[0];
        
        $this->landingpage->m_tempoOnline($dados);

    }
    
    public function c_atualizarStatusLandingPage()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "landingpages/atualizar/atualizarStatusLandingPage";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                $sinal = false;
            
                $id = $this->input->post('idLandingPage');
                
                $dados['id_landingPage'] = $id;
                
                $resultado = $this->landingpage->buscarLandingPage($id);
                
                $statusAtual = $resultado[0]->status_landingPage;

                if($statusAtual === '1')
                {

                    $dados['status_landingPage'] = '0';

                } else {

                    $dados['status_landingPage'] = '1';

                }

                if($sinal)
                {
                    echo json_encode($retorno);
                    exit;
                }

                $result = $this->landingpage->m_atualizarStatusLaingPage($dados);

                if ($result)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Status alterado com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível alterar o status!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }
    
    public function c_excluirLandingPages()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "landingpages/excluir/excluirLandingPages";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else { 
        
            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                
                $id= $this->input->post('idLandingPageExcluir');
                
                //EXCLUIR IMAGEM HEAD SEO
                $headSeo = $this->modelos->buscarDados_headSEO($id);
                
                if($headSeo->topo_head_SEO_imagem != 'imagem_default1.png')
                { unlink('assets/images/SEO/'.$headSeo->topo_head_SEO_imagem); }
                //EXCLUIR IMAGEM HEAD SEO

                // ------------------------------------------------------
                
                //EXCLUIR IMAGEM BANNER TOPO
                $logoMenuSlide = $this->modelos->m_buscarLogoMenuSlide($id);
                
                if($logoMenuSlide->body_slide_imagemFundoSlide != 'default2.png')
                { unlink('assets/images/slide/'.$logoMenuSlide->body_slide_imagemFundoSlide); }

                if($logoMenuSlide->body_slide_imagem1 != 'default.png')
                { unlink('assets/images/slide/'.$logoMenuSlide->body_slide_imagem1); }

                if($logoMenuSlide->body_slide_imagem2 != 'default.png')
                { unlink('assets/images/slide/'.$logoMenuSlide->body_slide_imagem2); }
                //EXCLUIR IMAGEM BANNER TOPO
                
                // ------------------------------------------------------
                
                //EXCLUIR IMAGEM SERVIÇO REDES SOCIAIS
                $redesSociais = $this->modelos->buscarDados_redesSociais($id);
                
                $redeSocial = $redesSociais['redesSociais'][0];

                if($redeSocial->body_servico_redesSociais_Imagem1 != 'default.png')
                { unlink('assets/images/social/'.$redeSocial->body_servico_redesSociais_Imagem1); }

                if($redeSocial->body_servico_redesSociais_Imagem2 != 'default.png')
                { unlink('assets/images/social/'.$redeSocial->body_servico_redesSociais_Imagem2); }

                if($redeSocial->body_servico_redesSociais_Imagem3 != 'default.png')
                { unlink('assets/images/social/'.$redeSocial->body_servico_redesSociais_Imagem3); }
                //EXCLUIR IMAGEM SERVIÇO REDES SOCIAIS
                
                // ------------------------------------------------------
                
                //EXCLUIR IMAGEM SERVIÇO 1
                $servicos = $this->modelos->buscarServicos($id);
                
                $servico1 = $servicos['servico1'][0];

                if($servico1->body_servico1Imagem1 != 'default_servico_1.png')
                { unlink('assets/images/servicos/'.$servico1->body_servico1Imagem1); }

                if($servico1->body_servico1Imagem2 != 'default_servico_1.png')
                { unlink('assets/images/servicos/'.$servico1->body_servico1Imagem2); }

                if($servico1->body_servico1Imagem3 != 'default_servico_1.png')
                { unlink('assets/images/servicos/'.$servico1->body_servico1Imagem3); }

                if($servico1->body_servico1Imagem4 != 'default_servico_1.png')
                { unlink('assets/images/servicos/'.$servico1->body_servico1Imagem4); }

                if($servico1->body_servico1Imagem5 != 'default_servico_1.png')
                { unlink('assets/images/servicos/'.$servico1->body_servico1Imagem5); }

                if($servico1->body_servico1Imagem6 != 'default_servico_1.png')
                { unlink('assets/images/servicos/'.$servico1->body_servico1Imagem6); }

                if($servico1->body_servico1Imagem7 != 'default_servico_1.png')
                { unlink('assets/images/servicos/'.$servico1->body_servico1Imagem7); }

                if($servico1->body_servico1Imagem8 != 'default_servico_1.png')
                { unlink('assets/images/servicos/'.$servico1->body_servico1Imagem8); }          
                //EXCLUIR IMAGEM SERVIÇO 1
                
                // ------------------------------------------------------

                //EXCLUIR IMAGEM SERVIÇO 2
                $servico2 = $servicos['servico2'][0];

                if($servico2->body_servico2Imagem1 != 'default_servico_2.png')
                { unlink('assets/images/servicos/'.$servico2->body_servico2Imagem1); }  
                
                // if($servico2->body_servico2Imagem2 != 'default_servico_2.png')
                // { unlink('assets/images/servicos/'.$servico2->body_servico2Imagem2); } 

                // if($servico2->body_servico2Imagem3 != 'default_servico_2.png')
                // { unlink('assets/images/servicos/'.$servico2->body_servico2Imagem3); } 

                // if($servico2->body_servico2Imagem4 != 'default_servico_2.png')
                // { unlink('assets/images/servicos/'.$servico2->body_servico2Imagem4); } 

                // if($servico2->body_servico2Imagem5 != 'default_servico_2.png')
                // { unlink('assets/images/servicos/'.$servico2->body_servico2Imagem5); } 

                // if($servico2->body_servico2Imagem6 != 'default_servico_2.png')
                // { unlink('assets/images/servicos/'.$servico2->body_servico2Imagem6); } 

                // if($servico2->body_servico2Imagem7 != 'default_servico_2.png')
                // { unlink('assets/images/servicos/'.$servico2->body_servico2Imagem7); }

                // if($servico2->body_servico2Imagem8 != 'default_servico_2.png')
                // { unlink('assets/images/servicos/'.$servico2->body_servico2Imagem8); } 
                //EXCLUIR IMAGEM SERVIÇO 2

                // ------------------------------------------------------
                
                //EXCLUIR IMAGEM SERVIÇO 3
                $servico3 = $servicos['servico3'][0];

                if($servico3->body_servico3Imagem1 != 'default_servico_3_1.png')
                { unlink('assets/images/servicos/'.$servico3->body_servico3Imagem1); } 

                if($servico3->body_servico3Imagem2 != 'default_servico_3_1.png')
                { unlink('assets/images/servicos/'.$servico3->body_servico3Imagem2); }

                if($servico3->body_servico3Imagem3 != 'default_servico_3_1.png')
                { unlink('assets/images/servicos/'.$servico3->body_servico3Imagem3); }

                if($servico3->body_servico3Imagem4 != 'default_servico_3_1.png')
                { unlink('assets/images/servicos/'.$servico3->body_servico3Imagem4); }
                
                if($servico3->body_servico3Imagem5 != 'default_servico_3_2.png')
                { unlink('assets/images/servicos/'.$servico3->body_servico3Imagem5); }
                //EXCLUIR IMAGEM SERVIÇO 3

                // ------------------------------------------------------

                //EXCLUIR IMAGEM SERVIÇO 4
                $servico4 = $servicos['servico4'][0];

                if($servico4->body_servico4Imagem1 != 'default_servico_4.png')
                { unlink('assets/images/servicos/'.$servico4->body_servico4Imagem1); }
                //EXCLUIR IMAGEM SERVIÇO 4

                // ------------------------------------------------------

                //EXCLUIR IMAGEM SERVIÇO 5
                $servico5 = $servicos['servico5'][0];

                if($servico5->body_servico5Imagem1 != 'default_servico_5.png')
                { unlink('assets/images/servicos/'.$servico5->body_servico5Imagem1); }

                if($servico5->body_servico5Imagem2 != 'default_servico_5.png')
                { unlink('assets/images/servicos/'.$servico5->body_servico5Imagem2); }

                if($servico5->body_servico5Imagem3 != 'default_servico_5.png')
                { unlink('assets/images/servicos/'.$servico5->body_servico5Imagem3); }

                if($servico5->body_servico5Imagem4 != 'default_servico_5.png')
                { unlink('assets/images/servicos/'.$servico5->body_servico5Imagem4); }

                if($servico5->body_servico5Imagem5 != 'default_servico_5.png')
                { unlink('assets/images/servicos/'.$servico5->body_servico5Imagem5); }

                if($servico5->body_servico5Imagem6 != 'default_servico_5.png')
                { unlink('assets/images/servicos/'.$servico5->body_servico5Imagem6); }

                if($servico5->body_servico5Imagem7 != 'default_servico_5.png')
                { unlink('assets/images/servicos/'.$servico5->body_servico5Imagem7); }

                if($servico5->body_servico5Imagem8 != 'default_servico_5.png')
                { unlink('assets/images/servicos/'.$servico5->body_servico5Imagem8); }

                //EXCLUIR IMAGEM SERVIÇO 5

                // ------------------------------------------------------

                //EXCLUIR IMAGEM SERVIÇO 6
                $servico6 = $servicos['servico6'][0];

                if($servico6->body_servico6Imagem1 != 'default_servico_6.png')
                { unlink('assets/images/servicos/'.$servico6->body_servico6Imagem1); }

                if($servico6->body_servico6Imagem2 != 'default_servico_6.png')
                { unlink('assets/images/servicos/'.$servico6->body_servico6Imagem2); }

                if($servico6->body_servico6Imagem3 != 'default_servico_6.png')
                { unlink('assets/images/servicos/'.$servico6->body_servico6Imagem3); }

                if($servico6->body_servico6Imagem4 != 'default_servico_6.png')
                { unlink('assets/images/servicos/'.$servico6->body_servico6Imagem4); }

                if($servico6->body_servico6Imagem5 != 'default_servico_6.png')
                { unlink('assets/images/servicos/'.$servico6->body_servico6Imagem5); }

                if($servico6->body_servico6Imagem6 != 'default_servico_6.png')
                { unlink('assets/images/servicos/'.$servico6->body_servico6Imagem6); }

                if($servico6->body_servico6Imagem7 != 'default_servico_6.png')
                { unlink('assets/images/servicos/'.$servico6->body_servico6Imagem7); }

                if($servico6->body_servico6Imagem8 != 'default_servico_6.png')
                { unlink('assets/images/servicos/'.$servico6->body_servico6Imagem8); }
                //EXCLUIR IMAGEM SERVIÇO 6
                
                // ------------------------------------------------------

                //EXCLUIR IMAGEM SERVIÇO 7
                $servico7 = $servicos['servico7'][0];

                if($servico7->body_servico7Imagem1 != 'default_servico_7.png')
                { unlink('assets/images/servicos/'.$servico7->body_servico7Imagem1); }

                if($servico7->body_servico7Imagem2 != 'default_servico_7.png')
                { unlink('assets/images/servicos/'.$servico7->body_servico7Imagem2); }

                if($servico7->body_servico7Imagem3 != 'default_servico_7.png')
                { unlink('assets/images/servicos/'.$servico7->body_servico7Imagem3); }

                if($servico7->body_servico7Imagem4 != 'default_servico_7.png')
                { unlink('assets/images/servicos/'.$servico7->body_servico7Imagem4); }

                if($servico7->body_servico7Imagem5 != 'default_servico_7.png')
                { unlink('assets/images/servicos/'.$servico7->body_servico7Imagem5); }

                if($servico7->body_servico7Imagem6 != 'default_servico_7.png')
                { unlink('assets/images/servicos/'.$servico7->body_servico7Imagem6); }

                if($servico7->body_servico7Imagem7 != 'default_servico_7.png')
                { unlink('assets/images/servicos/'.$servico7->body_servico7Imagem7); }

                if($servico7->body_servico7Imagem8 != 'default_servico_7.png')
                { unlink('assets/images/servicos/'.$servico7->body_servico7Imagem8); }
                //EXCLUIR IMAGEM SERVIÇO 7
                
                // =======================================
                
                $result = $this->landingpage->m_excluirLadingPage($id);
                
                if($result)
                {                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Landing Page excluída com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível excluir a Landing Page!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function c_editarLandingPage()
    {

        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "landingpages/atualizar/editarLandingPage";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '1')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            $retorno['msg'] = "";
            $sinal = false;
            
            if(empty($this->input->post('nomeLandingPageEditar')))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
                $sinal = true;
                
            }

            if(empty($this->input->post('tituloLandingPageEditar')))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'O <strong>TÍTULO1</strong> não pode ser vazio!<br>';
                $sinal = true;
                
            }
            
            $dados['id_landingPage'] = $this->input->post('idLandingPageEditar');
            $dados['nome_landingPage'] = $this->input->post('nomeLandingPageEditar');
            $dados['titulo_landingPage'] = $this->input->post('tituloLandingPageEditar');
            
            $landingPage = $this->landingpage->buscarLandingPage($dados['id_landingPage']);
            
            if($this->input->post('listaEmailsLandingPage') === ''){

                $dados['grupoLista_landingPage'] = $landingPage[0]->grupoLista_landingPage;

            } else {

                $dados['grupoLista_landingPage'] = $this->input->post('listaEmailsLandingPage');

            }

            $dados['titulo_landingPage'] = $this->input->post('tituloLandingPageEditar');
            
            if($sinal)
            {
                echo json_encode($retorno);
                exit;
            }
            
            $id = $dados['id_landingPage']; 
                
            $string = $dados['titulo_landingPage'];
            $nomeLink = str_replace(" ","-", preg_replace("/&([a-z])[a-z]+;/i", "$1", strtolower(htmlentities(trim($string)))));
            
            $dados['link_landingPage'] = base_url()."landingpages/".$id."/".$nomeLink;

            $result = $this->landingpage->m_atualizarDadosLanding($dados);
            
            if($result)
            {
                
                $retorno['ret'] = true;
                $retorno['msg'] = 'Landing Page alterada com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = 'Desculpa, não foi possível alterar a Landing Page!';
                echo json_encode($retorno);
            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

    }

    public function v_templateLandingPages()
    {
        $this->permitiracesso->verifica_sessao();
        
        $view = "v_templates";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);

        $nivelAcesso_usuario = $permitirAcessoPagina['dados']['dados']['nivelAcesso_usuario'];

        $permitirAcessoPagina['dados']['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);

        $resultado = $this->landingpage->buscarConfisgs($id = 1);
        
        $permitirAcessoPagina['dados']['configs'] = $resultado[0];
        
        $scripts = array(

            "titulo" => "Templates Landings Page | Solid System",
            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsTemplates.js'></script>",
            "scriptsCSS" => "",
            "scriptsJSacima" => "",
            "scriptsCSSacima" => ""
            
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view("v_templates", $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
        
    }
    
    public function c_cadastrarNovoTemplateLandingPage()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "dashboard/cadastro/cadastrarNovoTemplateLandingPage";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                $sinal = false;

                $id = $this->template->m_gerarCodigoCriarTemplate();

                $dados['id_templates'] = $id;
                
                if(empty($this->input->post('nomeTemplate')))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }

                $dados['nomeTemplate_templates'] = $this->input->post('nomeTemplate');

                if($_FILES['imagemTemplate']['size'] === 0 )
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'A <strong>FOTO</strong> não pode ser vazia!<br>';
                    $sinal = true;
                    
                }
                
                if($sinal)
                {
                    echo json_encode($retorno);
                    exit;
                }
                
                $config['upload_path'] = './assets/images/templates';
                $config['file_name'] = "img_templates_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['imagemTemplate_templates'] = $config['file_name'];

                $result = $this->template->m_criarNovoTemplate($dados);
                
                if($result)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Modelo de template criado com sucesso!';
                    echo json_encode($retorno);

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('imagemTemplate');
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível criar o modelo de template';
                    echo json_encode($retorno);
                }
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

    }

    public function c_listarTodosTemplates()
    {
        $dadosSessao['dados'] = $this->session->userdata;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $resultado['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $resultado['dados'] = $this->template->m_listarTodosTemplates();

        echo json_encode($resultado);
    }
    
    public function c_listarTodosTemplatesLandingPages()
    {
        $resultado = $this->template->m_listarTodosTemplatesLandingPages();

        echo json_encode($resultado);
    }

    public function c_atualizarStatusTemplates()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "dashboard/atualizar/atualizarStatusTemplates";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                
                $id = $this->input->post('idTemplate');

                $dados['id_templates'] = $id;
                
                $resultado = $this->template->buscarTemplate($id);
                
                $statusAtual = $resultado[0]->statusTemplate_templates;

                if($statusAtual === '1')
                {

                    $dados['statusTemplate_templates'] = '0';

                } else {

                    $dados['statusTemplate_templates'] = '1';

                }

                $result = $this->template->m_atualizarStatusTemplate($dados);

                if ($result)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Status alterado com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível alterar o status!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function c_atualizarDadosTemplate()
    {

        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "dashboard/atualizar/atualizarDadosTemplate";

        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                $sinal = false;

                $id = $this->input->post('idAtualizarTemplate');

                $dados['id_templates'] = $id;
                
                $resultado = $this->template->buscarTemplate($id);

                if(empty($this->input->post('nomeAtualizarTemplate')))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                } 
                
                if($sinal)
                {
                    echo json_encode($retorno);
                    exit;
                }

                $dados['nomeTemplate_templates'] = $this->input->post('nomeAtualizarTemplate');

                // SEM IMAGEM SELECIONADA
                // VAI MANTER A IMAGEM ATUAL
                if($_FILES['imagemAtualizarTemplate']['size'] === 0 )
                {
                    $id = $dados['id_templates'];

                    $resultado = $this->template->buscarTemplate($id);
                    
                    foreach ($resultado as $key){

                        $dados['imagemTemplate_templates'] = $key->imagemTemplate_templates;

                        $dados['statusTemplate_templates'] = $key->statusTemplate_templates;
                    }
                }

                if(!empty($_FILES['imagemAtualizarTemplate']['size']))
                {
                    $id = $dados['id_templates'];

                    $result = $this->template->buscarTemplate($id);

                    foreach ($result as $key) {
                        
                        $imagemTemplateExiste = 'assets/images/templates/'.$key->imagemTemplate_templates;

                        if (file_exists($imagemTemplateExiste)) {

                            unlink('assets/images/templates/'.$key->imagemTemplate_templates);

                            $config['upload_path'] = './assets/images/templates';
                            $config['file_name'] = "img_templates_".rand().".jpg";
                            $config['allowed_types'] = '*' ;

                            $dados['imagemTemplate_templates'] = $config['file_name'];

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            $this->upload->do_upload('imagemAtualizarTemplate');

                        } else {

                            $config['upload_path'] = './assets/images/templates';
                            $config['file_name'] = "img_templates_".rand().".jpg";
                            $config['allowed_types'] = '*' ;

                            $dados['imagemTemplate_templates'] = $config['file_name'];

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            $this->upload->do_upload('imagemAtualizarTemplate');

                        }

                        $dados['statusTemplate_templates'] = $key->statusTemplate_templates;
                        
                    }

                }
                
                $resultadoAtualizarDados = $this->template->m_atualizarDadosTemplate($dados);
                
                if ($resultadoAtualizarDados)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Dados do <strong>Template: <i>'.$dados["nomeTemplate_templates"].'</i></strong> alterado com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível alterar os dados do Template!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function c_excluirTemplate()
    {

        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "dashboard/excluir/excluirTemplate";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                
                $id = $this->input->post('idTemplateExcluir');

                $resultado = $this->template->buscarTemplate($id);
                    
                foreach ($resultado as $key){
                    
                    unlink('assets/images/templates/'.$key->imagemTemplate_templates);
                }
                
                $result = $this->template->m_excluirTemplate($id);
                
                if($result)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Template <strong>('.$id.')</strong> excluído com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível excluir o template!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function c_atualizarTemplateSelecionadoLandingPage()
    {

        $retorno['msg'] = "";
        
        $dados['id_landingPage'] = $this->input->post('idLandingPage');
        $dados['template_landingPage'] = $this->input->post('idTemplate');
        

        $resultado = $this->template->m_atualizarTemplateSelecionadoLandingPage($dados);

        if($resultado)
        {
            
            $retorno['ret'] = true;
            $retorno['msg'] = 'Modelo de template atualizado com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Desculpa, não foi possível atualizar o modelo de template!';
            echo json_encode($retorno);
        }
        
    }
    
    public function v_selecionarTemplate()
    {
        $view = "v_selecionarTemplates";

        $acessoPermitido = $this->permitiracesso->verificaAcessoPaginaParametro($view);
        
        $scripts = array(

            "titulo" => "Selecionar Template | Solid System",
            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsTemplates.js'></script>",
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",
            "scriptsJSacima" => "",
            "scriptsCSSacima" => ""
            
        );

        $resultado = $this->landingpage->buscarConfisgs(1);
        
        $templates = $this->template->m_listarTodosTemplatesLandingPages();
        
        $acessoPermitido['dados']['configs'] = $resultado[0];
        
        $acessoPermitido['dados']['templates'] = $templates;
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo', $acessoPermitido['dados']);
        $this->load->view('painel/Template/t_menuLateral');
        $this->load->view("v_selecionarTemplates", $acessoPermitido['dados']['dados']);
        $this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
    }

    public function v_configurarTemplate()
    {
        $this->permitiracesso->verifica_sessao();

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];
        
        $carregarLandingPage = $this->landingpage->buscarLandingPage($id);
        $carregarConfig = $this->landingpage->buscarConfisgs($idConf = 1);
        
        $carregarConfigsModeloHead = $this->template->buscarConfigsHead($id);
        $carregarConfigsModeloLogoMenuSlide = $this->template->buscarConfigsLogoMenuSlide($id);
        $carregarConfigsModeloRedesSociais = $this->template->buscarConfigsRedesSociais($id);
        $carregarConfigsModeloServico1 = $this->template->buscarConfigsServico1($id);
        $carregarConfigsModeloServico2 = $this->template->buscarConfigsServico2($id);
        $carregarConfigsModeloServico3 = $this->template->buscarConfigsServico3($id);
        $carregarConfigsModeloServico4 = $this->template->buscarConfigsServico4($id);
        $carregarConfigsModeloServico5 = $this->template->buscarConfigsServico5($id);
        $carregarConfigsModeloServico6 = $this->template->buscarConfigsServico6($id);
        $carregarConfigsModeloServico7 = $this->template->buscarConfigsServico7($id);
        $carregarConfigsModeloServico8 = $this->template->buscarConfigsServico8($id);
        $carregarConfigsModeloServicoPaginaSucesso = $this->template->buscarConfigsPaginaSucesso($id);
        $carregarConfigsModeloMaps = $this->template->buscarConfigsMaps($id);
        $carregarConfigsModeloForms = $this->template->buscarConfigsForms($id);
        $carregarConfigsModeloPersonalizacao = $this->template->buscarConfigsPersonalizacao($id);
        
        $carregarConfigsModelo = (object)array_merge(
            (array)$carregarConfigsModeloHead, 
            (array)$carregarConfigsModeloLogoMenuSlide,
            (array)$carregarConfigsModeloRedesSociais,
            (array)$carregarConfigsModeloServico1,
            (array)$carregarConfigsModeloServico2,
            (array)$carregarConfigsModeloServico3,
            (array)$carregarConfigsModeloServico4,
            (array)$carregarConfigsModeloServico5,
            (array)$carregarConfigsModeloServico6,
            (array)$carregarConfigsModeloServico7,
            (array)$carregarConfigsModeloServico8,
            (array)$carregarConfigsModeloMaps,
            (array)$carregarConfigsModeloForms,
            (array)$carregarConfigsModeloPersonalizacao,
            (array)$carregarConfigsModeloServicoPaginaSucesso
        );
        
        foreach ($carregarLandingPage as $dadosLandingPage) {

            $dadosSessao['dados'] = $this->session->userdata;

            $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
            $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

            $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
            $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
            $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
            $dadosSessao['paginas'] = $permissaoAcesso;
            
            $scripts = array(
                "titulo" => "Configurar modelo".$carregarConfigsModelo->id_modelo,

                "scriptsJS" => "<script src='/assets/libs/ckeditor/ckeditor.js'></script>
                                <script src='/assets/painel/js/vendor/scriptsModelo1.js'></script>",

                "scriptsCSS" => "<script src='/modelos/modelo1/js/jquery-3.4.1.min.js'></script>
                <link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

                "scriptsJSacima" => "",
                "scriptsCSSacima" => "",
            );
            
            $configsModelo = array(
                "configs" => $carregarConfig[0],
                "configsModelo" => $carregarConfigsModelo,
                "url_atual" => $url[0],
                "dadosLandingPage" => $dadosLandingPage
            );
            
            $this->load->view('painel/Template/t_head', $scripts);
            $this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
            $this->load->view('painel/Template/t_menuLateral');
            $this->load->view('modelos/modelo'.$carregarLandingPage[0]->template_landingPage.'/v_configurarTemplate', $dadosSessao);
            $this->load->view('painel/Template/t_footer');
            $this->load->view('painel/Template/t_scripts', $scripts);

        }
    }

    public function c_leadsCadastroForm1()
    {

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $dados['id_cadastros'] = $this->landingpage->m_gerarCodigoLeadsCadastros();
        $dados['nome_cadastros'] = $this->input->post('nomeVisitante');
        $dados['telefone_cadastros'] = "+55".preg_replace('/[^\d]/', '',$this->input->post('telefoneVisitante'));
        $dados['email_cadastros'] = $this->input->post('emailVisitante');
        $dados['ondeNosConheceu_cadastros'] = $this->input->post('ondeNosConheceu');
        $dados['landingPage_cadastros'] = $id;
        $dados['grupoLista_cadastros'] = $this->input->post('grupoLista_landingPage');
        
        $sinal = false;
        $retorno['msg'] = "";
        
        if(empty($dados['nome_cadastros']))
		{
            $retorno['ret'] = false;
            $retorno['msg'] = ' Por favor, preencha todos os campos!!';
            $sinal = true;
            
        } 

        if(empty($dados['telefone_cadastros']))
		{
            $retorno['ret'] = false;
            $retorno['msg'] = ' Por favor, preencha todos os campos!!';
            $sinal = true;
            
        }

        if(empty($dados['email_cadastros']))
		{
            $retorno['ret'] = false;
            $retorno['msg'] = ' Por favor, preencha todos os campos!!';
            $sinal = true;
            
        }
        

        if(empty($dados['ondeNosConheceu_cadastros']))
		{
            $retorno['ret'] = false;
            $retorno['msg'] = ' Por favor, preencha todos os campos!!';
            $sinal = true;
            
        }
        
        if($sinal)
		{
            echo json_encode($retorno);
            exit;
        }
        
        $ip_user = $_SERVER["REMOTE_ADDR"];
        
        $dados_ip = file_get_contents('http://ip-api.com/json/');
        $result_ip = json_decode($dados_ip);
        
        $dados['cidade_cadastros'] = $result_ip->city;
        $dados['estado_cadastros'] = $result_ip->regionName;

        $dados['data_cadastros'] = date('Ymd');
        $dados['hora_cadastros'] = date('Hi');

        $dadosEmpresa = (object)$this->empresa->m_listarInformacoesEmpresa();

        $dadosLandingPage = $this->landingpage->buscarLandingPage($id);

        $telefone = $this->input->post('telefoneVisitante');

        $configsLandingPage = $this->landingpage->buscarConfisgs($id = 1);

        $disparar_email = $this->emails->notificaLeadEmail($dados, $dadosEmpresa, $dadosLandingPage, $telefone, $configsLandingPage);
  
        $resultado = $this->landingpage->m_leadsCadastroForm($dados);
        
        if($resultado)
        {   
            $retorno['ret'] = true;
            
            echo json_encode($retorno);
            
        } else {
            
            $retorno['ret'] = false;
            $retorno['id'] = $dados['id_cadastros'];
            $retorno['msg'] = 'Desculpa, não foi possível atualizar o modelo de template!';

            echo json_encode($retorno);
        }
    
    }

    public function v_cadastroLeadsSucesso()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[2];
        
        $carregarLandingPage = $this->landingpage->buscarLandingPage($id);
        $carregarConfig = $this->landingpage->buscarConfisgs($idconf=1);
        $carregarConfigsModeloServicoPaginaSucesso = $this->template->buscarConfigsPaginaSucesso($id);
        $carregarConfigsModeloPersonalizacao = $this->template->buscarConfigsPersonalizacao($id);

        $carregarConfigsModelo = (object)array_merge(
            (array)$carregarLandingPage[0], 
            (array)$carregarConfig[0], 
            (array)$carregarConfigsModeloServicoPaginaSucesso,
            (array)$carregarConfigsModeloPersonalizacao

        );
        
        $configsModelo = array(

            "titulo" => "Cadastro realizado com sucesso!",
            "configs" => $carregarConfig[0],
            "dadosLandingPage" => $carregarConfigsModelo,
            "url_atual" => $url[0],
            "scriptCSS" => "",
            "scriptJS" => ""
            
        );
        
        $this->load->view('modelos/modelo'
                                .$carregarConfigsModelo->template_landingPage.
                                '/v_landingPageSucesso', $configsModelo);
        
    }

    public function c_clicks()
    {
    
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $dados['id_clicks'] = $this->landingpage->m_gerarCodigoClicks();
        $dados['idLandingPage_clicks'] = $id;
        $dados['token_clicks'] = $this->input->post('token');
        $dados['nomeBotao_clicks'] = $this->input->post('botao');
        $dados['linkBotao_clicks'] = $this->input->post('link');
        $dados['dataClick__clicks'] = date('Ymd');
        $dados['horaClick__clicks'] = date('Hi');
        
        $resultado = $this->landingpage->buscarLandingPageClicksToken($dados['token_clicks'], $id, $dados['nomeBotao_clicks']);
        
        if(empty($resultado))
        {
            $this->landingpage->m_clicks($dados);
        }
        
    }

    public function c_listarGrupos()
    {
    
        $resultado = $this->landingpage->m_listarGrupos();

        echo json_encode($resultado);

    }

    public function v_gruposLandingPage()
    {
        $this->permitiracesso->verifica_sessao();
        
        $view = "v_grupos";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);

        $nivelAcesso_usuario = $permitirAcessoPagina['dados']['dados']['nivelAcesso_usuario'];

        $permitirAcessoPagina['dados']['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);

        $resultado = $this->landingpage->buscarConfisgs($id = 1);
        
        $permitirAcessoPagina['dados']['configs'] = $resultado[0];
        
        $scripts = array(

            "titulo" => "Criar grupos para landing pages | Solid System",

            "scriptsJS" => "<script src='/assets/libs/custombox/custombox.min.js'></script>
                            <script src='/assets/painel/js/vendor/scriptsGrupo.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsCSSacima" => "<link href='/assets/libs/custombox/custombox.min.css' rel='stylesheet'>",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view("v_grupos", $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
        
    }
    
}
