
<style>
.fundo_card-body
{
    background-color: #373e48;
}

.custom-file-input:disabled~.custom-file-label {
    background-color: #4b5460;
}

</style>

<div class="container-fluid">
    
    <div class="row mt-2">
        
        <div class="col-12">

            <div class="page-title-box">
				<ol class="breadcrumb m-0">
					<li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
					<li class="breadcrumb-item"><a href="/dashboard/landingPages">Landing pages</a></li>
					<li class="breadcrumb-item active text-warning">Configurações</li>
				</ol>
			</div>
            
            <?php $this->load->view('botoesLandingPages'); ?>

            <div class="row mt-2">
                <div class="col-lg-12">
                    <!-- Portlet card -->
                    <div class="card">
                        <div class="card-body">
                            
                            <h5 class="card-title mb-3">Configurações gerais <small class="text-warning">(Aplica-se a todas as landingpages)</small></h5>

                            <form id="formConfiguracoes" enctype="multipart/form-data">

                                <div class="row">
                                    <div class="col-12">
                                        
                                        <div class="col-md-12">
                                            <div id="erroMsg"></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xl-12">

                                                <div id="accordion" class="mb-3">
                                                    
                                                    <div class="card mb-1">
                                                        <div class="card-header" id="headingOne">
                                                            <h5 class="m-0">
                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false">
                                                                    <i class="la la-phone mr-1 text-mute"></i> 
                                                                    Telefone e Whatsapp
                                                                </a>
                                                            </h5>
                                                        </div>
                                            
                                                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                                            <div class="card-body">
                                                                
                                                                <div class="form-row">
                                                                    <div class="form-group col-md-4">
                                                                        <label class="col-form-label">Fone 1 <small class="text-warning">(Sem ( ), sem traço e espaço, ex: 62999999999)</small></label>
                                                                        <input type="text" class="form-control" id="fone1_configs" name="fone1_configs" autocomplete="off">
                                                                    </div>

                                                                    <div class="form-group col-md-6">
                                                                        <label class="col-form-label">Texto/mensagem para o whatsapp <small class="text-warning">(Se marcado como whatsapp)</small></label>
                                                                        <input type="text" class="form-control" id="fone1TextoWhats_configs" name="fone1TextoWhats_configs" autocomplete="off">
                                                                    </div>

                                                                    <div class="form-group col-md-1">
                                                                        <label class="col-form-label">Whats?</label> <br>
                                                                        <input type="checkbox" class="switch_1 mt-1" id="confirmaWhatsapp1" name="confirmaWhatsapp1">
                                                                    </div>

                                                                    <div class="form-group col-md-1">
                                                                        <label class="col-form-label">Ativo?</label> <br>
                                                                        <input type="checkbox" class="switch_1 mt-1" id="ativoTelefone1" name="ativoTelefone1">
                                                                    </div>
                                                                    
                                                                    <div class="form-group col-md-4">
                                                                        <label class="col-form-label">Fone 2 <small class="text-warning">(Sem ( ), sem traço e espaço, ex: 62999999999)</small></label>
                                                                        <input type="text" class="form-control" id="fone2_configs" name="fone2_configs" autocomplete="off">
                                                                    </div>

                                                                    <div class="form-group col-md-6">
                                                                        <label class="col-form-label">Texto/mensagem para o whatsapp <small class="text-warning">(Se marcado como whatsapp)</small></label>
                                                                        <input type="text" class="form-control" id="fone2TextoWhats_configs" name="fone2TextoWhats_configs" autocomplete="off">
                                                                    </div>

                                                                    <div class="form-group col-md-1">
                                                                        <label class="col-form-label">Whats?</label> <br>
                                                                        <input type="checkbox" class="switch_1 mt-1" id="confirmaWhatsapp2" name="confirmaWhatsapp2">
                                                                    </div>

                                                                    <div class="form-group col-md-1">
                                                                        <label class="col-form-label">Ativo?</label> <br>
                                                                        <input type="checkbox" class="switch_1 mt-1" id="ativoTelefone2" name="ativoTelefone2">
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="card mb-1">
                                                        <div class="card-header" id="headingTwo">
                                                            <h5 class="m-0">
                                                                <a class="text-dark" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">
                                                                    <i class="la la-link mr-1 text-mute"></i> 
                                                                    Links
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                                            <div class="card-body">
                                                                
                                                                <div class="form-row">

                                                                    <div class="form-group col-md-5">
                                                                        <label class="col-form-label">Facebook <small class="text-warning">(Sem o @, ex: joaodasilva)</small></label>
                                                                        <input type="text" class="form-control" id="facebook_configs" name="facebook_configs" autocomplete="off">
                                                                    </div>

                                                                    <div class="form-group col-md-1">
                                                                        <label class="col-form-label">Ativo?</label> <br>
                                                                        <input type="checkbox" class="switch_1 mt-1" id="facebook_configs_ativo" name="facebook_configs_ativo">
                                                                    </div>
                                                                    
                                                                    <div class="form-group col-md-5">
                                                                        <label class="col-form-label">Instagram <small class="text-warning">(Sem o @, ex: joaodasilva)</small></label>
                                                                        <input type="text" class="form-control" id="instagram_configs" name="instagram_configs" autocomplete="off">
                                                                    </div>

                                                                    <div class="form-group col-md-1">
                                                                        <label class="col-form-label">Ativo?</label> <br>
                                                                        <input type="checkbox" class="switch_1 mt-1" id="instagram_configs_ativo" name="instagram_configs_ativo">
                                                                    </div>

                                                                    <div class="form-group col-md-12">
                                                                        <label class="col-form-label">Site</label>
                                                                        <input type="text" class="form-control" id="site_configs" name="site_configs" autocomplete="off">
                                                                    </div>

                                                                    <div class="form-group col-md-6">
                                                                        <label class="col-form-label">Link alternativo 1 <small class="text-warning">(Opcional, deixar vazio se não houver.)</small></label>
                                                                        <input type="text" class="form-control" id="linkAlternativo1_configs" name="linkAlternativo1_configs" autocomplete="off">
                                                                    </div>

                                                                    <div class="form-group col-md-6">
                                                                        <label class="col-form-label">Link alternativo 2 <small class="text-warning">(Opcional, deixar vazio se não houver.)</small></label>
                                                                        <input type="text" class="form-control" id="linkAlternativo2_configs" name="linkAlternativo2_configs" autocomplete="off">
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="card mb-1">
                                                        <div class="card-header" id="headingThree">
                                                            <h5 class="m-0">
                                                                <a class="text-dark" data-toggle="collapse" href="#collapseThree" aria-expanded="false">
                                                                    <i class="la la-map-marker mr-1 text-mute"></i>
                                                                    Contato
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                                            <div class="card-body">
                                                                
                                                                <div class="form-row">
                                                        
                                                                    <div class="form-group col-md-3">
                                                                        <label class="col-form-label">E-mail <small class="text-warning">(Para receber os leads por e-mail)</small></label>
                                                                        <input type="email" class="form-control" id="email_configs" name="email_configs" autocomplete="off">
                                                                    </div>

                                                                    <div class="form-group col-md-4">
                                                                        <label class="col-form-label">Nome <small class="text-warning">(Identificação ao receber leads por e-mail)</small></label>
                                                                        <input type="text" class="form-control" id="nomeEmail_configs" name="nomeEmail_configs" autocomplete="off">
                                                                    </div>

                                                                    <div class="form-group col-md-5">
                                                                        <label class="col-form-label">Endereço</label>
                                                                        <input type="text" class="form-control" id="endereco_configs" name="endereco_configs" autocomplete="off">
                                                                    </div>

                                                                    <div class="form-group col-md-12 mb-3">
                                                                        <label for="example-textarea"> Google Maps <small class="text-warning">(Colar o IFRAME de: Compartilhar -> incorporar um mapa)</small></label>
                                                                        <i class="mdi mdi-help-circle" data-toggle="tooltip" data-placement="top" title="Colocar width='100%'" data-original-title="Colocar width='100%'"></i>
                                                                        <textarea class="form-control" id="maps_configs" name="maps_configs" rows="5"></textarea>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                        
                                                    <div class="card mb-1">
                                                        <div class="card-header" id="headingFour">
                                                            <h5 class="m-0">
                                                                <a class="text-dark" data-toggle="collapse" href="#collapseFour" aria-expanded="false">
                                                                    <i class="la la-file-image-o mr-1 text-mute"></i> 
                                                                    Logo e Favicon
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseFour" class="collapse" aria-labelledby="collapseFour" data-parent="#accordion">
                                                            <div class="card-body">
                                                                
                                                                <div class="form-row">
                                                                    
                                                                    <div class="col-md-6">
                                                                        <label class="col-form-label">Logotipo
                                                                        <small class="text-warning"> (Minímo 500px)</small>
                                                                            <i class="mdi mdi-help-circle" data-toggle="tooltip" data-placement="bottom" title="Utilizar uma imagem com minímo 500px" data-original-title="Utilizar uma imagem com minímo 500px">
                                                                            </i>
                                                                        </label>
                                                                        <div class="custom-file">
                                                                            <input class="custom-file-input" type='file' name="imagemLogotipo_configs" id="imagemLogotipo_configs" onchange="readURL(this);" >
                                                                            <label class="custom-file-label">Procurar Logotipo..</label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="form-row">
                                                                            <div class="form-group col-md-12">
                                                                                <label class="col-form-label">Favicon 
                                                                                    <small class="text-warning">(16x16)</small>
                                                                                    <i class="mdi mdi-help-circle" data-toggle="tooltip" data-placement="bottom" title="" 
                                                                                        data-original-title="Tamanho da imagem: 16px x 16px">
                                                                                    </i>
                                                                                </label>
                                                                                <div class="custom-file">
                                                                                    <input class="custom-file-input" type='file' name="imagemFavicon_configs" id="imagemFavicon_configs" onchange="readURLFavicon(this);" >
                                                                                    <label class="custom-file-label">Procurar Favicon..</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">

                                                                        <div class="form-row">
                                                                            <div class="form-group col-md-12">
                                                                                <label class="col-form-label">Preview</label>
                                                                                
                                                                                <div class="card">
                                                                                    <img class="img-fluid w-25" id="previewLogo">
                                                                                </div>

                                                                            </div>
                                                                            
                                                                        </div>

                                                                    </div>

                                                                    <div class="col-md-6">

                                                                        <div class="form-row">
                                                                            <div class="form-group col-md-12">
                                                                                <label class="col-form-label">Preview</label>
                                                                                
                                                                                <div class="card">
                                                                                    <img class="img-fluid w-25" id="previewFavicon">
                                                                                </div>

                                                                            </div>
                                                                            
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="card mb-1">
                                                        <div class="card-header" id="headingFour">
                                                            <h5 class="m-0">
                                                                <a class="text-dark" data-toggle="collapse" href="#collapseFive" aria-expanded="false">
                                                                    <i class="la la-file-image-o mr-1 text-mute"></i> 
                                                                    Notificação de novo lead por e-mail
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseFive" class="collapse" aria-labelledby="collapseFive" data-parent="#accordion">
                                                            <div class="card-body">
                                                                
                                                                <div class="form-row">
                                                                    
                                                                <div class="card">
                                                                        <div class="card-body" style="background-color: #353b44;">
                                                                        
                                                                            <h6 class="text-center text-warning">OBS: NÃO FAÇA ALTERAÇÕES SE NÃO SOUBER SOBRE CONFIGURAÇÕES DE E-MAILS</h6>
                                                        
                                                                            
                                                                            <div class="form-row">
                                                                                
                                                                                <div class="form-group col-md-4">
                                                                                    <label>Host</label>
                                                                                    <input type="text" id="host_configNotificacaoLeads" name="host_configNotificacaoLeads" class="form-control">
                                                                                </div>
                                                                                
                                                                                <div class="form-group col-md-4">
                                                                                    <label>Usuário</label>
                                                                                    <input type="text" id="usuario_configNotificacaoLeads" name="usuario_configNotificacaoLeads" class="form-control">
                                                                                </div>

                                                                                <div class="form-group col-md-4">
                                                                                    <label>Senha</label>
                                                                                    <input type="text" id="senha_configNotificacaoLeads" name="senha_configNotificacaoLeads" class="form-control">
                                                                                </div>
                                                                                
                                                                                <div class="form-group col-md-2">  
                                                                                    <label>DEBUG <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="0 = Não mostra o debug, 1 = mostra algumas informações, 2 = mostra debug completo"></i></label>
                                                                                    <select class="form-control" id="debug_configNotificacaoLeads" name="debug_configNotificacaoLeads"></select>
                                                                                </div>

                                                                                <div class="form-group col-md-2">
                                                                                    <label>Segurança <small class="text-warning" style="font-size:8px;">(SSL/TLS)</small></label>
                                                                                    <select class="form-control" id="seguranca_configNotificacaoLeads" name="seguranca_configNotificacaoLeads"></select>
                                                                                </div>

                                                                                <div class="form-group col-md-2">
                                                                                    <label>Autenticação <small class="text-warning" style="font-size:8px;">(TRUE/FALSE)</small></label>
                                                                                    <select class="form-control" id="auth_configNotificacaoLeads" name="auth_configNotificacaoLeads"></select>
                                                                                </div>
                                                                                
                                                                                <div class="form-group col-md-2">
                                                                                    <label>Porta</label>
                                                                                    <input type="text" id="porta_configNotificacaoLeads" name="porta_configNotificacaoLeads" class="form-control">
                                                                                </div>

                                                                                <div class="form-group col-md-2">
                                                                                    <label>Encoding</label>
                                                                                    <input type="text" id="enconding_configNotificacaoLeads" name="enconding_configNotificacaoLeads" class="form-control">
                                                                                </div>
                                                                                
                                                                                <div class="form-group col-md-2">
                                                                                    <label>CharSet</label>
                                                                                    <input type="text" id="charSet_configNotificacaoLeads" name="charSet_configNotificacaoLeads" class="form-control">
                                                                                </div>
                                                                                
                                                                            </div>
                                                                            
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>

                                <button type="submit" id="botaoAtualizarConfigs" class="btn btn-block btn-sm btn-primary waves-effect waves-light">Atualizar</button>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
