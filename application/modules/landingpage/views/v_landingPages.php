<div class="container-fluid">
    
    <div class="row mt-2">
        
        <div class="col-12">

            <div class="page-title-box">
				<ol class="breadcrumb m-0">
					<li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
					<li class="breadcrumb-item active text-warning">Landing pages</li>
				</ol>
			</div>
        
            <?php
                $this->load->view('botoesLandingPages');
            ?>
            
            
            <div class="row mt-2">
                <div class="col-lg-12">
                    
                <div id="msgSucesso"></div>

                    <div class="card">
                    
                        <div class="card-body">
                            
                            <h5 class="card-title mb-4">Landings Pages cadastradas
                                
                                <div class="pull-right mt-2 mb-2">

                                    <div class="button-list mt-2 mb-2">
                                        
                                            <a href="/dashboard/cadastrarNovaLandingPage" class="mt-2">
                                                <button type="button" class="btn btn-xs btn-info" data-toggle="tooltip" 
                                                        data-placement="top" title="Cadastrar nova landing page!" 
                                                        data-original-title="Cadastrar nova landing page!">
                                                    <i class="fe-plus"></i></span> Criar nova landing page
                                                </button>
                                            </a>
                                            
                                            <a href="/dashboard/gruposLandingPage"  class="mt-2">
                                                <button type="button" class="btn btn-xs btn-light" data-toggle="tooltip" 
                                                        data-placement="top" title="Cadastrar novo grupo de captura de contato!" 
                                                        data-original-title="Cadastrar novo grupo de captura de contato!">
                                                    <i class="fe-plus"></i></span> Criar nova lista
                                                </button>
                                            </a> 
                                            
                                        <div class="card-widgets" id="atualizaDivLandingPages"></div>
                                        
                                    </div>

                                </div>

                            </h5>
                            
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                
                                    <thead>

                                        <tr>
                                            <th>#</th>

                                            <th>Nome</th>

                                            <th>Leads</th>

                                            <th>Status</th>

                                            <th>Template</th>

                                            <th>Ações</th>

                                        </tr>

                                    </thead>

                                    <tbody id="tabelaListarLandingPages"></tbody>
                                        
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>     
    
</div>

<div class="modal fade bs-example-modal-lg" id="modalEditarLandingPage" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Editar as informações da LandingPage: <span class="text-warning" id="tituloNomeLandingPageEditar"></span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <form id="formEditarLandingPage" enctype="multipart/form-data">

                <div class="row">
                    <div class="col-12">
                        <div class="col-md-12">
                            <div id="erroMsgEditarLandingPage">
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                
                                <h4 class="header-title">Nome e Título</h4>

                                <div class="form-row">
                                    
                                    <div class="form-group col-md-4">
                                        <label class="col-form-label">Nome (<small>Nome na lista</small>)</label>
                                        <input type="text" class="form-control" id="nomeLandingPageEditar" name="nomeLandingPageEditar" autocomplete="off">
                                    </div>

                                    <input type="hidden" name="idLandingPageEditar" id="idLandingPageEditar">

                                    <div class="form-group col-md-4">
                                        <label class="col-form-label">Título (<small>Nome na URL</small>)</label>
                                        <input type="text" class="form-control" id="tituloLandingPageEditar" name="tituloLandingPageEditar" autocomplete="off">
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label class="col-form-label">Lista (<small>Grupo para envio dos e-mails</small>)</label>
                                        <select class="form-control" id="listaEmailsLandingPage" name="listaEmailsLandingPage">
                                        </select>
                                    </div>

                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">Atualizar</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                </div>

            </form>
            
        </div>
    </div>
</div>

<div id="modalAtualizarStatus" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="mySmallModalLabel">Atualizar status</h4>
                <button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <form id="formAtualizaStatus">
                <div class="modal-body">
                    
                    <div id="msgErroAtualizarStatus"></div>

                    Tem certeza que deseja atualizar o status dessa landing page?
                    <input type="hidden" name="idLandingPage" id="idLandingPage">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">(Sim) Atualizar</button>
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="modalExcluirLandingPage" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="mySmallModalLabel">Excluir LandingPages (<span id="idLanding"></span>)</h4>
                <button type="button" id="fecharModalExcluirLandingPage" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <form id="formExcluirLandingPage">
                <div class="modal-body">
                    
                    <div id="msgErroExcluirLandingPage"></div>

                    Tem certeza que deseja excluir essa landing page?
                    <input type="hidden" name="idLandingPageExcluir" id="idLandingPageExcluir">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger waves-effect">(Sim) Excluir</button>
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>
    