<div class="row">

    <div class="col-xl-12">

        <div id="accordion">
        
            <div class="card mb-1">
                
                <a class="text-dark collapsed text-center" data-toggle="collapse" href="#menuNavegacaoPainel" aria-expanded="false">
                    <div class="card-header" id="headingOne">
                        <h5 class="m-0 text-center">
                            Menu de navegação
                        </h5>
                    </div>
                </a>

                <div id="menuNavegacaoPainel" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="">
                    <div class="card-body d-flex justify-content-center">

                        <a href="/dashboard/landingPages" data-toggle="tooltip" data-placement="top" data-original-title="Todas as landing pages">
                            <button type="button" class="btn mr-1 bg-soft-primary text-white">Landing pages</button>
                        </a>
                            
                        <a href="/dashboard/configuracoes" data-toggle="tooltip" data-placement="top" data-original-title="Aplica-se a todas as landingpages">
                            <button type="button" class="btn mr-1 bg-soft-danger text-white">Configurações gerais</button>
                        </a>
                        
                        <a href="/dashboard/templates" data-toggle="tooltip" data-placement="top" data-original-title="Configurações de templates">
                            <button type="button" class="btn mr-1 btn-danger text-white">Templates</button>
                        </a>

                    </div>

                </div>

            </div>

        </div>

    </div>
    
</div>
