<div class="container-fluid">
    
    <div class="row mt-4">
        
        <div class="col-12">
            
            <div class="row">

                <div class="col-lg-12">

                    <div id="erroMsg"></div>
                    
                    <div class="card">

                        <div class="card-body">
                            
                            <h5 class="card-title mb-3">Minha conta</h5>
                            
                            <div class="row">
                                
                                <div class="col-lg-12 col-xl-12">
                                    
                                    <div class="card-box">
                                        
                                        <form id="formMinhaContaUsuario">

                                        <input type="hidden" name="idUsuariologado" id="idUsuariologado">
                                            
                                            <div class="row">
                                                
                                                <div class="col-sm-12 col-md-8 col-lg-8">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="firstname">Login</label>
                                                            <input type="text" class="form-control" id="loginUsuarioLogado" name="loginUsuarioLogado" readonly autocomplete="off"
                                                            data-toggle="tooltip" data-placement="top" 
                                                            title="Não é possível alterar o login/usuário de acesso" 
                                                            data-original-title="Não é possível alterar o login/usuário de acesso">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="lastname">Nome</label>
                                                            <input type="text" class="form-control" id="nomeUsuarioLogado" name="nomeUsuarioLogado" autocomplete="off">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="lastname">E-mail</label>
                                                            <input type="email" class="form-control" id="emailUsuarioLogado" name="emailUsuarioLogado" autocomplete="off">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="lastname">Nova senha</label>
                                                            <input type="password" class="form-control" id="SenhaUsuarioLogado" name="SenhaUsuarioLogado" autocomplete="off">
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-sm-12 col-md-4 col-lg-4">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                        <label class="col-form-label">Foto
                                                            <i class="mdi mdi-help-circle" data-toggle="tooltip" data-placement="top" title="Utilizar uma imagem com Máximo 500px" 
                                                                data-original-title="Utilizar uma imagem com Máximo 500px x 500px">
                                                            </i>
                                                        </label>
                                                        <div class="custom-file">
                                                            <input class="custom-file-input" type='file' name="imagemPerfilUsuario" id="imagemPerfilUsuario" onchange="readURL(this);" >
                                                            <label class="custom-file-label text-truncate">Procurar foto para o perfil..</label>
                                                        </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="lastname">Preview</label>
                                                            
                                                            <div>
                                                                <img src="#" class="img-responsive" id="imagemUsuarioPreview" style="max-width: 240px; max-height: 280px; object-fit: cover;">
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div> 
                                            
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-block btn-success waves-effect waves-light mt-2">
                                                    <i class="mdi mdi-content-save"></i> Atualizar
                                                </button>
                                            </div>

                                        </form>

                                    </div>

                                </div> 
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
