<?php

date_default_timezone_set('America/Sao_Paulo');

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model {
    
    
    public function m_gerarCodigoCriarUsuarios() 
    {
        
        $result =  $this->db->query("SELECT MAX(id_usuario) as codigo FROM usuarios");

        foreach ($result->result_array() as $dados)
        {
            
            if(is_null($dados['codigo'])) return 1;

            return intval($dados['codigo'])+1;
        }
        
    }

    function m_verificaUsuarioExistente($login)
    {
        $result =  $this->db->query("SELECT login_usuario FROM usuarios WHERE login_usuario = '$login'");
        
        foreach ($result->result() as $dados)
        {
            return $dados;    
        }

    }

    function m_verificaEmailExistente($email)
    {
        $result =  $this->db->query("SELECT email_usuario FROM usuarios WHERE email_usuario = '$email'");
        
        foreach ($result->result() as $dados)
        {
            return $dados;    
        }

    }

    public function m_CriarUsuario($dados)
    {  
        return $this->db->insert('usuarios', $dados);
    }

    public function m_listarUsuarios()
    {
        $this->db->select("*");
        $this->db->order_by('id_usuario', 'DESC');
        $resultado = $this->db->get("usuarios")->result();

        return $resultado;
    }

    function m_verificaUsuarioID($id)
    {
        $result =  $this->db->query("SELECT status_usuario FROM usuarios WHERE id_usuario = '$id'");
        
        foreach ($result->result() as $dados)
        {
            return $dados;    
        }

    }

    function m_verificaUsuario($id)
    {
        $result =  $this->db->query("SELECT * FROM usuarios WHERE id_usuario = '$id'");
        
        foreach ($result->result() as $dados)
        {
            return $dados;    
        }

    }

    public function m_atualizarStatusUsuario($dados)
    {
        
        $resultado = $this->db->query("UPDATE usuarios SET status_usuario = '".$dados['status_usuario']."' WHERE id_usuario = '".$dados['id_usuario']."';");
        
        return $resultado;
    
    }

    public function m_atualizarDadosUsuario($dados)
    {
        
        $resultado = $this->db->query("UPDATE usuarios SET 
                                                login_usuario           = '".$dados['login_usuario']."', 
                                                senha_usuario           = '".$dados['senha_usuario']."', 
                                                nome_usuario            = '".$dados['nome_usuario']."', 
                                                email_usuario           = '".$dados['email_usuario']."', 
                                                status_usuario          = '".$dados['status_usuario']."', 
                                                nivelAcesso_usuario     = '".$dados['nivelAcesso_usuario']."',
                                                imagem_usuario          = '".$dados['imagem_usuario']."',
                                                token_usuario           = '".$dados['token_usuario']."'
                                            WHERE id_usuario            = '".$dados['id_usuario']."';");
        
        return $resultado;
    
    }

    public function m_deletarDadosUsuario($id)
    {

        $resultado = $this->db->query("DELETE FROM usuarios WHERE id_usuario = '".$id."';");
        
        return $resultado;
    
    }

    public function m_dadosUsuarioLogado($id)
    {
        
        $result =  $this->db->query("SELECT * FROM usuarios WHERE id_usuario = '$id'");
        
        foreach ($result->result() as $dados)
        {
            return $dados;    
        }
    }
    
}