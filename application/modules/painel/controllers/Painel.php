<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Painel extends MX_Controller {

	function __construct()
    {
        parent::__construct();

        $this->load->library('session');

        $this->load->model("Painel_model", "painel");
        $this->load->model("permissoes/Permissoes_model", "permissoes");
        $this->load->library('permitiracesso');
        $this->load->model("empresa/Empresa_model", "empresa");

        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->library('zip');

        $this->load->library('emails');
        
    }
    
	public function index()
	{
        $this->permitiracesso->verifica_sessao();
        
        $view = "v_home";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($view);
        // echo '<pre>';
        // print_r($permitirAcessoPagina);
        // echo '</pre>';
        // exit();
        
        $scripts = array(

            "titulo" => "Home | Solid System",
            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsHome.js'></script>
                            
                            <script src='/assets/painel/js/vendor/scriptsHomeRelatorio.js'></script>
                            <script src='/assets/painel/js/vendor/scriptsHomeInfosOurilandia.js'></script>
                            <script src='/assets/painel/js/vendor/loadPages.js'></script>
                            ",
            "scriptsCSS" => "", 
            "scriptsCSSacima" => "<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js'></script>"
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $permitirAcessoPagina['dados']);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view("v_home", $permitirAcessoPagina);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
        
    }
    
    public function login()
	{   
        $this->session->sess_destroy();

        $dadosEmpresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        
		$scripts = array(

            "titulo" => "Acessar painel | SolidSystem",
            
            "scriptsJS" => '',

            "scriptsCSS" => '<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
                            <link href="/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
                            <link href="/assets/css/app.min.css" rel="stylesheet" type="text/css"  id="app-stylesheet" />',

            "scriptsJSacima" => "",
            
            "favicon" => '<link rel="shortcut icon" href="/assets/images/favicon.png">',
            "logotipo" => '<img src="/assets/images/logologin.png" alt="" height="75">',
            "logoEmpresa" => $dadosEmpresa->imagem_empresa
        );
        
		$this->load->view('v_login', $scripts);
    }
    
    public function c_autenticarLogin()
	{
		
		$retorno['msg'] = "";
		$sinal = false;

		$login = $this->input->post('usuario');
        $senha = $this->input->post('senha');
        
		if(empty($login) || empty($senha))
        {
            
            $retorno['ret'] = false;
            $retorno['msg'].= ' Campo <strong>Usuário</strong> ou <strong>Senha</strong> não podem ser vazios!';
			$sinal = true;
			
		} else {
            
            $resultado = $this->painel->buscarLoginExistente($login);
            
            $senhaOK = (bool)password_verify($senha, $resultado['senha_usuario']);

            $dadosMinhaEmpresa = (object)$this->empresa->m_listarInformacoesEmpresa();
            
            if($resultado['login_usuario'] === $login && $senhaOK)
            {  
                if($resultado['status_usuario'] != '1')
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= ' Usuário >> <i><strong>'.$resultado['login_usuario'].'</strong></i> << bloqueado!<br>';
					$sinal = true;
					
                } else {
                    
                    $dados['id']                = $resultado['id_usuario'];
                    $dados['login']             = $resultado['login_usuario'];
                    $dados['senha']             = $resultado['senha_usuario'];
                    $dados['nome']              = $resultado['nome_usuario'];
                    $dados['status']            = $resultado['status_usuario'];
                    $dados['imagem_usuario']    = $resultado['imagem_usuario'];
                    $dados['token']             = session_id();
                    $dados['logado']            = TRUE;
                    $dados['nomeEmpresa']       = $dadosMinhaEmpresa->nomeFantasia_empresa;
                    $dados['imagemEmpresa']     = $dadosMinhaEmpresa->imagem_empresa;
                    $dados['statusEmpresa']     = $dadosMinhaEmpresa->status_empresa;
                    
					$token = $this->painel->atualizaToken($dados);
					
                    $this->session->set_userdata($dados);
                    
                    $retorno['ret'] = true;
                    $retorno['msg'].= '';
                    $sinal = true;
				}
				
            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= ' <strong>Usuário</strong> ou <strong>Senha</strong> inválidos!<br>';
				$sinal = true;
				
            }
        }
		
		if($sinal)
		{
            echo json_encode($retorno);
            exit;
		}	

    }

    public function c_recuperarSenha()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['email_usuario']      = $this->input->post('email_recuperar');
        

        if(empty($dados['email_usuario']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'O <strong>E-MAIL</strong> não pode ser vazio!<br>';
            echo json_encode($retorno);
            
        } else {

            $pesquisarNome = (object)array(
                'colunas'       => "*",
                'tabela'        => "usuarios",
                'coluna_where'  => "email_usuario",
                'valor_where'   => $dados['email_usuario']
            );
            
            $consulta1   = $this->painel->m_buscar_1_Item($pesquisarNome);
            
            if(!$consulta1)
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'O e-mail: <strong><i><u>'.$dados["email_usuario"].'</u></i></strong> não foi encontrado.<br>';
                $sinal = true;
            } 
            
            if($sinal)
            {
                echo json_encode($retorno);
                exit;
            }
            
            $dadosEmpresa = (object)$this->empresa->m_listarInformacoesEmpresa();
            
            $disparar_email = $this->emails->eviaEmailRecuperarSenha($consulta1, $dadosEmpresa);
            
            if($disparar_email['ret'])
            {
                $retorno['ret'] = true;
                $retorno['msg'] = ' Um link de recuperação de senha foi enviado para: <strong>'.$consulta1->email_usuario.'</strong>';
                echo json_encode($retorno);

                $info['senhaAlterada_usuario'] = '1';

                $infosDB = (object)array(
                    'coluna_where'  => 'email_usuario',
                    'valor_where'   => $dados['email_usuario'],
                    'tabela'        => 'usuarios'
                );
                
                $resultado = $this->painel->m_atualizar($info, $infosDB);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível enviar o link de recuperação de senha no momento, tente novamente em alguns minutos.';
                echo json_encode($retorno);
            }

        }
                
    }
    
    public function logout()
    {   
        $this->session->sess_destroy();

        redirect('/dashboard/login');
    }

    public function c_listaNivelAcessoUsuarios()
    {

        $resultado = $this->permissoes->m_listarNiveisAcesso();

        echo json_encode($resultado);
    
    }
    
    public function c_backupDB()
    {
        $this->load->dbutil();
        
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('.', $url_atual);
        $nomeSite = $url[0];
        
        $dataHoraAtual = date('Y-m-d');

        $db_format = array(
            'format' => 'zip',
            'filename' => $nomeSite.' - backup DB - '.$dataHoraAtual.'.sql');

        $backup = $this->dbutil->backup($db_format);

        $dbname = $nomeSite.' - backup DB - '.$dataHoraAtual.'.zip';
        $save='DB/'.$dbname;

        write_file($save, $backup);
        
        // Download na pasta download
        // force_download($dbname, $backup);
    }

    public function v_alterarSenha()
    {
        $dados['token_usuario']     = $this->input->get('id_recuperarSenha');
        
        $dadosEmpresa = (object)$this->empresa->m_listarInformacoesEmpresa();

        $titulo = "Alterar senha | ".$dadosEmpresa->nomeFantasia_empresa;
        $textoSEO = $dadosEmpresa->nomeFantasia_empresa;
        
        $pesquisarNome = (object)array(
            'colunas'       => "*",
            'tabela'        => "usuarios",
            'coluna_where'  => "token_usuario",
            'valor_where'   => $dados['token_usuario']
        );
        
        $resultado   = $this->painel->m_buscar_1_Item($pesquisarNome);

        if(empty($resultado))
        {   

            $msgGenerica = 'Olá, sua senha <strong class="text-warning"><u>já foi alterada</u></strong> ou não encontramos seu e-mail em nosso sistema, tente novamente em alguns minutos! <br> <br> <a href="'.base_url("dashboard/login").'"><button class="btn btn-success waves-effect waves-light" type="submit">Login</button></a>';
            $form = "";
            $email = "";
            $token = "";
            
        } else {

            if($resultado->status_usuario === '0')
            {
                $msgGenerica = "Olá <br><br><strong class='text-warning'><i>$resultado->nome_usuario</i></strong>,
                                <br><br>  Você está <span class='text-uppercase text-warning'>bloqueado</span> e não poderá efetuar a <span class='text-uppercase text-warning'>alteração de senha</span>, entre em contato com o administrador do site.";
                                
                $botao  = "";
                $email  = "";
                $token  = "";
                $form   = "";

            }

            if($resultado->senhaAlterada_usuario == '1')
            {
                $msgGenerica = "Olá <br><br><strong class='text-warning'><i>$resultado->nome_usuario</i></strong>, 
                            <br><br> Vamos alterar sua senha agora, ok? Informe sua nova senha e clique no botão e pronto, já poderá efetuar o login com sua nova senha!";

                $form = '<div class="form-group mb-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">***</span>
                                </div>
                                <input type="password" class="form-control" name="senhalUsuario" id="senhalUsuario" placeholder="Digite a nova senha" aria-label="Digite a nova senha">
                                <div class="input-group-append">
                                    <button class="btn btn-dark waves-effect waves-light" type="submit">Alterar</button>
                                </div>
                            </div>
                        </div>';
                
                $email = $resultado->email_usuario;
                $token = $resultado->token_usuario;
                
            } 

            if($resultado->senhaAlterada_usuario == '2')
            {

                $msgGenerica = 'Sua senha já foi alterada, faça o login <br> <br> <a href="'.base_url("dashboard/login").'"><button class="btn btn-success waves-effect waves-light" type="submit">Acessar</button></a>';
                
                $form   = '';
                $email  = '';
                $token  = '';
                
            }

        }
        

        $infos = array(

            "titulo" => $titulo,
            
            "scriptsJS" => "<script src='/assets/js/jquery.min.js'></script>
                            <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
                            <script src='/assets/js/vendor/login/recuperarSenha.js'></script>
                            <script src='/assets/js/vendor.min.js'></script>
                            <script src='/assets/js/app.min.js'></script",
            
            
            "msgGenerica" => $msgGenerica,
            
            "emailUsuario" => $email,

            "tokenUsuario" => $token,

            "textoSEO" => $textoSEO,

            "formulario" => $form,

            "logoEmpresa" => $dadosEmpresa->imagem_empresa

            
        );


        $this->load->view('v_alterar_senha', $infos);
    }

    public function c_alterarSenhaUsuario()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $token_usuario  = $this->input->post('tokenUsuario');
        $senha          = $this->input->post('senhalUsuario');
        
        if(empty($senha))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Por favor informe a nova <strong>SENHA</strong>!<br>';
            $sinal = true;
            
        } else {

            $pesquisarNome = (object)array(
                'colunas'       => "*",
                'tabela'        => "usuarios",
                'coluna_where'  => "token_usuario",
                'valor_where'   => $token_usuario
            );
            
            $consulta1   = $this->painel->m_buscar_1_Item($pesquisarNome);
            
            if(empty($consulta1))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' Autenticação de usuário inválida, tente novamente em alguns minutos!';
                echo json_encode($retorno);
                
            } else {
                
                $dados['senha_usuario'] = password_hash($senha, PASSWORD_DEFAULT);

                $infosDB = (object)array(
                    'coluna_where'  => 'token_usuario',
                    'valor_where'   => $token_usuario,
                    'tabela'        => 'usuarios'
                );
                
                $resultado = $this->painel->m_atualizar($dados, $infosDB);
                
                if($resultado)
                {   
                    $info['senhaAlterada_usuario'] = '2';

                    $infosDB = (object)array(
                        'coluna_where'  => 'email_usuario',
                        'valor_where'   => $consulta1->email_usuario,
                        'tabela'        => 'usuarios'
                    );
                    
                    $resultado = $this->painel->m_atualizar($info, $infosDB);
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = ' Sua senha foi atualizada com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = ' Não foi possível atualizar sua senha, tente novamente mais tarde!';
                    echo json_encode($retorno);
                }

            }

        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
    }
    
}
