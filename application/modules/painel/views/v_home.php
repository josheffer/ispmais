<?php
// echo '<pre>';
// print_r($dados);
// echo '</pre>';
// exit();
?>
<div class="container-fluid">
    
    <div class="row">

        <div class="col-12">
            
            <div class="page-title-box">
                
                <center class="mb-3">
                    
                    <h3 class="mt-4">Olá <?=$dados['dados']['nome']?>!</h3>
                    
                    <p class="text-muted">Seja bem vindo ao painel.</p>

                    <hr>

                </center>

                <div class="row">
                        
                    <div class="col-md-12">
                    
                        <div class="card">
                            
                            <div class="card-body">

                                <h5 class="card-title mb-0">
                                    <a data-toggle="collapse" class="text-white" href="#valoresOurilandia" role="button" aria-expanded="false" aria-controls="cardCollpase1">
                                        <i class="mdi mdi-arrow-right-thick"></i>
                                        <u><span class="text-warning">Valores gerados</span> - Ourilândia do Norte - <?=date('Y')?></u>  
                                    </a>

                                    <div class="card-widgets" id="divAtualizarValores"></div>

                                </h5>
                                
                                <div id="valoresOurilandia" class="collapse show pt-3">

                                    <div class="row">
                    
                                        <div class="col-md-2">

                                            <div class="col-md-12">

                                                <div class="card-box">
                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12 text-center">
                                                            <u><h5><span class="font-13 text-warning text-truncate">Ligações</span></h5></u>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-12">
                                                            <div class="text-center">
                                                                <h3 class="text-dark my-1 font-15">R$ <span data-plugin="counterup" id="ligacoesPerdidas"></span></h3>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 text-center mt-2">
                                                            <span class="badge badge-light-danger">Perdidas</span>
                                                        </div>

                                                    </div>
                                                    
                                                </div> 

                                            </div> 

                                        </div>

                                        <div class="col-md-2">

                                            <div class="col-md-12">

                                                <div class="card-box">
                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12 text-center">
                                                            <u><h5><span class="font-13 text-warning text-truncate">Ligações</span></h5></u>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-12">
                                                            <div class="text-center">
                                                                <h3 class="text-dark my-1 font-15">R$ <span data-plugin="counterup" id="ligacoesVendas"></span></h3>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 text-center mt-2">
                                                            <span class="badge badge-light-success">Realizadas</span>
                                                        </div>

                                                    </div>
                                                    
                                                </div> 

                                            </div> 

                                        </div>

                                        <div class="col-md-2">

                                            <div class="col-md-12">

                                                <div class="card-box">
                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12 text-center">
                                                            <u><h5><span class="font-13 text-info text-truncate">Residenciais</span></h5></u>
                                                        </div> 
                                                        
                                                        <div class="col-md-12 col-12">
                                                            <div class="text-center">
                                                                <h3 class="text-dark my-1 font-15">R$ <span data-plugin="counterup" id="residenciaisPerdidas"></span></h3>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 text-center mt-2">
                                                            <span class="badge badge-light-danger">Perdidas</span>
                                                        </div>

                                                    </div>
                                                    
                                                </div> 

                                            </div> 

                                        </div>

                                        <div class="col-md-2">

                                            <div class="col-md-12">

                                                <div class="card-box">
                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12 text-center">
                                                            <u><h5><span class="font-13 text-info text-truncate">Residenciais</span></h5></u>
                                                        </div>
                                                        
                                                            <div class="col-md-12 col-12">
                                                            <div class="text-center">
                                                                <h3 class="text-dark my-1 font-15">R$ <span data-plugin="counterup" id="residenciaisVendas"></span></h3>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 text-center mt-2">
                                                            <span class="badge badge-light-success">Realizadas</span>
                                                        </div>

                                                    </div>
                                                    
                                                </div> 

                                            </div> 

                                        </div>

                                        <div class="col-md-2">

                                            <div class="col-md-12">

                                                <div class="card-box">
                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12 text-center">
                                                            <u><h5><span class="font-13 text-white text-truncate">Empresariais</span></h5></u>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-12">
                                                            <div class="text-center">
                                                                <h3 class="text-dark my-1 font-15">R$ <span data-plugin="counterup" id="empresariaisPerdidas"></span></h3>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 text-center mt-2">
                                                            <span class="badge badge-light-danger">Perdidas</span>
                                                        </div>

                                                    </div>
                                                    
                                                </div> 

                                            </div> 

                                        </div>

                                        <div class="col-md-2">

                                            <div class="col-md-12">

                                                <div class="card-box">
                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12 text-center">
                                                            <u><h5><span class="font-13 text-white text-truncate">Empresariais</span></h5></u>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-12">
                                                            <div class="text-center">
                                                                <h3 class="text-dark my-1 font-15">R$ <span data-plugin="counterup" id="empresariaisVendas"></span></h3>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 text-center mt-2">
                                                            <span class="badge badge-light-success">Realizadas</span>
                                                        </div>

                                                    </div>
                                                    
                                                </div> 

                                            </div> 

                                        </div>
                                        
                                    </div>

                                </div>

                            </div>
                            
                        </div>
                    
                    </div>
                
                </div>
                
                <div class="row">
                            
                    <div class="col-md-12">
                    
                        <div class="card">
                            
                            <div class="card-body">

                                <h5 class="card-title mb-0">
                                    
                                    <i class="mdi mdi-arrow-right-thick"></i>
                                    <u><span class="text-warning">Gráfico geral (Supervisão)</span> - Ourilândia do Norte - <?=date('Y')?></u>  
                                    
                                    <div class="card-widgets" id="atualizaDivGraficoOurilandiaGeral"></div>

                                </h5>
                                
                                <div id="graficoOurilandia" class="collapse pt-3 show" style="position: relative; height: 80vh">
                                    <canvas class="grafico-bar-geral-Ourilandia"></canvas>
                                </div>

                            </div>
                            
                        </div>
                    
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-md-12">
                    
                        <div class="card">
                            
                            <div class="card-body">

                                <h5 class="card-title mb-0">
                                    
                                    <i class="mdi mdi-arrow-right-thick"></i>
                                    <u><span class="text-warning">Gráfico geral - Meus resultados</span> - Ourilândia do Norte - <?=date('Y')?></u>  

                                    <div class="card-widgets" id="atualizaDivGraficoOurilandia"></div>

                                </h5>
                                
                                <div id="graficoOurilandia" class="collapse pt-3 show" style="position: relative; height: 80vh">
                                    <canvas class="line-chart"></canvas>
                                </div>

                            </div>
                            
                        </div>
                    
                    </div>                    
                </div>
                    
                <div class="row">
                    
                    <div class="col-md-12">
                    
                        <div class="card">
                            
                            <div class="card-body">

                                <h5 class="card-title mb-0">
                                    <a data-toggle="collapse" class="text-white" href="#valoresOurilandia" role="button" aria-expanded="false" aria-controls="cardCollpase1">
                                        <i class="mdi mdi-arrow-right-thick"></i>
                                        <u><span class="text-warning">Valores gerados</span> - Ourilândia do Norte - <?=date('Y')?></u>  
                                    </a>

                                    <div class="card-widgets" id="divAtualizarValores"></div>

                                </h5>
                                
                                <div id="valoresOurilandia" class="collapse show pt-3">

                                    <div class="row">
                    
                                        <div class="col-md-2">

                                            <div class="col-md-12">

                                                <div class="card-box">
                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12 text-center">
                                                            <u><h5><span class="font-13 text-warning text-truncate">Ligações</span></h5></u>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-12">
                                                            <div class="text-center">
                                                                <h3 class="text-dark my-1 font-15">R$ <span data-plugin="counterup" id="ligacoesPerdidas"></span></h3>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 text-center mt-2">
                                                            <span class="badge badge-light-danger">Perdidas</span>
                                                        </div>

                                                    </div>
                                                    
                                                </div> 

                                            </div> 

                                        </div>

                                        <div class="col-md-2">

                                            <div class="col-md-12">

                                                <div class="card-box">
                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12 text-center">
                                                            <u><h5><span class="font-13 text-warning text-truncate">Ligações</span></h5></u>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-12">
                                                            <div class="text-center">
                                                                <h3 class="text-dark my-1 font-15">R$ <span data-plugin="counterup" id="ligacoesVendas"></span></h3>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 text-center mt-2">
                                                            <span class="badge badge-light-success">Realizadas</span>
                                                        </div>

                                                    </div>
                                                    
                                                </div> 

                                            </div> 

                                        </div>

                                        <div class="col-md-2">

                                            <div class="col-md-12">

                                                <div class="card-box">
                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12 text-center">
                                                            <u><h5><span class="font-13 text-info text-truncate">Residenciais</span></h5></u>
                                                        </div> 
                                                        
                                                        <div class="col-md-12 col-12">
                                                            <div class="text-center">
                                                                <h3 class="text-dark my-1 font-15">R$ <span data-plugin="counterup" id="residenciaisPerdidas"></span></h3>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 text-center mt-2">
                                                            <span class="badge badge-light-danger">Perdidas</span>
                                                        </div>

                                                    </div>
                                                    
                                                </div> 

                                            </div> 

                                        </div>

                                        <div class="col-md-2">

                                            <div class="col-md-12">

                                                <div class="card-box">
                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12 text-center">
                                                            <u><h5><span class="font-13 text-info text-truncate">Residenciais</span></h5></u>
                                                        </div>
                                                        
                                                            <div class="col-md-12 col-12">
                                                            <div class="text-center">
                                                                <h3 class="text-dark my-1 font-15">R$ <span data-plugin="counterup" id="residenciaisVendas"></span></h3>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 text-center mt-2">
                                                            <span class="badge badge-light-success">Realizadas</span>
                                                        </div>

                                                    </div>
                                                    
                                                </div> 

                                            </div> 

                                        </div>

                                        <div class="col-md-2">

                                            <div class="col-md-12">

                                                <div class="card-box">
                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12 text-center">
                                                            <u><h5><span class="font-13 text-white text-truncate">Empresariais</span></h5></u>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-12">
                                                            <div class="text-center">
                                                                <h3 class="text-dark my-1 font-15">R$ <span data-plugin="counterup" id="empresariaisPerdidas"></span></h3>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 text-center mt-2">
                                                            <span class="badge badge-light-danger">Perdidas</span>
                                                        </div>

                                                    </div>
                                                    
                                                </div> 

                                            </div> 

                                        </div>

                                        <div class="col-md-2">

                                            <div class="col-md-12">

                                                <div class="card-box">
                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12 text-center">
                                                            <u><h5><span class="font-13 text-white text-truncate">Empresariais</span></h5></u>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-12">
                                                            <div class="text-center">
                                                                <h3 class="text-dark my-1 font-15">R$ <span data-plugin="counterup" id="empresariaisVendas"></span></h3>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 text-center mt-2">
                                                            <span class="badge badge-light-success">Realizadas</span>
                                                        </div>

                                                    </div>
                                                    
                                                </div> 

                                            </div> 

                                        </div>
                                        
                                    </div>

                                </div>

                            </div>
                            
                        </div>
                    
                    </div>
                
                </div>
                    
                <div class="row">
                
                    <div class="col-md-12">
                    
                        <div class="card">
                            
                            <div class="card-body">

                                <div class="card-widgets" id="atualizaDivLigacoes"></div>

                                <h5 class="card-title mb-0">
                                    <a data-toggle="collapse" class="text-white" href="#solicitacoesLigacoes" role="button" aria-expanded="false" aria-controls="cardCollpase1">
                                        <i class="mdi mdi-arrow-right-thick"></i>
                                        <u><span class="text-warning">Ourilândia do Norte</span> - Minhas solicitações de ligações</u>  
                                    </a>
                                </h5>

                                <div id="solicitacoesLigacoes" class="collapse show pt-3">
                                    
                                    <div class="row no-gutters">
                                        
                                        <div class="col-md-6 col-xl-4">
                                            <div class="widget-rounded-circle bg-soft-primary rounded-0 card-box mb-0">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="avatar-lg rounded-circle bg-soft-primary">
                                                            <i class=" mdi mdi-clock-alert-outline avatar-title font-22 text-info"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="text-right">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup" id="ligacoesDisponiveis"></span></h3>
                                                            <p class="text-primary mb-1 text-truncate">Pendentes <i class="mdi mdi-arrow-right-bold"></i>
                                                                <a href="/dashboard/site_admin/ourilandia_ligacoes_pendentes">
                                                                    <span class="badge badge-light-primary">
                                                                        <i class="mdi mdi-eye"></i>
                                                                    </span>
                                                                </a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div> 
                                        
                                        <div class="col-md-6 col-xl-4">
                                            <div class="widget-rounded-circle bg-soft-warning rounded-0 card-box mb-0">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="avatar-lg rounded-circle bg-soft-warning">
                                                            <i class="fe-clock font-22 avatar-title text-warning"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="text-right">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup" id="ligacoesEmAndamento"></span></h3>
                                                            <p class="text-warning mb-1 text-truncate">Em andemento..</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-xl-4">
                                            <div class="widget-rounded-circle bg-soft-success rounded-0 card-box mb-0">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="avatar-lg rounded-circle bg-soft-success">
                                                            <i class="fe-check-circle font-22 avatar-title text-success"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="text-right">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup" id="ligacoesRealizadas"></span></h3>
                                                            <p class="text-success mb-1 text-truncate">Realizadas</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>

                                </div>

                            </div>

                        </div> 

                    </div>
                        
                    <div class="col-md-12">
                    
                        <div class="card">
                            
                            <div class="card-body">

                                <div class="card-widgets" id="atualizaDivResidenciais"></div>

                                <h5 class="card-title mb-0">
                                    <a data-toggle="collapse" class="text-white" href="#solicitacoesResidencias" role="button" aria-expanded="false" aria-controls="cardCollpase1">
                                        <i class="mdi mdi-arrow-right-thick"></i>
                                        <u><span class="text-warning">Ourilândia do Norte</span> - Minhas solicitações Residenciais</u>  
                                    </a>
                                </h5>

                                <div id="solicitacoesResidencias" class="collapse show pt-3">
                                    
                                    <div class="row no-gutters">
                                        
                                        <div class="col-md-6 col-xl-4">
                                            <div class="widget-rounded-circle bg-soft-primary rounded-0 card-box mb-0">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="avatar-lg rounded-circle bg-soft-primary">
                                                            <i class=" mdi mdi-clock-alert-outline avatar-title font-22 text-info"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="text-right">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup" id="residenciaisDisponiveis"></span></h3>
                                                            <p class="text-primary mb-1 text-truncate">Pendentes <i class="mdi mdi-arrow-right-bold"></i>
                                                                <a href="/dashboard/site_admin/ourilandia_solicitacoesResidencias_pendentes">
                                                                    <span class="badge badge-light-primary">
                                                                        <i class="mdi mdi-eye"></i>
                                                                    </span>
                                                                </a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div> 
                                        
                                        <div class="col-md-6 col-xl-4">
                                            <div class="widget-rounded-circle bg-soft-warning rounded-0 card-box mb-0">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="avatar-lg rounded-circle bg-soft-warning">
                                                            <i class="fe-clock font-22 avatar-title text-warning"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="text-right">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup" id="residenciaisEmAndamento"></span></h3>
                                                            <p class="text-warning mb-1 text-truncate">Em andemento..</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-xl-4">
                                            <div class="widget-rounded-circle bg-soft-success rounded-0 card-box mb-0">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="avatar-lg rounded-circle bg-soft-success">
                                                            <i class="fe-check-circle font-22 avatar-title text-success"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="text-right">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup" id="residenciaisRealizadas"></span></h3>
                                                            <p class="text-success mb-1 text-truncate">Realizadas</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>

                                </div>

                            </div>

                        </div> 

                    </div>
                    
                    <div class="col-md-12">
                    
                        <div class="card">
                            
                            <div class="card-body">

                                <div class="card-widgets" id="atualizaDivEmpresariais"></div>

                                <h5 class="card-title mb-0">
                                    <a data-toggle="collapse" class="text-white" href="#solicitacoesEmpresariais" role="button" aria-expanded="false" aria-controls="cardCollpase1">
                                        <i class="mdi mdi-arrow-right-thick"></i>
                                        <u><span class="text-warning">Ourilândia do Norte</span> - Minhas solicitações Empresariais</u>
                                    </a>
                                </h5>
                                
                                <div id="solicitacoesEmpresariais" class="collapse show pt-3">
                                    
                                    <div class="row no-gutters">
                                        
                                        <div class="col-md-6 col-xl-4">
                                            <div class="widget-rounded-circle bg-soft-primary rounded-0 card-box mb-0">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="avatar-lg rounded-circle bg-soft-primary">
                                                            <i class=" mdi mdi-clock-alert-outline avatar-title font-22 text-info"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="text-right">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup" id="empresariaisDisponiveis"></span></h3>
                                                            <p class="text-primary mb-1 text-truncate">Pendentes <i class="mdi mdi-arrow-right-bold"></i>
                                                                <a href="/dashboard/site_admin/ourilandia_solicitacoesEmpresariais_pendentes">
                                                                    <span class="badge badge-light-primary">
                                                                        <i class="mdi mdi-eye"></i>
                                                                    </span>
                                                                </a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div> 
                                        
                                        <div class="col-md-6 col-xl-4">
                                            <div class="widget-rounded-circle bg-soft-warning rounded-0 card-box mb-0">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="avatar-lg rounded-circle bg-soft-warning">
                                                            <i class="fe-clock font-22 avatar-title text-warning"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="text-right">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup" id="empresariaisEmAndamento"></span></h3>
                                                            <p class="text-warning mb-1 text-truncate">Em andemento..</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-xl-4">
                                            <div class="widget-rounded-circle bg-soft-success rounded-0 card-box mb-0">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="avatar-lg rounded-circle bg-soft-success">
                                                            <i class="fe-check-circle font-22 avatar-title text-success"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="text-right">
                                                            <h3 class="text-dark mt-1"><span data-plugin="counterup" id="empresariaisRealizadas"></span></h3>
                                                            <p class="text-success mb-1 text-truncate">Realizadas</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>

                                </div>

                            </div>

                        </div> 

                    </div>

                </div>
                
            </div>

        </div>
        