<!DOCTYPE html>

<html lang="pt-br">

    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title><?=$titulo?></title>
    <meta content="SolidSystem Softwares Inteligentes - Captura de leads com páginas personalizadas, disparo de e-mails em massa, acompanhamento em tempo real dos disparos.." name="description">
    <meta content="Josheffer Robert" name="author">
    <link rel="shortcut icon" href="/assets/painel/images/favicon.png">
    
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/metismenu.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
    
    <div class="col-md-12 fixed-top mt-2" id="msgErro"></div>
    
    <div class="container">

        <div class="col-md-12 mb-5 pb-5 py-5 mt-5">
            <div class="row justify-content-center">
                
                <div class="col-md-6">
                    <div class="card">

                        <div class="card-body p-4">

                        <h3 class="text-center">
                            <a href="https://solidsystem.net.br" target="_blank" class="logo logo-admin">
                                <img src="/assets/painel/images/logologin.png" class="img-fluid" style="width: 10em;" alt="SolidSystem, Solid System - Softwares Inteligentes em Goiânia">
                            </a>
                        </h3>

                        <h6 class="text-center font-12">Sistema desenvolvido em parceria com</h6>

                        <hr>

                        <h3 class="text-center mt-3">
                            <a href="javascript:void(0);" class="logo logo-admin">
                                <img src="<?=base_url('/assets/painel/images/empresa/logotipo/'.$logoEmpresa)?>" class="img-fluid" style="width: 15em;" alt="logo">
                            </a>
                        </h3>
                            

                        </div>
                    </div>
                    
                </div>

                <div class="col-md-6">
                    <div class="card">

                        <div class="card-body p-4">

                            <div class="p-3">
                                
                                <h4 class="text-white font-18 m-b-5 text-center">Seja bem vindo ao painel!</h4>
                                
                                <p class="text-white-50 text-center">Informe seus dados para acessar.</p>
                                
                                <form id="formLogin">
                                    
                                    <div class="form-group">
                                        <label for="username">Usuário</label>
                                        <input type="text"class="form-control" id="usuario" name="usuario" autocomplete="off" autofocus>
                                    </div>

                                    <div class="form-group">
                                        <label for="userpassword">Senha</label>
                                        <input type="password" class="form-control" id="senha" name="senha" autocomplete="off">
                                    </div>

                                    <div class="form-group row m-t-20">
                                        <div class="col-6 text-right">
                                            <button class="btn btn-block btn-primary w-md waves-effect waves-light" id="botaoAcessarLogin" type="submit">Acessar</button>
                                        </div>

                                        <div class="col-6 text-right">
                                            <button type="button" class="btn btn-block btn-secondary w-md waves-effect waves-light" data-toggle="modal" data-target="#modalRecuperarSenha">Recuperar Senha</button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                            
                        </div>

                    </div>
                    
                </div>

            </div>
        </div>

    </div>
    
    <footer style="width: 100%; bottom: 0; text-align: center; position: absolute; right: 0; background-color: #323a4e; -webkit-box-shadow: 0 -1px 2px 0 rgba(0,0,0,.05); box-shadow: 0 -1px 2px 0 rgba(0,0,0,.05);">
        <div class="container mt-4 text-center">
            <p>2019 - <?=date('Y')?> © Sistema desenvolvido por <a href="https://solidsystem.net.br" target="_blank" class="text-warning" rel="noopener noreferrer">solidsystem.net.br</a></p>
        </div>
    </footer>

    <div id="modalRecuperarSenha" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Recuperar minha senha</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">

                    <div id="erroRecuperarSenha"></div>
                    
                    <p>Não se preocupe, iremos enviar um link para você redefinir sua senha.</p>
                    
                    <hr>

                    <form id="formRecuperarSenha">
                        
                        <div class="form-group">
                            <label for="email_recuperar">Informe o e-mail utilizado no cadastro</label>
                            <input type="text" class="form-control" id="email_recuperar" name="email_recuperar" autocomplete="off">
                        </div>

                        <div class="form-group row m-t-20">

                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-block btn-primary w-md waves-effect waves-light" id="botaoRecuperarSenha">Solicitar link por e-mail</button>
                            </div>
                            
                        </div>

                    </form>
                
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Cancelar</button>
                </div>

            </div>
            
        </div>
        
    </div>
    
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/vendor.min.js"></script>
    <script src="/assets/js/vendor/login/autenticarlogin.js"></script>
    <script src="/assets/js/vendor/login/recuperarSenha.js"></script>
    <script src="/assets/js/jquery.slimscroll.js"></script>
    <script src="/assets/js/app.min.js"></script>

</body>
</html>