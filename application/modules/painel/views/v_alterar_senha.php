<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8" />
        <title><?=$titulo?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Coderthemes" name="Josheffer Robert" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link rel="shortcut icon" href="/assets/sites/ispmais/img/favicon.png">
        <link href="/assets/painel/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/painel/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/painel/css/app.min.css" rel="stylesheet" type="text/css" />

    </head>

    <body>

        <div class="col-md-12 fixed-top mt-2" id="msgErro"></div>

        <div class="col-md-12 fixed-top mt-2" id="msgErro"></div>

        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12 col-lg-6 col-xl-5">
                        <div class="card">

                            <div class="card-body p-4">
                                
                                <div class="text-center m-auto">
                                        <span><img src="<?=base_url('assets/painel/images/empresa/logotipo/').$logoEmpresa;?>" title="<?=$textoSEO;?>" alt="<?=$textoSEO;?>" width="280"></span>
                                    <hr class="mt-3 mb-3">
                                    
                                    <h5 class="text-muted mb-4 mt-3" id="msgGenerica"><?=$msgGenerica?></h5>
                                    
                                </div>

                                <form id="formAlterarSennha">
                                    
                                    <input class="form-control" type="hidden" value="<?=$tokenUsuario?>" name="tokenUsuario" id="tokenUsuario" required="required" >
                                    
                                    <?=$formulario;?>
                                    
                                </form>

                            </div> 

                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer footer-alt">
            <?=date('Y')?> &copy; 
            
            <?php
                $dominio = explode("/", base_url());
                echo "www.".$dominio[2];
            ?>
        </footer>
        
        <?=$scriptsJS?>
        
    </body>
    
</html>