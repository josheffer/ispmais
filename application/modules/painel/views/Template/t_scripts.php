        
        <div class="rightbar-overlay"></div>
        
        <script src="<?=base_url('assets/painel/js/vendor.min.js')?>"></script>
        <?=$scriptsJS?>
        <script src="<?=base_url('assets/painel/js/app.min.js')?>"></script>
        <script src="<?=base_url('assets/painel/js/vendor/loadPages.js')?>"></script>

        <?php 

            $urlAtual = base_url($_SERVER["REQUEST_URI"]);
            
            $paginaEmpresa = base_url('/dashboard/empresa/minhaEmpresa');
            
            if($urlAtual != $paginaEmpresa) { 
                
        ?>

        <script>
            
            primeiroAcessoChecarEmpresa();

            function primeiroAcessoChecarEmpresa() {

                $.ajax({

                    url: "/listagens/primeiroAcesso/checarEmpresa",

                    ajax: 'data.json',

                    success: function(data) {

                        var dados = JSON.parse(data);

                        if (dados.status_empresa === '1') {

                            $('#primeiroAcesso').modal('hide');

                        } else {

                            $('#primeiroAcesso').modal('show');

                        }

                    }
                });
            }
        </script>

        <div id="primeiroAcesso" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    
                    <div class="modal-header">
                        <h4 class="modal-title text-center" id="myModalLabel">Atenção!!</h4>
                    </div>
        
                    <div class="modal-body">
                        
                        <h5>Olá <span class="text-warning"><?=$dados['nome']?></span>, 
                        precisamos que você insira os dados da sua empresa!
                        Não vai demorar mais do que 5 minutos para fazer isso, vamos lá?
                        </h5>
        
                        <div class="col-md-12 mt-3">
                            <a href="/dashboard/empresa/minhaEmpresa">
                                <button type="button" class="btn btn-block btn-primary waves-effect waves-light">Vou fazer isso agora!</button>
                            </a>
                        </div>
                    
                    </div>
                    
                </div>
            </div>
        </div>

        <?php } ?>

    </body>
    
</html>