<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Painel_model extends CI_Model {

    public function m_consultaDadosSessao($id)
    {           
        $result =  $this->db->query("SELECT * FROM usuarios WHERE id_usuario = " . $id);

        foreach ($result->result_array() as $dados)
        {
            return $dados;    
        }
    }

    public function m_buscar_1_Item_like($dados)
    {
        $objeto = (object)$dados;

        
        
        $this->db->select($objeto->colunas);
        $this->db->like($objeto->coluna_where, $objeto->valor_where);
        $resultado = $this->db->get($objeto->tabela)->result();

        if(is_null($resultado))
        {
            return $resultado;

        } else {

            foreach ($resultado as $dados)
            {
                return $dados;    
            }   

        }
        
    }

    public function m_buscar_1_Item($dados)
    {
        $objeto = (object)$dados;

        
        
        $this->db->select($objeto->colunas);
        $this->db->where($objeto->coluna_where, $objeto->valor_where);
        
        $resultado = $this->db->get($objeto->tabela)->result();

        if(is_null($resultado))
        {
            return $resultado;

        } else {

            foreach ($resultado as $dados)
            {
                return $dados;    
            }   

        }
        
    }

    public function buscarLoginExistente($login)
    {
        
        $result = $this->db->query("SELECT * FROM usuarios WHERE BINARY login_usuario = '".$login."';");
        foreach ($result->result_array() as $dados) {
    
            return $dados;
            
		}
    }

    public function atualizaToken($dados)
    {
        return $this->db->update('usuarios', array('token_usuario'=>$dados['token']), array('id_usuario'=>$dados['id']));
    }

    public function consultaDadosSessao($id)
    {           
        $result =  $this->db->query("SELECT * FROM usuarios WHERE id_usuario = " . $id);

        foreach ($result->result_array() as $dados)
        {
            return $dados;    
        }
    }
    
    public function m_atualizar($dados, $infosDB)
    {
        
        $this->db->where($infosDB->coluna_where, $infosDB->valor_where);
        
        $resultado = $this->db->update($infosDB->tabela, $dados);

        return $resultado;
        
    }
    
}