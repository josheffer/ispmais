<div class="container-fluid">
    
    <div class="row mt-2">
    
        <div class="col-xl-12">

            <div class="page-title-box">
				<ol class="breadcrumb m-0">
					<li class="breadcrumb-item"><a href="/dashboard/newsletter">Newsletter</a></li>
					<li class="breadcrumb-item active text-warning">Disparos</li>
				</ol>
			</div>

            <?php
                $this->load->view('botoesAll');
            ?>

            <div class="card mt-3">
                <h5 class="card-header text-center">Campanhas sendo enviadas..</h5>
                <div class="card-body">
                    
                    <div class="row" id="paineisEnvios"></div>

                </div>
            </div>
            
        </div>

        <div class="col-12">
                        
            <div id="newsEnviadas" class="mb-3">
                
                <div class="card mb-1">
                    
                    <div class="card-header" id="newsEnviadasok">
                        
                        <h5 class="m-0">
                            
                            <a class="text-dark collapsed" data-toggle="collapse" href="#enviadas" aria-expanded="false">
                                Campanhas enviadas
                            </a>

                        </h5>

                    </div>
        
                    <div id="enviadas" class="collapse show" aria-labelledby="newsEnviadasok" data-parent="#newsEnviadas" style="">
                        
                        <div class="card-body">

                            <div class="table-responsive">
                        
                                <table class="table table-bordered table-hover table-centered  table-nowrap m-0">

                                    <thead class="thead-light">
                                        <tr>
                                            <th>#</th>
                                            <th>Titulo</th>
                                            <th>Assunto</th>
                                            <th>Relatório</th>
                                            <th>Informações</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody id="tabelaCampanhasEnviadas"></tbody>

                                </table>

                            </div> 
                            
                        </div>

                    </div>

                </div>
                
            </div>

        </div>

    </div>     
        
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">E-mails não enviadas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
               
                
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>E-mail</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>josheffer@gmail.com</td>
                        </tr>

                        <tr>
                            <td>1</td>
                            <td>josheffer@gmail.com</td>
                        </tr>

                        <tr>
                            <td>1</td>
                            <td>josheffer@gmail.com</td>
                        </tr>

                        <tr>
                            <td>1</td>
                            <td>josheffer@gmail.com</td>
                        </tr>
                    </tbody>
                </table>
                

            </div>

        </div>
    </div>
</div>
