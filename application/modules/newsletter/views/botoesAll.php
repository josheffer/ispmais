<div class="row">

    <div class="col-xl-12">

        <div id="accordion" class="mb-3">


            <div class="card mb-1">
                
                <a class="text-dark collapsed text-center" data-toggle="collapse" href="#menuNavegacaoPainel" aria-expanded="false">
                    <div class="card-header" id="headingOne">
                        <h5 class="m-0 text-center">
                            Menu de navegação
                        </h5>
                    </div>
                </a>

                <div id="menuNavegacaoPainel" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="">
                    <div class="card-body d-flex justify-content-center">

                        <a href="/dashboard/newsletter" data-toggle="tooltip" data-placement="top" data-original-title="Configure e faça os disparos!">
                            <button type="button" class="btn mr-1 text-white bg-soft-primary">Campanhas</button>
                        </a>

                        <a href="/dashboard/newsletter/disparos" data-toggle="tooltip" data-placement="top" data-original-title="Acompanhe em tempo real os disparos">
                            <button type="button" class="btn mr-1 bg-soft-pink text-white">Disparos</button>
                        </a>

                        <a href="/dashboard/newsletter/grupos" data-toggle="tooltip" data-placement="top" data-original-title="Configure os grupos para coletar os leads">
                            <button type="button" class="btn mr-1 bg-soft-info text-white">Listas</button>
                        </a>

                        <a href="/dashboard/newsletter/emails" data-toggle="tooltip" data-placement="top" data-original-title="Faça a importação ou exportação de listas">
                            <button type="button" class="btn mr-1 bg-soft-success text-white">E-mails</button>
                        </a>

                        <div class="btn-group mb-2 mr-1">
                            
                            <button type="button" class="btn bg-soft-red dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Templates de e-mail <i class="mdi mdi-chevron-down"></i></button>
                            
                            <div class="dropdown-menu">
                                
                                <a class="dropdown-item" href="/dashboard/newsletter/templateEmail" data-toggle="tooltip" data-placement="top" data-original-title="Selecione o template para a campanha">Escolher template</a>
                                
                                <div class="dropdown-divider"></div>
                                
                                <a class="dropdown-item" href="/dashboard/newsletter/gerenciar_template" data-toggle="tooltip" data-placement="top" data-original-title="Crie, altere, apague os templates de e-mail">Gerenciar templates</a>

                            </div>

                        </div>
                        
                        <a href="/dashboard/newsletter/configuracoes_disparo" data-toggle="tooltip" data-placement="top" data-original-title="E-mail, senha, porta, servidor..">
                            <button type="button" class="btn mr-1 bg-danger text-white">Configurações</button>
                        </a>

                    </div>

                </div>

            </div>

        </div>

    </div>
    
</div>
