<div class="row">

    <div class="col-xl-12">

        <div id="accordion" class="mb-3">


            <div class="card mb-1">
                
                <a class="text-dark collapsed text-center" data-toggle="collapse" href="#menuNavegacaoPainel" aria-expanded="false">
                    <div class="card-header" id="headingOne">
                        <h5 class="m-0 text-center">
                            Menu de navegação
                        </h5>
                    </div>
                </a>

                <div id="menuNavegacaoPainel" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="">
                    <div class="card-body d-flex justify-content-center">
                    
                        <a href="/dashboard/permissoes/niveisAcesso" data-toggle="tooltip" data-placement="top" data-original-title="Gerencie os níveis de acesso">
                            <button type="button" class="btn mr-1 bg-soft-primary text-white">Níveis de acesso</button>
                        </a>
                            
                        <a href="/dashboard/permissoes/paginas" data-toggle="tooltip" data-placement="top" data-original-title="Gerencie as páginas do sistema">
                            <button type="button" class="btn mr-1 bg-soft-danger text-white">Páginas do sistema</button>
                        </a>
                            
                    </div>

                </div>

            </div>

        </div>

    </div>
    
</div>
