<div class="container-fluid">
    
    <div class="row mt-2">
        
        <div class="col-12">
            
            <div class="page-title-box">
				<ol class="breadcrumb m-0">
					<li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
					<li class="breadcrumb-item"><a href="/dashboard/permissoes/niveisAcesso">Permissões</a></li>
					<li class="breadcrumb-item active text-warning">Páginas do sistema</li>
				</ol>
			</div>

            <?php
                $this->load->view('botoesNiveisAcesso');
            ?>
            
            <div class="row">

                <div class="col-lg-12">

                    <div id="msgErro"></div>
                    
                    <div class="card">

                        <div class="card-body">
                            
                            <h5 class="card-title mb-3">Criar página</h5>
                            
                            <div class="row">
                                
                                <div class="col-lg-12 col-xl-12">
                                    
                                    <div class="card-box">
                                        
                                        <form id="formCadastrarNovaPagina">
                                            
                                            <div class="row">
                                                
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="firstname">Nome (Menu)</label>
                                                        <input type="text" class="form-control" id="nomePagina" name="nomePagina" autocomplete="off">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="firstname">Apelido</label>
                                                        <input type="text" class="form-control" id="apelidoPagina" name="apelidoPagina" autocomplete="off">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="firstname">Rota</label>
                                                        <input type="text" class="form-control" id="enderecoPagina" name="enderecoPagina" autocomplete="off">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="firstname">Controller</label>
                                                        <input type="text" class="form-control" id="controllerPagina" name="controllerPagina" autocomplete="off">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="firstname">Observação</label>
                                                        <input type="text" class="form-control" id="observacaoPagina" placeholder="Módulo, Método, o que faz.." name="observacaoPagina" autocomplete="off">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="firstname">Tipo de rota</label>
                                                        <select class="form-control" name="tipoRotaPagina" id="tipoRotaPagina">
                                                            <option value="1" selected>GET</option>
                                                            <option value="2">POST</option>
                                                            <option value="3">DELETE</option>
                                                            <option value="4">PATCH</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="firstname">Ícone</label>
                                                        <input type="text" class="form-control" id="iconePagina" name="iconePagina" value="mdi mdi-newspaper" autocomplete="off">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="firstname">Procurar ícone</label>
                                                        <button type="button" class="btn btn-block btn--md btn-info waves-effect waves-light"
                                                                data-toggle="modal" data-target="#escolherIcone">
                                                            Pesquisar <i class="fe-search"></i> 
                                                        </button>
                                                    </div>
                                                </div>
                                                
                                            </div> 

                                            <div class="col-md-12">
                                                
                                                
                                                <div class="col-md-3">mdi mdi-cube-send</div>
                                                <div class="col-md-3">mdi mdi-newspaper</div>
                                                <div class="col-md-3"> mdi mdi-cloud-download</div>
                                                <div class="col-md-3"></div>
                                                
                                            </div>
                                            
                                            <div class="text-center">
                                                <button type="submit" id="botaoCadastrarPagina" class="btn btn-block btn-success waves-effect waves-light mt-2">
                                                    <i class="mdi mdi-content-save"></i> Cadastrar
                                                </button>
                                            </div>

                                        </form>

                                    </div>

                                </div> 
                            </div>
                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-12">
                            <div id="accordion" class="mb-3">
                                <div class="card mb-1">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="m-0">
                                            <a class="text-dark" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                                                Páginas cadastradas
                                            </a>
                                        </h5>
                                    </div>
                        
                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                        <div class="card-body">

                                            <div class="table-responsive">
                                                <table class="table mb-0 table-bordered">
                                                    <thead>
                                                    <tr class="d-flex">
                                                        <th class="col-1">#</th>
                                                        <th class="col-3">Nome (Menu)</th>
                                                        <th class="col-4">Apelido</th>
                                                        <th class="col-1">Obs</th>
                                                        <th class="col-1">Ícone</th>
                                                        <th class="col-2">Ações</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="tabelaListarPaginas">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
        
<div id="modalEditarPagina" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

    <div class="modal-dialog modal-full">

        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Alterar página: <u>#<span id="idPaginaAtualizar"></span> - <span class="text-warning" id="atualizarNomePaginaTitulo"></span></u></h4>
                <button type="button" id="fecharModalAtualizarUsuario" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <form id="formAtualziarPagina">

                <div class="modal-body p-4">

                    <div id="msgErroAtualizarPagina"></div>
                    
                    <input type="hidden" id="atualizarIdPagina" name="atualizarIdPagina">
                        
                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Nome (Menu)</label>
                                <input type="text" class="form-control" id="atualizarNomePagina" name="atualizarNomePagina" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Apelido</label>
                                <input type="text" class="form-control" id="atualizarApelidoPagina" name="atualizarApelidoPagina" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="field-2" class="control-label">Rota</label>
                                <input type="text" class="form-control" id="atualizarEnderecoPagina" name="atualizarEnderecoPagina" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="field-2" class="control-label">Controller</label>
                                <input type="text" class="form-control" id="atualizarControllerPagina" name="atualizarControllerPagina" autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Observação</label>
                                <input type="text" class="form-control" id="atualizarObservacaoPagina" name="atualizarObservacaoPagina" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="firstname">Tipo de rota <small class="text-warning">(Opcional)</small></label>
                                <select class="form-control" name="atualizarTipoRotaPagina" id="atualizarTipoRotaPagina">
                                    <option value="" selected>Manter atual</option>
                                    <option value="1">GET</option>
                                    <option value="2">POST</option>
                                    <option value="3">DELETE</option>
                                    <option value="4">PATCH</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Ícone</label>
                                <input type="text" class="form-control" id="atualizarIconePagina" name="atualizarIconePagina" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Procurar ícone</label>
                                <button type="button" class="btn btn-block btn--md btn-info waves-effect waves-light"
                                        data-toggle="modal" data-target="#escolherIcone">
                                    Pesquisar <i class="fe-search"></i> 
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect waves-light">Atualizar</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
                </div>

            </form>

        </div>
    </div>
</div>

<div id="modalDeletarPagina" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="mySmallModalLabel">Deletar Página</h4>
                <button type="button" id="fecharModalDeletarPagina" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <form id="formDeletarPagina">
                <input type="hidden" id="idDeletarPagina" name="idDeletarPagina">
                <div class="modal-body">
                    
                    <div id="msgErroDeletarPagina"></div>

                    Tem certeza que deseja deletar essa página?

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger waves-effect">(Sim) DELETAR</button>
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>
