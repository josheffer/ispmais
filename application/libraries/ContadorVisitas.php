<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Contadorvisitas {

        public function __construct()
        {
            $this->CI =& get_instance();
            
            $this->CI->load->model("Site/Site_admin_model", "site_admin");
            
            $this->CI->load->library('user_agent');

            $this->CI->load->helper('date');
            
        }

        public function contarVisitas($tabela)
        {
            date_default_timezone_set('America/Sao_Paulo');
            
            $gerarCodigo = (object)array(
                'coluna'       => "id_visita",
                'tabela'        => $tabela
            );
            
            $ip_user = file_get_contents('https://api.ipify.org');
            $dados_ip = file_get_contents('http://ip-api.com/json/'.$ip_user.'?fields=status,message,country,region,regionName,city,district,lat,lon,reverse,mobile,proxy,hosting');
            $result_ip = json_decode($dados_ip);

            if ($this->CI->agent->is_browser())
            {
                    $browser = $this->CI->agent->browser();

            } else {

                $browser = "---";
            }

            if ($this->CI->agent->is_robot())
            {
                $robot = $this->CI->agent->robot();

            } else {

                $robot = 0;
            }

            if ($this->CI->agent->is_mobile())
            {
                    $mobile = $this->CI->agent->mobile();

            } else {

                $mobile = 0;
            }
            
            $SO = $this->CI->agent->platform();
            $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            
            $visita = array(
                "id_visita"			    => $this->CI->site_admin->m_gerarCodigo($gerarCodigo),
                "ip_visita" 		    => $ip_user,
                "pagina_visita" 	    => $url_atual,
                "pais_visita" 		    => $result_ip->country,
                "estado_visita" 	    => $result_ip->regionName,
                "cidade_visita" 	    => $result_ip->city,
                "navegador_visita" 	    => $browser,
                "dispositivo_visita"    => $mobile,
                "so_visita" 	        => $SO,
                "data_visita"		    => date('Ymd'),
                "hora_visita" 		    => date('H:i'),
                "token_visita" 		    => session_id()
            );
            
            $pesquisaVisita = array(
                'colunas'       => "*",
                'tabela'        => $tabela,
                'coluna_where'  => "token_visita",
                'valor_where'   => $visita['token_visita']
            );
            
            $resultadoPesquisa = $this->CI->site_admin->m_buscar_1_Item($pesquisaVisita);
            
            if(!$resultadoPesquisa)
            {
                $tabela = $tabela;
            
                $resultado = $this->CI->site_admin->m_criar($visita, $tabela);
    
            } 
        }
    }