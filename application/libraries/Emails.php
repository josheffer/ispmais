<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Emails {

    public function __construct()
    {
        $this->CI =& get_instance();
        
		$this->CI->load->model("newsletter/newsletter_model", "newsletter", TRUE);
		
        $this->CI->load->model("landingpage/Landingpage_model", "landingpage", TRUE);

        $this->CI->load->library('conversores');
    }

    public function dispara_emails()
    {
        require 'vendor/autoload.php';

        $mail = new PHPMailer(true);

        $EnviarSituacaoEmails = '0';
			
		$dadosConfigEnvios = $this->CI->newsletter->m_dadosConfigEnvios();
		
        $mail->isSMTP();
		$mail->isHTML(true);

        $mail->SMTPDebug 	= $dadosConfigEnvios->debug_configsEnvios;
        $mail->Host       	= $dadosConfigEnvios->host_configsEnvios;
        $mail->SMTPAuth   	= $dadosConfigEnvios->SMTPAuth_configsEnvios;
        $mail->Username   	= $dadosConfigEnvios->usuario_configsEnvios;
		$mail->Password   	= $dadosConfigEnvios->senha_configsEnvios; 
        $mail->SMTPSecure 	= $dadosConfigEnvios->SMTPSecure_configsEnvios;
        $mail->Port       	= $dadosConfigEnvios->porta_configsEnvios;
        $mail->CharSet    	= $dadosConfigEnvios->CharSet_configsEnvios;
        $mail->Encoding   	= $dadosConfigEnvios->Encoding_configsEnvios;
		
        $arrayDeEmails = $this->CI->newsletter->m_buscarEmailsDisponiveis($EnviarSituacaoEmails);
        
        $codigo = 0;

        $retorno = array();
        
        if (count($arrayDeEmails) > 0) {

            foreach ($arrayDeEmails as $row) {
                
                $codigo = $row->id_campanha;
                
                try {

                    $mail->setFrom('newsletter@solidsystem.net.br', $row->nome_campanha_envios);
                    $mail->Subject = "Olá ".$row->nomePessoa_envios ."! ". $row->nome_campanha_envios;
                    $body    = $row->mensagem_envios;
                    $mail->msgHTML($body);
                    $mail->addAddress($row->emails_envios, $row->nomePessoa_envios);
                    
                    if($mail->send())
                    {
                        $dados['id_envios'] = $row->id_envios;
                        $dados['statusEmails_envios'] = '1';

                        $this->CI->newsletter->m_atualizarStatusEmailEnviado($dados);
                        
                        $ret = true;
                        $msg = 'Campanha <b><i>'.$row->nome_campanha_envios.'</i></b> enviada com sucesso para o e-mail: '.$row->emails_envios;
                        
                    }
                    
                } catch (Exception $e) {
                    
                    $dados['id_envios'] = $row->id_envios;
                    $dados['statusEmails_envios'] = '2';
                    
                    $this->CI->newsletter->m_atualizarStatusEmailEnviado($dados);
                     
                    echo $e->getMessage(); 

                    $mail->getSMTPInstance()->reset();
                    
                }
                
                $mail->clearAddresses();
                
            }
            
            $at['id_campanha'] = $codigo;
            $at['statusBotao_campanha'] = '2';
            
            $this->CI->newsletter->m_atualizarBotao($at);

        }
        
    }

    public function enviarEmail($dados)
    {
		
		$whatsapp = $this->CI->conversores->soNumeros($dados['telefone']);
		
		require_once( BASEPATH .'database/DB'. EXT );
		$db =& DB();
		$query = $db->get('site_contato_configs');
		$result = $query->result();

        require_once 'vendor/phpmailer/phpmailer/src/Exception.php';
        require_once 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
        require_once 'vendor/phpmailer/phpmailer/src/SMTP.php';

        $mail = new PHPMailer(true);
		$mail->CharSet = "utf-8";

        $enviada = false;

        $mensagem = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
		<html>
		
		<head>
			<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
		
			<meta property='og:title' content='Cadastro Talisman Dark Age' />
            <title>Cadastro Talisman Dark Age</title>
            <link href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet' integrity='sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN' crossorigin='anonymous'>
			<style type='text/css'>
				/* Client-specific Styles */
				#outlook a {
					padding: 0;
				}
		
				/* Force Outlook to provide a 'view in browser' button. */
				body {
					width: 100% !important;
				}
		
				.ReadMsgBody {
					width: 100%;
				}
		
				.ExternalClass {
					width: 100%;
				}
		
				/* Force Hotmail to display emails at full width */
				body {
					-webkit-text-size-adjust: none;
				}
		
				/* Prevent Webkit platforms from changing default text sizes. */
		
				/* Reset Styles */
				body {
					margin: 0;
					padding: 0;
				}
		
				img {
					border: 0;
					height: auto;
					line-height: 100%;
					outline: none;
					text-decoration: none;
				}
		
				table td {
					border-collapse: collapse;
				}
		
				#backgroundTable {
					height: 100% !important;
					margin: 0;
					padding: 0;
					width: 100% !important;
				}
		
				/* Template Styles */
		
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: COMMON PAGE ELEMENTS /\/\/\/\/\/\/\/\/\/\ */
		
				/**
					* @tab Page
					* @section background color
					* @tip Set the background color for your email. You may want to choose one that matches your company's branding.
					* @theme page
					*/
				body,
				#backgroundTable {
					/*@editable*/
					background-color: #EEEEEE;
				}
		
				/**
					* @tab Page
					* @section email border
					* @tip Set the border for your email.
					*/
				#templateContainer {
					/*@editable*/
					border: 1px solid #BBBBBB;
				}
		
				/**
					* @tab Page
					* @section heading 1
					* @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
					* @style heading 1
					*/
				h1,
				.h1 {
					/*@editable*/
					color: #202020;
					display: block;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 34px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					line-height: 100%;
					margin-top: 0;
					margin-right: 0;
					margin-bottom: 10px;
					margin-left: 0;
					/*@editable*/
					text-align: left;
				}
		
				/**
					* @tab Page
					* @section heading 2
					* @tip Set the styling for all second-level headings in your emails.
					* @style heading 2
					*/
				h2,
				.h2 {
					/*@editable*/
					color: #202020;
					display: block;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 30px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					line-height: 100%;
					margin-top: 0;
					margin-right: 0;
					margin-bottom: 10px;
					margin-left: 0;
					/*@editable*/
					text-align: left;
				}
		
				/**
					* @tab Page
					* @section heading 3
					* @tip Set the styling for all third-level headings in your emails.
					* @style heading 3
					*/
				h3,
				.h3 {
					/*@editable*/
					color: #202020;
					display: block;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 26px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					line-height: 100%;
					margin-top: 0;
					margin-right: 0;
					margin-bottom: 10px;
					margin-left: 0;
					/*@editable*/
					text-align: left;
				}
		
				/**
					* @tab Page
					* @section heading 4
					* @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
					* @style heading 4
					*/
				h4,
				.h4 {
					/*@editable*/
					color: #202020;
					display: block;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 22px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					line-height: 100%;
					margin-top: 0;
					margin-right: 0;
					margin-bottom: 10px;
					margin-left: 0;
					/*@editable*/
					text-align: left;
				}
		
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: HEADER /\/\/\/\/\/\/\/\/\/\ */
		
				/**
					* @tab Header
					* @section header style
					* @tip Set the background color and border for your email's header area.
					* @theme header
					*/
				#templateHeader {
					/*@editable*/
					background-color: #FFFFFF;
					/*@editable*/
					border-bottom: 0;
				}
		
				/**
					* @tab Header
					* @section header text
					* @tip Set the styling for your email's header text. Choose a size and color that is easy to read.
					*/
				.headerContent {
					/*@editable*/
					color: #202020;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 34px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					line-height: 100%;
					/*@editable*/
					padding: 0;
					/*@editable*/
					text-align: center;
					/*@editable*/
					vertical-align: middle;
				}
		
				/**
					* @tab Header
					* @section header link
					* @tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
					*/
				.headerContent a:link,
				.headerContent a:visited,
				/* Yahoo! Mail Override */
				.headerContent a .yshortcuts
		
				/* Yahoo! Mail Override */
					{
					/*@editable*/
					color: #3498db;
					/*@editable*/
					font-weight: normal;
					/*@editable*/
					text-decoration: underline;
				}
		
				#headerImage {
					height: auto;
					max-width: 600px !important;
				}
		
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: MAIN BODY /\/\/\/\/\/\/\/\/\/\ */
		
				/**
					* @tab Body
					* @section body style
					* @tip Set the background color for your email's body area.
					*/
				#templateContainer,
				.bodyContent {
					/*@editable*/
					background-color: #FFFFFF;
				}
		
				/**
					* @tab Body
					* @section body text
					* @tip Set the styling for your email's main content text. Choose a size and color that is easy to read.
					* @theme main
					*/
				.bodyContent div {
					/*@editable*/
					color: #505050;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 14px;
					/*@editable*/
					line-height: 150%;
					/*@editable*/
					text-align: left;
				}
		
				/**
					* @tab Body
					* @section body link
					* @tip Set the styling for your email's main content links. Choose a color that helps them stand out from your text.
					*/
				.bodyContent div a:link,
				.bodyContent div a:visited,
				/* Yahoo! Mail Override */
				.bodyContent div a .yshortcuts
		
				/* Yahoo! Mail Override */
					{
					/*@editable*/
					color: #3498db;
					/*@editable*/
					font-weight: normal;
					/*@editable*/
					text-decoration: underline;
				}
		
				/**
					* @tab Body
					* @section data table style
					* @tip Set the background color and border for your email's data table.
					*/
				.templateDataTable {
					/*@editable*/
					background-color: #FFFFFF;
					/*@editable*/
					border: 1px solid #DDDDDD;
				}
		
				/**
					* @tab Body
					* @section data table heading text
					* @tip Set the styling for your email's data table text. Choose a size and color that is easy to read.
					*/
				.dataTableHeading {
					/*@editable*/
					background-color: #dceffc;
					/*@editable*/
					color: #3498db;
					/*@editable*/
					font-family: Helvetica;
					/*@editable*/
					font-size: 14px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					line-height: 150%;
					/*@editable*/
					text-align: left;
				}
		
				/**
					* @tab Body
					* @section data table heading link
					* @tip Set the styling for your email's data table links. Choose a color that helps them stand out from your text.
					*/
				.dataTableHeading a:link,
				.dataTableHeading a:visited,
				/* Yahoo! Mail Override */
				.dataTableHeading a .yshortcuts
		
				/* Yahoo! Mail Override */
					{
					/*@editable*/
					color: #FFFFFF;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					text-decoration: underline;
				}
		
				/**
					* @tab Body
					* @section data table text
					* @tip Set the styling for your email's data table text. Choose a size and color that is easy to read.
					*/
				.dataTableContent {
					/*@editable*/
					border-top: 1px solid #DDDDDD;
					/*@editable*/
					border-bottom: 0;
					/*@editable*/
					color: #202020;
					/*@editable*/
					font-family: Helvetica;
					/*@editable*/
					font-size: 12px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					line-height: 150%;
					/*@editable*/
					text-align: left;
				}
		
				/**
					* @tab Body
					* @section data table link
					* @tip Set the styling for your email's data table links. Choose a color that helps them stand out from your text.
					*/
				.dataTableContent a:link,
				.dataTableContent a:visited,
				/* Yahoo! Mail Override */
				.dataTableContent a .yshortcuts
		
				/* Yahoo! Mail Override */
					{
					/*@editable*/
					color: #202020;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					text-decoration: underline;
				}
		
				/**
					* @tab Body
					* @section button style
					* @tip Set the styling for your email's button. Choose a style that draws attention.
					*/
				.templateButton {
					/*@editable*/
					background-color: #3498db;
					/*@editable*/
					border: 0;
					border-collapse: separate !important;
					border-bottom: 4px solid #2980b9;
				}
		
				/**
					* @tab Body
					* @section button style
					* @tip Set the styling for your email's button. Choose a style that draws attention.
					*/
				.templateButton,
				.templateButton a:link,
				.templateButton a:visited,
				/* Yahoo! Mail Override */
				.templateButton a .yshortcuts
		
				/* Yahoo! Mail Override */
					{
					/*@editable*/
					color: #FFFFFF;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 15px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					letter-spacing: -.5px;
					/*@editable*/
					line-height: 100%;
					text-align: center;
					text-decoration: none;
				}
		
				.bodyContent img {
					display: inline;
					height: auto;
				}
		
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: FOOTER /\/\/\/\/\/\/\/\/\/\ */
		
				/**
					* @tab Footer
					* @section footer style
					* @tip Set the background color and top border for your email's footer area.
					* @theme footer
					*/
				#templateFooter {
					/*@editable*/
					background-color: #FFFFFF;
					/*@editable*/
					border-top: 0;
				}
		
				/**
					* @tab Footer
					* @section footer text
					* @tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
					* @theme footer
					*/
				.footerContent div {
					/*@editable*/
					color: #707070;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 12px;
					/*@editable*/
					line-height: 125%;
					/*@editable*/
					text-align: center;
				}
		
				/**
					* @tab Footer
					* @section footer link
					* @tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
					*/
				.footerContent div a:link,
				.footerContent div a:visited,
				/* Yahoo! Mail Override */
				.footerContent div a .yshortcuts
		
				/* Yahoo! Mail Override */
					{
					/*@editable*/
					color: #3498db;
					/*@editable*/
					font-weight: normal;
					/*@editable*/
					text-decoration: underline;
				}
		
				.footerContent img {
					display: inline;
				}
		
				/**
					* @tab Footer
					* @section utility bar style
					* @tip Set the background color and border for your email's footer utility bar.
					* @theme footer
					*/
				#utility {
					/*@editable*/
					background-color: #FFFFFF;
					/*@editable*/
					border: 0;
				}
		
				/**
					* @tab Footer
					* @section utility bar style
					* @tip Set the background color and border for your email's footer utility bar.
					*/
				#utility div {
					/*@editable*/
					text-align: center;
				}
		
				#monkeyRewards img {
					max-width: 190px;
				}
			</style>
		</head>
		
		<body leftmargin='0' marginwidth='0' topmargin='0' marginheight='0' offset='0'>
			<center>
				<table border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' id='backgroundTable'>
					<tr>
						<td align='center' valign='top' style='padding-top:20px;'>
							<table border='0' cellpadding='0' cellspacing='0' width='600' id='templateContainer'>
								<tr>
									<td align='center' valign='top'>
		
										<table border='0' cellpadding='0' cellspacing='0' width='600' id='templateHeader' style='margin-top: 30px'>
											<tr>
												<td class='headerContent'>
		
													<img src='https://i.imgur.com/FwuYFu9.png' style='max-width:300px;'
														id='headerImage campaign-icon' mc:label='header_image'
														mc:edit='header_image' mc:allowdesigner mc:allowtext />
		
												</td>
											</tr>
										</table>
		
									</td>
								</tr>
								<tr>
									<td align='center' valign='top'>
		
										<table border='0' cellpadding='0' cellspacing='0' width='600' id='templateBody'>
											<tr>
												<td valign='top'>
		
													<table border='0' cellpadding='20' cellspacing='0' width='100%'>
														<tr>
															
															<td valign='top' class='bodyContent'>
																
																<hr><br>
		
																<div mc:edit='std_content00'>
																	<center style='color: #a47b4c;'><strong>".$dados['mensagem']."</strong></center>
																</div>
															</td>
														</tr>
														<tr>
															<td valign='top' style='padding-top:0; padding-bottom:0;'>
		
																<table border='0' cellpadding='10' cellspacing='0' width='100%' class='templateDataTable'>
                                                                    
                                                                    <tr mc:repeatable>
																		
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content00'>
																			Assunto:
																		</td>
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content01'>
																			".$dados['assunto']."
																		</td>
																	
                                                                    </tr>
                                                                    
                                                                    <tr mc:repeatable>
																		
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content00'>
																			Nome:
																		</td>
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content01'>
																			".$dados['nome']."
																		</td>
																	
                                                                    </tr>
                                                                    
																	<tr mc:repeatable>
																		
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content00'>
																			E-mail:
																		</td>
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content01'>
																			".$dados['email']."
																		</td>
																	
                                                                    </tr>
		
																	<tr mc:repeatable>
																		
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content00'>
																			Telefone:
																		</td>
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content01'>
																			<strong>".$dados['telefone']." &nbsp; &nbsp;<a href='https://api.whatsapp.com/send?phone=55".$whatsapp."' target='_blank' rel='noopener noreferrer'>[>>WhatsApp<<]</a></strong> 
																		</td>
																	
																	</tr>
																	
		
																</table>
		
															</td>
														</tr>
														
													</table>
		
												</td>
											</tr>
										</table>
		
									</td>
								</tr>
								<tr>
									<td align='center' valign='top'>
		
										<table border='0' cellpadding='10' cellspacing='0' width='600' id='templateFooter'>
											<tr>
												<td valign='top' class='footerContent'>
		
													<table border='0' cellpadding='10' cellspacing='0' width='100%'>
														
														<tr>
															<td valign='middle' id='utility'>
																<div mc:edit='std_utility'>
                                                                    <strong>
                                                                        <a href='https://goo.gl/maps/SVKqCGpt4oH4kodA6' target='_blank' style='text-decoration: none; color: #a47b4c;'>
                                                                            R. 83, 582 - St. Sul, Goiânia - GO, 74083-195
                                                                        </a>
                                                                    </strong>
																</div>
															</td>
														</tr>
		
													</table>
		
													<table border='0' cellpadding='10' cellspacing='0' width='100%'>
														
															<tr>
																<td valign='middle' id='utility'>
																	<div mc:edit='std_utility'>
																		&nbsp;
																		<a href='' target='_blank'>
																		   <span> </span>
																		</a> &nbsp;
																	</div>
																</td>
															</tr>
														</table>
												</td>
											</tr>
										</table>
		
									</td>
								</tr>
							</table>
							<br />
						</td>
					</tr>
				</table>
			</center>
		</body>
		
		</html>";

        try {
			
			$mail->SMTPDebug    = $result[0]->debug_contato;                                       
            $mail->isSMTP();                                           
            $mail->Host         = $result[0]->host_contato; 
            $mail->SMTPAuth     = $result[0]->SMTPAuth_contato;                              
            $mail->Username     = $result[0]->username_contato;                     
            $mail->Password     = $result[0]->password_contato;                             
            $mail->SMTPSecure   = $result[0]->SMTPSecure_contato;                        
			$mail->Port         = $result[0]->port_contato;
			$mail->Encoding = 'base64';
			
            $mail->setFrom($dados['email'], "Mensagem de: ".$dados['nome']);

            $mail->addAddress($result[0]->addAddress_contato);

            // Content
            $mail->isHTML(true);
            $mail->Subject = html_entity_decode($dados['assunto']);
            $mail->Body    = $mensagem;

            $enviada = $mail->send();

        } catch (Exception $e) {
            
            $enviada = false;

        }

        return $enviada;

	}
	
	public function eviaEmailRecuperarSenha($dados, $dadosEmpresa)
	{

		require 'vendor/autoload.php';

		$logoEmpresa = base_url('assets/painel/images/empresa/logotipo/').$dadosEmpresa->imagem_empresa;
		
		$mail = new PHPMailer(true);

		$mail->CharSet = "utf-8";

		$enviada = false;
		
		$urlPagina = base_url('/dashboard/alterarSenha?id_recuperarSenha='.$dados->token_usuario);
		
		$mensagem = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

		<html xmlns="http://www.w3.org/1999/xhtml" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
		
		<head>
			<meta name="viewport" content="width=device-width" />
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<title>'.$dadosEmpresa->nomeFantasia_empresa.'</title>
		</head>
		
		<body itemscope tyle="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
		
			<table class="body-wrap"
				style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;"
				bgcolor="#f6f6f6">
		
				<tr
					style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
		
					<td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
						valign="top"></td>
		
					<td class="container" width="600"
						style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;"
						valign="top">
		
						<div class="content"
							style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
		
							<table class="main" width="100%" cellpadding="0" cellspacing="0"
								style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;"
								bgcolor="#fff">
		
								<tr
									style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
		
									<td class=""
										style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #38414a; margin: 0; padding: 20px;"
										align="center" bgcolor="#71b6f9" valign="top">
										<a href="'.base_url().'"> <img src="'.$logoEmpresa.'" height="120" alt="logo" /></a>
										<br />
										<span style="margin-top: 10px;display: block;">Importante: Se você não solicitou o link para recuperação de senha desconsidere esse e-mail.</span>
									</td>
								</tr>
								<tr
									style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
									<td class="content-wrap"
										style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;"
										valign="top">
										<table width="100%" cellpadding="0" cellspacing="0"
											style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
											<tr
												style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
												<td class="content-block"
													style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
													valign="top"> Olá <strong>'.$dados->nome_usuario.'</strong>, como solicitado no painel, te enviamos um link para recuperação de senha, o passo a seguir é muito simples, clique no botão abaixo e insira sua nova senha.
												</td>
											</tr>
											
											<tr
												style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; text-align: center;">
												<td class="content-block"
													style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
													valign="top">
													<a href="'.$urlPagina.'" class="btn-primary"
														style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #02c0ce; margin: 0; border-color: #02c0ce; border-style: solid; border-width: 8px 16px;">Alterar minha senha!</a>
												</td>
											</tr>

											<tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; text-align: center;">
												<td class="content-block"
													style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
													valign="top"> Caso o botão não apareça, clique no link abaixo:
												</td>
												
											</tr>

											<tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; text-align: center;">
												<td class="content-block"
													style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
													valign="top"> <b><a href="'.$urlPagina.'" target="_blank" rel="noopener noreferrer">'.$urlPagina.'</a></b>
												</td>
											</tr>

										</table>
									</td>
								</tr>
							</table>
							
							<div class="footer"
								style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;">
								<table width="100%"
									style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
									<tr
										style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
										<td class="aligncenter content-block"
											style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;"
											align="center" valign="top">
											<a href="'.base_url().'"
												style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; color: #999; text-decoration: underline; margin: 0;">'.base_url().'</a>
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					</td>
					<td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
						valign="top"></td>
				</tr>
			</table>
		</body>
		
		</html>';

		$dadosConfigLP = $this->CI->landingpage->buscarConfisgs($id=1);
		
		$dadosConfigLPages 	= $dadosConfigLP[0];
		
		try {
				
			$mail->SMTPDebug 	= $dadosConfigLPages->debug_config_landingpage;
			$mail->isSMTP();
			$mail->Host      	= $dadosConfigLPages->host_config_landingpage;
			$mail->SMTPAuth   	= $dadosConfigLPages->auth_config_landingpage;
			$mail->Username		= $dadosConfigLPages->usuario_config_landingpage;                     
			$mail->Password     = $dadosConfigLPages->senha_config_landingpage;
			$mail->SMTPSecure 	= $dadosConfigLPages->seguranca_config_landingpage;
			$mail->Port       	= $dadosConfigLPages->porta_config_landingpage;
			$mail->Encoding 	= $dadosConfigLPages->encoding_config_landingpage;
			$mail->CharSet    	= $dadosConfigLPages->charset_config_landingpage;
			
			$mail->setFrom($mail->Username, "Link para recuperar sua senha");
			$mail->addAddress($dados->email_usuario, $dados->nome_usuario);
			
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Olá '.$dados->nome_usuario.', vamos recuperar sua senha!';
			$mail->Body    = $mensagem;
			
			if($mail->send())
			{
				$retorno['ret'] = true;
				$retorno['msg'] = "Enviado";
				
				return $retorno;
			}
			
		} catch (Exception $e) {

			$retorno['ret'] = false;
			$retorno['msg'] = "Não enviado";
			
			return $enviada;
		}
		
		
	}
	
	public function eviaEmailCadastroNovoUsuario($infos, $dadosEmpresa)
	{
		$infos = (object)$infos;

		require 'vendor/autoload.php';

		$logoEmpresa = base_url('assets/painel/images/empresa/logotipo/').$dadosEmpresa->imagem_empresa;
		
		$mail = new PHPMailer(true);

		$mail->CharSet = "utf-8";

		$enviada = false;
		
		$urlPagina = base_url('/dashboard');
		
		$mensagem = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

		<html xmlns="http://www.w3.org/1999/xhtml" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
		
		<head>
			<meta name="viewport" content="width=device-width" />
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<title>'.$dadosEmpresa->nomeFantasia_empresa.'</title>
		</head>
		
		<body itemscope tyle="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
		
			<table class="body-wrap"
				style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;"
				bgcolor="#f6f6f6">
		
				<tr
					style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
		
					<td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
						valign="top"></td>
		
					<td class="container" width="600"
						style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;"
						valign="top">
		
						<div class="content"
							style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
		
							<table class="main" width="100%" cellpadding="0" cellspacing="0"
								style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;"
								bgcolor="#fff">
		
								<tr
									style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
		
									<td class=""
										style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #38414a; margin: 0; padding: 20px;"
										align="center" bgcolor="#71b6f9" valign="top">
										<a href="'.base_url("/dashboard").'"> <img src="'.$logoEmpresa.'" height="120" alt="logo" /></a>
										<br />
										<span style="margin-top: 10px;display: block;">Seu cadastro no painel administrativo da '.$dadosEmpresa->nomeFantasia_empresa.' foi criado com sucesso!</span>
									</td>
								</tr>
								<tr
									style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
									<td class="content-wrap"
										style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;"
										valign="top">
										<table width="100%" cellpadding="0" cellspacing="0"
											style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
											<tr
												style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
												<td class="content-block"
													style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
													valign="top"> Olá <strong>'.$infos->nomeUsuarop.'</strong>, anote seus dados para acessar o painel administrativo em um lugar seguro.
												</td>
											</tr>
											
											<tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; text-align: center;">
												<td class="content-block"
													style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
													valign="top"> Usuário: '.$infos->loginUsuario.'
												</td>
											</tr>

											<tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; text-align: center;">
												<td class="content-block"
													style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
													valign="top"> E-mail: '.$infos->emailUsuario.'
												</td>
											</tr>

											<tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; text-align: center;">
												<td class="content-block"
													style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
													valign="top"> Senha temporária: '.$infos->senhaUsuario.'
												</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
							
							<div class="footer"
								style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;">
								<table width="100%"
									style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
									<tr
										style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
										<td class="aligncenter content-block"
											style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;"
											align="center" valign="top"><a href="'.base_url().'"
												style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; color: #999; text-decoration: underline; margin: 0;">'.base_url().'</a>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</td>
					<td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
						valign="top"></td>
				</tr>
			</table>
		</body>
		
		</html>';

		$dadosConfigLP = $this->CI->landingpage->buscarConfisgs($id=1);
		
		$dadosConfigLPages 	= $dadosConfigLP[0];
		
		try {
				
			$mail->SMTPDebug 	= $dadosConfigLPages->debug_config_landingpage;
			$mail->isSMTP();
			$mail->Host      	= $dadosConfigLPages->host_config_landingpage;
			$mail->SMTPAuth   	= $dadosConfigLPages->auth_config_landingpage;
			$mail->Username		= $dadosConfigLPages->usuario_config_landingpage;                     
			$mail->Password     = $dadosConfigLPages->senha_config_landingpage;
			$mail->SMTPSecure 	= $dadosConfigLPages->seguranca_config_landingpage;
			$mail->Port       	= $dadosConfigLPages->porta_config_landingpage;
			$mail->Encoding 	= $dadosConfigLPages->encoding_config_landingpage;
			$mail->CharSet    	= $dadosConfigLPages->charset_config_landingpage;
			
			$mail->setFrom($mail->Username, $dadosEmpresa->nomeFantasia_empresa);
			$mail->addAddress($infos->emailUsuario, $infos->nomeUsuarop);
			
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Olá '.$infos->nomeUsuarop.', seu perfil foi criado!';
			$mail->Body    = $mensagem;
			
			if($mail->send())
			{
				$retorno['ret'] = true;
				$retorno['msg'] = "Enviado";
				
				return $retorno;
			}
			
		} catch (Exception $e) {

			$retorno['ret'] = false;
			$retorno['msg'] = "Não enviado";
			
			return $enviada;
		}
		
		
	}

	public function notificaLeadEmail($infos, $dadosEmpresa, $dadosLandingPage, $telefone, $configsLandingPage)
	{
		$infos = (object)$infos;
		$dadosLandingPage = (object)$dadosLandingPage[0];
		$configsLandingPage = (object)$configsLandingPage[0];
		
		require 'vendor/autoload.php';

		$logoEmpresa = base_url('assets/painel/images/empresa/logotipo/').$dadosEmpresa->imagem_empresa;
		
		$mail = new PHPMailer(true);

		$mail->CharSet = "utf-8";

		$enviada = false;
		
		$urlPagina = base_url('/dashboard');
		
		$mensagem = '<!DOCTYPE html>
		<html>
		
		<head>
		
			<title>Novo lead</title>
		
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		
			<style type="text/css">
				/* CLIENT-SPECIFIC STYLES */
				body,
				table,
				td,
				a {
					-webkit-text-size-adjust: 100%;
					-ms-text-size-adjust: 100%;
				}
		
				/* Prevent WebKit and Windows mobile changing default text sizes */
				table,
				td {
					mso-table-lspace: 0pt;
					mso-table-rspace: 0pt;
				}
		
				/* Remove spacing between tables in Outlook 2007 and up */
				img {
					-ms-interpolation-mode: bicubic;
				}
		
				/* Allow smoother rendering of resized image in Internet Explorer */
		
				/* RESET STYLES */
				img {
					border: 0;
					height: auto;
					line-height: 100%;
					outline: none;
					text-decoration: none;
				}
		
				table {
					border-collapse: collapse !important;
				}
		
				body {
					height: 100% !important;
					margin: 0 !important;
					padding: 0 !important;
					width: 100% !important;
				}
		
				/* iOS BLUE LINKS */
				a[x-apple-data-detectors] {
					color: inherit !important;
					text-decoration: none !important;
					font-size: inherit !important;
					font-family: inherit !important;
					font-weight: inherit !important;
					line-height: inherit !important;
				}
		
				/* MOBILE STYLES */
				@media screen and (max-width: 525px) {
		
					/* ALLOWS FOR FLUID TABLES */
					.wrapper {
						width: 100% !important;
						max-width: 100% !important;
					}
		
					/* ADJUSTS LAYOUT OF LOGO IMAGE */
					.logo img {
						margin: 0 auto !important;
					}
		
					/* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
					.mobile-hide {
						display: none !important;
					}
		
					.img-max {
						max-width: 100% !important;
						width: 100% !important;
						height: auto !important;
					}
		
					/* FULL-WIDTH TABLES */
					.responsive-table {
						width: 100% !important;
					}
		
					/* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
					.padding {
						padding: 10px 5% 15px 5% !important;
					}
		
					.padding-meta {
						padding: 30px 5% 0px 5% !important;
						text-align: center;
					}
		
					.padding-copy {
						padding: 10px 5% 10px 5% !important;
						text-align: center;
					}
		
					.no-padding {
						padding: 0 !important;
					}
		
					.section-padding {
						padding: 50px 15px 50px 15px !important;
					}
		
					/* ADJUST BUTTONS ON MOBILE */
					.mobile-button-container {
						margin: 0 auto;
						width: 100% !important;
					}
		
					.mobile-button {
						padding: 15px !important;
						border: 0 !important;
						font-size: 16px !important;
						display: block !important;
					}
		
				}
		
				/* ANDROID CENTER FIX */
				div[style*="margin: 16px 0;"] {
					margin: 0 !important;
				}
			</style>
		
		</head>
		
		<body style="margin: 0 !important; padding: 0 !important;">
		
			<!-- HIDDEN PREHEADER TEXT -->
			<div
				style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
				Novo cadastro de lead de: <strong><u><i>'.$infos->nome_cadastros.'</i></u></strong>
			</div>
		
			<!-- HEADER -->
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td bgcolor="#ffffff" align="center">
						<!--[if (gte mso 9)|(IE)]>
					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
					<tr>
					<td align="center" valign="top" width="500">
					<![endif]-->
						<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;"
							class="wrapper">
							<tr>
								<td align="center" valign="top" style="padding: 15px 0; background-color: #bedeff;" class="logo">
									<a href="" target="_blank">
										<img alt="Logo" src="'.$logoEmpresa.'" width="220"
											style="display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;"
											border="0">
									</a>
								</td>
							</tr>
						</table>
						<!--[if (gte mso 9)|(IE)]>
					</td>
					</tr>
					</table>
					<![endif]-->
					</td>
				</tr>
				
				<tr>
					<td bgcolor="#ffffff" align="center" style="padding: 15px;">
						<!--[if (gte mso 9)|(IE)]>
					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
					<tr>
					<td align="center" valign="top" width="500">
					<![endif]-->
						<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;"
							class="responsive-table">
							<tr>
								<td>
									<!-- COPY -->
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="center"
												style="font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;"
												class="padding-copy">Novo lead vindo pela landing page: <a href="'.$dadosLandingPage->link_landingPage.'" target="_blank"><strong><u><i>'.$dadosLandingPage->nome_landingPage.'</i></u></strong></a> !</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--[if (gte mso 9)|(IE)]>
					</td>
					</tr>
					</table>
					<![endif]-->
					</td>
				</tr>

				<tr>
					<td bgcolor="#ffffff" align="center" style="padding: 15px;" class="padding">
						<!--[if (gte mso 9)|(IE)]>
					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
					<tr>
					<td align="center" valign="top" width="500">
					<![endif]-->
						<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;"
							class="responsive-table">
							
							<tr>
								<td style="padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 1px dashed #aaaaaa;">
									<!-- TWO COLUMNS -->
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tr>
											<td valign="top" class="mobile-wrapper">
												<!-- LEFT COLUMN -->
												<table cellpadding="0" cellspacing="0" border="0" width="47%"
													style="width: 27%;" align="left">
													<tr>
														<td style="padding: 0 0 10px 0;">
															<table cellpadding="0" cellspacing="0" border="0" width="100%">
																<tr>
																	<td align="left"
																		style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
																		Nome:</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- RIGHT COLUMN -->
												<table cellpadding="0" cellspacing="0" border="0" width="47%"
													style="width: 67%;" align="right">
													<tr>
														<td style="padding: 0 0 10px 0;">
															<table cellpadding="0" cellspacing="0" border="0" width="100%">
																<tr>
																	<td align="right"
																		style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
																		'.$infos->nome_cadastros.'</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							
							<tr>
								<td>
									<!-- TWO COLUMNS -->
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tr>
											<td valign="top" style="padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 1px dashed #aaaaaa;" class="mobile-wrapper">
												<!-- LEFT COLUMN -->
												<table cellpadding="0" cellspacing="0" border="0" width="47%"
													style="width: 27%;" align="left">
													<tr>
														<td style="padding: 0 0 10px 0;">
															<table cellpadding="0" cellspacing="0" border="0" width="100%">
																<tr>
																	<td align="left"
																		style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
																		Telefone: </td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- RIGHT COLUMN -->
												<table cellpadding="0" cellspacing="0" border="0" width="47%"
													style="width: 67%;" align="right">
													<tr>
														<td style="padding: 0 0 10px 0;">
															<table cellpadding="0" cellspacing="0" border="0" width="100%">
																<tr>
																	<td align="right"
																		style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
																		'.$telefone.' <a href="https://api.whatsapp.com/send?phone='.$infos->telefone_cadastros.'" target="_blank">(WhatsApp)</a>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
		
							<tr>
								<td>
									<!-- TWO COLUMNS -->
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tr>
											<td valign="top" style="padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 1px dashed #aaaaaa;" class="mobile-wrapper">
												<!-- LEFT COLUMN -->
												<table cellpadding="0" cellspacing="0" border="0" width="47%"
													style="width: 27%;" align="left">
													<tr>
														<td style="padding: 0 0 10px 0;">
															<table cellpadding="0" cellspacing="0" border="0" width="100%">
																<tr>
																	<td align="left"
																		style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
																		E-mail:</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- RIGHT COLUMN -->
												<table cellpadding="0" cellspacing="0" border="0" width="47%"
													style="width: 67%;" align="right">
													<tr>
														<td style="padding: 0 0 10px 0;">
															<table cellpadding="0" cellspacing="0" border="0" width="100%">
																<tr>
																	<td align="right"
																		style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
																		'.$infos->email_cadastros.'</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>

							<tr>
								<td>
									<!-- TWO COLUMNS -->
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tr>
											<td valign="top" style="padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 1px dashed #aaaaaa;" class="mobile-wrapper">
												<!-- LEFT COLUMN -->
												<table cellpadding="0" cellspacing="0" border="0" width="47%"
													style="width: 27%;" align="left">
													<tr>
														<td style="padding: 0 0 10px 0;">
															<table cellpadding="0" cellspacing="0" border="0" width="100%">
																<tr>
																	<td align="left"
																		style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
																		Cidade:</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- RIGHT COLUMN -->
												<table cellpadding="0" cellspacing="0" border="0" width="47%"
													style="width: 67%;" align="right">
													<tr>
														<td style="padding: 0 0 10px 0;">
															<table cellpadding="0" cellspacing="0" border="0" width="100%">
																<tr>
																	<td align="right"
																		style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
																		'.$infos->cidade_cadastros.'/'.$infos->estado_cadastros.'</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>

							<tr>
								<td>
									<!-- TWO COLUMNS -->
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tr>
											<td valign="top" style="padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 1px dashed #aaaaaa;" class="mobile-wrapper">
												<!-- LEFT COLUMN -->
												<table cellpadding="0" cellspacing="0" border="0" width="47%"
													style="width: 27%;" align="left">
													<tr>
														<td style="padding: 0 0 10px 0;">
															<table cellpadding="0" cellspacing="0" border="0" width="100%">
																<tr>
																	<td align="left"
																		style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
																		Como conheceu:</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- RIGHT COLUMN -->
												<table cellpadding="0" cellspacing="0" border="0" width="47%"
													style="width: 67%;" align="right">
													<tr>
														<td style="padding: 0 0 10px 0;">
															<table cellpadding="0" cellspacing="0" border="0" width="100%">
																<tr>
																	<td align="right"
																		style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
																		'.$infos->ondeNosConheceu_cadastros.'</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							
						</table>
						<!--[if (gte mso 9)|(IE)]>
					</td>
					</tr>
					</table>
					<![endif]-->
					</td>
				</tr>
		
				<tr>
					<td bgcolor="#ffffff" align="center" style="padding: 15px;">
						<!--[if (gte mso 9)|(IE)]>
					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
					<tr>
					<td align="center" valign="top" width="500">
					<![endif]-->
						<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;"
							class="responsive-table">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
												<!-- COPY -->
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td align="left"
															style="padding: 0 0 0 0; font-size: 14px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color: #aaaaaa; font-style: italic;"
															class="padding-copy">Além de receber essa notificação por e-mail, também está registrado no painel administrativo. Para acessar o painel, use o botão abaixo!</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--[if (gte mso 9)|(IE)]>
					</td>
					</tr>
					</table>
					<![endif]-->
					</td>
				</tr>
		
				<tr>
					<td bgcolor="#ffffff" align="center" style="padding: 15px;">
						<table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										
										<tr>
											<td align="center">
												<!-- BULLETPROOF BUTTON -->
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td align="center" style="padding-top: 25px;" class="padding">
															<table border="0" cellspacing="0" cellpadding="0"
																class="mobile-button-container">
																<tr>
																	<td align="center" style="border-radius: 3px;" bgcolor="#256F9C">
																		<a href="'.base_url("/dashboard").'" target="_blank" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; border-radius: 3px; padding: 15px 25px; border: 1px solid #256F9C; display: inline-block;"
																			class="mobile-button">Acessar painel</a></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--[if (gte mso 9)|(IE)]>
					</td>
					</tr>
					</table>
					<![endif]-->
					</td>
				</tr>
				
				<tr>
					<td bgcolor="#ffffff" align="center" style="padding: 20px 0px;">
						<!--[if (gte mso 9)|(IE)]>
					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
					<tr>
					<td align="center" valign="top" width="500">
					<![endif]-->
						<!-- UNSUBSCRIBE COPY -->
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 500px;"
							class="responsive-table">
							<tr>
								<td align="center"
									style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;">
									<a href="'.base_url().'" target="_blank">'.base_url().'</a>
								</td>
							</tr>
						</table>
						<!--[if (gte mso 9)|(IE)]>
					</td>
					</tr>
					</table>
					<![endif]-->
					</td>
				</tr>
				
			</table>
		
		</body>
		
		</html>';

		$dadosConfigLP = $this->CI->landingpage->buscarConfisgs($id=1);
		
		$dadosConfigLPages = $dadosConfigLP[0];
		
		try {
			
			$mail->SMTPDebug 	= $dadosConfigLPages->debug_config_landingpage;
			$mail->isSMTP();
			$mail->Host      	= $dadosConfigLPages->host_config_landingpage;
			$mail->SMTPAuth   	= $dadosConfigLPages->auth_config_landingpage;
			$mail->Username		= $dadosConfigLPages->usuario_config_landingpage;                     
			$mail->Password     = $dadosConfigLPages->senha_config_landingpage;
			$mail->SMTPSecure 	= $dadosConfigLPages->seguranca_config_landingpage;
			$mail->Port       	= $dadosConfigLPages->porta_config_landingpage;
			$mail->Encoding 	= $dadosConfigLPages->encoding_config_landingpage;
			$mail->CharSet    	= $dadosConfigLPages->charset_config_landingpage;
			
			$mail->setFrom($mail->Username, "Lead de ".$dadosEmpresa->nomeFantasia_empresa);
			
			$mail->addAddress($configsLandingPage->email_landingPage, $configsLandingPage->nomeEmail_landingPage);
			
			$mail->isHTML(true);
			$mail->Subject = "Novo lead de: ".$infos->nome_cadastros;
			$mail->Body    = $mensagem;
			
			if($mail->send())
			{
				$retorno['ret'] = true;
				$retorno['msg'] = "Enviado";
				
				return $retorno;
			}
			
		} catch (Exception $e) {

			$retorno['ret'] = false;
			$retorno['msg'] = "Não enviado";
			
			return $enviada;
		}
		
		
	}

}