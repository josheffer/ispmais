<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datas {

    function dataAnual($dataAnual)
    {
        $jan_ini    = date('Ymd',   mktime(0, 0, 0, '1' , 1 ,   $dataAnual));
        $jan_fim    = date('Ymd',   mktime(23, 59, 59, '1',     date("t"), $dataAnual));

        $fev_ini    = date('Ymd',   mktime(0, 0, 0, '2' , 1 ,   $dataAnual));
        $fev_dia    = date("t", mktime(0,0,0, '2','01', $dataAnual));
        $fev_fim    = $dataAnual."02".$fev_dia;
        
        $mar_ini    = date('Ymd',   mktime(0, 0, 0, '3' , 1 ,   $dataAnual));
        $mar_fim    = date('Ymd',   mktime(23, 59, 59, '3',     date("t"), $dataAnual));

        $abr_ini    = date('Ymd',   mktime(0, 0, 0, '4' , 1 ,   $dataAnual));
        $abr_Fim    = date('Ymd',   mktime(23, 59, 59, '4',     date("t"), $dataAnual));

        $mai_ini    = date('Ymd',   mktime(0, 0, 0, '5' , 1 ,   $dataAnual));
        $mai_fim    = date('Ymd',   mktime(23, 59, 59, '5',     date("t"), $dataAnual));

        $jun_ini    = date('Ymd',   mktime(0, 0, 0, '6' , 1 ,   $dataAnual));
        $jun_fim    = date('Ymd',   mktime(23, 59, 59, '6',     date("t"), $dataAnual));

        $jul_ini    = date('Ymd',   mktime(0, 0, 0, '7' , 1 ,   $dataAnual));
        $jul_fim    = date('Ymd',   mktime(23, 59, 59, '7',     date("t"), $dataAnual));

        $ago_ini    = date('Ymd',    mktime(0, 0, 0, '8' , 1 ,  $dataAnual));
        $ago_fim    = date('Ymd',   mktime(23, 59, 59, '8',     date("t"), $dataAnual));

        $set_ini    = date('Ymd',   mktime(0, 0, 0, '9' , 1 ,   $dataAnual));
        $set_fim    = date('Ymd',   mktime(23, 59, 59, '9',     date("t"), $dataAnual));

        $out_ini    = date('Ymd',   mktime(0, 0, 0, '10' , 1 ,  $dataAnual));
        $out_fim    = date('Ymd',   mktime(23, 59, 59, '10',    date("t"), $dataAnual));

        $nov_ini    = date('Ymd',   mktime(0, 0, 0, '11' , 1 ,  $dataAnual));
        $nov_fim    = date('Ymd',   mktime(23, 59, 59, '11',    date("t"), $dataAnual));

        $dez_ini    = date('Ymd',   mktime(0, 0, 0, '12' , 1 ,  $dataAnual));
        $dez_fim    = date('Ymd',   mktime(23, 59, 59, '12',    date("t"), $dataAnual));

        $datas = array(
            'jan_ini' => $jan_ini, 'jan_fim' => $jan_fim,
            'fev_ini' => $fev_ini, 'fev_fim' => $fev_fim,
            'mar_ini' => $mar_ini, 'mar_fim' => $mar_fim,
            'abr_ini' => $abr_ini, 'abr_fim' => $abr_Fim,
            'mai_ini' => $mai_ini, 'mai_fim' => $mai_fim,
            'jun_ini' => $jun_ini, 'jun_fim' => $jun_fim,
            'jul_ini' => $jul_ini, 'jul_fim' => $jul_fim,
            'ago_ini' => $ago_ini, 'ago_fim' => $ago_fim,
            'set_ini' => $set_ini, 'set_fim' => $set_fim,
            'out_ini' => $out_ini, 'out_fim' => $out_fim ,
            'nov_ini' => $nov_ini, 'nov_fim' => $nov_fim,
            'dez_ini' => $dez_ini, 'dez_fim' => $dez_fim,
        );
        
        return $datas;
    }
}