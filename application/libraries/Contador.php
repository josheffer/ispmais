<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Sinergi\BrowserDetector\Os;
use Sinergi\BrowserDetector\Browser;
use Sinergi\BrowserDetector\Device;


class Contador {

    public function dispositivo()
    {

        require 'vendor/autoload.php';
        
        $os = new Os();
        $device = new Device();
        $browser = new Browser();
        
        if ($os->getName() === Os::WINDOWS) {
            $dados['dispositivo'] = '0';
            $dados['sistemaOperacional'] = 'Windows';
        }

        if ($os->getName() === Os::LINUX) {
            $dados['dispositivo'] = '0';
            $dados['sistemaOperacional'] = 'Linux';
        }

        if ($os->getName() === Os::ANDROID) {
            $dados['dispositivo'] = '1';
            $dados['sistemaOperacional'] = 'Android';
        }

        if ($os->getName() === Os::IOS) {
            $dados['dispositivo'] = '0';
            $dados['sistemaOperacional'] = 'Mac';
        }
        
        if ($os->getName() === Os::OSX) {
            $dados['dispositivo'] = '0';
            $dados['sistemaOperacional'] = 'Mac';
        }

        if ($device->getName() === Device::IPAD) {
            $dados['dispositivo'] = '1';
            $dados['sistemaOperacional'] = 'Ipad';
        }

        if ($device->getName() === Device::IPHONE) {
            $dados['dispositivo'] = '1';
            $dados['sistemaOperacional'] = 'Iphone';
        }
        
        if ($browser->getName() === Browser::CHROME) {
            $dados['navegador'] = 'Chrome';
        }

        if ($browser->getName() === Browser::OPERA) {
            $dados['navegador'] = 'Opera';
        }
        
        if ($browser->getName() === Browser::IE || $browser->getName() === Browser::EDGE) {
            $dados['navegador'] = 'Explorer';
        }

        if ($browser->getName() === Browser::EDGE) {
            $dados['navegador'] = 'Edge';
        }

        if ($browser->getName() === Browser::FIREFOX) {
            $dados['navegador'] = 'Firefox';
        }
        
        if ($browser->getName() === Browser::FIREFOX || $browser->getName() === Browser::MOZILLA) {
            $dados['navegador'] = 'Firefox';
        }

        if ($browser->getName() === Browser::SAFARI) {
            $dados['navegador'] = 'Safari';
        }
        
        $u_agent = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('/XiaoMI/i', $u_agent)) {
            $dados['navegador'] = 'MiBrowser';
        }

        if (preg_match('/CriOS/i', $u_agent)) {
            $dados['navegador'] = 'Chrome';
        }
        
        return $dados;
        
    }

    public function consultaCidadeEstado($site_url){

        $ch = curl_init();
        $timeout = 5;
        curl_setopt ($ch, CURLOPT_URL, $site_url);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        ob_start();
        curl_exec($ch);
        curl_close($ch);
        $file_contents = ob_get_contents();
        ob_end_clean();

        return $file_contents;

    }
}