<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permitiracesso {

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model("permissoes/Permissoes_model", "permissoes");
        $this->CI->load->model("painel/Painel_model", "painel");
        $this->CI->load->model("empresa/Empresa_model", "empresa");
        $this->CI->load->model("landingpage/Landingpage_model", "landingpage");
    }

    public function verifica_sessao()
    {
        if (empty($this->CI->session->userdata('logado')))
        {   
            redirect('dashboard/logout');

        } else {

            $id = $_SESSION['id'];
            
            $resultado = $this->CI->painel->m_consultaDadosSessao($id);
            
            if($_SESSION['token'] === $resultado['token_usuario'])
            {
                $dados = array(
                    'id'                    => $resultado['id_usuario'],
                    'login'                 => $resultado['login_usuario'],
                    'senha'                 => $resultado['senha_usuario'],
                    'nome'                  => $resultado['nome_usuario'],
                    'nivelAcesso_usuario'   => $resultado['nivelAcesso_usuario'],
                    'status'                => $resultado['status_usuario'],
                    'logado'                => TRUE,
                    
                );
                
                $this->CI->session->set_userdata($dados);
                
                
            } else {
                
                redirect('dashboard/logout');
            }
        }
    
    }
    
    public function verificaAcessoPagina($view)
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $url2 = explode($url[0], $url_atual);

        $urlVerificada = substr($url2[1], 1);
        
        if($urlVerificada === "/dashboard/") : $urlVerificada = "/dashboard"; endif; 
        if($urlVerificada === "/") : $urlVerificada = "/dashboard"; endif;
        if($urlVerificada === "") : $urlVerificada = "/dashboard"; endif;

        $dadosSessao['dados'] = $this->CI->session->userdata;
        
        $empresa = (object)$this->CI->empresa->m_listarInformacoesEmpresa();
        
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;
        
        $nivelAcesso_usuario =  $dadosSessao['dados']['nivelAcesso_usuario'];
        
        $dadosSessao['permissaoLinks'] = $this->CI->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $dadosSessao['paginas'] = $this->CI->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        
        $infos['id_nivelAcesso'] = $nivelAcesso_usuario;
        $infos['endereco_pagina'] = $urlVerificada;
        
        $acessoPermitido = (array)$this->CI->permissoes->buscarPrmissaoAcesarPagina($infos);
               
        if(empty($acessoPermitido))
        {

            $acessoPermitido['view'] = "errors/html/semPermissao";
            $acessoPermitido['dados']   = $dadosSessao;
            
            return $acessoPermitido;

        } else {
            
            if(is_array($acessoPermitido))
            {
                if($acessoPermitido['permissao_niveisPaginas'] === '1' && $acessoPermitido['add_niveisPaginas'] === '1')
                {
                    $acessoPermitido['view']    = $view;
                    $acessoPermitido['dados']   = $dadosSessao;

                    return $acessoPermitido;

                } else {

                    $acessoPermitido['view'] = "errors/html/semPermissao";
                    $acessoPermitido['dados']   = $dadosSessao;

                    return $acessoPermitido;
                }
                
            } else {
                
                $acessoPermitido['view'] = "errors/html/semPermissao";

                return $acessoPermitido;
            }

        }
        
    }

    public function verificaAcessoPaginaParametro($view)
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $url2 = explode($url[0], $url_atual);
        
        $urlVerificada = substr($url2[1], 1);

        $verificaURL1 = substr($url2[1], 0, -1);

        $verificaURL2 = substr($verificaURL1, 1);
        
        $pesquisarItem = (object)array(
            'colunas'       => "*",
            'tabela'        => "pu_paginas",
            'coluna_where'  => "endereco_pagina",
            'valor_where'   => $verificaURL2
        );

        $resultadoDadoExiste =  $this->CI->painel->m_buscar_1_Item_like($pesquisarItem);
        
        $id = $url[4];
        
        $dadosSessao['idLead'] = $id;
        
        $carregarLandingPage =  $this->CI->landingpage->buscarLandingPage($id);
        
        $dadosSessao['dados'] = $this->CI->session->userdata;
        
        $empresa = (object)$this->CI->empresa->m_listarInformacoesEmpresa();
                
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;
        
        $nivelAcesso_usuario =  $dadosSessao['dados']['nivelAcesso_usuario'];
        
        $dadosSessao['permissaoLinks'] = $this->CI->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $dadosSessao['paginas'] = $this->CI->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        
        $infos['id_nivelAcesso'] = $nivelAcesso_usuario;
        $infos['endereco_pagina'] = $resultadoDadoExiste->endereco_pagina;
        
        $acessoPermitido = (array)$this->CI->permissoes->buscarPrmissaoAcesarPagina($infos);
        
        if(is_array($acessoPermitido))
        {
            if($acessoPermitido['permissao_niveisPaginas'] === '1' && $acessoPermitido['add_niveisPaginas'] === '1')
            {
                $acessoPermitido['view']                    = $view;
                $acessoPermitido['dados']                   = $dadosSessao;
                $acessoPermitido['dados']['landingpage']    = $carregarLandingPage;
                
                return $acessoPermitido;

            } else {

                $acessoPermitido['view'] = "errors/html/semPermissao";
                $acessoPermitido['dados']                   = $dadosSessao;
                $acessoPermitido['dados']['landingpage']    = $carregarLandingPage;
                                
                return $acessoPermitido;
            }
            
        } else {
            
            $acessoPermitido['view'] = "errors/html/semPermissao";
            $acessoPermitido['dados']                   = $dadosSessao;
            $acessoPermitido['dados']['landingpage']    = $carregarLandingPage;
            
            return $acessoPermitido;
        }
        
    }

    public function verificaAcessoBotao($nivelAcesso_usuario, $metodo)
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $url2 = explode($url[0], $url_atual);
        
        $urlVerificada = substr($url2[1], 1);
        
        $infos['id_nivelAcesso'] = $nivelAcesso_usuario;
        $infos['endereco_pagina'] = $metodo;
        
        $acessoPermitido = $this->CI->permissoes->buscarPrmissaoAcesarPagina($infos);
        
        if($acessoPermitido->permissao_niveisPaginas === '1' && $acessoPermitido->add_niveisPaginas === '1')
        {

        $acessoPermitido->view = $metodo;

        } else {

            $acessoPermitido->view = "errors/html/semPermissao";
        }
        
        return $acessoPermitido;

    }
    
}