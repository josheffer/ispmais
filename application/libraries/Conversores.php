<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conversores {

    function textoURL($str){
        $str = strtolower(utf8_decode($str)); $i=1;
        $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
        $str = preg_replace("/([^a-z0-9])/",'_',utf8_encode($str));
        while($i>0) $str = str_replace('--','_',$str,$i);
        if (substr($str, -1) == '_') $str = substr($str, 0, -1);
        return $str;
    }
}