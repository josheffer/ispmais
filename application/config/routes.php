<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['modules_locations'] = array(
    APPPATH.'modules/' => '../modules/',
);

$route['default_controller'] = 'site';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

require_once( BASEPATH .'database/DB'. EXT );
$db =& DB();
$query = $db->get( 'pu_paginas' );
$result = $query->result();

foreach( $result as $row )
{
    switch ($row->tipoRota_pagina) {
        case '1':
            $tipoRota = 'get';
        break;

        case '2':
            $tipoRota = 'post';
        break;

        case '3':
            $tipoRota = 'delete';
        break;

        case '4':
            $tipoRota = 'patch';
        break;
        
    }
    
    $route[$row->endereco_pagina][$tipoRota] = $row->controller_pagina;
    
}

$route['listagens/listarOpcoesOndeNosConheceu']['post']                         = 'landingpage/c_listarOpcoesOndeNosConheceu';